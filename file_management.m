(* ::Package:: *)

BeginPackage["ThesisFileManagement`"]

(*USAGE STATEMENTS*)
thesisImport::usage = "thesisImport[directory, filename_string] obtains the contents of a file whether it is a context state file (*.mx) or a data file (*.csv) within a specified directory."

thesisExport::usage = "thesisExport[directory, filename_string] saves the current state of the notebook (all saved variables and functions) as 'filename.mx' within a specified directory. 
thesisExport[directory, filename_string, expr] saves expr as 'filename.csv' within a specified directory."

Begin["`Private`"]


thesisExport[directory_, filenameString_, expr_: Null]:=
Module[{},
	
	(* Subdirectory is set. *)
	SetDirectory @ directory;

	(* Expression is exported. *)
	If[expr === Null,
		DumpSave[filenameString <> ".mx", "Global`"];,
		Export[filenameString <> ".csv", expr, "CSV"];
	];

	(* Directory is set back to default. *)
	SetDirectory @ NotebookDirectory[];
];


thesisImport[directory_, filenameString_]:= 
Module[{returnValue, importedData, checkStrings},

	(* Subdirectory is set. *)
	SetDirectory @ directory;

	If[StringMatchQ[filenameString,"*.csv"],

		(*Data is returned in lists of strings, it must be re-evaluated before it is returned.*)
		importedData = Import @ filenameString;
		If[StringMatchQ[ToString @ importedData, "*->*"] || StringMatchQ[ToString @ importedData, "*Undirected*"],
			returnValue = Flatten @ ToExpression @ ToString @ importedData;,			

			checkStrings = ToString[importedData, InputForm];
			If[StringMatchQ[checkStrings, "*\"*"],
				returnValue = ToExpression @ ToString @ importedData;,
				returnValue = ToExpression @ checkStrings;
			]
		],
		
		(*Data not in .csv format*)
		returnValue = Get @ filenameString;
	];

	(* Directory is set back to default. *)
	SetDirectory @ NotebookDirectory[];
	returnValue
];


End[]
EndPackage[]
