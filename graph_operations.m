(* ::Package:: *)

BeginPackage["ThesisGraphOperations`"]

(*USAGE STATEMENTS*)
createDirectedGraph::usage = "createDirectedGraph[vertex_list, directed_edge_list, edgeweight_list] generates a Graph[] object with custom settings that ease the manipulation and legibility of the graph."

createUndirectedGraph::usage = "createUndirectedGraph[vertex_list, undirected_edges, edgeweight_list] generates a Graph[] object with custom settings that ease the manipulation and legibility of the graph."

getEuclideanDistanceEdgeWeights::usage = "getEuclideanDistanceEdgeWeights[vertex_list, edge_list] returns the weights of each edge in 'edges' by measuring the euclidean distances between the two GEOGRAPHIC points in 'vertex_list'.
Note: Requires internet access."

getTotalGraphWeight::usage = "getTotalGraphWeight[graph] returns the sum of every edge weight for a graph with EdgeWeight properties. 
getTotalGraphWeight[graph, base_graph] returns the sum of every edge weight for a graph using the edges of a base graph. This is useful for Mathematica-generated Graph[] objects.
getTotalGraphWeight[selected_edgelist, all_edge_list, all_edgeweight_list] returns the sum of every edge weight for a graph using the edges and edgeweight variables instead of a graph.
Note: This is meaningless for directed graphs."

getAdjacentPolygonsToVertex::usage = "getAdjacentPolygonsToVertex[vertex, polygon_list] returns polygons (from polygon_list) that are adjacent to vertex."

getAdjacentPolygonsToVertices::usage = "getAdjacentPolygonsToVertices[vertex_list, polygon_list] returns all polygons (from polygon_list) that are adjacent to any of the vertices in vertex_list." 

getFrontier::usage = "getFrontier[checked_vertices, checked_polygons, unchecked_polygons] finds the graph frontier (a set of vertices) between checked and unchecked polygons, taking into consideration a group of checked vertices."

findShortestPathToVertexSet::usage = "findShortestPathToVertexSet[origin_vertex_list, destination_vertex_list, base_graph_vertices, base_graph] finds the shortest path between one origin_vertex and one destination_vertex. 
Returns the edges and vertices that create the minimimum path."

Begin["`Private`"]
SetDirectory @ NotebookDirectory[];
Needs["ThesisAuxiliaryFunctions`", "auxiliary_functions.m"];


createDirectedGraph[vertexList_, directedEdgeList_, edgeweightList_]:=
Module[{},
	Graph[

		(* Basic Graph Creation. *)
		Union[directedEdgeList /. Rule -> Sequence],
		directedEdgeList,
		VertexCoordinates -> vertexList,
		EdgeWeight -> edgeweightList,
	
		(* Graph Options. *)
		VertexLabelStyle -> 9,
		VertexSize -> Small,
		VertexLabels -> "Name",
		EdgeShapeFunction -> GraphElementData["ShortFilledArrow", "ArrowSize" -> .01],
		GraphLayout -> {
			"EdgeLayout" -> {
				"DividedEdgeBundling", 
				"CoulombConstant" -> 0,  (* Very Important! Drastically reduces edge curvature. *)
				"VelocityDamping" -> .2,
				"SmoothEdge" -> False, 
				"NewForce" -> False, 
				"Connectivity" -> True, 
				"Compatibility" -> True,
				"Threshold" -> .1, 
				"CoulombDecay" -> 1, 
				"LaneWidth" -> 1
			}
		}
	]
];


createUndirectedGraph[vertexList_, undirectedEdgeList_, edgeWeightList_]:=
Module[{},
	Graph[

		(*Basic Graph Creation*)
		Union[undirectedEdgeList /. UndirectedEdge -> Sequence],
		undirectedEdgeList,
		EdgeWeight -> edgeWeightList,
		VertexCoordinates -> vertexList,
		
		(* No need for many graph options, since output is not distorted be vertex coordinates. *)
		VertexLabelStyle -> 9,
		VertexSize -> Medium,
		VertexLabels -> "Name"
	]
];


getEuclideanDistanceEdgeWeights[vertexList_, edgeList_]:=
Module[
	{coordinates = Reverse[#]& /@ vertexList},
	
	(* Note: requires internet access. *)
	QuantityMagnitude[
		GeoDistance[
			coordinates[[#[[1]]]],
			coordinates[[#[[2]]]]
		], "Meters"
	]& /@ edgeList
];


(* When Graph object is supplied. *)
getTotalGraphWeight[graph_]:=
Module[{},

	(*Calculated with PropertyValue of the graph itself.*)	
	Total @ Thread @ PropertyValue[graph, EdgeWeight]
];

(* When Graph object and a base graph are supplied. *)
getTotalGraphWeight[graph_, baseGraph_]:=
Module[{},

	(* Calculated manually. *)
	Total[GraphDistance[baseGraph, #[[1]], #[[2]]]& /@ EdgeList[graph]]
];

(* When Edges and Edge Weights are supplied. *)
getTotalGraphWeight[selectedEdgeList_, allEdgeList_, allEdgeweightList_]:=
Module[
	{edgePositions, orderedEdges},

	(* Undirected edges are reordered to ensure that matching (Position[] function) occurs as desired. *)
	If[isDirected @ selectedEdgeList,
		orderedEdges = Sort @ selectedEdgeList;,
		orderedEdges = setUndirectedEdgeOrdering @ selectedEdgeList;		
	];
	edgePositions = Flatten[Position[allEdgeList, #] & /@ orderedEdges];
	Total[Flatten[allEdgeweightList[[#]]& /@ edgePositions]]
];


getAdjacentPolygonsToVertex[vertex_, polygonList_]:=
Module[{},
	Select[polygonList, MemberQ[#, vertex]&]
]


getAdjacentPolygonsToVertices[vertexList_, polygonList_]:=
Module[
	{
		uncheckedPolygons = polygonList,
		destinationPolygons = {},
		adjacentPolygons
	},

	(
		(* If there are still unchecked polygons. *)
		If[Length @ uncheckedPolygons > 0,

			(* Polygons are never checked multiple times. *)
			adjacentPolygons = getAdjacentPolygonsToVertex[#, uncheckedPolygons];
			{uncheckedPolygons, destinationPolygons} = moveElementSet[uncheckedPolygons, destinationPolygons, adjacentPolygons];
		];
	)&/@vertexList;
	destinationPolygons
]


getFrontier[checkedVertexList_:{}, checkedPolygonList_, uncheckedPolygonList_]:=
Module[{checked, unchecked},
	
	(* Polygons are treated as sets of vertices, ignoring the checked vertices. *)	
	checked = Complement[DeleteDuplicates @ Flatten[checkedPolygonList, 1], checkedVertexList];
	unchecked = DeleteDuplicates @ Flatten[uncheckedPolygonList, 1];
	Intersection[checked, unchecked]
]


findShortestPathToVertexSet[originVertexList_, destinationVertexList_, baseGraphVertexList_, baseGraph_]:=
Module[
	{
		(* Only the positions of these matter (positions are used in edges, not raw vertex coordinates). *)
		destinationPositions = Flatten[Position[baseGraphVertexList, #]& /@ destinationVertexList],
		checkedPositions = Flatten[Position[baseGraphVertexList, #]& /@ originVertexList],
		vertexPaths,
		pathSteps,
		pathTotals,
		smallestPath,
		edgeList,
		graphEdgeList,
		pathEdges,
		pathVertices
	},

	(* "vertexPaths" is a list of the possible paths to each frontier vertex. *)
	(* Outer[] generates every possible combination of vertex and destination vertex and checked vertex. *)
	vertexPaths = Flatten[Outer[FindShortestPath, {baseGraph}, checkedPositions, destinationPositions, 1], 2];	

	(* Separates path steps into individual steps, then calculates total distances for each path. *)
	pathSteps = Partition[#, 2, 1]& /@ vertexPaths;
	pathTotals = Map[Total, Map[GraphDistance[baseGraph, #[[1]], #[[2]]]&, pathSteps, {2}]];

	(* Returns path edges with smallest total distance, these edges should be removed. *)
	edgeList = pathSteps[[Position[pathTotals, Min @ pathTotals][[1]]]][[1]];
	If[DirectedGraphQ @ baseGraph,
		pathEdges = {#[[1]] -> #[[2]]}& /@ edgeList,
		pathEdges = {#[[1]] <-> #[[2]]}& /@ edgeList
	];

	(* Obtains the path vertices from the existing edges. *)
	pathVertices = Complement[DeleteDuplicates @ Flatten[baseGraphVertexList[[#]]& /@ edgeList, 1], originVertexList];
	{pathEdges, pathVertices}
]


End[]
EndPackage[]
