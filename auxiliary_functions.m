(* ::Package:: *)

BeginPackage["ThesisAuxiliaryFunctions`"]

(*USAGE STATEMENTS*)
setUndirectedEdgeOrdering::usage = "setUndirectedEdgeOrdering[undirected_edge_list] orders the vertices within each edge. 
{1 <-> 2, 3 <-> 2} is returned as {1 <-> 2, 2 <-> 3}."

isDirected::usage = "isDirected[edge_list] returns False if expr contains undirected edges; returns True otherwise."

removeVertex::usage = "removeVertex[target_vertex, vertex_list, edge_list] removes all occurances of target_vertex from vertices and edges provided without modifying the original lists.
Returns a list containing the remaining vertices and remaining edges in the following format:
	{remaining_vertices, remaining_edges}"

moveElementSet::usage = "moveElementSet[origin_set, destination_set, target_set] removes elements within target_set from origin_set and appends them to destination_set.
If target_set contains elements not in origin_set, they are automatically appended to destination_set as well. This is the desired functionality. 
Returns origin and destination in a list: {{new_origin_set}, {new_destination_set}}."

traversal::usage = "traversal[root_vertex, vertex_list, edge_list, traversal_type] traverses the tree (represented as a series of vertices and edges) starting from the root vertex in post-order or pre-order.
traversal_type parameter choices:
	\"PrevisitVertex\" -> Pre-order traversal.
	\"PostvisitVertex\" -> Post-order traversal."

isLeafVertex::usage = "isLeafVertex[vertex, edge_list] returns True if vertex is a leaf vertex of the tree that is in edge_list."

getOrderedSubgraphLeafVertices::usage = "getOrderedSubgraphLeafVertices[base_graph_edge_list, base_graph_edgeweight_list, subgraph_vertex_list, subgraph_edge_list] returns all of the leaf vertices of a subgraph ordered by edge weight in descending order (highest weight first).
This function is used for post-processing MLCP solutions."

getPolygonCenterVertices::usage = "getPolygonCenterVertices[polygon_list] returns a list of vertices indicating the geometric centers of each polygon in polygon_list."

getPolygonCenterEdges::usage = "getPolygonCenterEdges[base_polygon_list, polygon_center_vertex_list] returns a list of the edges matching the areas where two polygons (of polygon_center_vertex_list) meet."

getMSTEdgeIntersections::usage = "getMSTEdgeIntersections[base_graph_vertex_list, base_graph_polygon_list, polygon_center_MST_graph] returns the base graph edges that intersect the polygon-center-MST graph edges. 
	Note: This only takes into account the edges between the two polygon centers, not any other edges that are touched by the lines."

highlightGraphReduction::usage = "highlightGraphReduction[base_graph, unreduced_solution_graph, reduced_solution_graph] creates a simple representation of graph reduction."

createMesh::usage = "createMesh[n] is used to generate the necessary vertices, edges and polygon representation of an n x n GridGraph."

Begin["`Private`"]


setUndirectedEdgeOrdering[undirectedEdgeList_]:=
Module[
	{edgeList},
	edgeList = Flatten[undirectedEdgeList] /. UndirectedEdge -> List;
	If[#[[1]] > #[[2]], #[[2]]<->#[[1]], #[[1]]<->#[[2]]]& /@ edgeList
];


isDirected[edgeList_]:=
Module[{},
	If[
		StringMatchQ[ToString @ FullForm @ edgeList, "*UndirectedEdge*"],
		False,
		True
	]
];


moveElementSet[originSet_, destinationSet_, targetSet_] :=
Module[
	(* Extra variables necessary to ensure list immutability, no validation whether target is actually in origin. *)
	{target, origin, destination},
	
	origin = Complement[originSet, targetSet];
	destination = DeleteDuplicates @ Join[destinationSet, targetSet];
	
	(*Returns origin and destination sets.*)
	{origin, destination}
];


isLeafVertex[vertexList_, edgeList_]:= 
Module[{edgesListFormat},
	
	(* For directed edges, checks if vertex exists only once as the "destination vertex" and 0 times as the "origin vertex". *)
	If[isDirected @ edgeList,
		edgesListFormat = edgeList /. Rule -> List;
		If[Length @ Position[#[[2]]& /@ edgesListFormat, vertexList] == 1 && Length @ Position[#[[1]]& /@ edgesListFormat, vertexList] == 0,
			True, False
		],

		(* For undirected edges, simply checks if vertex is only found once (only one edge connection) in "edges". *)
		If[Length @ Position[Flatten[edgeList /. UndirectedEdge -> Sequence], vertexList] == 1,
			True, False
		]
	]
];


removeVertex[leafVertex_, vertexList_, edgeList_]:=
Module[
	{remainingVertices, remainingEdges},
	
	If[isDirected @ edgeList,
		remainingEdges = Complement[
			edgeList, 
			# /. List -> Rule& /@ Flatten[Cases[edgeList /. Rule -> List, {_, leafVertex}], {1}]
		],
		remainingEdges = Complement[edgeList, Cases[edgeList, leafVertex<->_|_<->leafVertex]]
	];
	remainingVertices = Complement[vertexList, {leafVertex}];
	
	{remainingVertices, remainingEdges}
];


traversal[rootVertex_, vertexList_, edgesList_, traversalType_]:=
Module[
	{treeGraph},
	treeGraph = Graph[Flatten @ vertexList, Flatten @ edgesList];
	Flatten[Reap @ DepthFirstScan[treeGraph, rootVertex, {traversalType -> Sow}][[2]]]
];


getOrderedSubgraphLeafVertices[baseGraphEdgeList_, baseGraphEdgeWeightList_, subgraphVertexList_, subgraphEdgeList_]:=
Module[
	{
		subEdges = If[isDirected @ subgraphEdgeList, subgraphEdgeList, setUndirectedEdgeOrdering @ subgraphEdgeList], 
		baseEdges = If[isDirected @ baseGraphEdgeList, baseGraphEdgeList, setUndirectedEdgeOrdering @ baseGraphEdgeList],
		leafVertices,
		leafEdges,
		leafEdgeList,
		orderedLeafVertices,
		orderedBaseEdges
	},

	(* Apply function to all vertices and return if condition is true, else do nothing. *)
	leafVertices = If[isLeafVertex[#, subEdges], #, ##&[]]& /@ subgraphVertexList;
	
	If[isDirected @ subEdges,
		leafEdgeList = subEdges /. Rule -> List;
		leafEdgeList = Flatten[Cases[leafEdgeList, {_, #}]& /@ leafVertices,1];
		leafEdges = # /. List -> Rule& /@ leafEdgeList;,

		(*Ordering is only necessary on undirected edges*)
		leafEdgeList = Cases[Flatten @ subEdges, #<->_|_<->#]& /@ leafVertices;
		leafEdges = setUndirectedEdgeOrdering @ leafEdgeList;
	];
	
	orderedLeafVertices = {baseGraphEdgeWeightList[[Flatten @ Position[baseEdges, #]]][[1]], leafVertices[[Flatten @ Position[leafEdges, #]]][[1]]}& /@ leafEdges;
	orderedLeafVertices = #[[2]]& /@ Sort[orderedLeafVertices, #1[[1]] > #2[[1]]&]
];


getPolygonCenterVertices[polygonList_]:=
Module[{},

	(* Polygon[] only works with a CLOSED polygon.*)
	RegionCentroid @ Polygon @ #& /@ polygonList
];


getPolygonCenterEdges[baseGraphPolygonList_, polygonCenterVertexList_]:=
Module[
	{
		possibleIntersections = Tuples @ {baseGraphPolygonList, baseGraphPolygonList},
		intersectingPolygonList,
		polygonCenterEdgeList,
		firstVertex,
		secondVertex
	},

	(* Find intersecting polygons. *)
	intersectingPolygonList = (

		(* Ignore cases where polygon is matched against itself and where inly one vertex matches. *)
		If[ !SameQ[#[[1]], #[[2]]] && Length[#[[1]] \[Intersection] #[[2]]] == 2,
			Flatten @ {Position[baseGraphPolygonList, #[[1]]], Position[baseGraphPolygonList, #[[2]]]}
		]
	)& /@ possibleIntersections;
	intersectingPolygonList = intersectingPolygonList /. Null -> Sequence[];

	(* Get Edge list using indices. *)
	polygonCenterEdgeList = (
		firstVertex = Flatten[Position[polygonCenterVertexList, polygonCenterVertexList[[#[[1]]]]]][[1]];
		secondVertex = Flatten[Position[polygonCenterVertexList, polygonCenterVertexList[[#[[2]]]]]][[1]];

		(* Removes Duplicates. *)
		If[firstVertex < secondVertex, firstVertex <-> secondVertex]
	)& /@ intersectingPolygonList;
	polygonCenterEdgeList /. Null -> Sequence[]
];


getMSTEdgeIntersections[baseGraphVertexList_, baseGraphPolygonList_, polygonCenterMSTGraph_]:=
Module[ 

	(* Edges must be refered by coordinates, not by index. *)
	{intersectingPolygons, sharedVertices, intersectingEdges},

	(* Intersections are not found geometrically as there are too many conflicting edges; it is found algorithmically. *)
	intersectingPolygons = {baseGraphPolygonList[[#[[1]]]], baseGraphPolygonList[[#[[2]]]]}& /@ EdgeList @ polygonCenterMSTGraph;
	sharedVertices = #[[1]] \[Intersection] #[[2]]& /@ intersectingPolygons;
	intersectingEdges = Flatten[Position[baseGraphVertexList, #[[1]]]][[1]] <-> Flatten[Position[baseGraphVertexList, #[[2]]]][[1]]& /@ sharedVertices;
	setUndirectedEdgeOrdering @ intersectingEdges
];


highlightGraphReduction[baseGraph_, solutionGraph_, reducedGraph_] := 
Module[{},
	HighlightGraph[baseGraph, Join @ {Style[solutionGraph, Red, Thick], Style[reducedGraph, Blue, Thick]}, GraphHighlightStyle -> "Thick"]
];


createMesh[n_]:= 
Module[
	{
		vertices,
		horizontalEdges,
		verticalEdges,
		edges,
		polygons
	},
   vertices = Flatten[Table[{Mod[j, n + 1], n - i + 1}, {i, n}, {j, n}], 1];
   horizontalEdges = Flatten[Table[{# + i <-> # + i + 1}, {i, n - 1}]& /@ Range[0, n (n - 1), n]];
   verticalEdges = Flatten[Table[{i <-> i + n}, {i, n (n - 1)}]];
   edges = Sort[Join[horizontalEdges, verticalEdges]];
   polygons = Flatten[Table[# + {i, i + 1, i + n, i + n + 1}, {i, n - 1}]& /@ Range[0, n (n - 2), n], 1];
   polygons = vertices[[#]]& /@ polygons;
   {vertices, edges, polygons}
];


End[]
EndPackage[]
