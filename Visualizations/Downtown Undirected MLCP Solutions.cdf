(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[    559185,      13551]
NotebookOptionsPosition[    557650,      13477]
NotebookOutlinePosition[    558108,      13497]
CellTagsIndexPosition[    558065,      13494]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\<\
MLCP Solution Visualizations: Undirected Quer\[EAcute]taro Downtown Solutions\
\
\>", "Title"],

Cell["\<\
Every unreduced and reduced solution for the undirected downtown graph is \
displayed here. For algorithms that produce multiple solutions, only the best \
solution is shown here. Unreduced solutions are displayed in red, reduced \
solutions (if they exist) are displayed in blue.\
\>", "Subsection"],

Cell[CellGroupData[{

Cell["Undirected Quer\[EAcute]taro Downtown Dataset - Base Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69}, {
      Null, {{1, 2}, {2, 5}, {3, 1}, {4, 3}, {5, 6}, {5, 17}, {6, 7}, {7, 
       8}, {7, 19}, {8, 10}, {9, 4}, {9, 10}, {10, 11}, {10, 21}, {11, 12}, {
       11, 22}, {12, 13}, {12, 23}, {13, 9}, {16, 2}, {17, 16}, {17, 25}, {18,
        6}, {18, 17}, {19, 18}, {19, 28}, {20, 8}, {20, 19}, {21, 20}, {21, 
       34}, {22, 21}, {22, 39}, {23, 22}, {23, 42}, {24, 16}, {25, 24}, {25, 
       40}, {26, 18}, {26, 25}, {27, 13}, {27, 23}, {28, 26}, {28, 45}, {30, 
       20}, {30, 28}, {33, 49}, {34, 30}, {34, 50}, {35, 44}, {37, 27}, {38, 
       24}, {39, 34}, {39, 53}, {40, 54}, {41, 29}, {41, 37}, {42, 39}, {42, 
       56}, {43, 26}, {44, 32}, {44, 41}, {45, 62}, {46, 44}, {47, 30}, {48, 
       37}, {48, 42}, {49, 46}, {49, 58}, {50, 65}, {51, 41}, {51, 48}, {52, 
       49}, {54, 38}, {55, 51}, {56, 66}, {57, 55}, {58, 57}, {59, 43}, {60, 
       48}, {61, 58}, {63, 51}, {64, 47}, {68, 60}, {1, 5}, {3, 7}, {4, 8}, {
       13, 14}, {14, 15}, {15, 31}, {27, 29}, {29, 32}, {31, 33}, {31, 52}, {
       32, 35}, {33, 36}, {35, 36}, {36, 46}, {38, 40}, {40, 43}, {43, 45}, {
       45, 47}, {47, 50}, {44, 55}, {46, 57}, {50, 53}, {52, 61}, {53, 56}, {
       54, 59}, {56, 60}, {59, 62}, {60, 63}, {61, 67}, {62, 64}, {63, 67}, {
       64, 65}, {65, 66}, {66, 68}, {67, 69}, {68, 69}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, EdgeWeight -> CompressedData["
1:eJwN0f0zmwcAB/Ck3FVttDPVnXmtMBfzEmvrZXW+F29lWKUpDVo8SRBplkqE
x8TbqJbOXrxr6uWmo+4YYavqqXg5pZW+uEvRcnTpOLq269KzNM2s6w+fv+Dj
TIhZfCqFQsl759Mc9V9F7SfAaYlYrAtIwI8Gsv72hAgUyrpOUZ0O67dZ7uI7
PDy7aNba51yAuRMfCvccjYe1+llEKZ2PSkPz1dq2IkT1PT+wh0fgD5ef6FYF
BCa/Kfe3q4tCgimxT68VQtfSxo55Ksf4w8GDtyJ4aLT5+/iyrxwdTGlVlCof
lZE0RqFDMUZmgxr9WedhftdElWQrx446i7T+aTboMcMkx4zEkKH6lySmHA5+
nq8GmHxQa69qi6JTMF81HHxkOw+SX9dmzAeKcH1Xk50TLQ2ccseD7HUufLLn
lQweia6mGlnvdBbSd03tDhwk0W40bWksJxBa7vuoyUji5fAVi3DnPFgJzq1t
DMXhyhsPF82YBAzWIQZtP4lT/v126QV8mEW4R841lEEc5LO3fFyGee3vO5bD
uBCP99F6dBJsj/bcDcokcelfo5o2SiC2tnYp5mQ2wqk5dP8aAsbb291j7lIk
f+YSZ6VPR+m+tYImOg+rOwP1S35SvNnUr51OE6CLVTuW7yuFRZs4q6RbBuIL
NOi1XJw+qmLS2HxwDQEf2VK4qLlUsT/pZwl6ApemulUSsB07Pg6J5SNlQGBf
WS2Gnyhldombi7A/czI8BGlYMLixRtykmFiv8KYGiRESoCy1vCaFk95S19ub
gq0n2abfnxVA1Hkjf4Eg4RTffIDnIkZ8rl7T97UACgcyQRvPg9OTHq/DTTJc
K/tEPacQgdt5f6VZIkfN03thu93T4BqXsOnTlgrbQ4Eq68syXD9WcuGhKhtt
8FImzrJxT6jkeNWfgSFgw17DyUXcoxSip7MEK0Nzek3dEbzuLtzkMMMx/5XN
sccToSjZ+486eOgsNMZWE3ScRKZOfyfjRRnkjRYbVCMPIa3kzdUiIZarj880
fJAJ7wdrOxm/5UGjSDy/oGZBtmmVam9N4ELM89aXrETs8Oyqt/tciGalRAfH
WHz5Wtvs2ZCBgS2891+VAOvT3nWtsacwc5kx6JzLRW6MnrSZFOFGKN02YVEI
H8p3gowfMqCfnFQo3hfBLS7VziaYi3EOpm5tiXDTNVz3YCkb92nxK2RSFvod
mdGuHiL4XqwZKbQsRsfU5CglOglc6mGTtBUS+ebJ0rerAiwWe51RvXvkFivb
H39L4BXzhSc7ogLn1CORLckl+B9BfbVH
        "], 
       VertexCoordinates -> CompressedData["
1:eJxF031ME3cYB/CjLKTAKKAbUEABF0rZZM5SRI3C1QSlZoBlbAqWMV4mwoZC
3QCt2hIJL9WClrZDnGOK860EmQWHglI7mIYwioKtW5ELolCzjkZEYJ2pfvuX
f1wun3yf5+733O93YTl7U7+mEQSRgMt5nzeMSHnMdJ1Q4Voac5JLGqrz3Tlw
/ITVyIEzulr1u+ChYEUGG66ZM5sqYLVlpZYOW2vbPONh3+1D40yYs2FeuwO+
PWnudoP5Sd2Nzv7iTWE0BqwyUkuk8Py1kx7PG7lk7/POfDUcTPbaKFiXvlXd
AO+ki7bpYXfq/6AO+NzF6IXjcBwrtelP2NDy+HUdbB7fw2cEput6pnWiSphK
HfIXw+Iz0n+/gn3v3OiSw+52z405sCZz6f1A9DM/ryqJhfVnVyRHOOev179k
wwvPygdJOE1zpc0D/qC65tRnsKXqvHXuB3yPypyPdsPEpaQfx+B65sVjEliZ
/yWjG37Y5chshqOD8hQnYLrm18pOuE4ZpSiFn47k7nSDBy6dzi6EfayLq3zg
0YrWC1/AcULRWCS8MYHhGwsfOZeiscAqT5UkCj5wV+nt3C+q8HRXMNynfYfp
gvkkOldXOnz5xpY125GbqmQlDjWXvBfStF+E/Bd+pGUeJo6+sofBmkGlkYKr
OWn9afC68FDWBDyV0UEUoZ+31fr3OLz8tlwZhdzb66jABAuOFATEwdf9okgz
/F/B2CkK9R/XDSSNwom/R+57EZCuW83zovTwhyXLJmXIGwUrf+uAU0Iq4l8h
p42OXu2E2XP2hkXkWTZH+RV418KT7DNwfRGzWA3/7D8jDHCeJyxDAR8bG3mw
DO+/L9vLPgg/WXr1xRrkszN3dxTBPbR7DRzk37c7DNmwJ2NVTSJyt57BdgH8
xwUiwABLarz8kmET17Y2AfX+RnnZFnj91IbcLOQTrJaBT2CPWwV2K5wYi5MC
L6n+NjQT9XmWh7xwmGEq9BYjX+3uP0uH33dJtj3GfMPiXDoB524uF/ugvjmO
JllU4f9w6d2sQH2g+RueDW4mZCfCkffficibhvsVooV1sHVbEt8I76kVFjvP
iy209PwwrIx+r6wHZvUJUztVb9cfbMgnb8I3mTFZ4cgjy5onW1Rv92f9dwfG
5fBf2mlODGy3Sv+RwcsdTa0CuPzQVGsKLD1M9e6Hz7KHI2aVXDKv7d3LXnh+
0Esf+QysZfktOvcz5HrMs2swm2/69Bb8qPGn9r4GLvkGIuD0cw==
        "]}]]}, 
    TagBox[GraphicsGroupBox[{
       {Hue[0.6, 0.7, 0.5], Opacity[0.7], 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394623, 20.598472}, {-100.393679, 20.598432}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394623, 20.598472}, {-100.396897, 20.598272}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394623, 20.598472}, {-100.394473, 20.598051}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393679, 20.598432}, {-100.394473, 20.598051}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393679, 20.598432}, {-100.392262, 20.594596}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396897, 20.598272}, {-100.39855, 20.597789}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396897, 20.598272}, {-100.396887, 20.59787}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39855, 20.597789}, {-100.398442, 20.597428}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39855, 20.597789}, {-100.399805, 20.597167}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394473, 20.598051}, {-100.396039, 20.597759}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394473, 20.598051}, {-100.393024, 20.594375}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396039, 20.597759}, {-100.396887, 20.59787}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396039, 20.597759}, {-100.39458, 20.593913}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396887, 20.59787}, {-100.398442, 20.597428}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396887, 20.59787}, {-100.395728, 20.593581}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398442, 20.597428}, {-100.399633, 20.596715}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398442, 20.597428}, {-100.396983, 20.59321}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399805, 20.597167}, {-100.399633, 20.596715}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399805, 20.597167}, {-100.407122, 20.595771}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399633, 20.596715}, {-100.401618, 20.595962}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399633, 20.596715}, {-100.39841, 20.592698}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.401618, 20.595962}, {-100.403174, 20.595932}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.401618, 20.595962}, {-100.400352, 20.592065}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.403174, 20.595932}, {-100.407122, 20.595771}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.403174, 20.595932}, {-100.401704, 20.591653}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.407122, 20.595771}, {-100.413763, 20.595229}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.407122, 20.595771}, {-100.404826, 20.590538}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.413763, 20.595229}, {-100.4149, 20.595249}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.4149, 20.595249}, {-100.413237, 20.589705}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.392262, 20.594596}, {-100.393024, 20.594375}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.392262, 20.594596}, {-100.391013, 20.591467}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393024, 20.594375}, {-100.39458, 20.593913}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393024, 20.594375}, {-100.391613, 20.591166}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39458, 20.593913}, {-100.395728, 20.593581}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39458, 20.593913}, {-100.39318, 20.590684}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395728, 20.593581}, {-100.396983, 20.59321}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395728, 20.593581}, {-100.394596, 20.590282}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396983, 20.59321}, {-100.39841, 20.592698}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396983, 20.59321}, {-100.395948, 20.58982}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39841, 20.592698}, {-100.400352, 20.592065}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39841, 20.592698}, {-100.397278, 20.589338}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.400352, 20.592065}, {-100.401704, 20.591653}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.400352, 20.592065}, {-100.399102, 20.588695}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.401704, 20.591653}, {-100.404826, 20.590538}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.401704, 20.591653}, {-100.400475, 20.588233}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391013, 20.591467}, {-100.391613, 20.591166}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391013, 20.591467}, {-100.389859, 20.588911}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391613, 20.591166}, {-100.39318, 20.590684}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391613, 20.591166}, {-100.390396, 20.58871}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39318, 20.590684}, {-100.394596, 20.590282}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39318, 20.590684}, {-100.392112, 20.588168}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404826, 20.590538}, {-100.406317, 20.589966}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404826, 20.590538}, {-100.404273, 20.589177}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394596, 20.590282}, {-100.395948, 20.58982}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394596, 20.590282}, {-100.393979, 20.587606}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.406317, 20.589966}, {-100.408324, 20.589353}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.406317, 20.589966}, {-100.405797, 20.588615}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395948, 20.58982}, {-100.397278, 20.589338}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395948, 20.58982}, {-100.395052, 20.587184}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.413237, 20.589705}, {-100.411424, 20.589373}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.413237, 20.589705}, {-100.411934, 20.586506}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.408324, 20.589353}, {-100.409032, 20.589223}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.408324, 20.589353}, {-100.408071, 20.587832}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.411424, 20.589373}, {-100.41004, 20.589283}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.411424, 20.589373}, {-100.410475, 20.587028}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.397278, 20.589338}, {-100.399102, 20.588695}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.397278, 20.589338}, {-100.396447, 20.586682}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.409032, 20.589223}, {-100.41004, 20.589283}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.409032, 20.589223}, {-100.408071, 20.587832}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.41004, 20.589283}, {-100.409337, 20.58743}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404273, 20.589177}, {-100.405797, 20.588615}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404273, 20.589177}, {-100.403265, 20.587129}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.389859, 20.588911}, {-100.390396, 20.58871}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.389859, 20.588911}, {-100.388851, 20.585938}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399102, 20.588695}, {-100.400475, 20.588233}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399102, 20.588695}, {-100.398136, 20.586074}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.390396, 20.58871}, {-100.392112, 20.588168}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.390396, 20.58871}, {-100.388851, 20.585938}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.405797, 20.588615}, {-100.408071, 20.587832}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.405797, 20.588615}, {-100.405089, 20.586466}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.400475, 20.588233}, {-100.403265, 20.587129}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.400475, 20.588233}, {-100.399553, 20.585682}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.392112, 20.588168}, {-100.393979, 20.587606}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.392112, 20.588168}, {-100.391018, 20.585215}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.408071, 20.587832}, {-100.409337, 20.58743}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.408071, 20.587832}, {-100.407256, 20.585823}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393979, 20.587606}, {-100.395052, 20.587184}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393979, 20.587606}, {-100.392906, 20.584532}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.409337, 20.58743}, {-100.410475, 20.587028}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.409337, 20.58743}, {-100.408543, 20.585562}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395052, 20.587184}, {-100.396447, 20.586682}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395052, 20.587184}, {-100.393872, 20.584151}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.403265, 20.587129}, {-100.405089, 20.586466}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.403265, 20.587129}, {-100.402128, 20.584798}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.410475, 20.587028}, {-100.411934, 20.586506}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.410475, 20.587028}, {-100.409788, 20.585301}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396447, 20.586682}, {-100.398136, 20.586074}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396447, 20.586682}, {-100.39561, 20.583247}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.405089, 20.586466}, {-100.407256, 20.585823}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.405089, 20.586466}, {-100.404273, 20.584196}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.411934, 20.586506}, {-100.410475, 20.584919}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398136, 20.586074}, {-100.399553, 20.585682}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.388851, 20.585938}, {-100.391018, 20.585215}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.407256, 20.585823}, {-100.408543, 20.585562}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399553, 20.585682}, {-100.402128, 20.584798}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399553, 20.585682}, {-100.398056, 20.58182}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.408543, 20.585562}, {-100.409788, 20.585301}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.409788, 20.585301}, {-100.410475, 20.584919}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391018, 20.585215}, {-100.392906, 20.584532}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.402128, 20.584798}, {-100.404273, 20.584196}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.402128, 20.584798}, {-100.399107, 20.580916}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.410475, 20.584919}, {-100.407084, 20.581765}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.392906, 20.584532}, {-100.393872, 20.584151}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404273, 20.584196}, {-100.407084, 20.581765}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393872, 20.584151}, {-100.39561, 20.583247}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39561, 20.583247}, {-100.398056, 20.58182}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398056, 20.58182}, {-100.399107, 20.580916}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.407084, 20.581765}, {-100.40224, 20.57722}}, 
          0.0003316037700000055]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399107, 20.580916}, {-100.40224, 20.57722}}, 
          0.0003316037700000055]}}, 
       {Hue[0.6, 0.2, 0.8], EdgeForm[{GrayLevel[0], Opacity[
        0.7]}], {DiskBox[{-100.394623, 20.598472}, 0.000037680897016661], 
         InsetBox[
          StyleBox["1",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39458531910297`, 20.598509680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.393679, 20.598432}, 0.000037680897016661], InsetBox[
          StyleBox["2",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39364131910298`, 20.598469680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.396897, 20.598272}, 0.000037680897016661], InsetBox[
          StyleBox["3",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39685931910297`, 20.598309680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.39855, 20.597789}, 0.000037680897016661], InsetBox[
          StyleBox["4",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39851231910298`, 20.597826680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.394473, 20.598051}, 0.000037680897016661], InsetBox[
          StyleBox["5",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39443531910298`, 20.598088680897018`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.396039, 20.597759}, 0.000037680897016661], InsetBox[
          StyleBox["6",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39600131910298`, 20.597796680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.396887, 20.59787}, 0.000037680897016661], InsetBox[
          StyleBox["7",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39684931910298`, 20.597907680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.398442, 20.597428}, 0.000037680897016661], InsetBox[
          StyleBox["8",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39840431910298`, 20.597465680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399805, 20.597167}, 0.000037680897016661], InsetBox[
          StyleBox["9",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39976731910298`, 20.597204680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399633, 20.596715}, 0.000037680897016661], InsetBox[
          StyleBox["10",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39959531910297`, 20.596752680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.401618, 20.595962}, 0.000037680897016661], InsetBox[
          StyleBox["11",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40158031910298`, 20.595999680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.403174, 20.595932}, 0.000037680897016661], InsetBox[
          StyleBox["12",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40313631910298`, 20.595969680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.407122, 20.595771}, 0.000037680897016661], InsetBox[
          StyleBox["13",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40708431910298`, 20.595808680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.413763, 20.595229}, 0.000037680897016661], InsetBox[
          StyleBox["14",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41372531910298`, 20.595266680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.4149, 20.595249}, 0.000037680897016661], InsetBox[
          StyleBox["15",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41486231910298`, 20.595286680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.392262, 20.594596}, 0.000037680897016661], InsetBox[
          StyleBox["16",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39222431910298`, 20.594633680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.393024, 20.594375}, 0.000037680897016661], InsetBox[
          StyleBox["17",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39298631910297`, 20.594412680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.39458, 20.593913}, 0.000037680897016661], InsetBox[
          StyleBox["18",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39454231910298`, 20.593950680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.395728, 20.593581}, 0.000037680897016661], InsetBox[
          StyleBox["19",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39569031910298`, 20.593618680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.396983, 20.59321}, 0.000037680897016661], InsetBox[
          StyleBox["20",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39694531910298`, 20.593247680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.39841, 20.592698}, 0.000037680897016661], InsetBox[
          StyleBox["21",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39837231910298`, 20.592735680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.400352, 20.592065}, 0.000037680897016661], InsetBox[
          StyleBox["22",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40031431910297`, 20.592102680897018`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.401704, 20.591653}, 0.000037680897016661], InsetBox[
          StyleBox["23",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40166631910297`, 20.591690680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.391013, 20.591467}, 0.000037680897016661], InsetBox[
          StyleBox["24",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39097531910298`, 20.591504680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.391613, 20.591166}, 0.000037680897016661], InsetBox[
          StyleBox["25",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39157531910298`, 20.591203680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.39318, 20.590684}, 0.000037680897016661], InsetBox[
          StyleBox["26",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39314231910298`, 20.590721680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.404826, 20.590538}, 0.000037680897016661], InsetBox[
          StyleBox["27",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40478831910298`, 20.590575680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.394596, 20.590282}, 0.000037680897016661], InsetBox[
          StyleBox["28",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39455831910298`, 20.590319680897014`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.406317, 20.589966}, 0.000037680897016661], InsetBox[
          StyleBox["29",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40627931910298`, 20.590003680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.395948, 20.58982}, 0.000037680897016661], InsetBox[
          StyleBox["30",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39591031910298`, 20.589857680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.413237, 20.589705}, 0.000037680897016661], InsetBox[
          StyleBox["31",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41319931910297`, 20.589742680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.408324, 20.589353}, 0.000037680897016661], InsetBox[
          StyleBox["32",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40828631910297`, 20.589390680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.411424, 20.589373}, 0.000037680897016661], InsetBox[
          StyleBox["33",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41138631910297`, 20.589410680897014`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.397278, 20.589338}, 0.000037680897016661], InsetBox[
          StyleBox["34",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39724031910298`, 20.589375680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.409032, 20.589223}, 0.000037680897016661], InsetBox[
          StyleBox["35",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40899431910297`, 20.589260680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.41004, 20.589283}, 0.000037680897016661], InsetBox[
          StyleBox["36",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41000231910297`, 20.589320680897014`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.404273, 20.589177}, 0.000037680897016661], InsetBox[
          StyleBox["37",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40423531910298`, 20.589214680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.389859, 20.588911}, 0.000037680897016661], InsetBox[
          StyleBox["38",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38982131910298`, 20.588948680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399102, 20.588695}, 0.000037680897016661], InsetBox[
          StyleBox["39",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39906431910298`, 20.588732680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.390396, 20.58871}, 0.000037680897016661], InsetBox[
          StyleBox["40",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39035831910297`, 20.588747680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.405797, 20.588615}, 0.000037680897016661], InsetBox[
          StyleBox["41",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40575931910298`, 20.588652680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.400475, 20.588233}, 0.000037680897016661], InsetBox[
          StyleBox["42",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40043731910298`, 20.588270680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.392112, 20.588168}, 0.000037680897016661], InsetBox[
          StyleBox["43",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39207431910297`, 20.588205680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.408071, 20.587832}, 0.000037680897016661], InsetBox[
          StyleBox["44",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40803331910298`, 20.587869680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.393979, 20.587606}, 0.000037680897016661], InsetBox[
          StyleBox["45",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39394131910298`, 20.587643680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.409337, 20.58743}, 0.000037680897016661], InsetBox[
          StyleBox["46",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40929931910297`, 20.587467680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.395052, 20.587184}, 0.000037680897016661], InsetBox[
          StyleBox["47",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39501431910298`, 20.587221680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.403265, 20.587129}, 0.000037680897016661], InsetBox[
          StyleBox["48",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40322731910298`, 20.587166680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.410475, 20.587028}, 0.000037680897016661], InsetBox[
          StyleBox["49",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41043731910298`, 20.587065680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.396447, 20.586682}, 0.000037680897016661], InsetBox[
          StyleBox["50",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39640931910297`, 20.586719680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.405089, 20.586466}, 0.000037680897016661], InsetBox[
          StyleBox["51",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40505131910298`, 20.586503680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.411934, 20.586506}, 0.000037680897016661], InsetBox[
          StyleBox["52",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41189631910298`, 20.586543680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.398136, 20.586074}, 0.000037680897016661], InsetBox[
          StyleBox["53",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39809831910297`, 20.586111680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.388851, 20.585938}, 0.000037680897016661], InsetBox[
          StyleBox["54",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38881331910298`, 20.585975680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.407256, 20.585823}, 0.000037680897016661], InsetBox[
          StyleBox["55",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40721831910298`, 20.585860680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399553, 20.585682}, 0.000037680897016661], InsetBox[
          StyleBox["56",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39951531910297`, 20.585719680897014`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.408543, 20.585562}, 0.000037680897016661], InsetBox[
          StyleBox["57",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40850531910297`, 20.585599680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.409788, 20.585301}, 0.000037680897016661], InsetBox[
          StyleBox["58",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40975031910298`, 20.585338680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.391018, 20.585215}, 0.000037680897016661], InsetBox[
          StyleBox["59",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39098031910298`, 20.585252680897018`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.402128, 20.584798}, 0.000037680897016661], InsetBox[
          StyleBox["60",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40209031910298`, 20.584835680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.410475, 20.584919}, 0.000037680897016661], InsetBox[
          StyleBox["61",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41043731910298`, 20.584956680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.392906, 20.584532}, 0.000037680897016661], InsetBox[
          StyleBox["62",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39286831910297`, 20.584569680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.404273, 20.584196}, 0.000037680897016661], InsetBox[
          StyleBox["63",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40423531910298`, 20.584233680897015`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.393872, 20.584151}, 0.000037680897016661], InsetBox[
          StyleBox["64",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39383431910298`, 20.584188680897014`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.39561, 20.583247}, 0.000037680897016661], InsetBox[
          StyleBox["65",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39557231910298`, 20.583284680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.398056, 20.58182}, 0.000037680897016661], InsetBox[
          StyleBox["66",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39801831910297`, 20.581857680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.407084, 20.581765}, 0.000037680897016661], InsetBox[
          StyleBox["67",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40704631910297`, 20.581802680897017`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399107, 20.580916}, 0.000037680897016661], InsetBox[
          StyleBox["68",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39906931910298`, 20.580953680897014`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.40224, 20.57722}, 0.000037680897016661], InsetBox[
          StyleBox["69",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40220231910298`, 20.577257680897016`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15,
  ImageSize->{390.6666666666637, Automatic}]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["31298.538418343607`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Quer\[EAcute]taro Downtown Dataset - MCST Solution Graph", \
"Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69}, {
      Null, {{1, 2}, {2, 5}, {3, 1}, {4, 3}, {5, 6}, {5, 17}, {6, 7}, {7, 
       8}, {7, 19}, {8, 10}, {9, 4}, {9, 10}, {10, 11}, {10, 21}, {11, 12}, {
       11, 22}, {12, 13}, {12, 23}, {13, 9}, {16, 2}, {17, 16}, {17, 25}, {18,
        6}, {18, 17}, {19, 18}, {19, 28}, {20, 8}, {20, 19}, {21, 20}, {21, 
       34}, {22, 21}, {22, 39}, {23, 22}, {23, 42}, {24, 16}, {25, 24}, {25, 
       40}, {26, 18}, {26, 25}, {27, 13}, {27, 23}, {28, 26}, {28, 45}, {30, 
       20}, {30, 28}, {33, 49}, {34, 30}, {34, 50}, {35, 44}, {37, 27}, {38, 
       24}, {39, 34}, {39, 53}, {40, 54}, {41, 29}, {41, 37}, {42, 39}, {42, 
       56}, {43, 26}, {44, 32}, {44, 41}, {45, 62}, {46, 44}, {47, 30}, {48, 
       37}, {48, 42}, {49, 46}, {49, 58}, {50, 65}, {51, 41}, {51, 48}, {52, 
       49}, {54, 38}, {55, 51}, {56, 66}, {57, 55}, {58, 57}, {59, 43}, {60, 
       48}, {61, 58}, {63, 51}, {64, 47}, {68, 60}, {1, 5}, {3, 7}, {4, 8}, {
       13, 14}, {14, 15}, {15, 31}, {27, 29}, {29, 32}, {31, 33}, {31, 52}, {
       32, 35}, {33, 36}, {35, 36}, {36, 46}, {38, 40}, {40, 43}, {43, 45}, {
       45, 47}, {47, 50}, {44, 55}, {46, 57}, {50, 53}, {52, 61}, {53, 56}, {
       54, 59}, {56, 60}, {59, 62}, {60, 63}, {61, 67}, {62, 64}, {63, 67}, {
       64, 65}, {65, 66}, {66, 68}, {67, 69}, {68, 69}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[33, 36], 3, 32, 
         UndirectedEdge[12, 13], 
         UndirectedEdge[44, 46], 
         UndirectedEdge[26, 28], 59, 21, 35, 10, 15, 
         UndirectedEdge[1, 5], 63, 
         UndirectedEdge[3, 7], 
         UndirectedEdge[45, 47], 
         UndirectedEdge[31, 33], 
         UndirectedEdge[18, 19], 66, 
         UndirectedEdge[62, 64], 49, 
         UndirectedEdge[38, 40], 
         UndirectedEdge[20, 21], 64, 
         UndirectedEdge[50, 53], 
         UndirectedEdge[59, 62], 
         UndirectedEdge[49, 52], 
         UndirectedEdge[22, 23], 
         UndirectedEdge[15, 31], 
         UndirectedEdge[32, 35], 20, 52, 62, 
         UndirectedEdge[53, 56], 17, 2, 53, 47, 
         UndirectedEdge[57, 58], 
         UndirectedEdge[23, 27], 
         UndirectedEdge[27, 37], 12, 
         UndirectedEdge[24, 25], 
         UndirectedEdge[29, 41], 
         UndirectedEdge[14, 15], 4, 57, 
         UndirectedEdge[54, 59], 
         UndirectedEdge[19, 20], 51, 40, 
         UndirectedEdge[43, 45], 29, 18, 
         UndirectedEdge[40, 43], 33, 
         UndirectedEdge[25, 26], 
         UndirectedEdge[35, 36], 55, 56, 58, 68, 13, 
         UndirectedEdge[60, 63], 
         UndirectedEdge[9, 10], 
         UndirectedEdge[55, 57], 27, 22, 26, 
         UndirectedEdge[32, 44], 65, 8, 31, 
         UndirectedEdge[6, 7], 
         UndirectedEdge[47, 50], 46, 
         UndirectedEdge[34, 39], 41, 48, 
         UndirectedEdge[16, 17], 
         UndirectedEdge[21, 22], 
         UndirectedEdge[51, 55], 
         UndirectedEdge[8, 10], 
         UndirectedEdge[56, 60], 
         UndirectedEdge[46, 49], 42, 
         UndirectedEdge[66, 68], 6, 
         UndirectedEdge[7, 8], 14, 69, 60, 7, 38, 30, 
         UndirectedEdge[2, 5], 5, 45, 61, 
         UndirectedEdge[5, 6], 
         UndirectedEdge[48, 51], 
         UndirectedEdge[68, 69], 
         UndirectedEdge[30, 34], 37, 44, 11, 25, 
         UndirectedEdge[10, 11], 
         UndirectedEdge[38, 54], 1, 
         UndirectedEdge[49, 58], 67, 
         UndirectedEdge[63, 67], 
         UndirectedEdge[42, 56], 
         UndirectedEdge[39, 42], 
         UndirectedEdge[65, 66], 28, 23, 
         UndirectedEdge[11, 12], 39, 
         UndirectedEdge[27, 29], 
         UndirectedEdge[4, 8], 34, 
         UndirectedEdge[17, 18], 
         UndirectedEdge[64, 65], 
         UndirectedEdge[58, 61], 24, 50, 
         UndirectedEdge[28, 30], 9, 36, 
         UndirectedEdge[5, 17], 19, 
         UndirectedEdge[51, 63], 
         UndirectedEdge[29, 32], 16, 54, 43}, 
       GraphHighlightStyle -> {
        "Thick", UndirectedEdge[32, 35] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 15 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[20, 21] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[26, 28] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 46 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[42, 56] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 65 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 39 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[24, 25] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[11, 12] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[59, 62] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 66 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[10, 11] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[28, 30] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 54 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 58 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 67 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[18, 19] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 2 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[60, 63] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 42 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[50, 53] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[14, 15] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[66, 68] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 8 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 51 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[15, 31] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 52 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 62 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[38, 54] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 23 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 35 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[22, 23] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[56, 60] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 19 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 17 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 55 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[57, 58] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[34, 39] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[1, 5] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 11 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[38, 40] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 53 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 41 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 56 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[33, 36] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 43 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 16 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[19, 20] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 44 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[64, 65] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[48, 51] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[54, 59] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[49, 58] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[51, 55] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 32 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[30, 34] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 48 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 40 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 29 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 7 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[17, 18] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 20 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 26 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[12, 13] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 38 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[3, 7] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 9 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 31 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 33 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[53, 56] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[25, 26] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[29, 32] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[4, 8] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 57 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[68, 69] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[32, 44] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 63 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[9, 10] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[23, 27] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 50 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 18 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[55, 57] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 13 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 34 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[49, 52] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[5, 6] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 3 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 12 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 28 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[2, 5] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[27, 37] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[27, 29] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 36 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[44, 46] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 4 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[58, 61] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[43, 45] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 25 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 45 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[45, 47] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 1 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[39, 42] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[46, 49] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 64 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 24 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[21, 22] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 47 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[31, 33] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 21 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 37 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[47, 50] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[29, 41] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 59 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 61 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 69 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 10 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[6, 7] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[7, 8] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[65, 66] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 49 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 22 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 60 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[63, 67] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 30 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[5, 17] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 27 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 68 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[40, 43] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[62, 64] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[35, 36] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 5 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[8, 10] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[51, 63] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 14 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 6 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[16, 17] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}}, EdgeWeight -> CompressedData["
1:eJwN0f0zmwcAB/Ck3FVttDPVnXmtMBfzEmvrZXW+F29lWKUpDVo8SRBplkqE
x8TbqJbOXrxr6uWmo+4YYavqqXg5pZW+uEvRcnTpOLq269KzNM2s6w+fv+Dj
TIhZfCqFQsl759Mc9V9F7SfAaYlYrAtIwI8Gsv72hAgUyrpOUZ0O67dZ7uI7
PDy7aNba51yAuRMfCvccjYe1+llEKZ2PSkPz1dq2IkT1PT+wh0fgD5ef6FYF
BCa/Kfe3q4tCgimxT68VQtfSxo55Ksf4w8GDtyJ4aLT5+/iyrxwdTGlVlCof
lZE0RqFDMUZmgxr9WedhftdElWQrx446i7T+aTboMcMkx4zEkKH6lySmHA5+
nq8GmHxQa69qi6JTMF81HHxkOw+SX9dmzAeKcH1Xk50TLQ2ccseD7HUufLLn
lQweia6mGlnvdBbSd03tDhwk0W40bWksJxBa7vuoyUji5fAVi3DnPFgJzq1t
DMXhyhsPF82YBAzWIQZtP4lT/v126QV8mEW4R841lEEc5LO3fFyGee3vO5bD
uBCP99F6dBJsj/bcDcokcelfo5o2SiC2tnYp5mQ2wqk5dP8aAsbb291j7lIk
f+YSZ6VPR+m+tYImOg+rOwP1S35SvNnUr51OE6CLVTuW7yuFRZs4q6RbBuIL
NOi1XJw+qmLS2HxwDQEf2VK4qLlUsT/pZwl6ApemulUSsB07Pg6J5SNlQGBf
WS2Gnyhldombi7A/czI8BGlYMLixRtykmFiv8KYGiRESoCy1vCaFk95S19ub
gq0n2abfnxVA1Hkjf4Eg4RTffIDnIkZ8rl7T97UACgcyQRvPg9OTHq/DTTJc
K/tEPacQgdt5f6VZIkfN03thu93T4BqXsOnTlgrbQ4Eq68syXD9WcuGhKhtt
8FImzrJxT6jkeNWfgSFgw17DyUXcoxSip7MEK0Nzek3dEbzuLtzkMMMx/5XN
sccToSjZ+486eOgsNMZWE3ScRKZOfyfjRRnkjRYbVCMPIa3kzdUiIZarj880
fJAJ7wdrOxm/5UGjSDy/oGZBtmmVam9N4ELM89aXrETs8Oyqt/tciGalRAfH
WHz5Wtvs2ZCBgS2891+VAOvT3nWtsacwc5kx6JzLRW6MnrSZFOFGKN02YVEI
H8p3gowfMqCfnFQo3hfBLS7VziaYi3EOpm5tiXDTNVz3YCkb92nxK2RSFvod
mdGuHiL4XqwZKbQsRsfU5CglOglc6mGTtBUS+ebJ0rerAiwWe51RvXvkFivb
H39L4BXzhSc7ogLn1CORLckl+B9BfbVH
        "], 
       VertexCoordinates -> CompressedData["
1:eJxF031ME3cYB/CjLKTAKKAbUEABF0rZZM5SRI3C1QSlZoBlbAqWMV4mwoZC
3QCt2hIJL9WClrZDnGOK860EmQWHglI7mIYwioKtW5ELolCzjkZEYJ2pfvuX
f1wun3yf5+733O93YTl7U7+mEQSRgMt5nzeMSHnMdJ1Q4Voac5JLGqrz3Tlw
/ITVyIEzulr1u+ChYEUGG66ZM5sqYLVlpZYOW2vbPONh3+1D40yYs2FeuwO+
PWnudoP5Sd2Nzv7iTWE0BqwyUkuk8Py1kx7PG7lk7/POfDUcTPbaKFiXvlXd
AO+ki7bpYXfq/6AO+NzF6IXjcBwrtelP2NDy+HUdbB7fw2cEput6pnWiSphK
HfIXw+Iz0n+/gn3v3OiSw+52z405sCZz6f1A9DM/ryqJhfVnVyRHOOev179k
wwvPygdJOE1zpc0D/qC65tRnsKXqvHXuB3yPypyPdsPEpaQfx+B65sVjEliZ
/yWjG37Y5chshqOD8hQnYLrm18pOuE4ZpSiFn47k7nSDBy6dzi6EfayLq3zg
0YrWC1/AcULRWCS8MYHhGwsfOZeiscAqT5UkCj5wV+nt3C+q8HRXMNynfYfp
gvkkOldXOnz5xpY125GbqmQlDjWXvBfStF+E/Bd+pGUeJo6+sofBmkGlkYKr
OWn9afC68FDWBDyV0UEUoZ+31fr3OLz8tlwZhdzb66jABAuOFATEwdf9okgz
/F/B2CkK9R/XDSSNwom/R+57EZCuW83zovTwhyXLJmXIGwUrf+uAU0Iq4l8h
p42OXu2E2XP2hkXkWTZH+RV418KT7DNwfRGzWA3/7D8jDHCeJyxDAR8bG3mw
DO+/L9vLPgg/WXr1xRrkszN3dxTBPbR7DRzk37c7DNmwJ2NVTSJyt57BdgH8
xwUiwABLarz8kmET17Y2AfX+RnnZFnj91IbcLOQTrJaBT2CPWwV2K5wYi5MC
L6n+NjQT9XmWh7xwmGEq9BYjX+3uP0uH33dJtj3GfMPiXDoB524uF/ugvjmO
JllU4f9w6d2sQH2g+RueDW4mZCfCkffficibhvsVooV1sHVbEt8I76kVFjvP
iy209PwwrIx+r6wHZvUJUztVb9cfbMgnb8I3mTFZ4cgjy5onW1Rv92f9dwfG
5fBf2mlODGy3Sv+RwcsdTa0CuPzQVGsKLD1M9e6Hz7KHI2aVXDKv7d3LXnh+
0Esf+QysZfktOvcz5HrMs2swm2/69Bb8qPGn9r4GLvkGIuD0cw==
        "]}]], 
    Typeset`boxes, Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$12", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$31", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$16", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$18", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$18", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$43", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$37", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$32", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$31", Automatic, Center], 
          DynamicLocation["VertexID$33", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$32", Automatic, Center], 
          DynamicLocation["VertexID$35", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$32", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$33", Automatic, Center], 
          DynamicLocation["VertexID$36", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$34", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$35", Automatic, Center], 
          DynamicLocation["VertexID$36", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$38", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$38", Automatic, Center], 
          DynamicLocation["VertexID$54", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$42", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$42", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$45", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$46", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$45", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$49", Automatic, Center], 
          DynamicLocation["VertexID$52", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$49", Automatic, Center], 
          DynamicLocation["VertexID$58", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$53", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$50", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$63", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$53", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$54", Automatic, Center], 
          DynamicLocation["VertexID$59", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$58", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$58", Automatic, Center], 
          DynamicLocation["VertexID$61", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$59", Automatic, Center], 
          DynamicLocation["VertexID$62", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$60", Automatic, Center], 
          DynamicLocation["VertexID$63", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$62", Automatic, Center], 
          DynamicLocation["VertexID$64", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$63", Automatic, Center], 
          DynamicLocation["VertexID$67", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$64", Automatic, Center], 
          DynamicLocation["VertexID$65", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$66", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$66", Automatic, Center], 
          DynamicLocation["VertexID$68", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$68", Automatic, Center], 
          DynamicLocation["VertexID$69", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394623, 20.598472}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393679, 20.598432}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396897, 20.598272}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39855, 20.597789}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394473, 20.598051}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396039, 20.597759}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396887, 20.59787}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398442, 20.597428}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399805, 20.597167}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399633, 20.596715}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401618, 20.595962}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403174, 20.595932}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407122, 20.595771}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.413763, 20.595229}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.4149, 20.595249}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392262, 20.594596}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393024, 20.594375}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39458, 20.593913}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395728, 20.593581}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396983, 20.59321}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39841, 20.592698}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400352, 20.592065}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401704, 20.591653}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391013, 20.591467}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391613, 20.591166}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39318, 20.590684}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404826, 20.590538}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394596, 20.590282}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406317, 20.589966}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395948, 20.58982}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.413237, 20.589705}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408324, 20.589353}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.411424, 20.589373}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397278, 20.589338}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409032, 20.589223}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.41004, 20.589283}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404273, 20.589177}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389859, 20.588911}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399102, 20.588695}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.390396, 20.58871}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405797, 20.588615}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400475, 20.588233}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392112, 20.588168}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408071, 20.587832}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393979, 20.587606}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409337, 20.58743}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395052, 20.587184}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403265, 20.587129}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410475, 20.587028}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396447, 20.586682}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405089, 20.586466}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.411934, 20.586506}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398136, 20.586074}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388851, 20.585938}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407256, 20.585823}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399553, 20.585682}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408543, 20.585562}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409788, 20.585301}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391018, 20.585215}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402128, 20.584798}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410475, 20.584919}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392906, 20.584532}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404273, 20.584196}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393872, 20.584151}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39561, 20.583247}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398056, 20.58182}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407084, 20.581765}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399107, 20.580916}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40224, 20.57722}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.32094401737027`}, {-111.51894401734512`, 97.38769513245455}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weights", "Subsection"],

Cell[BoxData["12737.696374712032`"], "Output"],

Cell[BoxData["5640.692902110319`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Quer\[EAcute]taro Downtown Dataset - GSH-T Solution Graph", \
"Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69}, {
      Null, {{1, 2}, {2, 5}, {3, 1}, {4, 3}, {5, 6}, {5, 17}, {6, 7}, {7, 
       8}, {7, 19}, {8, 10}, {9, 4}, {9, 10}, {10, 11}, {10, 21}, {11, 12}, {
       11, 22}, {12, 13}, {12, 23}, {13, 9}, {16, 2}, {17, 16}, {17, 25}, {18,
        6}, {18, 17}, {19, 18}, {19, 28}, {20, 8}, {20, 19}, {21, 20}, {21, 
       34}, {22, 21}, {22, 39}, {23, 22}, {23, 42}, {24, 16}, {25, 24}, {25, 
       40}, {26, 18}, {26, 25}, {27, 13}, {27, 23}, {28, 26}, {28, 45}, {30, 
       20}, {30, 28}, {33, 49}, {34, 30}, {34, 50}, {35, 44}, {37, 27}, {38, 
       24}, {39, 34}, {39, 53}, {40, 54}, {41, 29}, {41, 37}, {42, 39}, {42, 
       56}, {43, 26}, {44, 32}, {44, 41}, {45, 62}, {46, 44}, {47, 30}, {48, 
       37}, {48, 42}, {49, 46}, {49, 58}, {50, 65}, {51, 41}, {51, 48}, {52, 
       49}, {54, 38}, {55, 51}, {56, 66}, {57, 55}, {58, 57}, {59, 43}, {60, 
       48}, {61, 58}, {63, 51}, {64, 47}, {68, 60}, {1, 5}, {3, 7}, {4, 8}, {
       13, 14}, {14, 15}, {15, 31}, {27, 29}, {29, 32}, {31, 33}, {31, 52}, {
       32, 35}, {33, 36}, {35, 36}, {36, 46}, {38, 40}, {40, 43}, {43, 45}, {
       45, 47}, {47, 50}, {44, 55}, {46, 57}, {50, 53}, {52, 61}, {53, 56}, {
       54, 59}, {56, 60}, {59, 62}, {60, 63}, {61, 67}, {62, 64}, {63, 67}, {
       64, 65}, {65, 66}, {66, 68}, {67, 69}, {68, 69}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {46, 
         UndirectedEdge[42, 56], 
         UndirectedEdge[19, 20], 30, 6, 47, 
         UndirectedEdge[5, 6], 29, 40, 
         UndirectedEdge[46, 49], 
         UndirectedEdge[17, 18], 21, 57, 8, 
         UndirectedEdge[43, 45], 23, 34, 41, 
         UndirectedEdge[27, 29], 22, 7, 
         UndirectedEdge[44, 46], 39, 20, 43, 
         UndirectedEdge[49, 58], 
         UndirectedEdge[34, 39], 26, 
         UndirectedEdge[26, 43], 
         UndirectedEdge[45, 47], 
         UndirectedEdge[29, 32], 
         UndirectedEdge[50, 53], 
         UndirectedEdge[39, 42], 
         UndirectedEdge[22, 23], 
         UndirectedEdge[55, 57], 25, 
         UndirectedEdge[26, 28], 10, 
         UndirectedEdge[6, 7], 51, 60, 56, 
         UndirectedEdge[41, 51], 5, 
         UndirectedEdge[5, 17], 19, 55, 50, 
         UndirectedEdge[32, 44], 32, 
         UndirectedEdge[30, 34], 
         UndirectedEdge[51, 63], 58, 42, 17, 24, 63, 27, 
         UndirectedEdge[25, 26], 
         UndirectedEdge[8, 10], 
         UndirectedEdge[7, 8], 45, 
         UndirectedEdge[57, 58], 28, 
         UndirectedEdge[60, 63], 
         UndirectedEdge[20, 21], 
         UndirectedEdge[28, 30], 
         UndirectedEdge[47, 50], 44, 
         UndirectedEdge[18, 19], 
         UndirectedEdge[53, 56], 18, 
         UndirectedEdge[21, 22], 
         UndirectedEdge[23, 27], 
         UndirectedEdge[24, 25], 49, 
         UndirectedEdge[29, 41], 53, 
         UndirectedEdge[40, 43], 
         UndirectedEdge[51, 55], 
         UndirectedEdge[56, 60]}, 
       GraphHighlightStyle -> {"Thick", 26 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 39 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 58 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 7 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 6 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 25 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 32 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 34 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[32, 44] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 56 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 20 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[24, 25] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[47, 50] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 44 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[5, 17] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[44, 46] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[57, 58] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 51 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 60 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 8 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[23, 27] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[29, 32] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[55, 57] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[8, 10] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[21, 22] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[49, 58] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[27, 29] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[50, 53] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[26, 43] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[56, 60] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 28 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 22 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 24 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 41 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 40 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 42 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 5 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[51, 55] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 19 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[39, 42] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[5, 6] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 29 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 45 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 23 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[19, 20] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 57 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 55 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 21 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[46, 49] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[18, 19] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[43, 45] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[53, 56] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 47 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[42, 56] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[45, 47] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[26, 28] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[41, 51] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[51, 63] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[6, 7] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 63 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[20, 21] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 46 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[28, 30] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[7, 8] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 27 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[25, 26] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 50 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[17, 18] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 49 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[34, 39] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[60, 63] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[29, 41] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 10 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 18 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[40, 43] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[30, 34] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[22, 23] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 43 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 30 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 17 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 53 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}}, EdgeWeight -> CompressedData["
1:eJwN0f0zmwcAB/Ck3FVttDPVnXmtMBfzEmvrZXW+F29lWKUpDVo8SRBplkqE
x8TbqJbOXrxr6uWmo+4YYavqqXg5pZW+uEvRcnTpOLq269KzNM2s6w+fv+Dj
TIhZfCqFQsl759Mc9V9F7SfAaYlYrAtIwI8Gsv72hAgUyrpOUZ0O67dZ7uI7
PDy7aNba51yAuRMfCvccjYe1+llEKZ2PSkPz1dq2IkT1PT+wh0fgD5ef6FYF
BCa/Kfe3q4tCgimxT68VQtfSxo55Ksf4w8GDtyJ4aLT5+/iyrxwdTGlVlCof
lZE0RqFDMUZmgxr9WedhftdElWQrx446i7T+aTboMcMkx4zEkKH6lySmHA5+
nq8GmHxQa69qi6JTMF81HHxkOw+SX9dmzAeKcH1Xk50TLQ2ccseD7HUufLLn
lQweia6mGlnvdBbSd03tDhwk0W40bWksJxBa7vuoyUji5fAVi3DnPFgJzq1t
DMXhyhsPF82YBAzWIQZtP4lT/v126QV8mEW4R841lEEc5LO3fFyGee3vO5bD
uBCP99F6dBJsj/bcDcokcelfo5o2SiC2tnYp5mQ2wqk5dP8aAsbb291j7lIk
f+YSZ6VPR+m+tYImOg+rOwP1S35SvNnUr51OE6CLVTuW7yuFRZs4q6RbBuIL
NOi1XJw+qmLS2HxwDQEf2VK4qLlUsT/pZwl6ApemulUSsB07Pg6J5SNlQGBf
WS2Gnyhldombi7A/czI8BGlYMLixRtykmFiv8KYGiRESoCy1vCaFk95S19ub
gq0n2abfnxVA1Hkjf4Eg4RTffIDnIkZ8rl7T97UACgcyQRvPg9OTHq/DTTJc
K/tEPacQgdt5f6VZIkfN03thu93T4BqXsOnTlgrbQ4Eq68syXD9WcuGhKhtt
8FImzrJxT6jkeNWfgSFgw17DyUXcoxSip7MEK0Nzek3dEbzuLtzkMMMx/5XN
sccToSjZ+486eOgsNMZWE3ScRKZOfyfjRRnkjRYbVCMPIa3kzdUiIZarj880
fJAJ7wdrOxm/5UGjSDy/oGZBtmmVam9N4ELM89aXrETs8Oyqt/tciGalRAfH
WHz5Wtvs2ZCBgS2891+VAOvT3nWtsacwc5kx6JzLRW6MnrSZFOFGKN02YVEI
H8p3gowfMqCfnFQo3hfBLS7VziaYi3EOpm5tiXDTNVz3YCkb92nxK2RSFvod
mdGuHiL4XqwZKbQsRsfU5CglOglc6mGTtBUS+ebJ0rerAiwWe51RvXvkFivb
H39L4BXzhSc7ogLn1CORLckl+B9BfbVH
        "], 
       VertexCoordinates -> CompressedData["
1:eJxF031ME3cYB/CjLKTAKKAbUEABF0rZZM5SRI3C1QSlZoBlbAqWMV4mwoZC
3QCt2hIJL9WClrZDnGOK860EmQWHglI7mIYwioKtW5ELolCzjkZEYJ2pfvuX
f1wun3yf5+733O93YTl7U7+mEQSRgMt5nzeMSHnMdJ1Q4Voac5JLGqrz3Tlw
/ITVyIEzulr1u+ChYEUGG66ZM5sqYLVlpZYOW2vbPONh3+1D40yYs2FeuwO+
PWnudoP5Sd2Nzv7iTWE0BqwyUkuk8Py1kx7PG7lk7/POfDUcTPbaKFiXvlXd
AO+ki7bpYXfq/6AO+NzF6IXjcBwrtelP2NDy+HUdbB7fw2cEput6pnWiSphK
HfIXw+Iz0n+/gn3v3OiSw+52z405sCZz6f1A9DM/ryqJhfVnVyRHOOev179k
wwvPygdJOE1zpc0D/qC65tRnsKXqvHXuB3yPypyPdsPEpaQfx+B65sVjEliZ
/yWjG37Y5chshqOD8hQnYLrm18pOuE4ZpSiFn47k7nSDBy6dzi6EfayLq3zg
0YrWC1/AcULRWCS8MYHhGwsfOZeiscAqT5UkCj5wV+nt3C+q8HRXMNynfYfp
gvkkOldXOnz5xpY125GbqmQlDjWXvBfStF+E/Bd+pGUeJo6+sofBmkGlkYKr
OWn9afC68FDWBDyV0UEUoZ+31fr3OLz8tlwZhdzb66jABAuOFATEwdf9okgz
/F/B2CkK9R/XDSSNwom/R+57EZCuW83zovTwhyXLJmXIGwUrf+uAU0Iq4l8h
p42OXu2E2XP2hkXkWTZH+RV418KT7DNwfRGzWA3/7D8jDHCeJyxDAR8bG3mw
DO+/L9vLPgg/WXr1xRrkszN3dxTBPbR7DRzk37c7DNmwJ2NVTSJyt57BdgH8
xwUiwABLarz8kmET17Y2AfX+RnnZFnj91IbcLOQTrJaBT2CPWwV2K5wYi5MC
L6n+NjQT9XmWh7xwmGEq9BYjX+3uP0uH33dJtj3GfMPiXDoB524uF/ugvjmO
JllU4f9w6d2sQH2g+RueDW4mZCfCkffficibhvsVooV1sHVbEt8I76kVFjvP
iy209PwwrIx+r6wHZvUJUztVb9cfbMgnb8I3mTFZ4cgjy5onW1Rv92f9dwfG
5fBf2mlODGy3Sv+RwcsdTa0CuPzQVGsKLD1M9e6Hz7KHI2aVXDKv7d3LXnh+
0Esf+QysZfktOvcz5HrMs2swm2/69Bb8qPGn9r4GLvkGIuD0cw==
        "]}]], 
    Typeset`boxes, Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$18", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$18", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$37", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$32", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$32", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$34", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$42", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$41", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$42", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$45", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$46", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$45", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$49", Automatic, Center], 
          DynamicLocation["VertexID$58", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$53", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$50", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$63", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$53", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$58", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$60", Automatic, Center], 
          DynamicLocation["VertexID$63", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394623, 20.598472}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393679, 20.598432}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.396897, 20.598272}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39855, 20.597789}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394473, 20.598051}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396039, 20.597759}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396887, 20.59787}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398442, 20.597428}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399805, 20.597167}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399633, 20.596715}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.401618, 20.595962}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.403174, 20.595932}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407122, 20.595771}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413763, 20.595229}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.4149, 20.595249}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392262, 20.594596}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393024, 20.594375}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39458, 20.593913}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395728, 20.593581}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396983, 20.59321}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39841, 20.592698}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400352, 20.592065}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401704, 20.591653}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391013, 20.591467}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391613, 20.591166}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39318, 20.590684}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404826, 20.590538}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394596, 20.590282}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406317, 20.589966}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395948, 20.58982}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413237, 20.589705}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408324, 20.589353}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411424, 20.589373}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397278, 20.589338}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409032, 20.589223}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41004, 20.589283}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404273, 20.589177}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389859, 20.588911}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399102, 20.588695}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.390396, 20.58871}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405797, 20.588615}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400475, 20.588233}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392112, 20.588168}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408071, 20.587832}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393979, 20.587606}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409337, 20.58743}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395052, 20.587184}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.403265, 20.587129}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410475, 20.587028}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396447, 20.586682}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405089, 20.586466}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411934, 20.586506}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398136, 20.586074}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.388851, 20.585938}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407256, 20.585823}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399553, 20.585682}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408543, 20.585562}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409788, 20.585301}, 0.000037680897016661], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391018, 20.585215}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402128, 20.584798}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410475, 20.584919}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392906, 20.584532}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404273, 20.584196}, 0.000037680897016661], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393872, 20.584151}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39561, 20.583247}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398056, 20.58182}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407084, 20.581765}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399107, 20.580916}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40224, 20.57722}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.32094401737027`}, {-111.51894401734512`, 97.38769513245455}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weights", "Subsection"],

Cell[BoxData["6791.302902533311`"], "Output"],

Cell[BoxData["5337.621954032939`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Quer\[EAcute]taro Downtown Dataset - GSH-P Solution Graph", \
"Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69}, {
      Null, {{1, 2}, {2, 5}, {3, 1}, {4, 3}, {5, 6}, {5, 17}, {6, 7}, {7, 
       8}, {7, 19}, {8, 10}, {9, 4}, {9, 10}, {10, 11}, {10, 21}, {11, 12}, {
       11, 22}, {12, 13}, {12, 23}, {13, 9}, {16, 2}, {17, 16}, {17, 25}, {18,
        6}, {18, 17}, {19, 18}, {19, 28}, {20, 8}, {20, 19}, {21, 20}, {21, 
       34}, {22, 21}, {22, 39}, {23, 22}, {23, 42}, {24, 16}, {25, 24}, {25, 
       40}, {26, 18}, {26, 25}, {27, 13}, {27, 23}, {28, 26}, {28, 45}, {30, 
       20}, {30, 28}, {33, 49}, {34, 30}, {34, 50}, {35, 44}, {37, 27}, {38, 
       24}, {39, 34}, {39, 53}, {40, 54}, {41, 29}, {41, 37}, {42, 39}, {42, 
       56}, {43, 26}, {44, 32}, {44, 41}, {45, 62}, {46, 44}, {47, 30}, {48, 
       37}, {48, 42}, {49, 46}, {49, 58}, {50, 65}, {51, 41}, {51, 48}, {52, 
       49}, {54, 38}, {55, 51}, {56, 66}, {57, 55}, {58, 57}, {59, 43}, {60, 
       48}, {61, 58}, {63, 51}, {64, 47}, {68, 60}, {1, 5}, {3, 7}, {4, 8}, {
       13, 14}, {14, 15}, {15, 31}, {27, 29}, {29, 32}, {31, 33}, {31, 52}, {
       32, 35}, {33, 36}, {35, 36}, {36, 46}, {38, 40}, {40, 43}, {43, 45}, {
       45, 47}, {47, 50}, {44, 55}, {46, 57}, {50, 53}, {52, 61}, {53, 56}, {
       54, 59}, {56, 60}, {59, 62}, {60, 63}, {61, 67}, {62, 64}, {63, 67}, {
       64, 65}, {65, 66}, {66, 68}, {67, 69}, {68, 69}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {46, 
         UndirectedEdge[48, 60], 
         UndirectedEdge[19, 20], 30, 6, 47, 
         UndirectedEdge[5, 6], 29, 40, 
         UndirectedEdge[46, 49], 
         UndirectedEdge[17, 18], 21, 48, 8, 23, 
         UndirectedEdge[43, 45], 37, 
         UndirectedEdge[37, 48], 34, 
         UndirectedEdge[27, 29], 41, 22, 7, 
         UndirectedEdge[44, 46], 39, 20, 43, 
         UndirectedEdge[34, 39], 26, 
         UndirectedEdge[45, 47], 
         UndirectedEdge[39, 42], 
         UndirectedEdge[50, 53], 
         UndirectedEdge[22, 23], 
         UndirectedEdge[41, 44], 25, 
         UndirectedEdge[26, 28], 10, 51, 
         UndirectedEdge[6, 7], 60, 56, 
         UndirectedEdge[41, 51], 5, 
         UndirectedEdge[42, 48], 
         UndirectedEdge[5, 17], 19, 
         UndirectedEdge[25, 40], 50, 
         UndirectedEdge[30, 34], 
         UndirectedEdge[37, 41], 42, 17, 
         UndirectedEdge[48, 51], 27, 
         UndirectedEdge[25, 26], 
         UndirectedEdge[8, 10], 45, 
         UndirectedEdge[7, 8], 28, 
         UndirectedEdge[20, 21], 
         UndirectedEdge[28, 30], 
         UndirectedEdge[47, 50], 44, 
         UndirectedEdge[18, 19], 
         UndirectedEdge[53, 56], 18, 
         UndirectedEdge[23, 27], 
         UndirectedEdge[21, 22], 49, 53, 
         UndirectedEdge[29, 41], 
         UndirectedEdge[40, 43], 
         UndirectedEdge[56, 60]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0f0zmwcAB/Ck3FVttDPVnXmtMBfzEmvrZXW+F29lWKUpDVo8SRBplkqE
x8TbqJbOXrxr6uWmo+4YYavqqXg5pZW+uEvRcnTpOLq269KzNM2s6w+fv+Dj
TIhZfCqFQsl759Mc9V9F7SfAaYlYrAtIwI8Gsv72hAgUyrpOUZ0O67dZ7uI7
PDy7aNba51yAuRMfCvccjYe1+llEKZ2PSkPz1dq2IkT1PT+wh0fgD5ef6FYF
BCa/Kfe3q4tCgimxT68VQtfSxo55Ksf4w8GDtyJ4aLT5+/iyrxwdTGlVlCof
lZE0RqFDMUZmgxr9WedhftdElWQrx446i7T+aTboMcMkx4zEkKH6lySmHA5+
nq8GmHxQa69qi6JTMF81HHxkOw+SX9dmzAeKcH1Xk50TLQ2ccseD7HUufLLn
lQweia6mGlnvdBbSd03tDhwk0W40bWksJxBa7vuoyUji5fAVi3DnPFgJzq1t
DMXhyhsPF82YBAzWIQZtP4lT/v126QV8mEW4R841lEEc5LO3fFyGee3vO5bD
uBCP99F6dBJsj/bcDcokcelfo5o2SiC2tnYp5mQ2wqk5dP8aAsbb291j7lIk
f+YSZ6VPR+m+tYImOg+rOwP1S35SvNnUr51OE6CLVTuW7yuFRZs4q6RbBuIL
NOi1XJw+qmLS2HxwDQEf2VK4qLlUsT/pZwl6ApemulUSsB07Pg6J5SNlQGBf
WS2Gnyhldombi7A/czI8BGlYMLixRtykmFiv8KYGiRESoCy1vCaFk95S19ub
gq0n2abfnxVA1Hkjf4Eg4RTffIDnIkZ8rl7T97UACgcyQRvPg9OTHq/DTTJc
K/tEPacQgdt5f6VZIkfN03thu93T4BqXsOnTlgrbQ4Eq68syXD9WcuGhKhtt
8FImzrJxT6jkeNWfgSFgw17DyUXcoxSip7MEK0Nzek3dEbzuLtzkMMMx/5XN
sccToSjZ+486eOgsNMZWE3ScRKZOfyfjRRnkjRYbVCMPIa3kzdUiIZarj880
fJAJ7wdrOxm/5UGjSDy/oGZBtmmVam9N4ELM89aXrETs8Oyqt/tciGalRAfH
WHz5Wtvs2ZCBgS2891+VAOvT3nWtsacwc5kx6JzLRW6MnrSZFOFGKN02YVEI
H8p3gowfMqCfnFQo3hfBLS7VziaYi3EOpm5tiXDTNVz3YCkb92nxK2RSFvod
mdGuHiL4XqwZKbQsRsfU5CglOglc6mGTtBUS+ebJ0rerAiwWe51RvXvkFivb
H39L4BXzhSc7ogLn1CORLckl+B9BfbVH
        "], 
       VertexCoordinates -> CompressedData["
1:eJxF031ME3cYB/CjLKTAKKAbUEABF0rZZM5SRI3C1QSlZoBlbAqWMV4mwoZC
3QCt2hIJL9WClrZDnGOK860EmQWHglI7mIYwioKtW5ELolCzjkZEYJ2pfvuX
f1wun3yf5+733O93YTl7U7+mEQSRgMt5nzeMSHnMdJ1Q4Voac5JLGqrz3Tlw
/ITVyIEzulr1u+ChYEUGG66ZM5sqYLVlpZYOW2vbPONh3+1D40yYs2FeuwO+
PWnudoP5Sd2Nzv7iTWE0BqwyUkuk8Py1kx7PG7lk7/POfDUcTPbaKFiXvlXd
AO+ki7bpYXfq/6AO+NzF6IXjcBwrtelP2NDy+HUdbB7fw2cEput6pnWiSphK
HfIXw+Iz0n+/gn3v3OiSw+52z405sCZz6f1A9DM/ryqJhfVnVyRHOOev179k
wwvPygdJOE1zpc0D/qC65tRnsKXqvHXuB3yPypyPdsPEpaQfx+B65sVjEliZ
/yWjG37Y5chshqOD8hQnYLrm18pOuE4ZpSiFn47k7nSDBy6dzi6EfayLq3zg
0YrWC1/AcULRWCS8MYHhGwsfOZeiscAqT5UkCj5wV+nt3C+q8HRXMNynfYfp
gvkkOldXOnz5xpY125GbqmQlDjWXvBfStF+E/Bd+pGUeJo6+sofBmkGlkYKr
OWn9afC68FDWBDyV0UEUoZ+31fr3OLz8tlwZhdzb66jABAuOFATEwdf9okgz
/F/B2CkK9R/XDSSNwom/R+57EZCuW83zovTwhyXLJmXIGwUrf+uAU0Iq4l8h
p42OXu2E2XP2hkXkWTZH+RV418KT7DNwfRGzWA3/7D8jDHCeJyxDAR8bG3mw
DO+/L9vLPgg/WXr1xRrkszN3dxTBPbR7DRzk37c7DNmwJ2NVTSJyt57BdgH8
xwUiwABLarz8kmET17Y2AfX+RnnZFnj91IbcLOQTrJaBT2CPWwV2K5wYi5MC
L6n+NjQT9XmWh7xwmGEq9BYjX+3uP0uH33dJtj3GfMPiXDoB524uF/ugvjmO
JllU4f9w6d2sQH2g+RueDW4mZCfCkffficibhvsVooV1sHVbEt8I76kVFjvP
iy209PwwrIx+r6wHZvUJUztVb9cfbMgnb8I3mTFZ4cgjy5onW1Rv92f9dwfG
5fBf2mlODGy3Sv+RwcsdTa0CuPzQVGsKLD1M9e6Hz7KHI2aVXDKv7d3LXnh+
0Esf+QysZfktOvcz5HrMs2swm2/69Bb8qPGn9r4GLvkGIuD0cw==
        "]}]], 
    Typeset`boxes, Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$18", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$18", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$43", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$37", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$34", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$37", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$37", Automatic, Center], 
          DynamicLocation["VertexID$48", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$42", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$41", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$41", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$42", Automatic, Center], 
          DynamicLocation["VertexID$48", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$56", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$45", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$46", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$45", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$53", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$50", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$53", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394623, 20.598472}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393679, 20.598432}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.396897, 20.598272}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39855, 20.597789}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394473, 20.598051}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396039, 20.597759}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396887, 20.59787}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398442, 20.597428}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399805, 20.597167}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399633, 20.596715}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.401618, 20.595962}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.403174, 20.595932}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407122, 20.595771}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413763, 20.595229}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.4149, 20.595249}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392262, 20.594596}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393024, 20.594375}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39458, 20.593913}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395728, 20.593581}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396983, 20.59321}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39841, 20.592698}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400352, 20.592065}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401704, 20.591653}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391013, 20.591467}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391613, 20.591166}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39318, 20.590684}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404826, 20.590538}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394596, 20.590282}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406317, 20.589966}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395948, 20.58982}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413237, 20.589705}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.408324, 20.589353}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411424, 20.589373}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397278, 20.589338}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409032, 20.589223}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41004, 20.589283}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404273, 20.589177}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389859, 20.588911}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399102, 20.588695}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.390396, 20.58871}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405797, 20.588615}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400475, 20.588233}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392112, 20.588168}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408071, 20.587832}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393979, 20.587606}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409337, 20.58743}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395052, 20.587184}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403265, 20.587129}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410475, 20.587028}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396447, 20.586682}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405089, 20.586466}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411934, 20.586506}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398136, 20.586074}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.388851, 20.585938}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407256, 20.585823}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399553, 20.585682}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.408543, 20.585562}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409788, 20.585301}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391018, 20.585215}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402128, 20.584798}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410475, 20.584919}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392906, 20.584532}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404273, 20.584196}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393872, 20.584151}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39561, 20.583247}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398056, 20.58182}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407084, 20.581765}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399107, 20.580916}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40224, 20.57722}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{16.585786437626894`, 
       359.4232268526573}, {-147.69562685241158`, 134.05954881332275`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15,
  ImageSize->{483.99999999999966`, Automatic}]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["7204.476442803815`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Quer\[EAcute]taro Downtown Dataset - GSH-C Solution Graph", \
"Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69}, {
      Null, {{1, 2}, {2, 5}, {3, 1}, {4, 3}, {5, 6}, {5, 17}, {6, 7}, {7, 
       8}, {7, 19}, {8, 10}, {9, 4}, {9, 10}, {10, 11}, {10, 21}, {11, 12}, {
       11, 22}, {12, 13}, {12, 23}, {13, 9}, {16, 2}, {17, 16}, {17, 25}, {18,
        6}, {18, 17}, {19, 18}, {19, 28}, {20, 8}, {20, 19}, {21, 20}, {21, 
       34}, {22, 21}, {22, 39}, {23, 22}, {23, 42}, {24, 16}, {25, 24}, {25, 
       40}, {26, 18}, {26, 25}, {27, 13}, {27, 23}, {28, 26}, {28, 45}, {30, 
       20}, {30, 28}, {33, 49}, {34, 30}, {34, 50}, {35, 44}, {37, 27}, {38, 
       24}, {39, 34}, {39, 53}, {40, 54}, {41, 29}, {41, 37}, {42, 39}, {42, 
       56}, {43, 26}, {44, 32}, {44, 41}, {45, 62}, {46, 44}, {47, 30}, {48, 
       37}, {48, 42}, {49, 46}, {49, 58}, {50, 65}, {51, 41}, {51, 48}, {52, 
       49}, {54, 38}, {55, 51}, {56, 66}, {57, 55}, {58, 57}, {59, 43}, {60, 
       48}, {61, 58}, {63, 51}, {64, 47}, {68, 60}, {1, 5}, {3, 7}, {4, 8}, {
       13, 14}, {14, 15}, {15, 31}, {27, 29}, {29, 32}, {31, 33}, {31, 52}, {
       32, 35}, {33, 36}, {35, 36}, {36, 46}, {38, 40}, {40, 43}, {43, 45}, {
       45, 47}, {47, 50}, {44, 55}, {46, 57}, {50, 53}, {52, 61}, {53, 56}, {
       54, 59}, {56, 60}, {59, 62}, {60, 63}, {61, 67}, {62, 64}, {63, 67}, {
       64, 65}, {65, 66}, {66, 68}, {67, 69}, {68, 69}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {46, 
         UndirectedEdge[19, 20], 
         UndirectedEdge[48, 60], 30, 6, 47, 
         UndirectedEdge[5, 6], 29, 11, 40, 
         UndirectedEdge[46, 49], 
         UndirectedEdge[17, 18], 21, 57, 48, 8, 23, 
         UndirectedEdge[43, 45], 37, 34, 41, 22, 7, 39, 
         UndirectedEdge[44, 46], 20, 43, 
         UndirectedEdge[10, 11], 
         UndirectedEdge[49, 58], 
         UndirectedEdge[34, 39], 26, 
         UndirectedEdge[45, 47], 
         UndirectedEdge[29, 32], 
         UndirectedEdge[50, 53], 
         UndirectedEdge[22, 23], 
         UndirectedEdge[55, 57], 25, 
         UndirectedEdge[26, 28], 10, 
         UndirectedEdge[6, 7], 51, 60, 56, 5, 
         UndirectedEdge[5, 17], 19, 
         UndirectedEdge[25, 40], 55, 32, 50, 
         UndirectedEdge[32, 44], 
         UndirectedEdge[30, 34], 
         UndirectedEdge[37, 41], 58, 17, 
         UndirectedEdge[48, 51], 
         UndirectedEdge[27, 37], 27, 
         UndirectedEdge[25, 26], 
         UndirectedEdge[8, 10], 
         UndirectedEdge[7, 8], 45, 
         UndirectedEdge[57, 58], 28, 
         UndirectedEdge[20, 21], 
         UndirectedEdge[28, 30], 
         UndirectedEdge[47, 50], 44, 
         UndirectedEdge[18, 19], 
         UndirectedEdge[53, 56], 18, 
         UndirectedEdge[11, 22], 
         UndirectedEdge[21, 22], 
         UndirectedEdge[23, 27], 49, 53, 
         UndirectedEdge[29, 41], 
         UndirectedEdge[40, 43], 
         UndirectedEdge[56, 60], 
         UndirectedEdge[51, 55], 
         UndirectedEdge[22, 39]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0f0zmwcAB/Ck3FVttDPVnXmtMBfzEmvrZXW+F29lWKUpDVo8SRBplkqE
x8TbqJbOXrxr6uWmo+4YYavqqXg5pZW+uEvRcnTpOLq269KzNM2s6w+fv+Dj
TIhZfCqFQsl759Mc9V9F7SfAaYlYrAtIwI8Gsv72hAgUyrpOUZ0O67dZ7uI7
PDy7aNba51yAuRMfCvccjYe1+llEKZ2PSkPz1dq2IkT1PT+wh0fgD5ef6FYF
BCa/Kfe3q4tCgimxT68VQtfSxo55Ksf4w8GDtyJ4aLT5+/iyrxwdTGlVlCof
lZE0RqFDMUZmgxr9WedhftdElWQrx446i7T+aTboMcMkx4zEkKH6lySmHA5+
nq8GmHxQa69qi6JTMF81HHxkOw+SX9dmzAeKcH1Xk50TLQ2ccseD7HUufLLn
lQweia6mGlnvdBbSd03tDhwk0W40bWksJxBa7vuoyUji5fAVi3DnPFgJzq1t
DMXhyhsPF82YBAzWIQZtP4lT/v126QV8mEW4R841lEEc5LO3fFyGee3vO5bD
uBCP99F6dBJsj/bcDcokcelfo5o2SiC2tnYp5mQ2wqk5dP8aAsbb291j7lIk
f+YSZ6VPR+m+tYImOg+rOwP1S35SvNnUr51OE6CLVTuW7yuFRZs4q6RbBuIL
NOi1XJw+qmLS2HxwDQEf2VK4qLlUsT/pZwl6ApemulUSsB07Pg6J5SNlQGBf
WS2Gnyhldombi7A/czI8BGlYMLixRtykmFiv8KYGiRESoCy1vCaFk95S19ub
gq0n2abfnxVA1Hkjf4Eg4RTffIDnIkZ8rl7T97UACgcyQRvPg9OTHq/DTTJc
K/tEPacQgdt5f6VZIkfN03thu93T4BqXsOnTlgrbQ4Eq68syXD9WcuGhKhtt
8FImzrJxT6jkeNWfgSFgw17DyUXcoxSip7MEK0Nzek3dEbzuLtzkMMMx/5XN
sccToSjZ+486eOgsNMZWE3ScRKZOfyfjRRnkjRYbVCMPIa3kzdUiIZarj880
fJAJ7wdrOxm/5UGjSDy/oGZBtmmVam9N4ELM89aXrETs8Oyqt/tciGalRAfH
WHz5Wtvs2ZCBgS2891+VAOvT3nWtsacwc5kx6JzLRW6MnrSZFOFGKN02YVEI
H8p3gowfMqCfnFQo3hfBLS7VziaYi3EOpm5tiXDTNVz3YCkb92nxK2RSFvod
mdGuHiL4XqwZKbQsRsfU5CglOglc6mGTtBUS+ebJ0rerAiwWe51RvXvkFivb
H39L4BXzhSc7ogLn1CORLckl+B9BfbVH
        "], 
       VertexCoordinates -> CompressedData["
1:eJxF031ME3cYB/CjLKTAKKAbUEABF0rZZM5SRI3C1QSlZoBlbAqWMV4mwoZC
3QCt2hIJL9WClrZDnGOK860EmQWHglI7mIYwioKtW5ELolCzjkZEYJ2pfvuX
f1wun3yf5+733O93YTl7U7+mEQSRgMt5nzeMSHnMdJ1Q4Voac5JLGqrz3Tlw
/ITVyIEzulr1u+ChYEUGG66ZM5sqYLVlpZYOW2vbPONh3+1D40yYs2FeuwO+
PWnudoP5Sd2Nzv7iTWE0BqwyUkuk8Py1kx7PG7lk7/POfDUcTPbaKFiXvlXd
AO+ki7bpYXfq/6AO+NzF6IXjcBwrtelP2NDy+HUdbB7fw2cEput6pnWiSphK
HfIXw+Iz0n+/gn3v3OiSw+52z405sCZz6f1A9DM/ryqJhfVnVyRHOOev179k
wwvPygdJOE1zpc0D/qC65tRnsKXqvHXuB3yPypyPdsPEpaQfx+B65sVjEliZ
/yWjG37Y5chshqOD8hQnYLrm18pOuE4ZpSiFn47k7nSDBy6dzi6EfayLq3zg
0YrWC1/AcULRWCS8MYHhGwsfOZeiscAqT5UkCj5wV+nt3C+q8HRXMNynfYfp
gvkkOldXOnz5xpY125GbqmQlDjWXvBfStF+E/Bd+pGUeJo6+sofBmkGlkYKr
OWn9afC68FDWBDyV0UEUoZ+31fr3OLz8tlwZhdzb66jABAuOFATEwdf9okgz
/F/B2CkK9R/XDSSNwom/R+57EZCuW83zovTwhyXLJmXIGwUrf+uAU0Iq4l8h
p42OXu2E2XP2hkXkWTZH+RV418KT7DNwfRGzWA3/7D8jDHCeJyxDAR8bG3mw
DO+/L9vLPgg/WXr1xRrkszN3dxTBPbR7DRzk37c7DNmwJ2NVTSJyt57BdgH8
xwUiwABLarz8kmET17Y2AfX+RnnZFnj91IbcLOQTrJaBT2CPWwV2K5wYi5MC
L6n+NjQT9XmWh7xwmGEq9BYjX+3uP0uH33dJtj3GfMPiXDoB524uF/ugvjmO
JllU4f9w6d2sQH2g+RueDW4mZCfCkffficibhvsVooV1sHVbEt8I76kVFjvP
iy209PwwrIx+r6wHZvUJUztVb9cfbMgnb8I3mTFZ4cgjy5onW1Rv92f9dwfG
5fBf2mlODGy3Sv+RwcsdTa0CuPzQVGsKLD1M9e6Hz7KHI2aVXDKv7d3LXnh+
0Esf+QysZfktOvcz5HrMs2swm2/69Bb8qPGn9r4GLvkGIuD0cw==
        "]}]], 
    Typeset`boxes, Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$18", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$18", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$43", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$37", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$32", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$32", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$34", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$37", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$56", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$45", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$46", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$45", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$49", Automatic, Center], 
          DynamicLocation["VertexID$58", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$53", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$50", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$53", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$58", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394623, 20.598472}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393679, 20.598432}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.396897, 20.598272}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39855, 20.597789}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394473, 20.598051}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396039, 20.597759}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396887, 20.59787}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398442, 20.597428}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399805, 20.597167}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399633, 20.596715}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401618, 20.595962}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.403174, 20.595932}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407122, 20.595771}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413763, 20.595229}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.4149, 20.595249}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392262, 20.594596}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393024, 20.594375}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39458, 20.593913}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395728, 20.593581}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396983, 20.59321}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39841, 20.592698}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400352, 20.592065}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401704, 20.591653}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391013, 20.591467}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391613, 20.591166}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39318, 20.590684}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404826, 20.590538}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394596, 20.590282}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406317, 20.589966}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395948, 20.58982}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413237, 20.589705}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408324, 20.589353}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411424, 20.589373}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397278, 20.589338}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409032, 20.589223}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41004, 20.589283}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404273, 20.589177}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389859, 20.588911}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399102, 20.588695}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.390396, 20.58871}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405797, 20.588615}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.400475, 20.588233}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392112, 20.588168}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408071, 20.587832}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393979, 20.587606}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409337, 20.58743}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395052, 20.587184}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403265, 20.587129}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410475, 20.587028}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396447, 20.586682}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405089, 20.586466}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411934, 20.586506}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398136, 20.586074}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.388851, 20.585938}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407256, 20.585823}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399553, 20.585682}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408543, 20.585562}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409788, 20.585301}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391018, 20.585215}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402128, 20.584798}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410475, 20.584919}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392906, 20.584532}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404273, 20.584196}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393872, 20.584151}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39561, 20.583247}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398056, 20.58182}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407084, 20.581765}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399107, 20.580916}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40224, 20.57722}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.32094401737027`}, {-111.51894401734512`, 97.38769513245455}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["8150.174774330835`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Quer\[EAcute]taro Downtown Dataset - GC-IE Solution Graph", \
"Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69}, {
      Null, {{1, 2}, {2, 5}, {3, 1}, {4, 3}, {5, 6}, {5, 17}, {6, 7}, {7, 
       8}, {7, 19}, {8, 10}, {9, 4}, {9, 10}, {10, 11}, {10, 21}, {11, 12}, {
       11, 22}, {12, 13}, {12, 23}, {13, 9}, {16, 2}, {17, 16}, {17, 25}, {18,
        6}, {18, 17}, {19, 18}, {19, 28}, {20, 8}, {20, 19}, {21, 20}, {21, 
       34}, {22, 21}, {22, 39}, {23, 22}, {23, 42}, {24, 16}, {25, 24}, {25, 
       40}, {26, 18}, {26, 25}, {27, 13}, {27, 23}, {28, 26}, {28, 45}, {30, 
       20}, {30, 28}, {33, 49}, {34, 30}, {34, 50}, {35, 44}, {37, 27}, {38, 
       24}, {39, 34}, {39, 53}, {40, 54}, {41, 29}, {41, 37}, {42, 39}, {42, 
       56}, {43, 26}, {44, 32}, {44, 41}, {45, 62}, {46, 44}, {47, 30}, {48, 
       37}, {48, 42}, {49, 46}, {49, 58}, {50, 65}, {51, 41}, {51, 48}, {52, 
       49}, {54, 38}, {55, 51}, {56, 66}, {57, 55}, {58, 57}, {59, 43}, {60, 
       48}, {61, 58}, {63, 51}, {64, 47}, {68, 60}, {1, 5}, {3, 7}, {4, 8}, {
       13, 14}, {14, 15}, {15, 31}, {27, 29}, {29, 32}, {31, 33}, {31, 52}, {
       32, 35}, {33, 36}, {35, 36}, {36, 46}, {38, 40}, {40, 43}, {43, 45}, {
       45, 47}, {47, 50}, {44, 55}, {46, 57}, {50, 53}, {52, 61}, {53, 56}, {
       54, 59}, {56, 60}, {59, 62}, {60, 63}, {61, 67}, {62, 64}, {63, 67}, {
       64, 65}, {65, 66}, {66, 68}, {67, 69}, {68, 69}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {46, 
         UndirectedEdge[42, 56], 
         UndirectedEdge[48, 60], 
         UndirectedEdge[60, 68], 30, 6, 47, 
         UndirectedEdge[5, 6], 11, 40, 
         UndirectedEdge[46, 49], 
         UndirectedEdge[34, 50], 21, 
         UndirectedEdge[23, 42], 
         UndirectedEdge[20, 30], 57, 48, 62, 8, 23, 37, 34, 
         UndirectedEdge[37, 48], 41, 22, 
         UndirectedEdge[28, 45], 7, 39, 20, 
         UndirectedEdge[62, 64], 
         UndirectedEdge[8, 20], 
         UndirectedEdge[50, 53], 
         UndirectedEdge[55, 57], 
         UndirectedEdge[12, 23], 
         UndirectedEdge[21, 34], 
         UndirectedEdge[41, 44], 25, 
         UndirectedEdge[39, 53], 10, 
         UndirectedEdge[10, 21], 
         UndirectedEdge[6, 7], 60, 56, 5, 
         UndirectedEdge[66, 68], 
         UndirectedEdge[5, 17], 19, 
         UndirectedEdge[25, 40], 55, 32, 50, 
         UndirectedEdge[32, 44], 
         UndirectedEdge[56, 66], 
         UndirectedEdge[7, 19], 
         UndirectedEdge[37, 41], 42, 17, 12, 
         UndirectedEdge[8, 10], 45, 28, 
         UndirectedEdge[11, 12], 66, 
         UndirectedEdge[47, 64], 
         UndirectedEdge[45, 62], 44, 
         UndirectedEdge[17, 25], 64, 
         UndirectedEdge[11, 22], 
         UndirectedEdge[19, 28], 68, 49, 53, 
         UndirectedEdge[30, 47], 
         UndirectedEdge[46, 57], 
         UndirectedEdge[44, 55], 
         UndirectedEdge[22, 39]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0f0zmwcAB/Ck3FVttDPVnXmtMBfzEmvrZXW+F29lWKUpDVo8SRBplkqE
x8TbqJbOXrxr6uWmo+4YYavqqXg5pZW+uEvRcnTpOLq269KzNM2s6w+fv+Dj
TIhZfCqFQsl759Mc9V9F7SfAaYlYrAtIwI8Gsv72hAgUyrpOUZ0O67dZ7uI7
PDy7aNba51yAuRMfCvccjYe1+llEKZ2PSkPz1dq2IkT1PT+wh0fgD5ef6FYF
BCa/Kfe3q4tCgimxT68VQtfSxo55Ksf4w8GDtyJ4aLT5+/iyrxwdTGlVlCof
lZE0RqFDMUZmgxr9WedhftdElWQrx446i7T+aTboMcMkx4zEkKH6lySmHA5+
nq8GmHxQa69qi6JTMF81HHxkOw+SX9dmzAeKcH1Xk50TLQ2ccseD7HUufLLn
lQweia6mGlnvdBbSd03tDhwk0W40bWksJxBa7vuoyUji5fAVi3DnPFgJzq1t
DMXhyhsPF82YBAzWIQZtP4lT/v126QV8mEW4R841lEEc5LO3fFyGee3vO5bD
uBCP99F6dBJsj/bcDcokcelfo5o2SiC2tnYp5mQ2wqk5dP8aAsbb291j7lIk
f+YSZ6VPR+m+tYImOg+rOwP1S35SvNnUr51OE6CLVTuW7yuFRZs4q6RbBuIL
NOi1XJw+qmLS2HxwDQEf2VK4qLlUsT/pZwl6ApemulUSsB07Pg6J5SNlQGBf
WS2Gnyhldombi7A/czI8BGlYMLixRtykmFiv8KYGiRESoCy1vCaFk95S19ub
gq0n2abfnxVA1Hkjf4Eg4RTffIDnIkZ8rl7T97UACgcyQRvPg9OTHq/DTTJc
K/tEPacQgdt5f6VZIkfN03thu93T4BqXsOnTlgrbQ4Eq68syXD9WcuGhKhtt
8FImzrJxT6jkeNWfgSFgw17DyUXcoxSip7MEK0Nzek3dEbzuLtzkMMMx/5XN
sccToSjZ+486eOgsNMZWE3ScRKZOfyfjRRnkjRYbVCMPIa3kzdUiIZarj880
fJAJ7wdrOxm/5UGjSDy/oGZBtmmVam9N4ELM89aXrETs8Oyqt/tciGalRAfH
WHz5Wtvs2ZCBgS2891+VAOvT3nWtsacwc5kx6JzLRW6MnrSZFOFGKN02YVEI
H8p3gowfMqCfnFQo3hfBLS7VziaYi3EOpm5tiXDTNVz3YCkb92nxK2RSFvod
mdGuHiL4XqwZKbQsRsfU5CglOglc6mGTtBUS+ebJ0rerAiwWe51RvXvkFivb
H39L4BXzhSc7ogLn1CORLckl+B9BfbVH
        "], 
       VertexCoordinates -> CompressedData["
1:eJxF031ME3cYB/CjLKTAKKAbUEABF0rZZM5SRI3C1QSlZoBlbAqWMV4mwoZC
3QCt2hIJL9WClrZDnGOK860EmQWHglI7mIYwioKtW5ELolCzjkZEYJ2pfvuX
f1wun3yf5+733O93YTl7U7+mEQSRgMt5nzeMSHnMdJ1Q4Voac5JLGqrz3Tlw
/ITVyIEzulr1u+ChYEUGG66ZM5sqYLVlpZYOW2vbPONh3+1D40yYs2FeuwO+
PWnudoP5Sd2Nzv7iTWE0BqwyUkuk8Py1kx7PG7lk7/POfDUcTPbaKFiXvlXd
AO+ki7bpYXfq/6AO+NzF6IXjcBwrtelP2NDy+HUdbB7fw2cEput6pnWiSphK
HfIXw+Iz0n+/gn3v3OiSw+52z405sCZz6f1A9DM/ryqJhfVnVyRHOOev179k
wwvPygdJOE1zpc0D/qC65tRnsKXqvHXuB3yPypyPdsPEpaQfx+B65sVjEliZ
/yWjG37Y5chshqOD8hQnYLrm18pOuE4ZpSiFn47k7nSDBy6dzi6EfayLq3zg
0YrWC1/AcULRWCS8MYHhGwsfOZeiscAqT5UkCj5wV+nt3C+q8HRXMNynfYfp
gvkkOldXOnz5xpY125GbqmQlDjWXvBfStF+E/Bd+pGUeJo6+sofBmkGlkYKr
OWn9afC68FDWBDyV0UEUoZ+31fr3OLz8tlwZhdzb66jABAuOFATEwdf9okgz
/F/B2CkK9R/XDSSNwom/R+57EZCuW83zovTwhyXLJmXIGwUrf+uAU0Iq4l8h
p42OXu2E2XP2hkXkWTZH+RV418KT7DNwfRGzWA3/7D8jDHCeJyxDAR8bG3mw
DO+/L9vLPgg/WXr1xRrkszN3dxTBPbR7DRzk37c7DNmwJ2NVTSJyt57BdgH8
xwUiwABLarz8kmET17Y2AfX+RnnZFnj91IbcLOQTrJaBT2CPWwV2K5wYi5MC
L6n+NjQT9XmWh7xwmGEq9BYjX+3uP0uH33dJtj3GfMPiXDoB524uF/ugvjmO
JllU4f9w6d2sQH2g+RueDW4mZCfCkffficibhvsVooV1sHVbEt8I76kVFjvP
iy209PwwrIx+r6wHZvUJUztVb9cfbMgnb8I3mTFZ4cgjy5onW1Rv92f9dwfG
5fBf2mlODGy3Sv+RwcsdTa0CuPzQVGsKLD1M9e6Hz7KHI2aVXDKv7d3LXnh+
0Esf+QysZfktOvcz5HrMs2swm2/69Bb8qPGn9r4GLvkGIuD0cw==
        "]}]], 
    Typeset`boxes, Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$12", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$42", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$43", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$37", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$45", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$32", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$34", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$37", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$37", Automatic, Center], 
          DynamicLocation["VertexID$48", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$53", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$43", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$41", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$42", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$45", Automatic, Center], 
          DynamicLocation["VertexID$62", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$64", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$53", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$50", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$56", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$66", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$60", Automatic, Center], 
          DynamicLocation["VertexID$68", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$62", Automatic, Center], 
          DynamicLocation["VertexID$64", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$66", Automatic, Center], 
          DynamicLocation["VertexID$68", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394623, 20.598472}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393679, 20.598432}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.396897, 20.598272}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39855, 20.597789}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394473, 20.598051}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396039, 20.597759}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396887, 20.59787}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398442, 20.597428}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399805, 20.597167}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399633, 20.596715}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401618, 20.595962}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403174, 20.595932}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407122, 20.595771}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413763, 20.595229}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.4149, 20.595249}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392262, 20.594596}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393024, 20.594375}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39458, 20.593913}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395728, 20.593581}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396983, 20.59321}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39841, 20.592698}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400352, 20.592065}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401704, 20.591653}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391013, 20.591467}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391613, 20.591166}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39318, 20.590684}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404826, 20.590538}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394596, 20.590282}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.406317, 20.589966}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395948, 20.58982}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413237, 20.589705}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408324, 20.589353}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411424, 20.589373}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397278, 20.589338}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409032, 20.589223}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41004, 20.589283}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404273, 20.589177}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389859, 20.588911}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399102, 20.588695}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.390396, 20.58871}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405797, 20.588615}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400475, 20.588233}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392112, 20.588168}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408071, 20.587832}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393979, 20.587606}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409337, 20.58743}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395052, 20.587184}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403265, 20.587129}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410475, 20.587028}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396447, 20.586682}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.405089, 20.586466}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411934, 20.586506}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398136, 20.586074}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.388851, 20.585938}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407256, 20.585823}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399553, 20.585682}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408543, 20.585562}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409788, 20.585301}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391018, 20.585215}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402128, 20.584798}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410475, 20.584919}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392906, 20.584532}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404273, 20.584196}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393872, 20.584151}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39561, 20.583247}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398056, 20.58182}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407084, 20.581765}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399107, 20.580916}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40224, 20.57722}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.32094401737027`}, {-111.51894401734512`, 97.38769513245455}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["11559.88054215697`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Quer\[EAcute]taro Downtown Dataset - GC-PO Solution Graph", \
"Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69}, {
      Null, {{1, 2}, {2, 5}, {3, 1}, {4, 3}, {5, 6}, {5, 17}, {6, 7}, {7, 
       8}, {7, 19}, {8, 10}, {9, 4}, {9, 10}, {10, 11}, {10, 21}, {11, 12}, {
       11, 22}, {12, 13}, {12, 23}, {13, 9}, {16, 2}, {17, 16}, {17, 25}, {18,
        6}, {18, 17}, {19, 18}, {19, 28}, {20, 8}, {20, 19}, {21, 20}, {21, 
       34}, {22, 21}, {22, 39}, {23, 22}, {23, 42}, {24, 16}, {25, 24}, {25, 
       40}, {26, 18}, {26, 25}, {27, 13}, {27, 23}, {28, 26}, {28, 45}, {30, 
       20}, {30, 28}, {33, 49}, {34, 30}, {34, 50}, {35, 44}, {37, 27}, {38, 
       24}, {39, 34}, {39, 53}, {40, 54}, {41, 29}, {41, 37}, {42, 39}, {42, 
       56}, {43, 26}, {44, 32}, {44, 41}, {45, 62}, {46, 44}, {47, 30}, {48, 
       37}, {48, 42}, {49, 46}, {49, 58}, {50, 65}, {51, 41}, {51, 48}, {52, 
       49}, {54, 38}, {55, 51}, {56, 66}, {57, 55}, {58, 57}, {59, 43}, {60, 
       48}, {61, 58}, {63, 51}, {64, 47}, {68, 60}, {1, 5}, {3, 7}, {4, 8}, {
       13, 14}, {14, 15}, {15, 31}, {27, 29}, {29, 32}, {31, 33}, {31, 52}, {
       32, 35}, {33, 36}, {35, 36}, {36, 46}, {38, 40}, {40, 43}, {43, 45}, {
       45, 47}, {47, 50}, {44, 55}, {46, 57}, {50, 53}, {52, 61}, {53, 56}, {
       54, 59}, {56, 60}, {59, 62}, {60, 63}, {61, 67}, {62, 64}, {63, 67}, {
       64, 65}, {65, 66}, {66, 68}, {67, 69}, {68, 69}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {46, 
         UndirectedEdge[48, 60], 
         UndirectedEdge[19, 20], 30, 47, 29, 11, 40, 
         UndirectedEdge[46, 49], 
         UndirectedEdge[17, 18], 21, 48, 8, 
         UndirectedEdge[43, 45], 23, 37, 
         UndirectedEdge[1, 3], 34, 
         UndirectedEdge[37, 48], 
         UndirectedEdge[27, 29], 41, 22, 7, 39, 
         UndirectedEdge[44, 46], 20, 1, 43, 
         UndirectedEdge[10, 11], 
         UndirectedEdge[34, 39], 26, 
         UndirectedEdge[45, 47], 4, 
         UndirectedEdge[39, 42], 
         UndirectedEdge[22, 23], 
         UndirectedEdge[41, 44], 25, 
         UndirectedEdge[26, 28], 10, 
         UndirectedEdge[1, 5], 51, 60, 5, 
         UndirectedEdge[42, 48], 
         UndirectedEdge[4, 8], 
         UndirectedEdge[5, 17], 
         UndirectedEdge[25, 40], 19, 50, 
         UndirectedEdge[30, 34], 42, 17, 12, 
         UndirectedEdge[48, 51], 
         UndirectedEdge[27, 37], 
         UndirectedEdge[25, 26], 27, 
         UndirectedEdge[8, 10], 
         UndirectedEdge[3, 7], 45, 28, 
         UndirectedEdge[20, 21], 
         UndirectedEdge[11, 12], 
         UndirectedEdge[28, 30], 
         UndirectedEdge[47, 50], 44, 
         UndirectedEdge[18, 19], 3, 18, 
         UndirectedEdge[23, 27], 
         UndirectedEdge[21, 22], 
         UndirectedEdge[3, 4], 49, 
         UndirectedEdge[29, 41], 
         UndirectedEdge[40, 43]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0f0zmwcAB/Ck3FVttDPVnXmtMBfzEmvrZXW+F29lWKUpDVo8SRBplkqE
x8TbqJbOXrxr6uWmo+4YYavqqXg5pZW+uEvRcnTpOLq269KzNM2s6w+fv+Dj
TIhZfCqFQsl759Mc9V9F7SfAaYlYrAtIwI8Gsv72hAgUyrpOUZ0O67dZ7uI7
PDy7aNba51yAuRMfCvccjYe1+llEKZ2PSkPz1dq2IkT1PT+wh0fgD5ef6FYF
BCa/Kfe3q4tCgimxT68VQtfSxo55Ksf4w8GDtyJ4aLT5+/iyrxwdTGlVlCof
lZE0RqFDMUZmgxr9WedhftdElWQrx446i7T+aTboMcMkx4zEkKH6lySmHA5+
nq8GmHxQa69qi6JTMF81HHxkOw+SX9dmzAeKcH1Xk50TLQ2ccseD7HUufLLn
lQweia6mGlnvdBbSd03tDhwk0W40bWksJxBa7vuoyUji5fAVi3DnPFgJzq1t
DMXhyhsPF82YBAzWIQZtP4lT/v126QV8mEW4R841lEEc5LO3fFyGee3vO5bD
uBCP99F6dBJsj/bcDcokcelfo5o2SiC2tnYp5mQ2wqk5dP8aAsbb291j7lIk
f+YSZ6VPR+m+tYImOg+rOwP1S35SvNnUr51OE6CLVTuW7yuFRZs4q6RbBuIL
NOi1XJw+qmLS2HxwDQEf2VK4qLlUsT/pZwl6ApemulUSsB07Pg6J5SNlQGBf
WS2Gnyhldombi7A/czI8BGlYMLixRtykmFiv8KYGiRESoCy1vCaFk95S19ub
gq0n2abfnxVA1Hkjf4Eg4RTffIDnIkZ8rl7T97UACgcyQRvPg9OTHq/DTTJc
K/tEPacQgdt5f6VZIkfN03thu93T4BqXsOnTlgrbQ4Eq68syXD9WcuGhKhtt
8FImzrJxT6jkeNWfgSFgw17DyUXcoxSip7MEK0Nzek3dEbzuLtzkMMMx/5XN
sccToSjZ+486eOgsNMZWE3ScRKZOfyfjRRnkjRYbVCMPIa3kzdUiIZarj880
fJAJ7wdrOxm/5UGjSDy/oGZBtmmVam9N4ELM89aXrETs8Oyqt/tciGalRAfH
WHz5Wtvs2ZCBgS2891+VAOvT3nWtsacwc5kx6JzLRW6MnrSZFOFGKN02YVEI
H8p3gowfMqCfnFQo3hfBLS7VziaYi3EOpm5tiXDTNVz3YCkb92nxK2RSFvod
mdGuHiL4XqwZKbQsRsfU5CglOglc6mGTtBUS+ebJ0rerAiwWe51RvXvkFivb
H39L4BXzhSc7ogLn1CORLckl+B9BfbVH
        "], 
       VertexCoordinates -> CompressedData["
1:eJxF031ME3cYB/CjLKTAKKAbUEABF0rZZM5SRI3C1QSlZoBlbAqWMV4mwoZC
3QCt2hIJL9WClrZDnGOK860EmQWHglI7mIYwioKtW5ELolCzjkZEYJ2pfvuX
f1wun3yf5+733O93YTl7U7+mEQSRgMt5nzeMSHnMdJ1Q4Voac5JLGqrz3Tlw
/ITVyIEzulr1u+ChYEUGG66ZM5sqYLVlpZYOW2vbPONh3+1D40yYs2FeuwO+
PWnudoP5Sd2Nzv7iTWE0BqwyUkuk8Py1kx7PG7lk7/POfDUcTPbaKFiXvlXd
AO+ki7bpYXfq/6AO+NzF6IXjcBwrtelP2NDy+HUdbB7fw2cEput6pnWiSphK
HfIXw+Iz0n+/gn3v3OiSw+52z405sCZz6f1A9DM/ryqJhfVnVyRHOOev179k
wwvPygdJOE1zpc0D/qC65tRnsKXqvHXuB3yPypyPdsPEpaQfx+B65sVjEliZ
/yWjG37Y5chshqOD8hQnYLrm18pOuE4ZpSiFn47k7nSDBy6dzi6EfayLq3zg
0YrWC1/AcULRWCS8MYHhGwsfOZeiscAqT5UkCj5wV+nt3C+q8HRXMNynfYfp
gvkkOldXOnz5xpY125GbqmQlDjWXvBfStF+E/Bd+pGUeJo6+sofBmkGlkYKr
OWn9afC68FDWBDyV0UEUoZ+31fr3OLz8tlwZhdzb66jABAuOFATEwdf9okgz
/F/B2CkK9R/XDSSNwom/R+57EZCuW83zovTwhyXLJmXIGwUrf+uAU0Iq4l8h
p42OXu2E2XP2hkXkWTZH+RV418KT7DNwfRGzWA3/7D8jDHCeJyxDAR8bG3mw
DO+/L9vLPgg/WXr1xRrkszN3dxTBPbR7DRzk37c7DNmwJ2NVTSJyt57BdgH8
xwUiwABLarz8kmET17Y2AfX+RnnZFnj91IbcLOQTrJaBT2CPWwV2K5wYi5MC
L6n+NjQT9XmWh7xwmGEq9BYjX+3uP0uH33dJtj3GfMPiXDoB524uF/ugvjmO
JllU4f9w6d2sQH2g+RueDW4mZCfCkffficibhvsVooV1sHVbEt8I76kVFjvP
iy209PwwrIx+r6wHZvUJUztVb9cfbMgnb8I3mTFZ4cgjy5onW1Rv92f9dwfG
5fBf2mlODGy3Sv+RwcsdTa0CuPzQVGsKLD1M9e6Hz7KHI2aVXDKv7d3LXnh+
0Esf+QysZfktOvcz5HrMs2swm2/69Bb8qPGn9r4GLvkGIuD0cw==
        "]}]], 
    Typeset`boxes, Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$18", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$18", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$43", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$37", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$34", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$37", Automatic, Center], 
          DynamicLocation["VertexID$48", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$42", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$41", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$42", Automatic, Center], 
          DynamicLocation["VertexID$48", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$56", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$45", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$46", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$45", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$50", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$50", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$56", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394623, 20.598472}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393679, 20.598432}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396897, 20.598272}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39855, 20.597789}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394473, 20.598051}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.396039, 20.597759}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396887, 20.59787}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398442, 20.597428}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399805, 20.597167}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399633, 20.596715}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401618, 20.595962}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403174, 20.595932}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407122, 20.595771}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413763, 20.595229}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.4149, 20.595249}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392262, 20.594596}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393024, 20.594375}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39458, 20.593913}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395728, 20.593581}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396983, 20.59321}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39841, 20.592698}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400352, 20.592065}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401704, 20.591653}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391013, 20.591467}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391613, 20.591166}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39318, 20.590684}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404826, 20.590538}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394596, 20.590282}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406317, 20.589966}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395948, 20.58982}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413237, 20.589705}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.408324, 20.589353}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411424, 20.589373}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397278, 20.589338}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409032, 20.589223}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41004, 20.589283}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404273, 20.589177}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389859, 20.588911}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399102, 20.588695}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.390396, 20.58871}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405797, 20.588615}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400475, 20.588233}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392112, 20.588168}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408071, 20.587832}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393979, 20.587606}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409337, 20.58743}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395052, 20.587184}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403265, 20.587129}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410475, 20.587028}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396447, 20.586682}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405089, 20.586466}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411934, 20.586506}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398136, 20.586074}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.388851, 20.585938}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407256, 20.585823}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399553, 20.585682}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.408543, 20.585562}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409788, 20.585301}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391018, 20.585215}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402128, 20.584798}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410475, 20.584919}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392906, 20.584532}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404273, 20.584196}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393872, 20.584151}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39561, 20.583247}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398056, 20.58182}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407084, 20.581765}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399107, 20.580916}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40224, 20.57722}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.32094401737027`}, {-111.51894401734512`, 97.38769513245455}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["6831.901319372017`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Undirected Quer\[EAcute]taro Downtown Dataset - Shared Vertex Solution Graph\
\>", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69}, {
      Null, {{1, 2}, {2, 5}, {3, 1}, {4, 3}, {5, 6}, {5, 17}, {6, 7}, {7, 
       8}, {7, 19}, {8, 10}, {9, 4}, {9, 10}, {10, 11}, {10, 21}, {11, 12}, {
       11, 22}, {12, 13}, {12, 23}, {13, 9}, {16, 2}, {17, 16}, {17, 25}, {18,
        6}, {18, 17}, {19, 18}, {19, 28}, {20, 8}, {20, 19}, {21, 20}, {21, 
       34}, {22, 21}, {22, 39}, {23, 22}, {23, 42}, {24, 16}, {25, 24}, {25, 
       40}, {26, 18}, {26, 25}, {27, 13}, {27, 23}, {28, 26}, {28, 45}, {30, 
       20}, {30, 28}, {33, 49}, {34, 30}, {34, 50}, {35, 44}, {37, 27}, {38, 
       24}, {39, 34}, {39, 53}, {40, 54}, {41, 29}, {41, 37}, {42, 39}, {42, 
       56}, {43, 26}, {44, 32}, {44, 41}, {45, 62}, {46, 44}, {47, 30}, {48, 
       37}, {48, 42}, {49, 46}, {49, 58}, {50, 65}, {51, 41}, {51, 48}, {52, 
       49}, {54, 38}, {55, 51}, {56, 66}, {57, 55}, {58, 57}, {59, 43}, {60, 
       48}, {61, 58}, {63, 51}, {64, 47}, {68, 60}, {1, 5}, {3, 7}, {4, 8}, {
       13, 14}, {14, 15}, {15, 31}, {27, 29}, {29, 32}, {31, 33}, {31, 52}, {
       32, 35}, {33, 36}, {35, 36}, {36, 46}, {38, 40}, {40, 43}, {43, 45}, {
       45, 47}, {47, 50}, {44, 55}, {46, 57}, {50, 53}, {52, 61}, {53, 56}, {
       54, 59}, {56, 60}, {59, 62}, {60, 63}, {61, 67}, {62, 64}, {63, 67}, {
       64, 65}, {65, 66}, {66, 68}, {67, 69}, {68, 69}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {22, 17, 47, 
         UndirectedEdge[18, 19], 41, 40, 
         UndirectedEdge[6, 7], 21, 
         UndirectedEdge[19, 20], 
         UndirectedEdge[17, 18], 44, 43, 
         UndirectedEdge[37, 48], 
         UndirectedEdge[21, 22], 
         UndirectedEdge[20, 30], 49, 
         UndirectedEdge[48, 51], 
         UndirectedEdge[46, 49], 37, 
         UndirectedEdge[50, 53], 
         UndirectedEdge[30, 47], 
         UndirectedEdge[7, 8], 
         UndirectedEdge[45, 47], 51, 53, 5, 48, 10, 50, 
         UndirectedEdge[47, 50], 
         UndirectedEdge[5, 6], 
         UndirectedEdge[43, 45], 
         UndirectedEdge[37, 41], 6, 18, 56, 60, 
         UndirectedEdge[53, 56], 
         UndirectedEdge[5, 17], 
         UndirectedEdge[44, 46], 
         UndirectedEdge[20, 21], 27, 7, 
         UndirectedEdge[48, 60], 
         UndirectedEdge[40, 43], 46, 8, 20, 45, 
         UndirectedEdge[8, 10], 19, 
         UndirectedEdge[41, 44], 
         UndirectedEdge[27, 37], 30, 
         UndirectedEdge[56, 60]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0f0zmwcAB/Ck3FVttDPVnXmtMBfzEmvrZXW+F29lWKUpDVo8SRBplkqE
x8TbqJbOXrxr6uWmo+4YYavqqXg5pZW+uEvRcnTpOLq269KzNM2s6w+fv+Dj
TIhZfCqFQsl759Mc9V9F7SfAaYlYrAtIwI8Gsv72hAgUyrpOUZ0O67dZ7uI7
PDy7aNba51yAuRMfCvccjYe1+llEKZ2PSkPz1dq2IkT1PT+wh0fgD5ef6FYF
BCa/Kfe3q4tCgimxT68VQtfSxo55Ksf4w8GDtyJ4aLT5+/iyrxwdTGlVlCof
lZE0RqFDMUZmgxr9WedhftdElWQrx446i7T+aTboMcMkx4zEkKH6lySmHA5+
nq8GmHxQa69qi6JTMF81HHxkOw+SX9dmzAeKcH1Xk50TLQ2ccseD7HUufLLn
lQweia6mGlnvdBbSd03tDhwk0W40bWksJxBa7vuoyUji5fAVi3DnPFgJzq1t
DMXhyhsPF82YBAzWIQZtP4lT/v126QV8mEW4R841lEEc5LO3fFyGee3vO5bD
uBCP99F6dBJsj/bcDcokcelfo5o2SiC2tnYp5mQ2wqk5dP8aAsbb291j7lIk
f+YSZ6VPR+m+tYImOg+rOwP1S35SvNnUr51OE6CLVTuW7yuFRZs4q6RbBuIL
NOi1XJw+qmLS2HxwDQEf2VK4qLlUsT/pZwl6ApemulUSsB07Pg6J5SNlQGBf
WS2Gnyhldombi7A/czI8BGlYMLixRtykmFiv8KYGiRESoCy1vCaFk95S19ub
gq0n2abfnxVA1Hkjf4Eg4RTffIDnIkZ8rl7T97UACgcyQRvPg9OTHq/DTTJc
K/tEPacQgdt5f6VZIkfN03thu93T4BqXsOnTlgrbQ4Eq68syXD9WcuGhKhtt
8FImzrJxT6jkeNWfgSFgw17DyUXcoxSip7MEK0Nzek3dEbzuLtzkMMMx/5XN
sccToSjZ+486eOgsNMZWE3ScRKZOfyfjRRnkjRYbVCMPIa3kzdUiIZarj880
fJAJ7wdrOxm/5UGjSDy/oGZBtmmVam9N4ELM89aXrETs8Oyqt/tciGalRAfH
WHz5Wtvs2ZCBgS2891+VAOvT3nWtsacwc5kx6JzLRW6MnrSZFOFGKN02YVEI
H8p3gowfMqCfnFQo3hfBLS7VziaYi3EOpm5tiXDTNVz3YCkb92nxK2RSFvod
mdGuHiL4XqwZKbQsRsfU5CglOglc6mGTtBUS+ebJ0rerAiwWe51RvXvkFivb
H39L4BXzhSc7ogLn1CORLckl+B9BfbVH
        "], 
       VertexCoordinates -> CompressedData["
1:eJxF031ME3cYB/CjLKTAKKAbUEABF0rZZM5SRI3C1QSlZoBlbAqWMV4mwoZC
3QCt2hIJL9WClrZDnGOK860EmQWHglI7mIYwioKtW5ELolCzjkZEYJ2pfvuX
f1wun3yf5+733O93YTl7U7+mEQSRgMt5nzeMSHnMdJ1Q4Voac5JLGqrz3Tlw
/ITVyIEzulr1u+ChYEUGG66ZM5sqYLVlpZYOW2vbPONh3+1D40yYs2FeuwO+
PWnudoP5Sd2Nzv7iTWE0BqwyUkuk8Py1kx7PG7lk7/POfDUcTPbaKFiXvlXd
AO+ki7bpYXfq/6AO+NzF6IXjcBwrtelP2NDy+HUdbB7fw2cEput6pnWiSphK
HfIXw+Iz0n+/gn3v3OiSw+52z405sCZz6f1A9DM/ryqJhfVnVyRHOOev179k
wwvPygdJOE1zpc0D/qC65tRnsKXqvHXuB3yPypyPdsPEpaQfx+B65sVjEliZ
/yWjG37Y5chshqOD8hQnYLrm18pOuE4ZpSiFn47k7nSDBy6dzi6EfayLq3zg
0YrWC1/AcULRWCS8MYHhGwsfOZeiscAqT5UkCj5wV+nt3C+q8HRXMNynfYfp
gvkkOldXOnz5xpY125GbqmQlDjWXvBfStF+E/Bd+pGUeJo6+sofBmkGlkYKr
OWn9afC68FDWBDyV0UEUoZ+31fr3OLz8tlwZhdzb66jABAuOFATEwdf9okgz
/F/B2CkK9R/XDSSNwom/R+57EZCuW83zovTwhyXLJmXIGwUrf+uAU0Iq4l8h
p42OXu2E2XP2hkXkWTZH+RV418KT7DNwfRGzWA3/7D8jDHCeJyxDAR8bG3mw
DO+/L9vLPgg/WXr1xRrkszN3dxTBPbR7DRzk37c7DNmwJ2NVTSJyt57BdgH8
xwUiwABLarz8kmET17Y2AfX+RnnZFnj91IbcLOQTrJaBT2CPWwV2K5wYi5MC
L6n+NjQT9XmWh7xwmGEq9BYjX+3uP0uH33dJtj3GfMPiXDoB524uF/ugvjmO
JllU4f9w6d2sQH2g+RueDW4mZCfCkffficibhvsVooV1sHVbEt8I76kVFjvP
iy209PwwrIx+r6wHZvUJUztVb9cfbMgnb8I3mTFZ4cgjy5onW1Rv92f9dwfG
5fBf2mlODGy3Sv+RwcsdTa0CuPzQVGsKLD1M9e6Hz7KHI2aVXDKv7d3LXnh+
0Esf+QysZfktOvcz5HrMs2swm2/69Bb8qPGn9r4GLvkGIuD0cw==
        "]}]], 
    Typeset`boxes, Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$18", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$18", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$43", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$37", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$37", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$37", Automatic, Center], 
          DynamicLocation["VertexID$48", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$41", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$56", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$45", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$46", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$45", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$53", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$50", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$53", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394623, 20.598472}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393679, 20.598432}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.396897, 20.598272}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39855, 20.597789}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394473, 20.598051}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396039, 20.597759}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396887, 20.59787}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398442, 20.597428}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399805, 20.597167}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399633, 20.596715}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.401618, 20.595962}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.403174, 20.595932}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407122, 20.595771}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413763, 20.595229}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.4149, 20.595249}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392262, 20.594596}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393024, 20.594375}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39458, 20.593913}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395728, 20.593581}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396983, 20.59321}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39841, 20.592698}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.400352, 20.592065}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.401704, 20.591653}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391013, 20.591467}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391613, 20.591166}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39318, 20.590684}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404826, 20.590538}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394596, 20.590282}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.406317, 20.589966}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395948, 20.58982}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413237, 20.589705}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.408324, 20.589353}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411424, 20.589373}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.397278, 20.589338}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409032, 20.589223}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41004, 20.589283}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404273, 20.589177}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389859, 20.588911}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399102, 20.588695}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.390396, 20.58871}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405797, 20.588615}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.400475, 20.588233}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392112, 20.588168}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408071, 20.587832}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393979, 20.587606}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409337, 20.58743}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395052, 20.587184}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403265, 20.587129}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410475, 20.587028}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396447, 20.586682}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405089, 20.586466}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.411934, 20.586506}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398136, 20.586074}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.388851, 20.585938}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407256, 20.585823}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399553, 20.585682}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.408543, 20.585562}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409788, 20.585301}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391018, 20.585215}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402128, 20.584798}, 0.000037680897016661], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410475, 20.584919}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392906, 20.584532}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404273, 20.584196}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393872, 20.584151}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39561, 20.583247}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398056, 20.58182}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407084, 20.581765}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399107, 20.580916}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40224, 20.57722}, 0.000037680897016661], 
          "DynamicName", BoxID -> "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{19.120000000281035`, 
       459.9072405160405}, {-188.26204051577952`, 173.34642051050275`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15,
  ImageSize->{621.333333333333, Automatic}]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["5395.308949135353`"], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1319, 744},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
CellContext->Notebook,
TrackCellChangeTimes->False,
Magnification:>0.75 Inherited,
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 104, 3, 72, "Title"],
Cell[1593, 40, 310, 5, 56, "Subsection"],
Cell[CellGroupData[{
Cell[1928, 49, 77, 0, 41, "Section"],
Cell[2008, 51, 41615, 925, 256, "Output"],
Cell[CellGroupData[{
Cell[43648, 980, 34, 0, 35, "Subsection"],
Cell[43685, 982, 46, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[43780, 988, 88, 1, 51, "Section"],
Cell[43871, 991, 90045, 2173, 237, "Output"],
Cell[CellGroupData[{
Cell[133941, 3168, 35, 0, 35, "Subsection"],
Cell[133979, 3170, 46, 0, 24, "Output"],
Cell[134028, 3172, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[134122, 3178, 89, 1, 51, "Section"],
Cell[134214, 3181, 75674, 1783, 237, "Output"],
Cell[CellGroupData[{
Cell[209913, 4968, 35, 0, 35, "Subsection"],
Cell[209951, 4970, 45, 0, 24, "Output"],
Cell[209999, 4972, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[210093, 4978, 89, 1, 51, "Section"],
Cell[210185, 4981, 69421, 1690, 313, "Output"],
Cell[CellGroupData[{
Cell[279631, 6675, 34, 0, 35, "Subsection"],
Cell[279668, 6677, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[279762, 6683, 89, 1, 51, "Section"],
Cell[279854, 6686, 70978, 1741, 237, "Output"],
Cell[CellGroupData[{
Cell[350857, 8431, 34, 0, 35, "Subsection"],
Cell[350894, 8433, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[350988, 8439, 89, 1, 51, "Section"],
Cell[351080, 8442, 70229, 1716, 237, "Output"],
Cell[CellGroupData[{
Cell[421334, 10162, 34, 0, 35, "Subsection"],
Cell[421371, 10164, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[421465, 10170, 89, 1, 51, "Section"],
Cell[421557, 10173, 69822, 1703, 237, "Output"],
Cell[CellGroupData[{
Cell[491404, 11880, 34, 0, 35, "Subsection"],
Cell[491441, 11882, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[491535, 11888, 103, 2, 51, "Section"],
Cell[491641, 11892, 65862, 1574, 398, "Output"],
Cell[CellGroupData[{
Cell[557528, 13470, 34, 0, 35, "Subsection"],
Cell[557565, 13472, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature nxT3HdMVQ8pKUBKUlMZikpLx *)
