(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[     49399,       1413]
NotebookOptionsPosition[     47966,       1345]
NotebookOutlinePosition[     48430,       1365]
CellTagsIndexPosition[     48387,       1362]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["MLCP Directed Solution Results", "Title"],

Cell[CellGroupData[{

Cell["\<\
The directed graphs are compared according to edge weights in a simialr \
fashion to the meshes. These results have already been generated and saved.\
\>", "Subsection"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetDirectory", " ", "@", " ", 
   RowBox[{"NotebookDirectory", "[", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"<<", "\"\<file_management.m\>\""}], "\[IndentingNewLine]", 
 RowBox[{"<<", "\"\<auxiliary_functions.m\>\""}], "\[IndentingNewLine]", 
 RowBox[{"<<", "\"\<graph_operations.m\>\""}], "\[IndentingNewLine]", 
 RowBox[{"<<", "\"\<mlcp_operations.m\>\""}]}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Directed Sample Solution Weights ", "Section"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Sample Directed Base Graph\>\"", ",", 
   "\"\<sample_directed_base_graph.mx\>\""}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleBase", "=", 
   RowBox[{"getTotalGraphWeight", "@", "sampleDirectedBaseGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Sample MCA Solution Graph\>\"", ",", 
   "\"\<sample_mca_solution_graph.mx\>\""}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Sample Reduced MCA Solution Graph\>\"", ",", 
   "\"\<sample_reduced_mca_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleMCA", "=", 
   RowBox[{"getTotalGraphWeight", "@", "sampleMCASolutionGraph"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleReducedMCA", "=", 
   RowBox[{"getTotalGraphWeight", "@", "sampleReducedMCASolutionGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Sample Directed GSHT Solution Graph\>\"", ",", 
   "\"\<sample_directed_gsht_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Sample Directed Reduced GSHT Solution Graph\>\"", ",", 
   "\"\<sample_directed_reduced_gsht_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleGSHT", "=", 
   RowBox[{"getTotalGraphWeight", "@", "sampleDirectedGSHTSolutionGraph"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleReducedGSHT", "=", 
   RowBox[{"getTotalGraphWeight", "@", "sampleReducedGSHTSolutionGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Sample Directed GSHP Solution Graph\>\"", ",", 
   "\"\<sample_directed_gshp_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleGSHP", "=", 
   RowBox[{"getTotalGraphWeight", "@", "sampleDirectedGSHPSolutionGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Sample Directed GSHC Solution Graph\>\"", ",", 
   "\"\<sample_directed_gshc_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleGSHC", "=", 
   RowBox[{"getTotalGraphWeight", "@", "sampleDirectedGSHCSolutionGraph"}]}], 
  ";"}]}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Directed Quer\[EAcute]taro Downtown Solution Weights", "Section"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Downtown Directed Base Graph\>\"", ",", 
   "\"\<downtown_directed_base_graph.mx\>\""}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownBase", "=", 
   RowBox[{"getTotalGraphWeight", "@", "downtownDirectedBaseGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Downtown MCA Solution Graph\>\"", ",", 
   "\"\<downtown_mca_solution_graph.mx\>\""}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Downtown Reduced MCA Solution Graph\>\"", ",", 
   "\"\<downtown_reduced_mca_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownMCA", "=", 
   RowBox[{"getTotalGraphWeight", "@", "downtownMCASolutionGraph"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownReducedMCA", "=", 
   RowBox[{"getTotalGraphWeight", "@", "downtownReducedMCASolutionGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Downtown Directed GSHT Solution Graph\>\"", ",", 
   "\"\<downtown_directed_gsht_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Downtown Directed Reduced GSHT Solution Graph\>\"", ",", 
   "\"\<downtown_directed_reduced_gsht_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownGSHT", "=", 
   RowBox[{
   "getTotalGraphWeight", "@", "downtownDirectedGSHTSolutionGraph"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownReducedGSHT", "=", 
   RowBox[{"getTotalGraphWeight", "@", "downtownReducedGSHTSolutionGraph"}]}],
   ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Downtown Directed GSHP Solution Graph\>\"", ",", 
   "\"\<downtown_directed_gshp_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownGSHP", "=", 
   RowBox[{
   "getTotalGraphWeight", "@", "downtownDirectedGSHPSolutionGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Downtown Directed GSHC Solution Graph\>\"", ",", 
   "\"\<downtown_directed_gshc_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownGSHC", "=", 
   RowBox[{
   "getTotalGraphWeight", "@", "downtownDirectedGSHCSolutionGraph"}]}], 
  ";"}]}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Directed Quer\[EAcute]taro Urban Zone Solution Weights", "Section"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Urban Zone Directed Base Graph\>\"", ",", 
   "\"\<urban_zone_directed_base_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneBase", "=", 
   RowBox[{"getTotalGraphWeight", "@", "urbanZoneDirectedBaseGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Urban Zone MCA Solution Graph\>\"", ",", 
   "\"\<urban_zone_mca_solution_graph.mx\>\""}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Urban Zone Reduced MCA Solution Graph\>\"", ",", 
   "\"\<urban_zone_reduced_mca_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneMCA", "=", 
   RowBox[{"getTotalGraphWeight", "@", "urbanZoneMCASolutionGraph"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneReducedMCA", "=", 
   RowBox[{"getTotalGraphWeight", "@", "urbanZoneReducedMCASolutionGraph"}]}],
   ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Urban Zone Directed GSHT Solution Graph\>\"", ",", 
   "\"\<urban_zone_directed_gsht_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Urban Zone Directed Reduced GSHT Solution Graph\>\"", ",", 
   "\"\<urban_zone_directed_reduced_gsht_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneGSHT", "=", 
   RowBox[{
   "getTotalGraphWeight", "@", "urbanZoneDirectedGSHTSolutionGraph"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneReducedGSHT", "=", 
   RowBox[{
   "getTotalGraphWeight", "@", "urbanZoneReducedGSHTSolutionGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Urban Zone Directed GSHP Solution Graph\>\"", ",", 
   "\"\<urban_zone_directed_gshp_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneGSHP", "=", 
   RowBox[{
   "getTotalGraphWeight", "@", "urbanZoneDirectedGSHPSolutionGraph"}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{"thesisImport", "[", 
  RowBox[{
  "\"\<../Urban Zone Directed GSHC Solution Graph\>\"", ",", 
   "\"\<urban_zone_directed_gshc_solution_graph.mx\>\""}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneGSHC", "=", 
   RowBox[{
   "getTotalGraphWeight", "@", "urbanZoneDirectedGSHCSolutionGraph"}]}], 
  ";"}]}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Timing Comparisons", "Section"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"sampleMCATiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{"\"\<../Timing\>\"", ",", "\"\<sample_mca_timing.csv\>\""}], 
     "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleReducedMCATiming", "=", 
   RowBox[{
    RowBox[{"Flatten", "@", 
     RowBox[{"thesisImport", "[", 
      RowBox[{
      "\"\<../Timing\>\"", ",", "\"\<sample_reduced_mca_timing.csv\>\""}], 
      "]"}]}], "+", "sampleMCATiming"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleGSHTTiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{
     "\"\<../Timing\>\"", ",", "\"\<sample_directed_gsht_timing.csv\>\""}], 
     "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleReducedGSHTTiming", "=", 
   RowBox[{
    RowBox[{"Flatten", "@", 
     RowBox[{"thesisImport", "[", 
      RowBox[{
      "\"\<../Timing\>\"", ",", 
       "\"\<sample_directed_reduced_gsht_timing.csv\>\""}], "]"}]}], "+", 
    "sampleGSHTTiming"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleGSHPTiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{
     "\"\<../Timing\>\"", ",", "\"\<sample_directed_gshp_timing.csv\>\""}], 
     "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sampleGSHCTiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{
     "\"\<../Timing\>\"", ",", "\"\<sample_directed_gshc_timing.csv\>\""}], 
     "]"}]}]}], ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"downtownMCATiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{"\"\<../Timing\>\"", ",", "\"\<downtown_mca_timing.csv\>\""}], 
     "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownReducedMCATiming", "=", 
   RowBox[{
    RowBox[{"Flatten", "@", 
     RowBox[{"thesisImport", "[", 
      RowBox[{
      "\"\<../Timing\>\"", ",", "\"\<downtown_reduced_mca_timing.csv\>\""}], 
      "]"}]}], "+", "downtownMCATiming"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownGSHTTiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{
     "\"\<../Timing\>\"", ",", "\"\<downtown_directed_gsht_timing.csv\>\""}], 
     "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownReducedGSHTTiming", "=", 
   RowBox[{
    RowBox[{"Flatten", "@", 
     RowBox[{"thesisImport", "[", 
      RowBox[{
      "\"\<../Timing\>\"", ",", 
       "\"\<downtown_directed_reduced_gsht_timing.csv\>\""}], "]"}]}], "+", 
    "downtownGSHTTiming"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownGSHPTiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{
     "\"\<../Timing\>\"", ",", "\"\<downtown_directed_gshp_timing.csv\>\""}], 
     "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownGSHCTiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{
     "\"\<../Timing\>\"", ",", "\"\<downtown_directed_gshc_timing.csv\>\""}], 
     "]"}]}]}], ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"urbanZoneMCATiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{"\"\<../Timing\>\"", ",", "\"\<urban_zone_mca_timing.csv\>\""}], 
     "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneReducedMCATiming", "=", 
   RowBox[{
    RowBox[{"Flatten", "@", 
     RowBox[{"thesisImport", "[", 
      RowBox[{
      "\"\<../Timing\>\"", ",", "\"\<urban_zone_reduced_mca_timing.csv\>\""}],
       "]"}]}], "+", "urbanZoneMCATiming"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneGSHTTiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{
     "\"\<../Timing\>\"", ",", 
      "\"\<urban_zone_directed_gsht_timing.csv\>\""}], "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneReducedGSHTTiming", "=", 
   RowBox[{
    RowBox[{"Flatten", "@", 
     RowBox[{"thesisImport", "[", 
      RowBox[{
      "\"\<../Timing\>\"", ",", 
       "\"\<urban_zone_directed_reduced_gsht_timing.csv\>\""}], "]"}]}], "+", 
    "urbanZoneGSHTTiming"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneGSHPTiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{
     "\"\<../Timing\>\"", ",", 
      "\"\<urban_zone_directed_gshp_timing.csv\>\""}], "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneGSHCTiming", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"thesisImport", "[", 
     RowBox[{
     "\"\<../Timing\>\"", ",", 
      "\"\<urban_zone_directed_gshc_timing.csv\>\""}], "]"}]}]}], 
  ";"}]}], "Input"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"sampleTimings", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"{", 
     RowBox[{
     "sampleMCATiming", ",", "sampleReducedMCATiming", ",", 
      "sampleGSHTTiming", ",", "sampleReducedGSHTTiming", ",", 
      "sampleGSHPTiming", ",", "sampleGSHCTiming"}], "}"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"downtownTimings", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"{", 
     RowBox[{
     "downtownMCATiming", ",", "downtownReducedMCATiming", ",", 
      "downtownGSHTTiming", ",", "downtownReducedGSHTTiming", ",", 
      "downtownGSHPTiming", ",", "downtownGSHCTiming"}], "}"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"urbanZoneTimings", "=", 
   RowBox[{"Flatten", "@", 
    RowBox[{"{", 
     RowBox[{
     "urbanZoneMCATiming", ",", "urbanZoneReducedMCATiming", ",", 
      "urbanZoneGSHTTiming", ",", "urbanZoneReducedGSHTTiming", ",", 
      "urbanZoneGSHPTiming", ",", "urbanZoneGSHCTiming"}], "}"}]}]}], 
  ";"}]}], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "Graphing the Results - Comparing the weights of each heuristic (",
 Cell[BoxData[
  FormBox[
   OverscriptBox["f", "^"], TraditionalForm]]],
 ")."
}], "Section"],

Cell[CellGroupData[{

Cell["\<\
All algorithms provide similar results for each of the different graphs. The \
reduced GSHT heuristic providing the lowest weight.\
\>", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], 
    PointBox[{{1., 30.25311030987094}, {2., 3.414213562373095}, {3., 
     3.414213562373095}, {4., 3.414213562373095}, {5., 3.414213562373095}, {
     6., 10.537319187990756`}}]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox[
     RowBox[{"Algorithms", " ", "Graph", " ", "Sample"}], TraditionalForm], 
    FormBox[
     OverscriptBox["f", "^"], TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImageSize->{693.3333333333337, Automatic},
  Method->{},
  PlotRange->{{0., 6.}, {0, 21.221977626417246`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output"],

Cell[BoxData[
 GraphicsBox[{{}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], 
    PointBox[{{1., 14988.441574981909`}, {2., 5913.541770539345}, {3., 
     6031.496864958035}, {4., 5822.081430671208}, {5., 6726.822079516825}, {
     6., 8504.330553043837}}]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox[
     RowBox[{"Algorithms", " ", "Downtown", " ", "Graph"}], TraditionalForm], 
    FormBox[
     OverscriptBox["f", "^"], TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImageSize->{671.1111111111113, Automatic},
  Method->{},
  PlotRange->{{0., 6.}, {0, 12390.513726800576`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output"],

Cell[BoxData[
 GraphicsBox[{{}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], 
    PointBox[{{1., 35694.79981183556}, {2., 18228.89447448174}, {3., 
     17834.573340379953`}, {4., 16284.655692961063`}, {5., 
     21301.446540633573`}, {6., 26054.85582678773}}]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox[
     RowBox[{"Algorithms", " ", "Graph", " ", "Urban", " ", "Zone"}], 
     TraditionalForm], 
    FormBox[
     OverscriptBox["f", "^"], TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImageSize->{602.222222222222, Automatic},
  Method->{},
  PlotRange->{{0., 6.}, {0, 35694.79981183556}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Graphing the Results - Timings", "Section"],

Cell[CellGroupData[{

Cell["\<\
Timings are compared on a normal scale and a logarithmic scale as well (when \
necessary).\
\>", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], 
    PointBox[{{1., 0.03125}, {2., 0.109375}, {3., 0.0625}, {4., 0.078125}, {
     5., 0.03125}, {6., 0.}}]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox[
     RowBox[{"Algorithms", " ", "Graph", " ", "Sample"}], TraditionalForm], 
    FormBox["seconds", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImageSize->{706.6666666666665, Automatic},
  Method->{},
  PlotRange->{{0., 6.}, {0, 0.109375}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "The shared vertex solution is also faster than ",
 StyleBox["Mathematica",
  FontSlant->"Italic"],
 "\[CloseCurlyQuote];s in-built MCST function. The second graph is on a \
logarithmic scale."
}], "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], 
    PointBox[{{1., 11.515625}, {2., 72.5}, {3., 143.3125}, {4., 149.640625}, {
     5., 13.96875}, {6., 0.296875}}]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox[
     RowBox[{"Algorithms", " ", "Downtown", " ", "Graph"}], TraditionalForm], 
    FormBox["seconds", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImageSize->{750.0000000000011, Automatic},
  Method->{},
  PlotRange->{{0., 6.}, {0, 149.640625}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     PointBox[{{1., 2.443704808829665}, {2., 4.283586561860629}, {3., 
      4.965027560618124}, {4., 5.008236586161948}, {5., 2.636822691813842}, {
      6., -1.2144441041932315`}}]}, {}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox[
     RowBox[{"Algorithms", " ", "Downtown", " ", "Graph"}], TraditionalForm], 
    FormBox["\"seconds (log)\"", TraditionalForm]},
  AxesOrigin->{0, -1.6660902833319142`},
  CoordinatesToolOptions:>{"DisplayFunction" -> ({
      Part[#, 1], 
      Exp[
       Part[#, 2]]}& ), "CopiedValueFunction" -> ({
      Part[#, 1], 
      Exp[
       Part[#, 2]]}& )},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{
     Charting`ScaledTicks[{Log, Exp}], 
     Charting`ScaledFrameTicks[{Log, Exp}]}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImageSize->{792.2222222222223, Automatic},
  Method->{},
  PlotRange->{{0., 6}, {-1.5601485869907414`, 5.008236586161948}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->FrontEndValueCache[{Automatic, 
     Charting`ScaledTicks[{Log, Exp}]}, {Automatic, {{0., 
       FormBox["1", TraditionalForm], {0.01, 0.}, {
        AbsoluteThickness[0.1]}}, {2.302585092994046, 
       FormBox["10", TraditionalForm], {0.01, 0.}, {
        AbsoluteThickness[0.1]}}, {4.605170185988092, 
       FormBox["100", TraditionalForm], {0.01, 0.}, {
        AbsoluteThickness[0.1]}}, {-4.605170185988091, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-3.912023005428146, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-3.506557897319982, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-3.2188758248682006`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-2.995732273553991, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-2.8134107167600364`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-2.659260036932778, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-2.5257286443082556`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-2.4079456086518722`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-2.3025850929940455`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-1.6094379124341003`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-1.2039728043259361`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.916290731874155, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.6931471805599453, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.5108256237659907, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.35667494393873245`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.2231435513142097, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.10536051565782628`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {0.6931471805599453, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.0986122886681098`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.3862943611198906`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.6094379124341003`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.791759469228055, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.9459101490553132`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {2.0794415416798357`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {2.1972245773362196`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {2.995732273553991, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {3.4011973816621555`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {3.6888794541139363`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {3.912023005428146, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {4.0943445622221, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {4.248495242049359, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {4.382026634673881, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {4.499809670330265, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}}}]]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
The shared vertex solution is also the fastest. GSHT is the slowest by far.\
\>", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, 
   {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`], 
    AbsoluteThickness[1.6], 
    PointBox[{{1., 63.5}, {2., 343.40625}, {3., 967.828125}, {4., 
     1006.078125}, {5., 67.9375}, {6., 1.03125}}]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox[
     RowBox[{"Algorithms", " ", "Graph", " ", "Urban", " ", "Zone"}], 
     TraditionalForm], 
    FormBox["seconds", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImageSize->{761.1111111111113, Automatic},
  Method->{},
  PlotRange->{{0., 6.}, {0, 1006.078125}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     PointBox[{{1., 4.151039905898646}, {2., 5.838914148647198}, {3., 
      6.875054514693317}, {4., 6.913815006690102}, {5., 4.218588164881428}, {
      6., 0.030771658666753687`}}]}, {}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox[
     RowBox[{"Algorithms", " ", "Graph", " ", "Urban", " ", "Zone"}], 
     TraditionalForm], 
    FormBox["\"seconds (log)\"", TraditionalForm]},
  AxesOrigin->{0, -0.5018091537113785},
  CoordinatesToolOptions:>{"DisplayFunction" -> ({
      Part[#, 1], 
      Exp[
       Part[#, 2]]}& ), "CopiedValueFunction" -> ({
      Part[#, 1], 
      Exp[
       Part[#, 2]]}& )},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{
     Charting`ScaledTicks[{Log, Exp}], 
     Charting`ScaledFrameTicks[{Log, Exp}]}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImageSize->{753.3333333333337, Automatic},
  Method->{},
  PlotRange->{{0., 6}, {-0.38410083370500575`, 6.913815006690102}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->FrontEndValueCache[{Automatic, 
     Charting`ScaledTicks[{Log, Exp}]}, {Automatic, {{0., 
       FormBox["1", TraditionalForm], {0.01, 0.}, {
        AbsoluteThickness[0.1]}}, {2.302585092994046, 
       FormBox["10", TraditionalForm], {0.01, 0.}, {
        AbsoluteThickness[0.1]}}, {4.605170185988092, 
       FormBox["100", TraditionalForm], {0.01, 0.}, {
        AbsoluteThickness[0.1]}}, {6.907755278982137, 
       FormBox["1000", TraditionalForm], {0.01, 0.}, {
        AbsoluteThickness[0.1]}}, {-2.3025850929940455`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-1.6094379124341003`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-1.2039728043259361`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.916290731874155, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.6931471805599453, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.5108256237659907, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.35667494393873245`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.2231435513142097, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {-0.10536051565782628`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {0.6931471805599453, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.0986122886681098`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.3862943611198906`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.6094379124341003`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.791759469228055, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {1.9459101490553132`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {2.0794415416798357`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {2.1972245773362196`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {2.995732273553991, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {3.4011973816621555`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {3.6888794541139363`, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {3.912023005428146, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {4.0943445622221, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {4.248495242049359, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {4.382026634673881, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {4.499809670330265, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {5.298317366548036, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {5.703782474656201, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {5.991464547107982, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {6.214608098422191, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {6.396929655216146, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {6.551080335043404, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {6.684611727667927, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}, {6.802394763324311, 
       FormBox[
        InterpretationBox[
         StyleBox[
          
          GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
           Baseline], "CacheGraphics" -> False], 
         Spacer[{0., 0.}]], TraditionalForm], {0.005, 0.}, {
        AbsoluteThickness[0.1]}}}}]]], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1904, 997},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
CellContext->Notebook,
TrackCellChangeTimes->False,
Magnification:>0.9 Inherited,
FrontEndVersion->"10.0 for Microsoft Windows (64-bit) (July 1, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 47, 0, 81, "Title"],
Cell[CellGroupData[{
Cell[1558, 39, 179, 3, 39, "Subsection"],
Cell[1740, 44, 415, 7, 110, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2192, 56, 52, 0, 55, "Section"],
Cell[2247, 58, 313, 8, 50, "Input"],
Cell[2563, 68, 650, 17, 90, "Input"],
Cell[3216, 87, 705, 18, 90, "Input"],
Cell[3924, 107, 342, 9, 50, "Input"],
Cell[4269, 118, 342, 9, 50, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4648, 132, 71, 0, 55, "Section"],
Cell[4722, 134, 321, 8, 50, "Input"],
Cell[5046, 144, 666, 17, 90, "Input"],
Cell[5715, 163, 725, 19, 90, "Input"],
Cell[6443, 184, 354, 10, 50, "Input"],
Cell[6800, 196, 354, 10, 50, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7191, 211, 73, 0, 55, "Section"],
Cell[7267, 213, 330, 9, 50, "Input"],
Cell[7600, 224, 678, 17, 90, "Input"],
Cell[8281, 243, 741, 20, 90, "Input"],
Cell[9025, 265, 360, 10, 50, "Input"],
Cell[9388, 277, 360, 10, 50, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9785, 292, 37, 0, 55, "Section"],
Cell[9825, 294, 1572, 44, 130, "Input"],
Cell[11400, 340, 1600, 44, 130, "Input"],
Cell[13003, 386, 1632, 47, 130, "Input"],
Cell[14638, 435, 1014, 27, 70, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15689, 467, 179, 6, 60, "Section"],
Cell[CellGroupData[{
Cell[15893, 477, 160, 3, 39, "Subsection"],
Cell[16056, 482, 1148, 31, 357, "Output"],
Cell[17207, 515, 1150, 31, 328, "Output"],
Cell[18360, 548, 1167, 32, 286, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[19576, 586, 49, 0, 55, "Section"],
Cell[CellGroupData[{
Cell[19650, 590, 120, 3, 39, "Subsection"],
Cell[19773, 595, 1045, 29, 356, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20855, 629, 228, 6, 40, "Subsection"],
Cell[21086, 637, 1058, 29, 376, "Output"],
Cell[22147, 668, 12377, 318, 398, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[34561, 991, 105, 2, 39, "Subsection"],
Cell[34669, 995, 1076, 30, 375, "Output"],
Cell[35748, 1027, 12178, 313, 372, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature Cv0duGkYCJIl5B1tyt7s73r4 *)
