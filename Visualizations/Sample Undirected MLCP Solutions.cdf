(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[     87535,       2198]
NotebookOptionsPosition[     86063,       2124]
NotebookOutlinePosition[     86521,       2144]
CellTagsIndexPosition[     86478,       2141]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["MLCP Solution Visualizations: Undirected Sample Solutions", "Title"],

Cell["\<\
Every unreduced and reduced solution for the undirected sample graph is \
displayed here. For algorithms that produce multiple solutions, only the best \
solution is shown here. Unreduced solutions are displayed in red, reduced \
solutions (if they exist) are displayed in blue.\
\>", "Subsection"],

Cell[CellGroupData[{

Cell["Undirected Sample Dataset - Base Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, {
      Null, {{1, 2}, {1, 8}, {1, 9}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {3, 6}, {
       4, 7}, {4, 10}, {5, 6}, {5, 9}, {6, 7}, {7, 9}, {8, 11}, {9, 10}, {9, 
       11}, {10, 11}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, 
       EdgeWeight -> {1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 2.8284271247461903`, 4.242640687119286, 
        4.47213595499958, 2.23606797749979, 4., 1.4142135623730951`, 2., 3., 
        4.123105625617661, 5., 5.0990195135927845`, 3.1622776601683795`, 
        6.324555320336759}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}}]]}, 
    TagBox[GraphicsGroupBox[{
       {Hue[0.6, 0.7, 0.5], Opacity[0.7], 
        {Arrowheads[0.], ArrowBox[{{3., 9.}, {4., 9.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{3., 9.}, {1., 4.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{3., 9.}, {6., 4.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{4., 9.}, {9., 9.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{4., 9.}, {6., 6.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], 
         ArrowBox[{{9., 9.}, {11., 7.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{9., 9.}, {6., 6.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{9., 9.}, {7., 5.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], 
         ArrowBox[{{11., 7.}, {10., 5.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], 
         ArrowBox[{{11., 7.}, {11., 3.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{6., 6.}, {7., 5.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{6., 6.}, {6., 4.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], 
         ArrowBox[{{7., 5.}, {10., 5.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], 
         ArrowBox[{{10., 5.}, {6., 4.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{1., 4.}, {5., 1.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], 
         ArrowBox[{{6., 4.}, {11., 3.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], ArrowBox[{{6., 4.}, {5., 1.}}, 0.06561181434599156]}, 
        {Arrowheads[0.], 
         ArrowBox[{{11., 3.}, {5., 1.}}, 0.06561181434599156]}}, 
       {Hue[0.6, 0.2, 0.8], EdgeForm[{GrayLevel[0], Opacity[
        0.7]}], {DiskBox[{3., 9.}, 0.1], InsetBox[
          StyleBox["1",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {3.1, 9.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{4., 9.}, 0.1], InsetBox[
          StyleBox["2",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {4.1, 9.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{9., 9.}, 0.1], InsetBox[
          StyleBox["3",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {9.1, 9.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{11., 7.}, 0.1], InsetBox[
          StyleBox["4",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {11.1, 7.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 6.}, 0.1], InsetBox[
          StyleBox["5",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6.1, 6.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{7., 5.}, 0.1], InsetBox[
          StyleBox["6",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {7.1, 5.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{10., 5.}, 0.1], InsetBox[
          StyleBox["7",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {10.1, 5.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{1., 4.}, 0.1], InsetBox[
          StyleBox["8",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {1.1, 4.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 4.}, 0.1], InsetBox[
          StyleBox["9",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6.1, 4.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{11., 3.}, 0.1], InsetBox[
          StyleBox["10",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {11.1, 3.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{5., 1.}, 0.1], InsetBox[
          StyleBox["11",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {5.1, 1.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["68.72411140389733`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Sample Dataset - MCST Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, {
      Null, {{1, 2}, {1, 8}, {1, 9}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {3, 6}, {
       4, 7}, {4, 10}, {5, 6}, {5, 9}, {6, 7}, {7, 9}, {8, 11}, {9, 10}, {9, 
       11}, {10, 11}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {1, 3, 
         UndirectedEdge[9, 11], 
         UndirectedEdge[4, 7], 
         UndirectedEdge[6, 7], 4, 
         UndirectedEdge[5, 9], 
         UndirectedEdge[4, 10], 11, 5, 10, 
         UndirectedEdge[5, 6], 6, 2, 
         UndirectedEdge[1, 2], 
         UndirectedEdge[2, 5], 
         UndirectedEdge[3, 4], 9, 7, 8, 
         UndirectedEdge[8, 11]}, 
       GraphHighlightStyle -> {"Thick", 1 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[1, 2] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 3 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[8, 11] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[5, 6] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 5 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 6 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 2 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 11 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 7 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[5, 9] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[6, 7] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[4, 10] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 9 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[2, 5] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[3, 4] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[9, 11] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[4, 7] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 8 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 4 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 10 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}}, 
       EdgeWeight -> {1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 2.8284271247461903`, 4.242640687119286, 
        4.47213595499958, 2.23606797749979, 4., 1.4142135623730951`, 2., 3., 
        4.123105625617661, 5., 5.0990195135927845`, 3.1622776601683795`, 
        6.324555320336759}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$2", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{3., 9.}, 0.1], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{4., 9.}, 0.1], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{9., 9.}, 0.1], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{11., 7.}, 0.1], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.1], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.1], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{10., 5.}, 0.1], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{1., 4.}, 0.1], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.1], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{11., 3.}, 0.1], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{5., 1.}, 0.1], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{15.2, 267.32483787147993`}, {-109.52283787147994`, 
       95.01376470588232}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weights", "Subsection"],

Cell[BoxData["28.246537600251443`"], "Output"],

Cell[BoxData["3.414213562373095`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Sample Dataset - GSH-T Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, {
      Null, {{1, 2}, {1, 8}, {1, 9}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {3, 6}, {
       4, 7}, {4, 10}, {5, 6}, {5, 9}, {6, 7}, {7, 9}, {8, 11}, {9, 10}, {9, 
       11}, {10, 11}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[5, 9], 6, 5, 
         UndirectedEdge[5, 6], 9}, 
       GraphHighlightStyle -> {
        "Thick", UndirectedEdge[5, 6] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[5, 9] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 5 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 9 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 6 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}}, 
       EdgeWeight -> {1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 2.8284271247461903`, 4.242640687119286, 
        4.47213595499958, 2.23606797749979, 4., 1.4142135623730951`, 2., 3., 
        4.123105625617661, 5., 5.0990195135927845`, 3.1622776601683795`, 
        6.324555320336759}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{3., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{9., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 7.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.1], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.1], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{10., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.1], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{15.2, 267.32483787147993`}, {-109.52283787148005`, 
       95.01376470588232}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weights", "Subsection"],

Cell[BoxData["3.414213562373095`"], "Output"],

Cell[BoxData["3.414213562373095`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Sample Dataset - GSH-P Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, {
      Null, {{1, 2}, {1, 8}, {1, 9}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {3, 6}, {
       4, 7}, {4, 10}, {5, 6}, {5, 9}, {6, 7}, {7, 9}, {8, 11}, {9, 10}, {9, 
       11}, {10, 11}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[5, 9], 6, 5, 
         UndirectedEdge[5, 6], 9}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 2.8284271247461903`, 4.242640687119286, 
        4.47213595499958, 2.23606797749979, 4., 1.4142135623730951`, 2., 3., 
        4.123105625617661, 5., 5.0990195135927845`, 3.1622776601683795`, 
        6.324555320336759}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{3., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{9., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 7.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{10., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{15.2, 267.32483787147993`}, {-109.52283787148014`, 
       95.0137647058823}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["3.414213562373095`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Sample Dataset - GSH-C Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, {
      Null, {{1, 2}, {1, 8}, {1, 9}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {3, 6}, {
       4, 7}, {4, 10}, {5, 6}, {5, 9}, {6, 7}, {7, 9}, {8, 11}, {9, 10}, {9, 
       11}, {10, 11}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[5, 9], 6, 5, 
         UndirectedEdge[5, 6], 9, 
         UndirectedEdge[6, 7], 
         UndirectedEdge[7, 9], 7}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 2.8284271247461903`, 4.242640687119286, 
        4.47213595499958, 2.23606797749979, 4., 1.4142135623730951`, 2., 3., 
        4.123105625617661, 5., 5.0990195135927845`, 3.1622776601683795`, 
        6.324555320336759}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{3., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{9., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 7.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{10., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{15.2, 267.32483787147993`}, {-109.52283787148002`, 
       95.0137647058823}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["10.537319187990756`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Sample Dataset - GC-IE Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, {
      Null, {{1, 2}, {1, 8}, {1, 9}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {3, 6}, {
       4, 7}, {4, 10}, {5, 6}, {5, 9}, {6, 7}, {7, 9}, {8, 11}, {9, 10}, {9, 
       11}, {10, 11}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {1, 6, 5, 
         UndirectedEdge[5, 6], 9, 
         UndirectedEdge[1, 9], 
         UndirectedEdge[1, 2], 2, 
         UndirectedEdge[2, 5]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 2.8284271247461903`, 4.242640687119286, 
        4.47213595499958, 2.23606797749979, 4., 1.4142135623730951`, 2., 3., 
        4.123105625617661, 5., 5.0990195135927845`, 3.1622776601683795`, 
        6.324555320336759}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$2", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{3., 9.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{4., 9.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{9., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 7.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{10., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{15.2, 267.32483787147993`}, {-109.52283787147968`, 
       95.0137647058823}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["11.850716732682386`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Sample Dataset - GC-PO Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, {
      Null, {{1, 2}, {1, 8}, {1, 9}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {3, 6}, {
       4, 7}, {4, 10}, {5, 6}, {5, 9}, {6, 7}, {7, 9}, {8, 11}, {9, 10}, {9, 
       11}, {10, 11}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[5, 9], 6, 5, 
         UndirectedEdge[5, 6], 9}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 2.8284271247461903`, 4.242640687119286, 
        4.47213595499958, 2.23606797749979, 4., 1.4142135623730951`, 2., 3., 
        4.123105625617661, 5., 5.0990195135927845`, 3.1622776601683795`, 
        6.324555320336759}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{3., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{9., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 7.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{10., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{15.2, 267.32483787147993`}, {-109.52283787147965`, 
       95.01376470588232}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["3.414213562373095`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Undirected Sample Dataset - Shared Vertex Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}, {
      Null, {{1, 2}, {1, 8}, {1, 9}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {3, 6}, {
       4, 7}, {4, 10}, {5, 6}, {5, 9}, {6, 7}, {7, 9}, {8, 11}, {9, 10}, {9, 
       11}, {10, 11}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[5, 9], 
         UndirectedEdge[3, 5], 5, 9, 3}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 2.8284271247461903`, 4.242640687119286, 
        4.47213595499958, 2.23606797749979, 4., 1.4142135623730951`, 2., 3., 
        4.123105625617661, 5., 5.0990195135927845`, 3.1622776601683795`, 
        6.324555320336759}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{3., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 9.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{9., 9.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 7.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{7., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{10., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{15.2, 267.32483787147993`}, {-109.52283787147996`, 
       95.0137647058823}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["6.242640687119286`"], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1319, 744},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
CellContext->Notebook,
TrackCellChangeTimes->False,
Magnification:>0.75 Inherited,
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 74, 0, 72, "Title"],
Cell[1563, 37, 308, 5, 56, "Subsection"],
Cell[CellGroupData[{
Cell[1896, 46, 57, 0, 41, "Section"],
Cell[1956, 48, 5060, 97, 234, "Output"],
Cell[CellGroupData[{
Cell[7041, 149, 34, 0, 35, "Subsection"],
Cell[7078, 151, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7172, 157, 66, 0, 51, "Section"],
Cell[7241, 159, 14582, 364, 234, "Output"],
Cell[CellGroupData[{
Cell[21848, 527, 35, 0, 35, "Subsection"],
Cell[21886, 529, 46, 0, 24, "Output"],
Cell[21935, 531, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[22029, 537, 67, 0, 51, "Section"],
Cell[22099, 539, 10403, 244, 234, "Output"],
Cell[CellGroupData[{
Cell[32527, 787, 35, 0, 35, "Subsection"],
Cell[32565, 789, 45, 0, 24, "Output"],
Cell[32613, 791, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[32707, 797, 67, 0, 51, "Section"],
Cell[32777, 799, 10165, 242, 234, "Output"],
Cell[CellGroupData[{
Cell[42967, 1045, 34, 0, 35, "Subsection"],
Cell[43004, 1047, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[43098, 1053, 67, 0, 51, "Section"],
Cell[43168, 1055, 10746, 262, 234, "Output"],
Cell[CellGroupData[{
Cell[53939, 1321, 34, 0, 35, "Subsection"],
Cell[53976, 1323, 46, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[54071, 1329, 67, 0, 51, "Section"],
Cell[54141, 1331, 10983, 270, 234, "Output"],
Cell[CellGroupData[{
Cell[65149, 1605, 34, 0, 35, "Subsection"],
Cell[65186, 1607, 46, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[65281, 1613, 67, 0, 51, "Section"],
Cell[65351, 1615, 10166, 242, 234, "Output"],
Cell[CellGroupData[{
Cell[75542, 1861, 34, 0, 35, "Subsection"],
Cell[75579, 1863, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[75673, 1869, 75, 0, 51, "Section"],
Cell[75751, 1871, 10165, 242, 234, "Output"],
Cell[CellGroupData[{
Cell[85941, 2117, 34, 0, 35, "Subsection"],
Cell[85978, 2119, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature sxp0w#KvU3RUmA1MX1MZp9xl *)
