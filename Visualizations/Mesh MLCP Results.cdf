(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[    225973,       5636]
NotebookOptionsPosition[    223798,       5539]
NotebookOutlinePosition[    224231,       5558]
CellTagsIndexPosition[    224188,       5555]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["MLCP Solution Evaluation: Mesh Graphs", "Title"],

Cell["\<\
Meshes are used in order to compare the results produced by the algorithms to \
the optimal result. Every edgeweight is 1 in these graphs. Because of the \
regularity of these graphs, a pattern (usually describes by a polynomial \
equation) usually emerges. A few of these meshes are visualized in order to \
easily understand the process.

Note: Gravity Center solutions are not considered here because they are based \
on the \[OpenCurlyDoubleQuote]unevenness\[CloseCurlyDoubleQuote] of the graph \
to function.\
\>", "Subsection"],

Cell[CellGroupData[{

Cell["Base Meshes", "Section"],

Cell[CellGroupData[{

Cell["\<\
The base meshes follow the same structure as this example (5 x 5 in this \
case).\
\>", "Subsection"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36}, {
      Null, {{1, 2}, {1, 7}, {2, 3}, {2, 8}, {3, 4}, {3, 9}, {4, 5}, {4, 
       10}, {5, 6}, {5, 11}, {6, 12}, {7, 8}, {7, 13}, {8, 9}, {8, 14}, {9, 
       10}, {9, 15}, {10, 11}, {10, 16}, {11, 12}, {11, 17}, {12, 18}, {13, 
       14}, {13, 19}, {14, 15}, {14, 20}, {15, 16}, {15, 21}, {16, 17}, {16, 
       22}, {17, 18}, {17, 23}, {18, 24}, {19, 20}, {19, 25}, {20, 21}, {20, 
       26}, {21, 22}, {21, 27}, {22, 23}, {22, 28}, {23, 24}, {23, 29}, {24, 
       30}, {25, 26}, {25, 31}, {26, 27}, {26, 32}, {27, 28}, {27, 33}, {28, 
       29}, {28, 34}, {29, 30}, {29, 35}, {30, 36}, {31, 32}, {32, 33}, {33, 
       34}, {34, 35}, {35, 36}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, 
       EdgeWeight -> {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 
       VertexCoordinates -> {{1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}, {
        1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 5}, {6, 5}, {1, 4}, {2, 4}, {3, 
        4}, {4, 4}, {5, 4}, {6, 4}, {1, 3}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {
        6, 3}, {1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {1, 1}, {2, 
        1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}}}]]}, 
    TagBox[GraphicsGroupBox[{
       {Hue[0.6, 0.7, 0.5], Opacity[0.7], 
        {Arrowheads[0.], 
         ArrowBox[{{1., 6.}, {2., 6.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 6.}, {1., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 6.}, {3., 6.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 6.}, {2., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 6.}, {4., 6.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 6.}, {3., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 6.}, {5., 6.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 6.}, {4., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 6.}, {6., 6.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 6.}, {5., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{6., 6.}, {6., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 5.}, {2., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 5.}, {1., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 5.}, {3., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 5.}, {2., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 5.}, {4., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 5.}, {3., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 5.}, {5., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 5.}, {4., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 5.}, {6., 5.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 5.}, {5., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{6., 5.}, {6., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 4.}, {2., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 4.}, {1., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 4.}, {3., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 4.}, {2., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 4.}, {4., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 4.}, {3., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 4.}, {5., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 4.}, {4., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 4.}, {6., 4.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 4.}, {5., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{6., 4.}, {6., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 3.}, {2., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 3.}, {1., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 3.}, {3., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 3.}, {2., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 3.}, {4., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 3.}, {3., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 3.}, {5., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 3.}, {4., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 3.}, {6., 3.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 3.}, {5., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{6., 3.}, {6., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 2.}, {2., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 2.}, {1., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 2.}, {3., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 2.}, {2., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 2.}, {4., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 2.}, {3., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 2.}, {5., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 2.}, {4., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 2.}, {6., 2.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 2.}, {5., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{6., 2.}, {6., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{1., 1.}, {2., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{2., 1.}, {3., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{3., 1.}, {4., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{4., 1.}, {5., 1.}}, 0.043048128342245986`]}, 
        {Arrowheads[0.], 
         ArrowBox[{{5., 1.}, {6., 1.}}, 0.043048128342245986`]}}, 
       {Hue[0.6, 0.2, 0.8], EdgeForm[{GrayLevel[0], Opacity[
        0.7]}], {DiskBox[{1., 6.}, 0.1], InsetBox[
          StyleBox["1",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {1.1, 6.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{2., 6.}, 0.1], InsetBox[
          StyleBox["2",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {2.1, 6.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{3., 6.}, 0.1], InsetBox[
          StyleBox["3",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {3.1, 6.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{4., 6.}, 0.1], InsetBox[
          StyleBox["4",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {4.1, 6.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{5., 6.}, 0.1], InsetBox[
          StyleBox["5",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {5.1, 6.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 6.}, 0.1], InsetBox[
          StyleBox["6",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6.1, 6.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{1., 5.}, 0.1], InsetBox[
          StyleBox["7",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {1.1, 5.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{2., 5.}, 0.1], InsetBox[
          StyleBox["8",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {2.1, 5.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{3., 5.}, 0.1], InsetBox[
          StyleBox["9",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {3.1, 5.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{4., 5.}, 0.1], InsetBox[
          StyleBox["10",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {4.1, 5.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{5., 5.}, 0.1], InsetBox[
          StyleBox["11",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {5.1, 5.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 5.}, 0.1], InsetBox[
          StyleBox["12",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6.1, 5.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{1., 4.}, 0.1], InsetBox[
          StyleBox["13",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {1.1, 4.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{2., 4.}, 0.1], InsetBox[
          StyleBox["14",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {2.1, 4.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{3., 4.}, 0.1], InsetBox[
          StyleBox["15",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {3.1, 4.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{4., 4.}, 0.1], InsetBox[
          StyleBox["16",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {4.1, 4.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{5., 4.}, 0.1], InsetBox[
          StyleBox["17",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {5.1, 4.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 4.}, 0.1], InsetBox[
          StyleBox["18",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6.1, 4.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{1., 3.}, 0.1], InsetBox[
          StyleBox["19",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {1.1, 3.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{2., 3.}, 0.1], InsetBox[
          StyleBox["20",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {2.1, 3.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{3., 3.}, 0.1], InsetBox[
          StyleBox["21",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {3.1, 3.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{4., 3.}, 0.1], InsetBox[
          StyleBox["22",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {4.1, 3.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{5., 3.}, 0.1], InsetBox[
          StyleBox["23",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {5.1, 3.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 3.}, 0.1], InsetBox[
          StyleBox["24",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6.1, 3.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{1., 2.}, 0.1], InsetBox[
          StyleBox["25",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {1.1, 2.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{2., 2.}, 0.1], InsetBox[
          StyleBox["26",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {2.1, 2.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{3., 2.}, 0.1], InsetBox[
          StyleBox["27",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {3.1, 2.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{4., 2.}, 0.1], InsetBox[
          StyleBox["28",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {4.1, 2.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{5., 2.}, 0.1], InsetBox[
          StyleBox["29",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {5.1, 2.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 2.}, 0.1], InsetBox[
          StyleBox["30",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6.1, 2.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{1., 1.}, 0.1], InsetBox[
          StyleBox["31",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {1.1, 1.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{2., 1.}, 0.1], InsetBox[
          StyleBox["32",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {2.1, 1.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{3., 1.}, 0.1], InsetBox[
          StyleBox["33",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {3.1, 1.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{4., 1.}, 0.1], InsetBox[
          StyleBox["34",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {4.1, 1.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{5., 1.}, 0.1], InsetBox[
          StyleBox["35",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {5.1, 1.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 1.}, 0.1], InsetBox[
          StyleBox["36",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6.1, 1.1}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"]
}, Open  ]],

Cell[TextData[{
 "The weight of the optimal MLCP solution on an ",
 StyleBox["n x n",
  FontWeight->"Bold"],
 " mesh (represented by ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["f", "*"], TraditionalForm]]],
 ") can be obtained using the following equation: \n\n\t\t",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["f", "*"], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[LeftCeiling]", 
    RowBox[{"(", 
     FractionBox[
      SuperscriptBox["n", "2"], "2"], ")"}], "\[RightCeiling]"}], 
   TraditionalForm]],
  FormatType->"TraditionalForm"],
 "- 2"
}], "Subsection"],

Cell["\<\
The first few optimal MLCP Solution Edge Weights (from n = 3  to n = 20) are:

{3,6,11,16,23,30,39,48,59,70,83,96,111,126,143,160,179,198}\
\>", "Subsection"]
}, Open  ]],

Cell[CellGroupData[{

Cell["MCST Meshes", "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 "A MCST solution on a 5 x 5 mesh follows a pattern. The weight of this MLCP \
solution on an ",
 StyleBox["n x n",
  FontWeight->"Bold"],
 " mesh (represented by ",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    OverscriptBox["f", "^"], "MCST"], TraditionalForm]]],
 ") can be obtained using the following equation: \n\n\t\t",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    OverscriptBox["f", "^"], "MCST"], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"n", "+", "1"}], ")"}], "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 "-1"
}], "Subsection"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36}, {
      Null, {{1, 2}, {1, 7}, {2, 3}, {2, 8}, {3, 4}, {3, 9}, {4, 5}, {4, 
       10}, {5, 6}, {5, 11}, {6, 12}, {7, 8}, {7, 13}, {8, 9}, {8, 14}, {9, 
       10}, {9, 15}, {10, 11}, {10, 16}, {11, 12}, {11, 17}, {12, 18}, {13, 
       14}, {13, 19}, {14, 15}, {14, 20}, {15, 16}, {15, 21}, {16, 17}, {16, 
       22}, {17, 18}, {17, 23}, {18, 24}, {19, 20}, {19, 25}, {20, 21}, {20, 
       26}, {21, 22}, {21, 27}, {22, 23}, {22, 28}, {23, 24}, {23, 29}, {24, 
       30}, {25, 26}, {25, 31}, {26, 27}, {26, 32}, {27, 28}, {27, 33}, {28, 
       29}, {28, 34}, {29, 30}, {29, 35}, {30, 36}, {31, 32}, {32, 33}, {33, 
       34}, {34, 35}, {35, 36}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[26, 32], 30, 16, 6, 
         UndirectedEdge[5, 6], 13, 29, 
         UndirectedEdge[18, 24], 11, 
         UndirectedEdge[14, 20], 
         UndirectedEdge[13, 19], 
         UndirectedEdge[1, 7], 21, 
         UndirectedEdge[28, 34], 8, 23, 34, 22, 
         UndirectedEdge[4, 5], 7, 
         UndirectedEdge[4, 10], 20, 
         UndirectedEdge[3, 9], 14, 33, 1, 
         UndirectedEdge[12, 18], 
         UndirectedEdge[6, 12], 26, 
         UndirectedEdge[20, 26], 4, 9, 
         UndirectedEdge[11, 17], 
         UndirectedEdge[15, 21], 25, 
         UndirectedEdge[10, 16], 
         UndirectedEdge[24, 30], 10, 
         UndirectedEdge[9, 15], 
         UndirectedEdge[1, 2], 
         UndirectedEdge[23, 29], 
         UndirectedEdge[2, 8], 5, 19, 15, 32, 17, 12, 24, 
         UndirectedEdge[27, 33], 
         UndirectedEdge[2, 3], 
         UndirectedEdge[17, 23], 36, 
         UndirectedEdge[22, 28], 27, 
         UndirectedEdge[29, 35], 
         UndirectedEdge[8, 14], 
         UndirectedEdge[7, 13], 28, 3, 18, 35, 
         UndirectedEdge[3, 4], 
         UndirectedEdge[30, 36], 
         UndirectedEdge[5, 11], 
         UndirectedEdge[19, 25], 31, 2, 
         UndirectedEdge[16, 22], 
         UndirectedEdge[25, 31], 
         UndirectedEdge[21, 27]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 
       VertexCoordinates -> {{1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}, {
        1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 5}, {6, 5}, {1, 4}, {2, 4}, {3, 
        4}, {4, 4}, {5, 4}, {6, 4}, {1, 3}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {
        6, 3}, {1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {1, 1}, {2, 
        1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$2", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$6", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$14", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$16", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$12", Automatic, Center], 
          DynamicLocation["VertexID$18", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$16", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$18", Automatic, Center], 
          DynamicLocation["VertexID$24", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$31", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$32", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$33", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$35", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$36", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{1., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{2., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{3., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{4., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{5., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{1., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{2., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{3., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{4., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{5., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 5.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{1., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{2., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{3., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{4., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{5., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{1., 3.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{2., 3.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{3., 3.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{4., 3.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{5., 3.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 3.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{1., 2.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{2., 2.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{3., 2.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{4., 2.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{5., 2.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 2.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{1., 1.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{2., 1.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{3., 1.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{4., 1.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{5., 1.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 1.}, 0.1], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{20.59999999999999, 
       350.32271117118466`}, {-173.58671117118456`, 158.13599999999997`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "The reduced version of the algorithm is visualized. The weight of this MLCP \
solution on an ",
 StyleBox["n x n",
  FontWeight->"Bold"],
 " mesh can be obtained using the following equation: \n\n\t\tif (n % 2 = 0)\t\
 \t",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    OverscriptBox["f", "^"], 
    RowBox[{"MCST", "-", "R"}]], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   FractionBox["1", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 "(",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["n", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " + 2n - 3)\n\t\tif (n % 2 = 1) \t\t",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    OverscriptBox["f", "^"], 
    RowBox[{"MCST", "-", "R"}]], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   FractionBox["1", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 "(",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["n", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " + 2n - 5)"
}], "Subsection"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36}, {
      Null, {{1, 2}, {1, 7}, {2, 3}, {2, 8}, {3, 4}, {3, 9}, {4, 5}, {4, 
       10}, {5, 6}, {5, 11}, {6, 12}, {7, 8}, {7, 13}, {8, 9}, {8, 14}, {9, 
       10}, {9, 15}, {10, 11}, {10, 16}, {11, 12}, {11, 17}, {12, 18}, {13, 
       14}, {13, 19}, {14, 15}, {14, 20}, {15, 16}, {15, 21}, {16, 17}, {16, 
       22}, {17, 18}, {17, 23}, {18, 24}, {19, 20}, {19, 25}, {20, 21}, {20, 
       26}, {21, 22}, {21, 27}, {22, 23}, {22, 28}, {23, 24}, {23, 29}, {24, 
       30}, {25, 26}, {25, 31}, {26, 27}, {26, 32}, {27, 28}, {27, 33}, {28, 
       29}, {28, 34}, {29, 30}, {29, 35}, {30, 36}, {31, 32}, {32, 33}, {33, 
       34}, {34, 35}, {35, 36}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[21, 27], 
         UndirectedEdge[2, 3], 
         UndirectedEdge[3, 9], 
         UndirectedEdge[9, 15], 
         UndirectedEdge[19, 25], 
         UndirectedEdge[7, 13], 
         UndirectedEdge[1, 7], 
         UndirectedEdge[15, 21], 
         UndirectedEdge[23, 29], 
         UndirectedEdge[1, 2], 
         UndirectedEdge[11, 17], 
         UndirectedEdge[13, 19], 
         UndirectedEdge[17, 23], 
         UndirectedEdge[5, 11], 
         UndirectedEdge[4, 5], 
         UndirectedEdge[3, 4]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 
       VertexCoordinates -> {{1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}, {
        1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 5}, {6, 5}, {1, 4}, {2, 4}, {3, 
        4}, {4, 4}, {5, 4}, {6, 4}, {1, 3}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {
        6, 3}, {1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {1, 1}, {2, 
        1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$2", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{1., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{20.59999999999999, 
       350.32271117118466`}, {-173.58671117118473`, 158.13599999999997`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["GSH - T Meshes", "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 "A GSH-T solution on a 5 x 5 mesh starting on vertex 1. The weight of this \
MLCP solution on an ",
 StyleBox["n x n",
  FontWeight->"Bold"],
 " mesh can be obtained using the following equation: \n\n\t\t",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    OverscriptBox["f", "^"], 
    RowBox[{"GSH", "-", "T"}]], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["n", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " - 1"
}], "Subsection"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36}, {
      Null, {{1, 2}, {1, 7}, {2, 3}, {2, 8}, {3, 4}, {3, 9}, {4, 5}, {4, 
       10}, {5, 6}, {5, 11}, {6, 12}, {7, 8}, {7, 13}, {8, 9}, {8, 14}, {9, 
       10}, {9, 15}, {10, 11}, {10, 16}, {11, 12}, {11, 17}, {12, 18}, {13, 
       14}, {13, 19}, {14, 15}, {14, 20}, {15, 16}, {15, 21}, {16, 17}, {16, 
       22}, {17, 18}, {17, 23}, {18, 24}, {19, 20}, {19, 25}, {20, 21}, {20, 
       26}, {21, 22}, {21, 27}, {22, 23}, {22, 28}, {23, 24}, {23, 29}, {24, 
       30}, {25, 26}, {25, 31}, {26, 27}, {26, 32}, {27, 28}, {27, 33}, {28, 
       29}, {28, 34}, {29, 30}, {29, 35}, {30, 36}, {31, 32}, {32, 33}, {33, 
       34}, {34, 35}, {35, 36}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[2, 3], 
         UndirectedEdge[21, 22], 
         UndirectedEdge[8, 9], 
         UndirectedEdge[27, 28], 
         UndirectedEdge[14, 15], 
         UndirectedEdge[25, 26], 
         UndirectedEdge[28, 29], 
         UndirectedEdge[26, 27], 
         UndirectedEdge[22, 23], 
         UndirectedEdge[19, 25], 
         UndirectedEdge[7, 13], 
         UndirectedEdge[9, 10], 
         UndirectedEdge[19, 20], 
         UndirectedEdge[1, 7], 
         UndirectedEdge[15, 16], 
         UndirectedEdge[20, 21], 
         UndirectedEdge[13, 14], 
         UndirectedEdge[1, 2], 
         UndirectedEdge[10, 11], 
         UndirectedEdge[7, 8], 
         UndirectedEdge[13, 19], 
         UndirectedEdge[16, 17], 
         UndirectedEdge[4, 5], 
         UndirectedEdge[3, 4]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 
       VertexCoordinates -> {{1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}, {
        1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 5}, {6, 5}, {1, 4}, {2, 4}, {3, 
        4}, {4, 4}, {5, 4}, {6, 4}, {1, 3}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {
        6, 3}, {1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {1, 1}, {2, 
        1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$2", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$14", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$16", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$16", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{1., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{20.59999999999999, 
       350.32271117118466`}, {-173.58671117118536`, 158.13599999999997`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "The reduced version of the algorithm is visualized. The weight of this MLCP \
solution on an ",
 StyleBox["n x n",
  FontWeight->"Bold"],
 " mesh can be obtained using the SAME equation as the MCST-R: \n\n\t\tif (n \
% 2 = 0)\t \t",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    OverscriptBox["f", "^"], 
    RowBox[{"GSH", "-", "T", "-", "R"}]], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   FractionBox["1", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 "(",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["n", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " + 2n - 3)\n\t\tif (n % 2 = 1) \t\t",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    OverscriptBox["f", "^"], 
    RowBox[{"GSH", "-", "T", "-", "R"}]], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   FractionBox["1", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 "(",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["n", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " + 2n - 5)"
}], "Subsection"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36}, {
      Null, {{1, 2}, {1, 7}, {2, 3}, {2, 8}, {3, 4}, {3, 9}, {4, 5}, {4, 
       10}, {5, 6}, {5, 11}, {6, 12}, {7, 8}, {7, 13}, {8, 9}, {8, 14}, {9, 
       10}, {9, 15}, {10, 11}, {10, 16}, {11, 12}, {11, 17}, {12, 18}, {13, 
       14}, {13, 19}, {14, 15}, {14, 20}, {15, 16}, {15, 21}, {16, 17}, {16, 
       22}, {17, 18}, {17, 23}, {18, 24}, {19, 20}, {19, 25}, {20, 21}, {20, 
       26}, {21, 22}, {21, 27}, {22, 23}, {22, 28}, {23, 24}, {23, 29}, {24, 
       30}, {25, 26}, {25, 31}, {26, 27}, {26, 32}, {27, 28}, {27, 33}, {28, 
       29}, {28, 34}, {29, 30}, {29, 35}, {30, 36}, {31, 32}, {32, 33}, {33, 
       34}, {34, 35}, {35, 36}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[2, 3], 
         UndirectedEdge[27, 28], 
         UndirectedEdge[14, 15], 
         UndirectedEdge[25, 26], 
         UndirectedEdge[28, 29], 
         UndirectedEdge[26, 27], 
         UndirectedEdge[19, 25], 
         UndirectedEdge[7, 13], 
         UndirectedEdge[1, 7], 
         UndirectedEdge[15, 16], 
         UndirectedEdge[13, 14], 
         UndirectedEdge[1, 2], 
         UndirectedEdge[13, 19], 
         UndirectedEdge[16, 17], 
         UndirectedEdge[4, 5], 
         UndirectedEdge[3, 4]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 
       VertexCoordinates -> {{1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}, {
        1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 5}, {6, 5}, {1, 4}, {2, 4}, {3, 
        4}, {4, 4}, {5, 4}, {6, 4}, {1, 3}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {
        6, 3}, {1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {1, 1}, {2, 
        1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$2", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$14", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$16", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$16", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{1., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{20.59999999999999, 
       350.32271117118466`}, {-173.58671117118496`, 158.13599999999997`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["GSH - P Meshes", "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 "A GSH-P solution on a 5 x 5 mesh starting on vertex 1. The weight of this \
MLCP solution on an ",
 StyleBox["n x n",
  FontWeight->"Bold"],
 " mesh can be obtained using the following equation: \n\n\t\t",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    OverscriptBox["f", "^"], 
    RowBox[{"GSH", "-", "P"}]], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["n", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " - n - 1"
}], "Subsection"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36}, {
      Null, {{1, 2}, {1, 7}, {2, 3}, {2, 8}, {3, 4}, {3, 9}, {4, 5}, {4, 
       10}, {5, 6}, {5, 11}, {6, 12}, {7, 8}, {7, 13}, {8, 9}, {8, 14}, {9, 
       10}, {9, 15}, {10, 11}, {10, 16}, {11, 12}, {11, 17}, {12, 18}, {13, 
       14}, {13, 19}, {14, 15}, {14, 20}, {15, 16}, {15, 21}, {16, 17}, {16, 
       22}, {17, 18}, {17, 23}, {18, 24}, {19, 20}, {19, 25}, {20, 21}, {20, 
       26}, {21, 22}, {21, 27}, {22, 23}, {22, 28}, {23, 24}, {23, 29}, {24, 
       30}, {25, 26}, {25, 31}, {26, 27}, {26, 32}, {27, 28}, {27, 33}, {28, 
       29}, {28, 34}, {29, 30}, {29, 35}, {30, 36}, {31, 32}, {32, 33}, {33, 
       34}, {34, 35}, {35, 36}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[21, 27], 
         UndirectedEdge[16, 10], 
         UndirectedEdge[27, 28], 
         UndirectedEdge[14, 15], 
         UndirectedEdge[25, 26], 
         UndirectedEdge[19, 25], 
         UndirectedEdge[28, 22], 
         UndirectedEdge[7, 13], 
         UndirectedEdge[1, 7], 
         UndirectedEdge[15, 21], 
         UndirectedEdge[23, 29], 
         UndirectedEdge[1, 2], 
         UndirectedEdge[11, 17], 
         UndirectedEdge[20, 14], 
         UndirectedEdge[10, 11], 
         UndirectedEdge[26, 20], 
         UndirectedEdge[13, 19], 
         UndirectedEdge[17, 23], 
         UndirectedEdge[22, 16]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 
       VertexCoordinates -> {{1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}, {
        1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 5}, {6, 5}, {1, 4}, {2, 4}, {3, 
        4}, {4, 4}, {5, 4}, {6, 4}, {1, 3}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {
        6, 3}, {1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {1, 1}, {2, 
        1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$2", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$16", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$16", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{1., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{20.59999999999999, 
       350.32271117118466`}, {-173.5867111711849, 158.13599999999997`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Shared Vertex Meshes", "Section"],

Cell[CellGroupData[{

Cell[TextData[{
 "A shared vertex solution on a 5 x 5 mesh. The weight of this MLCP solution \
on an ",
 StyleBox["n x n",
  FontWeight->"Bold"],
 " mesh can be ",
 StyleBox["APPROXIMATED",
  FontWeight->"Bold"],
 " using the following equation: \n\n\t\t",
 Cell[BoxData[
  FormBox[
   SubscriptBox[
    OverscriptBox["f", "^"], "SV"], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   FractionBox["6", "10"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 Cell[BoxData[
  FormBox[
   SuperscriptBox["n", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " - n - 1"
}], "Subsection"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36}, {
      Null, {{1, 2}, {1, 7}, {2, 3}, {2, 8}, {3, 4}, {3, 9}, {4, 5}, {4, 
       10}, {5, 6}, {5, 11}, {6, 12}, {7, 8}, {7, 13}, {8, 9}, {8, 14}, {9, 
       10}, {9, 15}, {10, 11}, {10, 16}, {11, 12}, {11, 17}, {12, 18}, {13, 
       14}, {13, 19}, {14, 15}, {14, 20}, {15, 16}, {15, 21}, {16, 17}, {16, 
       22}, {17, 18}, {17, 23}, {18, 24}, {19, 20}, {19, 25}, {20, 21}, {20, 
       26}, {21, 22}, {21, 27}, {22, 23}, {22, 28}, {23, 24}, {23, 29}, {24, 
       30}, {25, 26}, {25, 31}, {26, 27}, {26, 32}, {27, 28}, {27, 33}, {28, 
       29}, {28, 34}, {29, 30}, {29, 35}, {30, 36}, {31, 32}, {32, 33}, {33, 
       34}, {34, 35}, {35, 36}}}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[10, 16], 
         UndirectedEdge[29, 30], 
         UndirectedEdge[8, 9], 
         UndirectedEdge[27, 28], 
         UndirectedEdge[28, 29], 
         UndirectedEdge[26, 27], 
         UndirectedEdge[9, 10], 
         UndirectedEdge[8, 14], 
         UndirectedEdge[14, 20], 
         UndirectedEdge[10, 11], 
         UndirectedEdge[16, 17], 
         UndirectedEdge[17, 18], 
         UndirectedEdge[20, 26]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 
       VertexCoordinates -> {{1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}, {
        1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 5}, {6, 5}, {1, 4}, {2, 4}, {3, 
        4}, {4, 4}, {5, 4}, {6, 4}, {1, 3}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {
        6, 3}, {1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {1, 1}, {2, 
        1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}}}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$11", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$14", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$11", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$16", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$16", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$18", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$17", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$22", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$24", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$26", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$28", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$29", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{1., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 6.}, 0.1], "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 5.}, 0.1], "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 4.}, 0.1], "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 3.}, 0.1], "DynamicName", BoxID -> "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 2.}, 0.1], "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{2., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{3., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{6., 1.}, 0.1], "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{20.59999999999999, 
       350.32271117118466`}, {-173.5867111711854, 158.13599999999997`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "Graphing the Results - Comparing the result of the heuristics (",
 Cell[BoxData[
  FormBox[
   OverscriptBox["f", "^"], TraditionalForm]]],
 ") to the amount of polygons on one side of the mesh (n)."
}], "Section"],

Cell[CellGroupData[{

Cell["MSCT and the reduced version:", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 15.}, {2., 24.}, {3., 35.}, {4., 48.}, {5., 63.}, {6., 
      80.}, {7., 99.}}]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 6.}, {2., 8.}, {3., 16.}, {4., 19.}, {5., 30.}, {6., 
      34.}, {7., 48.}}]}, 
    {RGBColor[0.560181, 0.691569, 0.194885], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 3.}, {2., 6.}, {3., 11.}, {4., 16.}, {5., 23.}, {6., 
      30.}, {7., 39.}}]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox["n", TraditionalForm], 
    FormBox[
     OverscriptBox["f", "^"], TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImageSize->{326., Automatic},
  Method->{},
  PlotRange->{{0, 7}, {0, 100}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0, 0}, {0, 0}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell["GSH-T and the reduced version:", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 8.}, {2., 15.}, {3., 24.}, {4., 35.}, {5., 48.}, {6., 
      63.}, {7., 80.}}]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 6.}, {2., 8.}, {3., 16.}, {4., 19.}, {5., 30.}, {6., 
      34.}, {7., 48.}}]}, 
    {RGBColor[0.560181, 0.691569, 0.194885], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 3.}, {2., 6.}, {3., 11.}, {4., 16.}, {5., 23.}, {6., 
      30.}, {7., 39.}}]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox["n", TraditionalForm], 
    FormBox[
     OverscriptBox["f", "^"], TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 7}, {0, 100}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0, 0}, {0, 0}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell["GSH-P and the shared vertex heuristics:", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 6.}, {2., 11.}, {3., 19.}, {4., 29.}, {5., 41.}, {6., 
      55.}, {7., 71.}}]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 5.}, {2., 6.}, {3., 13.}, {4., 16.}, {5., 26.}, {6., 
      30.}, {7., 43.}}]}, 
    {RGBColor[0.560181, 0.691569, 0.194885], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 3.}, {2., 6.}, {3., 11.}, {4., 16.}, {5., 23.}, {6., 
      30.}, {7., 39.}}]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox["n", TraditionalForm], 
    FormBox[
     OverscriptBox["f", "^"], TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 7}, {0, 100}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0, 0}, {0, 0}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "Graphing the Results - Obtaining the Approximation Ratio (\[Rho] = ",
 Cell[BoxData[
  FormBox[
   FractionBox[
    RowBox[{"\n", 
     OverscriptBox["f", "^"]}], 
    SuperscriptBox["f", "*"]], TraditionalForm]]],
 ") to the amount of polygons on one side of the mesh (n)."
}], "Section"],

Cell[CellGroupData[{

Cell["MSCT and the reduced version:", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 5.}, {2., 4.}, {3., 3.1818181818181817`}, {4., 3.}, {5., 
      2.739130434782609}, {6., 2.6666666666666665`}, {7., 
      2.5384615384615383`}}]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 2.}, {2., 1.3333333333333333`}, {3., 
      1.4545454545454546`}, {4., 1.1875}, {5., 1.3043478260869565`}, {6., 
      1.1333333333333333`}, {7., 1.2307692307692308`}}]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox["n", TraditionalForm], 
    FormBox["\[Rho]", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 7}, {0, 5}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0, 0}, {0, 0}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell["GSH-T and the reduced version:", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 2.6666666666666665`}, {2., 2.5}, {3., 
      2.1818181818181817`}, {4., 2.1875}, {5., 2.0869565217391304`}, {6., 
      2.1}, {7., 2.051282051282051}}]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 2.}, {2., 1.3333333333333333`}, {3., 
      1.4545454545454546`}, {4., 1.1875}, {5., 1.3043478260869565`}, {6., 
      1.1333333333333333`}, {7., 1.2307692307692308`}}]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox["n", TraditionalForm], 
    FormBox["\[Rho]", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 7}, {0, 5}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0, 0}, {0, 0}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell["GSH-P and the shared vertex heuristics:", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 2.}, {2., 1.8333333333333333`}, {3., 
      1.7272727272727273`}, {4., 1.8125}, {5., 1.7826086956521738`}, {6., 
      1.8333333333333333`}, {7., 1.8205128205128205`}}]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 1.6666666666666667`}, {2., 1.}, {3., 
      1.1818181818181819`}, {4., 1.}, {5., 1.1304347826086956`}, {6., 1.}, {
      7., 1.1025641025641026`}}]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox["n", TraditionalForm], 
    FormBox["\[Rho]", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 7}, {0, 5}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0, 0}, {0, 0}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Graphing the Results - Timing Comparison for each algorithms on same axes.\
\>", "Section"],

Cell[CellGroupData[{

Cell["MSCT (almost constant in this view) and the reduced version:", \
"Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 0.}, {2., 0.}, {3., 0.}, {4., 0.}, {5., 0.}, {6., 0.}, {7.,
       0.}}]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 0.015625}, {2., 0.046875}, {3., 0.15625}, {4., 0.390625}, {
      5., 0.84375}, {6., 1.640625}, {7., 3.03125}}]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{"", 
    FormBox["seconds", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 7}, {0, 5}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0, 0}, {0, 0}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell["GSH-T and the reduced version:", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 0.015625}, {2., 0.03125}, {3., 0.140625}, {4., 0.484375}, {
      5., 1.3125}, {6., 3.203125}, {6.406360424028269, 5.}}]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 0.015625}, {2., 0.0625}, {3., 0.203125}, {4., 0.65625}, {
      5., 1.671875}, {6., 4.078125}, {6.182098765432099, 5.}}]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{"", 
    FormBox["seconds", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 7}, {0, 5}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0, 0}, {0, 0}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell["GSH-P and the shared vertex heuristics:", "Subsection"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 0.}, {2., 0.015625}, {3., 0.015625}, {4., 0.046875}, {5., 
      0.109375}, {6., 0.171875}, {7., 0.3125}}]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     LineBox[{{1., 0.}, {2., 0.}, {3., 0.015625}, {4., 0.015625}, {5., 
      0.140625}, {6., 0.203125}, {7., 0.921875}}]}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{"", 
    FormBox["seconds", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 7}, {0, 5}},
  PlotRangeClipping->True,
  PlotRangePadding->{{0, 0}, {0, 0}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{944, 981},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
CellContext->Notebook,
TrackCellChangeTimes->False,
FrontEndVersion->"10.0 for Microsoft Windows (64-bit) (July 1, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 54, 0, 90, "Title"],
Cell[1543, 37, 543, 10, 187, "Subsection"],
Cell[CellGroupData[{
Cell[2111, 51, 30, 0, 49, "Section"],
Cell[CellGroupData[{
Cell[2166, 55, 111, 3, 43, "Subsection"],
Cell[2280, 60, 14786, 300, 374, "Output"]
}, Open  ]],
Cell[17081, 363, 594, 22, 133, "Subsection"],
Cell[17678, 387, 168, 4, 83, "Subsection"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17883, 396, 30, 0, 63, "Section"],
Cell[CellGroupData[{
Cell[17938, 400, 635, 23, 127, "Subsection"],
Cell[18576, 425, 41056, 1088, 374, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[59669, 1518, 1019, 39, 164, "Subsection"],
Cell[60691, 1559, 29198, 686, 374, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[89938, 2251, 33, 0, 63, "Section"],
Cell[CellGroupData[{
Cell[89996, 2255, 490, 17, 121, "Subsection"],
Cell[90489, 2274, 30589, 734, 374, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[121115, 3013, 1046, 39, 164, "Subsection"],
Cell[122164, 3054, 29202, 686, 374, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[151415, 3746, 33, 0, 63, "Section"],
Cell[CellGroupData[{
Cell[151473, 3750, 494, 17, 121, "Subsection"],
Cell[151970, 3769, 29728, 704, 374, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[181747, 4479, 39, 0, 63, "Section"],
Cell[CellGroupData[{
Cell[181811, 4483, 607, 23, 129, "Subsection"],
Cell[182421, 4508, 28685, 668, 374, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[211155, 5182, 232, 6, 102, "Section"],
Cell[CellGroupData[{
Cell[211412, 5192, 51, 0, 43, "Subsection"],
Cell[211466, 5194, 1347, 33, 232, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[212850, 5232, 52, 0, 43, "Subsection"],
Cell[212905, 5234, 1314, 32, 253, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[214256, 5271, 61, 0, 43, "Subsection"],
Cell[214320, 5273, 1314, 32, 253, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[215683, 5311, 307, 9, 148, "Section"],
Cell[CellGroupData[{
Cell[216015, 5324, 51, 0, 43, "Subsection"],
Cell[216069, 5326, 1239, 29, 259, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[217345, 5360, 52, 0, 43, "Subsection"],
Cell[217400, 5362, 1245, 29, 259, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[218682, 5396, 61, 0, 43, "Subsection"],
Cell[218746, 5398, 1241, 29, 259, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[220036, 5433, 101, 2, 97, "Section"],
Cell[CellGroupData[{
Cell[220162, 5439, 84, 1, 43, "Subsection"],
Cell[220249, 5442, 1078, 26, 254, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[221364, 5473, 52, 0, 43, "Subsection"],
Cell[221419, 5475, 1135, 26, 254, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[222591, 5506, 61, 0, 43, "Subsection"],
Cell[222655, 5508, 1103, 26, 254, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature zxpgyfbzEz5KeC1pZntJDUY# *)
