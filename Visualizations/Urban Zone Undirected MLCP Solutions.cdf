(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[    807652,      19608]
NotebookOptionsPosition[    806104,      19534]
NotebookOutlinePosition[    806562,      19554]
CellTagsIndexPosition[    806519,      19551]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\<\
MLCP Solution Visualizations: Undirected Quer\[EAcute]taro Urban Zone \
Solutions\
\>", "Title"],

Cell["\<\
Every unreduced and reduced solution for the undirected urban zone graph is \
displayed here. For algorithms that produce multiple solutions, only the best \
solution is shown here. Unreduced solutions are displayed in red, reduced \
solutions (if they exist) are displayed in blue.\
\>", "Subsection"],

Cell[CellGroupData[{

Cell["Undirected Quer\[EAcute]taro Urban Zone Dataset - Base Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
       74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
       92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104}, {
      Null, CompressedData["
1:eJwVxdV2ggAAAFBF7O5ARQwsDEBRUQwsyhY7sfa2D9tPbnu45yLK91IBVCrV
z5//1YAaBDQAqNFq7KANDGt1WqtOrzPpjXqXQW8wGpxGk9FhspgtZqvZY/Fa
3Ta7DbK7HU6nzxVwRd1Bj9cT8/p9AV/EHwmEglAQDkVDCBSG4HAqgkaTcAzO
xdJIHEGRRLyUSCaIZDaVSxXQNFpMZ9LlTD6DZ4lsP4flsXwFK2C1QrNYLlZL
ZKlaxss0XsEZgiQokiLrlVplUKWrPWpIjWuNWrfeq4uNdoOlGVpotprzVqc1
ZQbMrN1pjzpcl+3yPaG3Ykfsoj/uXwb8QB5Kw8mIGy3HEjfl1vyC3wsz4ShO
xJWoSBPpPp1PNzN5dppvF8vFYble7VbP9Ubey9fNdnveHXev/WF/O5yPp9P1
9D4/Lvfr7fq5Pe6K8nx8PV+v9/vz+foFCE1JZg==
       "]}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, EdgeWeight -> CompressedData["
1:eJwN0fk7FAgABmBHrsassZFtiw5RmcQmKjm+EhUpHUTGNePIEXPPYGLMYNxH
ytFBtKF1lZVUtLSh0j5soYiENuvYypHcbT+8f8G7nhpy3EdaSkqq5LuXEz6m
0abxmG/dTbvVzUa2sVWgKD4Sauwflh7fiUXmjjZblcIYnJMLahmyiIfG0Klv
/8qdB+dSUFTdwUvofJplLFFJQ7NMyrM+KRFmWbXkvUVhiNdc2yy1modznMW6
zb50FHYRXj1cSsfA8ot5ZhUh8LCbSk9SomCT7inZKmkh6k6lDGhMhKPDKKSp
sEKAKycjRigCFkip+8l/uzDQsXxhfs4mCKK0k0+c7cMgr62g2+8QigLSuh02
xHMwUnco6r4fiheEqt+IiwkwnBjP2WsvwL4eioZMtjcCV/nEOWm4o7/SVpNg
ysKZQcWno80SVLtOjWs7h0M99O4Ts2A6yj09zmtIxHDQu33Pyl4E+YA9f7IH
2JgbksulmMagvbW3ROVIJJQOWhwyd2eif6Go/J1uAGrV8hwNOphwcqVVsi2i
MNXS+TZdQYiOy2pxZF4yXlx1K8u8yYR4+FU1mSLEh7FZnZLdsfCKsWaFfxJg
l87Gl+POEph//G96RWs8PP22re7ti4bcjKpN2R0n5AW7OBt0pGPCLuBMzn46
ckkV1vOTCTAo5Qp6RyKxN7CQEbMiBcoXRKN66VEYLmNQsxmp6FOMWmMaIUQP
YfuYoygS9e3E4AD3CExVubVM1lARqzOroXdYiD92VlYbz/Lg/XJebno0Aq6d
g2aUaxLM9Rhb+NRmQOvyPRu2+wkEj67q4U/xEan43rzOT4jEMKu21w3xMKjM
sc6/KMaPzEVaFl2MH+7u2ZxnIsSDLNddXg488J4rL6v+/tGlfn5fxapEfEna
wp6Jp2EjdXilnzYfVgaSdfs4XJiw05VLrfmYK53XdC+koKtL1pn8IhgzB3hV
vg4MjPTOuO5qSYCwKtvloAkXCto3a6+/5cMzc6lxPTEKX5U19Rs3s3DA/s3I
5F8M9JUz5pZJxaImo/gseYs33in3GZPtvKFE2tz+uIINyy/6jwRMDs4NG2q1
zAaDLO1o6ehDg/iiy+6m11Twhg51bjNhoeTqfcPk1AAQ2b2W+ZqheDjd99nc
jwrtMXZ2gQIT3J99jtTKc3GMFKTntJWF2scL8X1HaIi5J6JlLPlAOk5+cMVi
ANzsRhM9jvoin8JTtPULQaTWDRybjEJL8sfU7AwOnC3GxSKlEATVb8jrVw+B
btKZAXZ4LEaGZ5+2t0lQY3sr26OcBVk/u8SvaixsfaV7gzzpC0rdHj1VIg2n
C4zeq/l54U2r2D9X9SzGavKojXIR4BaZZd7I9USCkcmj5w8D0bChhsQm+mOr
b7ROeQcD+73fXLk+TkP1SpPmJ3QubpjkVESqpuKXFtm1lWI2cqvbf9L7loYt
lh9KG5w8cDx3sEmclACZBfniHct8cTqz0WjAPgBlVz0/DDSzQXWiP6835CO/
XfEZdz4Iny09aGqEYPh/Ozjj/YCOpk9btQUHvNBUrJXaZMlCxa9pihXaaWB2
6+RdrveAzwRJcWQdE1rp11/RJgU49fJ5zeHe0zj5jG1Ot4iE2+HmwtJMOrbt
qHe/H5MC3u1D+aa/n8XarqvdXa+F0DkxFP1PEx+jR2VMLY75w6xAQCJtYmHT
gcTPDT2JsBQ+yikuiMVTff3SzOQLaCf6XrtHoGJpp/MDwrQEIVaM944uQlAI
2wOPmCTCU9SWk7iGAw9O7pf0W+FwzIpKbszkgz/9QCWLGQa9uJzcF2IOFuQ0
ajkWLPwP2e946w==
        "], VertexCoordinates -> CompressedData["
1:eJwt1Hs0lHkcBvDBiUlSolxDNZouSo2XMtheuVaThtZlOqTpJFJis5nGYa2N
XFIUJlG5xNayp0yyYlKDapMiayK51Ig0uUVZVLTPe87+MWfO5zzP9/v+zvvO
O8v2h3kGKtNotGB8qO+lRkM7NQx50gcnnpWy8whyiltolmbAk3LkDuWDVwhy
Y07WsCOctDZoNBWeqHqVMgPbz9djxMBuYXfSuvR50kVJ7RPz4LYG/RRbWDu6
9XPHZYK0CRiqmdLjSUdV0l89gEP7qlf3wyvZjeZbYQvLI5kTcOwO2uxq2NVH
McLG/OiN8mXfLxFk10eJQgRv+p1pPgYPB3YeOgwPmjix+uDcYwmCKrjgUHJW
C6wiyHcX4nwRa/RpJfDBeyfzNsOdXdlXymBFTsOmTlxPEVu1ZB1cq7TOvxYu
lipns+AIt5CCAzBxhd5rB+f5Xnsyif3mWtdnHWBnmVNeGhxFvMiYziXInrJQ
j4NwZl/YxgnY4ZONQIJ5ZrJZ/wjsppvrrY5c5Hj6yAD8aDqXm4Tc9KOUVgeL
bvcaquN804X2e91guUztvAC5csdgtwk8W916mYk8WKH+lxZcYFEzUwVbCZ+E
VOcQJGeXb68S9nf45xdJ4eHKyEIG5isSa7iUg58yPsxBfxHHqlwETxn5nk1H
f1eDjTgRTl1slX0K/YBKeurERYJMl9B3Z8Im2qufvYMj9TJm98JflaPkTbDn
gpb3mrDD/Nd11fB6A5ZxMfbXL2i6dhaWLfSassT+JAttZzd4ZFjU9ytMe7fn
Ng1++7XXMwte/mZhiSybII3mEi9lujxpnZXg832Ye+bC0UXY12bC1yuD1WIU
w83o849OHsuDO3uObtNEXlc0NRVPzTMThNTv9W665gI+LFNvnTRA36x5ImYz
XNxhp3kS5+X8fSlo6AJBWme+Oh2M3JEWPtQBJ29trI6FdV2iPCRwWGByEwv9
a4pylRuwjuIXdwfYcjbmUQJ8U+gVVIF+ibPTRiG8/aLsuSrc5dax/BBcnN+5
wwD9mt1Kk3awBnn1ZwX1e/YWvTeHtTVrHtYjZ/movlCFebqVLTTqfWo5Llej
9gti7/qgP5FntWVWRJDT203NIpE/1TAMnIHXD3j6ecHXvx0W9sPBJmunQtF3
UWPwX8NkT6n3SuQZ4cvS3sCzms5xP8AVtcnv2+GG732jPeiz6ew9L+Anifkh
n3Ge/dOfQmvhsHyp7TTyeXwrURkcsmF+vSueD1uc+K8YtuZdNS5EzlkyaXIR
Zqz5cZMu5u1d+ySZMNKWpbge/+0XQQysPdY2XYl5M3FRF2U2o4LLQr7KValw
Hyx9vsKFer7nmNvaObCLhm3qNrj9cBSXCzfrsG+KsV+37YzAFQ43s7VWho9H
sjZvh5WSJFGL4Z3rH0nZsLH89htn7LfTmfPFCXambgz2RfNZXhvg4t9SSgKo
831t0DOBPeJlw0PI8w0suhlwybj9Ci3kf3C7/b5lEWRcS/8zOfbn10cLlZAb
0RuUqfsbUTB+sx85nm7AecyPuXoEjcAP/9R1tEFumxIha4Hl+8L2u8H0UnF8
Rdb/7w/6lvUuvPuw+oHIJTVwIrEm/BYsDmNqyWGLL+r/pMIGyc/p1nD8x+C3
SfDO4PO+nvAt1+v6nnDzhopi6n143NT5ciiTIO82Okczcb/Lo/1PfYf9zvqY
RqG/l2PZPQgr4tJqU6jruTdLxDB/94DHGPqtQWp+PnBphqUO9f9zR9xx7kMG
QY6rqTRT59vyE23mHhxblKRRiryHZvm6FnaPMRUOwI+zJaJ0+JQ3M2gV+j0Z
iQl7YGPDuBP+sAcHE/Ct8cZ9qdT/aU6Tuyr8H1Ql8gA=
        "]}]]}, 
    TagBox[GraphicsGroupBox[{
       {Hue[0.6, 0.7, 0.5], Opacity[0.7], 
        {Arrowheads[0.], 
         ArrowBox[{{-100.422626, 20.621989}, {-100.414987, 20.620784}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.422626, 20.621989}, {-100.421725, 20.619097}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.414987, 20.620784}, {-100.410396, 20.619338}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.414987, 20.620784}, {-100.421725, 20.619097}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.410396, 20.619338}, {-100.404087, 20.61737}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.410396, 20.619338}, {-100.409706, 20.61207}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.421725, 20.619097}, {-100.413654, 20.611969}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.421725, 20.619097}, {-100.417602, 20.604407}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404087, 20.61737}, {-100.394259, 20.616607}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404087, 20.61737}, {-100.401945, 20.612573}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394259, 20.616607}, {-100.390182, 20.616285}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394259, 20.616607}, {-100.394222, 20.613268}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.390182, 20.616285}, {-100.388987, 20.614291}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.390182, 20.616285}, {-100.389989, 20.613922}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.390182, 20.616285}, {-100.381142, 20.610304}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.388987, 20.614291}, {-100.389989, 20.613922}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.388987, 20.614291}, {-100.386802, 20.610147}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.389989, 20.613922}, {-100.394222, 20.613268}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.389989, 20.613922}, {-100.38838, 20.610063}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.394222, 20.613268}, {-100.397181, 20.612857}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399836, 20.613055}, {-100.397181, 20.612857}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399836, 20.613055}, {-100.401945, 20.612573}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399836, 20.613055}, {-100.399391, 20.609277}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.397181, 20.612857}, {-100.39687, 20.6092101}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.401945, 20.612573}, {-100.405749, 20.610382}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.413654, 20.611969}, {-100.409706, 20.61207}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.413654, 20.611969}, {-100.408668, 20.605739}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.409706, 20.61207}, {-100.405749, 20.610382}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.38838, 20.610063}, {-100.386802, 20.610147}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.386802, 20.610147}, {-100.386442, 20.609114}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.381142, 20.610304}, {-100.38332, 20.608429}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.381142, 20.610304}, {-100.377236, 20.604497}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.405749, 20.610382}, {-100.406867, 20.606573}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399391, 20.609277}, {-100.39687, 20.6092101}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399391, 20.609277}, {-100.399448, 20.603633}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39687, 20.6092101}, {-100.391256, 20.609}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.386442, 20.609114}, {-100.38332, 20.608429}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.386442, 20.609114}, {-100.39074, 20.604502}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391256, 20.609}, {-100.39074, 20.604502}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.38332, 20.608429}, {-100.382298, 20.605955}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.406867, 20.606573}, {-100.408668, 20.605739}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.406867, 20.606573}, {-100.406611, 20.603862}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.382298, 20.605955}, {-100.377236, 20.604497}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.382298, 20.605955}, {-100.383215, 20.601403}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.408668, 20.605739}, {-100.417602, 20.604407}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.408668, 20.605739}, {-100.406611, 20.603862}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.417602, 20.604407}, {-100.416328, 20.599833}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39074, 20.604502}, {-100.393797, 20.598759}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.377236, 20.604497}, {-100.37591, 20.600506}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.406611, 20.603862}, {-100.399448, 20.603633}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.406611, 20.603862}, {-100.407375, 20.596437}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399448, 20.603633}, {-100.398492, 20.597667}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.383215, 20.601403}, {-100.384053, 20.601178}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.383215, 20.601403}, {-100.380798, 20.600813}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.383215, 20.601403}, {-100.393797, 20.598759}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.384053, 20.601178}, {-100.382999, 20.593451}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.380798, 20.600813}, {-100.37591, 20.600506}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.380798, 20.600813}, {-100.378101, 20.592499}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.37591, 20.600506}, {-100.372366, 20.596679}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.416328, 20.599833}, {-100.407375, 20.596437}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.416328, 20.599833}, {-100.415007, 20.595231}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393797, 20.598759}, {-100.398492, 20.597667}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393797, 20.598759}, {-100.392271, 20.594612}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398492, 20.597667}, {-100.399739, 20.596995}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398492, 20.597667}, {-100.397006, 20.593169}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399739, 20.596995}, {-100.403316, 20.596183}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399739, 20.596995}, {-100.398421, 20.592702}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.372366, 20.596679}, {-100.378101, 20.592499}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.372366, 20.596679}, {-100.363755, 20.588653}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.407375, 20.596437}, {-100.407122, 20.595779}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.403316, 20.596183}, {-100.407122, 20.595779}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.403316, 20.596183}, {-100.401697, 20.591693}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.407122, 20.595779}, {-100.415007, 20.595231}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.407122, 20.595779}, {-100.404874, 20.590529}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.415007, 20.595231}, {-100.413461, 20.589804}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.392271, 20.594612}, {-100.397006, 20.593169}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.392271, 20.594612}, {-100.391041, 20.591448}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.382999, 20.593451}, {-100.378986, 20.591885}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.382999, 20.593451}, {-100.391041, 20.591448}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.397006, 20.593169}, {-100.398421, 20.592702}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.397006, 20.593169}, {-100.39598, 20.589817}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398421, 20.592702}, {-100.401697, 20.591693}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398421, 20.592702}, {-100.397337, 20.589346}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.378101, 20.592499}, {-100.378986, 20.591885}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.378101, 20.592499}, {-100.376605, 20.590774}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.378986, 20.591885}, {-100.376605, 20.590774}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.378986, 20.591885}, {-100.386949, 20.589948}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.401697, 20.591693}, {-100.404874, 20.590529}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.401697, 20.591693}, {-100.40052, 20.588274}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391041, 20.591448}, {-100.39598, 20.589817}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391041, 20.591448}, {-100.389918, 20.588889}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.376605, 20.590774}, {-100.376417, 20.588207}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.376605, 20.590774}, {-100.370604, 20.587858}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404874, 20.590529}, {-100.4063, 20.589964}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404874, 20.590529}, {-100.404232, 20.589193}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.386949, 20.589948}, {-100.389918, 20.588889}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.386949, 20.589948}, {-100.385845, 20.587028}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.4063, 20.589964}, {-100.40859, 20.589364}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.4063, 20.589964}, {-100.405837, 20.588632}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39598, 20.589817}, {-100.397337, 20.589346}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39598, 20.589817}, {-100.395114, 20.587163}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.413461, 20.589804}, {-100.41146, 20.589439}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.413461, 20.589804}, {-100.412089, 20.586412}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.41146, 20.589439}, {-100.410065, 20.589232}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.41146, 20.589439}, {-100.410515, 20.58699}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.397337, 20.589346}, {-100.40052, 20.588274}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.397337, 20.589346}, {-100.396554, 20.586675}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.40859, 20.589364}, {-100.410065, 20.589232}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.40859, 20.589364}, {-100.40807, 20.587852}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.410065, 20.589232}, {-100.409321, 20.587412}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404232, 20.589193}, {-100.405837, 20.588632}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404232, 20.589193}, {-100.403276, 20.58711}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.389918, 20.588889}, {-100.395114, 20.587163}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.389918, 20.588889}, {-100.388782, 20.58596}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.405837, 20.588632}, {-100.40807, 20.587852}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.405837, 20.588632}, {-100.405086, 20.5865}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.363755, 20.588653}, {-100.370604, 20.587858}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.363755, 20.588653}, {-100.361785, 20.582024}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.40052, 20.588274}, {-100.403276, 20.58711}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.40052, 20.588274}, {-100.399559, 20.585669}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.376417, 20.588207}, {-100.375135, 20.58707}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.376417, 20.588207}, {-100.376311, 20.586834}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.40807, 20.587852}, {-100.409321, 20.587412}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.40807, 20.587852}, {-100.407296, 20.585882}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.370604, 20.587858}, {-100.375135, 20.58707}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.409321, 20.587412}, {-100.410515, 20.58699}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.409321, 20.587412}, {-100.40857, 20.585536}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.403276, 20.58711}, {-100.405086, 20.5865}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.403276, 20.58711}, {-100.402166, 20.584754}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395114, 20.587163}, {-100.396554, 20.586675}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395114, 20.587163}, {-100.393923, 20.58412}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.385845, 20.587028}, {-100.376311, 20.586834}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.385845, 20.587028}, {-100.388782, 20.58596}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.385845, 20.587028}, {-100.382268, 20.577798}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.375135, 20.58707}, {-100.376311, 20.586834}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.375135, 20.58707}, {-100.374104, 20.579458}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.410515, 20.58699}, {-100.412089, 20.586412}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.410515, 20.58699}, {-100.409745, 20.585233}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396554, 20.586675}, {-100.399559, 20.585669}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.396554, 20.586675}, {-100.395651, 20.583284}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.412089, 20.586412}, {-100.410668, 20.584801}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.405086, 20.5865}, {-100.407296, 20.585882}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.405086, 20.5865}, {-100.404362, 20.58418}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.407296, 20.585882}, {-100.40857, 20.585536}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.388782, 20.58596}, {-100.391535, 20.584951}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.388782, 20.58596}, {-100.38544, 20.577177}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.40857, 20.585536}, {-100.409745, 20.585233}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399559, 20.585669}, {-100.402166, 20.584754}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399559, 20.585669}, {-100.398079, 20.581724}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.409745, 20.585233}, {-100.410668, 20.584801}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.410668, 20.584801}, {-100.407144, 20.581739}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391535, 20.584951}, {-100.393923, 20.58412}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.391535, 20.584951}, {-100.389128, 20.576425}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.402166, 20.584754}, {-100.404362, 20.58418}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.402166, 20.584754}, {-100.399094, 20.580837}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.404362, 20.58418}, {-100.407144, 20.581739}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393923, 20.58412}, {-100.395651, 20.583284}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395651, 20.583284}, {-100.398079, 20.581724}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.395651, 20.583284}, {-100.393091, 20.5756}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.407144, 20.581739}, {-100.40221, 20.577133}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.361785, 20.582024}, {-100.374104, 20.579458}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398079, 20.581724}, {-100.399094, 20.580837}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.398079, 20.581724}, {-100.39633, 20.574997}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.399094, 20.580837}, {-100.40221, 20.577133}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.374104, 20.579458}, {-100.382268, 20.577798}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.382268, 20.577798}, {-100.38544, 20.577177}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.40221, 20.577133}, {-100.399193, 20.574315}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.38544, 20.577177}, {-100.389128, 20.576425}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.389128, 20.576425}, {-100.393091, 20.5756}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.393091, 20.5756}, {-100.39633, 20.574997}}, 
          0.0007745059299999538]}, 
        {Arrowheads[0.], 
         ArrowBox[{{-100.39633, 20.574997}, {-100.399193, 20.574315}}, 
          0.0007745059299999538]}}, 
       {Hue[0.6, 0.2, 0.8], EdgeForm[{GrayLevel[0], Opacity[
        0.7]}], {DiskBox[{-100.422626, 20.621989}, 0.00007049631196042688], 
         InsetBox[
          StyleBox["1",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.42255550368803`, 20.62205949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.414987, 20.620784}, 0.00007049631196042688], InsetBox[
          StyleBox["2",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41491650368803`, 20.62085449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.410396, 20.619338}, 0.00007049631196042688], InsetBox[
          StyleBox["3",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41032550368804`, 20.61940849631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.421725, 20.619097}, 0.00007049631196042688], InsetBox[
          StyleBox["4",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.42165450368803`, 20.61916749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.404087, 20.61737}, 0.00007049631196042688], InsetBox[
          StyleBox["5",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40401650368804`, 20.61744049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.394259, 20.616607}, 0.00007049631196042688], InsetBox[
          StyleBox["6",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39418850368804`, 20.616677496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.390182, 20.616285}, 0.00007049631196042688], InsetBox[
          StyleBox["7",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39011150368803`, 20.61635549631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.388987, 20.614291}, 0.00007049631196042688], InsetBox[
          StyleBox["8",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38891650368804`, 20.61436149631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.389989, 20.613922}, 0.00007049631196042688], InsetBox[
          StyleBox["9",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38991850368804`, 20.61399249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.394222, 20.613268}, 0.00007049631196042688], InsetBox[
          StyleBox["10",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39415150368804`, 20.61333849631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399836, 20.613055}, 0.00007049631196042688], InsetBox[
          StyleBox["11",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39976550368803`, 20.61312549631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.397181, 20.612857}, 0.00007049631196042688], InsetBox[
          StyleBox["12",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39711050368804`, 20.61292749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.401945, 20.612573}, 0.00007049631196042688], InsetBox[
          StyleBox["13",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40187450368803`, 20.61264349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.413654, 20.611969}, 0.00007049631196042688], InsetBox[
          StyleBox["14",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41358350368803`, 20.612039496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.409706, 20.61207}, 0.00007049631196042688], InsetBox[
          StyleBox["15",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40963550368804`, 20.61214049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.38838, 20.610063}, 0.00007049631196042688], InsetBox[
          StyleBox["16",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38830950368803`, 20.61013349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.386802, 20.610147}, 0.00007049631196042688], InsetBox[
          StyleBox["17",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38673150368804`, 20.61021749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.381142, 20.610304}, 0.00007049631196042688], InsetBox[
          StyleBox["18",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38107150368803`, 20.61037449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.405749, 20.610382}, 0.00007049631196042688], InsetBox[
          StyleBox["19",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40567850368804`, 20.61045249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399391, 20.609277}, 0.00007049631196042688], InsetBox[
          StyleBox["20",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39932050368803`, 20.60934749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.39687, 20.6092101}, 0.00007049631196042688], InsetBox[
          StyleBox["21",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39679950368804`, 20.609280596311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.386442, 20.609114}, 0.00007049631196042688], InsetBox[
          StyleBox["22",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38637150368804`, 20.60918449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.391256, 20.609}, 0.00007049631196042688], InsetBox[
          StyleBox["23",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39118550368804`, 20.60907049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.38332, 20.608429}, 0.00007049631196042688], InsetBox[
          StyleBox["24",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38324950368803`, 20.60849949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.406867, 20.606573}, 0.00007049631196042688], InsetBox[
          StyleBox["25",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40679650368804`, 20.60664349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.382298, 20.605955}, 0.00007049631196042688], InsetBox[
          StyleBox["26",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38222750368804`, 20.60602549631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.408668, 20.605739}, 0.00007049631196042688], InsetBox[
          StyleBox["27",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40859750368804`, 20.60580949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.417602, 20.604407}, 0.00007049631196042688], InsetBox[
          StyleBox["28",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41753150368804`, 20.604477496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.39074, 20.604502}, 0.00007049631196042688], InsetBox[
          StyleBox["29",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39066950368803`, 20.60457249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.377236, 20.604497}, 0.00007049631196042688], InsetBox[
          StyleBox["30",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37716550368803`, 20.604567496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.406611, 20.603862}, 0.00007049631196042688], InsetBox[
          StyleBox["31",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40654050368803`, 20.60393249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399448, 20.603633}, 0.00007049631196042688], InsetBox[
          StyleBox["32",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39937750368804`, 20.603703496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.383215, 20.601403}, 0.00007049631196042688], InsetBox[
          StyleBox["33",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38314450368804`, 20.60147349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.384053, 20.601178}, 0.00007049631196042688], InsetBox[
          StyleBox["34",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38398250368803`, 20.60124849631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.380798, 20.600813}, 0.00007049631196042688], InsetBox[
          StyleBox["35",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38072750368804`, 20.60088349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.37591, 20.600506}, 0.00007049631196042688], InsetBox[
          StyleBox["36",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37583950368804`, 20.60057649631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.416328, 20.599833}, 0.00007049631196042688], InsetBox[
          StyleBox["37",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41625750368803`, 20.59990349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.393797, 20.598759}, 0.00007049631196042688], InsetBox[
          StyleBox["38",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39372650368804`, 20.59882949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.398492, 20.597667}, 0.00007049631196042688], InsetBox[
          StyleBox["39",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39842150368804`, 20.59773749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399739, 20.596995}, 0.00007049631196042688], InsetBox[
          StyleBox["40",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39966850368803`, 20.59706549631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.372366, 20.596679}, 0.00007049631196042688], InsetBox[
          StyleBox["41",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37229550368804`, 20.59674949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.407375, 20.596437}, 0.00007049631196042688], InsetBox[
          StyleBox["42",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40730450368804`, 20.59650749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.403316, 20.596183}, 0.00007049631196042688], InsetBox[
          StyleBox["43",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40324550368804`, 20.59625349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.407122, 20.595779}, 0.00007049631196042688], InsetBox[
          StyleBox["44",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40705150368804`, 20.59584949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.415007, 20.595231}, 0.00007049631196042688], InsetBox[
          StyleBox["45",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41493650368804`, 20.595301496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.392271, 20.594612}, 0.00007049631196042688], InsetBox[
          StyleBox["46",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39220050368803`, 20.59468249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.382999, 20.593451}, 0.00007049631196042688], InsetBox[
          StyleBox["47",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38292850368803`, 20.59352149631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.397006, 20.593169}, 0.00007049631196042688], InsetBox[
          StyleBox["48",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39693550368804`, 20.59323949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.398421, 20.592702}, 0.00007049631196042688], InsetBox[
          StyleBox["49",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39835050368804`, 20.59277249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.378101, 20.592499}, 0.00007049631196042688], InsetBox[
          StyleBox["50",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37803050368804`, 20.59256949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.378986, 20.591885}, 0.00007049631196042688], InsetBox[
          StyleBox["51",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37891550368803`, 20.59195549631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.401697, 20.591693}, 0.00007049631196042688], InsetBox[
          StyleBox["52",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40162650368804`, 20.59176349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.391041, 20.591448}, 0.00007049631196042688], InsetBox[
          StyleBox["53",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39097050368804`, 20.59151849631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.376605, 20.590774}, 0.00007049631196042688], InsetBox[
          StyleBox["54",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37653450368803`, 20.59084449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.404874, 20.590529}, 0.00007049631196042688], InsetBox[
          StyleBox["55",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40480350368804`, 20.59059949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.386949, 20.589948}, 0.00007049631196042688], InsetBox[
          StyleBox["56",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38687850368804`, 20.59001849631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.4063, 20.589964}, 0.00007049631196042688], InsetBox[
          StyleBox["57",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40622950368804`, 20.590034496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.39598, 20.589817}, 0.00007049631196042688], InsetBox[
          StyleBox["58",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39590950368803`, 20.58988749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.413461, 20.589804}, 0.00007049631196042688], InsetBox[
          StyleBox["59",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41339050368803`, 20.58987449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.41146, 20.589439}, 0.00007049631196042688], InsetBox[
          StyleBox["60",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41138950368804`, 20.58950949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.397337, 20.589346}, 0.00007049631196042688], InsetBox[
          StyleBox["61",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39726650368803`, 20.58941649631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.40859, 20.589364}, 0.00007049631196042688], InsetBox[
          StyleBox["62",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40851950368804`, 20.58943449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.410065, 20.589232}, 0.00007049631196042688], InsetBox[
          StyleBox["63",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40999450368804`, 20.58930249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.404232, 20.589193}, 0.00007049631196042688], InsetBox[
          StyleBox["64",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40416150368803`, 20.58926349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.389918, 20.588889}, 0.00007049631196042688], InsetBox[
          StyleBox["65",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38984750368803`, 20.58895949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.405837, 20.588632}, 0.00007049631196042688], InsetBox[
          StyleBox["66",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40576650368804`, 20.58870249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.363755, 20.588653}, 0.00007049631196042688], InsetBox[
          StyleBox["67",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.36368450368803`, 20.58872349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.40052, 20.588274}, 0.00007049631196042688], InsetBox[
          StyleBox["68",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40044950368804`, 20.588344496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.376417, 20.588207}, 0.00007049631196042688], InsetBox[
          StyleBox["69",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37634650368804`, 20.58827749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.40807, 20.587852}, 0.00007049631196042688], InsetBox[
          StyleBox["70",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40799950368803`, 20.58792249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.370604, 20.587858}, 0.00007049631196042688], InsetBox[
          StyleBox["71",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37053350368804`, 20.58792849631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.409321, 20.587412}, 0.00007049631196042688], InsetBox[
          StyleBox["72",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40925050368804`, 20.58748249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.403276, 20.58711}, 0.00007049631196042688], InsetBox[
          StyleBox["73",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40320550368804`, 20.58718049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.395114, 20.587163}, 0.00007049631196042688], InsetBox[
          StyleBox["74",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39504350368804`, 20.58723349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.385845, 20.587028}, 0.00007049631196042688], InsetBox[
          StyleBox["75",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38577450368804`, 20.58709849631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.375135, 20.58707}, 0.00007049631196042688], InsetBox[
          StyleBox["76",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37506450368804`, 20.58714049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.376311, 20.586834}, 0.00007049631196042688], InsetBox[
          StyleBox["77",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37624050368804`, 20.58690449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.410515, 20.58699}, 0.00007049631196042688], InsetBox[
          StyleBox["78",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41044450368804`, 20.58706049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.396554, 20.586675}, 0.00007049631196042688], InsetBox[
          StyleBox["79",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39648350368803`, 20.58674549631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.412089, 20.586412}, 0.00007049631196042688], InsetBox[
          StyleBox["80",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41201850368803`, 20.58648249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.405086, 20.5865}, 0.00007049631196042688], InsetBox[
          StyleBox["81",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40501550368803`, 20.58657049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.407296, 20.585882}, 0.00007049631196042688], InsetBox[
          StyleBox["82",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40722550368804`, 20.58595249631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.388782, 20.58596}, 0.00007049631196042688], InsetBox[
          StyleBox["83",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38871150368804`, 20.58603049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.40857, 20.585536}, 0.00007049631196042688], InsetBox[
          StyleBox["84",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40849950368803`, 20.58560649631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399559, 20.585669}, 0.00007049631196042688], InsetBox[
          StyleBox["85",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39948850368803`, 20.58573949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.409745, 20.585233}, 0.00007049631196042688], InsetBox[
          StyleBox["86",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40967450368804`, 20.58530349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.410668, 20.584801}, 0.00007049631196042688], InsetBox[
          StyleBox["87",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.41059750368804`, 20.58487149631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.391535, 20.584951}, 0.00007049631196042688], InsetBox[
          StyleBox["88",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39146450368804`, 20.58502149631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.402166, 20.584754}, 0.00007049631196042688], InsetBox[
          StyleBox["89",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40209550368803`, 20.58482449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.404362, 20.58418}, 0.00007049631196042688], InsetBox[
          StyleBox["90",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40429150368804`, 20.58425049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.393923, 20.58412}, 0.00007049631196042688], InsetBox[
          StyleBox["91",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39385250368804`, 20.584190496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.395651, 20.583284}, 0.00007049631196042688], InsetBox[
          StyleBox["92",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39558050368804`, 20.58335449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.407144, 20.581739}, 0.00007049631196042688], InsetBox[
          StyleBox["93",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40707350368804`, 20.58180949631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.361785, 20.582024}, 0.00007049631196042688], InsetBox[
          StyleBox["94",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.36171450368803`, 20.58209449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.398079, 20.581724}, 0.00007049631196042688], InsetBox[
          StyleBox["95",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39800850368803`, 20.58179449631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399094, 20.580837}, 0.00007049631196042688], InsetBox[
          StyleBox["96",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39902350368804`, 20.58090749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.374104, 20.579458}, 0.00007049631196042688], InsetBox[
          StyleBox["97",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.37403350368804`, 20.57952849631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.382268, 20.577798}, 0.00007049631196042688], InsetBox[
          StyleBox["98",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38219750368803`, 20.57786849631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.40221, 20.577133}, 0.00007049631196042688], InsetBox[
          StyleBox["99",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.40213950368803`, 20.57720349631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.38544, 20.577177}, 0.00007049631196042688], InsetBox[
          StyleBox["100",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38536950368804`, 20.57724749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.389128, 20.576425}, 0.00007049631196042688], InsetBox[
          StyleBox["101",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.38905750368804`, 20.57649549631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.393091, 20.5756}, 0.00007049631196042688], InsetBox[
          StyleBox["102",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39302050368804`, 20.57567049631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.39633, 20.574997}, 0.00007049631196042688], InsetBox[
          StyleBox["103",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39625950368804`, 20.57506749631196}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {
         DiskBox[{-100.399193, 20.574315}, 0.00007049631196042688], InsetBox[
          StyleBox["104",
           StripOnInput->False,
           FontSize->9], 
          Offset[{2, 2}, {-100.39912250368803`, 20.574385496311958`}], 
          ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15,
  ImageSize->{390.6666666666637, Automatic}]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["78337.4734252318`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Undirected Quer\[EAcute]taro Urban Zone Dataset - MCST Solution Graph\
\>", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
       74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
       92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104}, {
      Null, CompressedData["
1:eJwVxdV2ggAAAFBF7O5ARQwsDEBRUQwsyhY7sfa2D9tPbnu45yLK91IBVCrV
z5//1YAaBDQAqNFq7KANDGt1WqtOrzPpjXqXQW8wGpxGk9FhspgtZqvZY/Fa
3Ta7DbK7HU6nzxVwRd1Bj9cT8/p9AV/EHwmEglAQDkVDCBSG4HAqgkaTcAzO
xdJIHEGRRLyUSCaIZDaVSxXQNFpMZ9LlTD6DZ4lsP4flsXwFK2C1QrNYLlZL
ZKlaxss0XsEZgiQokiLrlVplUKWrPWpIjWuNWrfeq4uNdoOlGVpotprzVqc1
ZQbMrN1pjzpcl+3yPaG3Ykfsoj/uXwb8QB5Kw8mIGy3HEjfl1vyC3wsz4ShO
xJWoSBPpPp1PNzN5dppvF8vFYble7VbP9Ubey9fNdnveHXev/WF/O5yPp9P1
9D4/Lvfr7fq5Pe6K8nx8PV+v9/vz+foFCE1JZg==
       "]}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {73, 25, 
         UndirectedEdge[2, 3], 76, 
         UndirectedEdge[56, 75], 77, 
         UndirectedEdge[81, 82], 
         UndirectedEdge[1, 4], 
         UndirectedEdge[56, 65], 
         UndirectedEdge[11, 12], 
         UndirectedEdge[18, 24], 71, 
         UndirectedEdge[71, 76], 
         UndirectedEdge[9, 16], 59, 
         UndirectedEdge[74, 91], 
         UndirectedEdge[59, 60], 
         UndirectedEdge[20, 32], 
         UndirectedEdge[61, 79], 
         UndirectedEdge[26, 33], 
         UndirectedEdge[95, 96], 
         UndirectedEdge[30, 36], 45, 14, 12, 
         UndirectedEdge[39, 40], 
         UndirectedEdge[75, 83], 42, 98, 89, 
         UndirectedEdge[67, 71], 40, 18, 
         UndirectedEdge[78, 86], 50, 
         UndirectedEdge[57, 66], 22, 92, 
         UndirectedEdge[5, 13], 48, 
         UndirectedEdge[102, 103], 97, 90, 31, 28, 
         UndirectedEdge[15, 19], 38, 
         UndirectedEdge[20, 21], 78, 
         UndirectedEdge[85, 89], 
         UndirectedEdge[79, 85], 6, 21, 51, 
         UndirectedEdge[45, 59], 
         UndirectedEdge[58, 61], 
         UndirectedEdge[78, 80], 95, 
         UndirectedEdge[22, 24], 85, 
         UndirectedEdge[7, 8], 46, 
         UndirectedEdge[37, 45], 
         UndirectedEdge[11, 13], 99, 
         UndirectedEdge[10, 12], 
         UndirectedEdge[43, 44], 84, 66, 65, 67, 
         UndirectedEdge[17, 22], 54, 
         UndirectedEdge[52, 55], 41, 
         UndirectedEdge[55, 57], 68, 23, 
         UndirectedEdge[82, 84], 37, 
         UndirectedEdge[83, 88], 
         UndirectedEdge[40, 49], 
         UndirectedEdge[86, 87], 29, 58, 34, 
         UndirectedEdge[32, 39], 93, 5, 
         UndirectedEdge[81, 90], 
         UndirectedEdge[33, 34], 
         UndirectedEdge[28, 37], 17, 
         UndirectedEdge[12, 21], 30, 
         UndirectedEdge[36, 41], 
         UndirectedEdge[53, 65], 64, 74, 
         UndirectedEdge[96, 99], 43, 
         UndirectedEdge[27, 31], 
         UndirectedEdge[90, 93], 
         UndirectedEdge[8, 9], 69, 
         UndirectedEdge[101, 102], 19, 
         UndirectedEdge[3, 5], 
         UndirectedEdge[6, 7], 86, 44, 33, 
         UndirectedEdge[25, 27], 101, 
         UndirectedEdge[50, 54], 
         UndirectedEdge[103, 104], 100, 
         UndirectedEdge[2, 4], 
         UndirectedEdge[84, 86], 36, 53, 10, 87, 13, 
         UndirectedEdge[48, 49], 32, 91, 
         UndirectedEdge[67, 94], 
         UndirectedEdge[23, 29], 
         UndirectedEdge[60, 63], 81, 
         UndirectedEdge[16, 17], 
         UndirectedEdge[89, 90], 
         UndirectedEdge[73, 81], 88, 96, 63, 47, 8, 56, 57, 
         UndirectedEdge[19, 25], 
         UndirectedEdge[6, 10], 7, 
         UndirectedEdge[74, 79], 4, 80, 70, 39, 24, 
         UndirectedEdge[55, 64], 
         UndirectedEdge[14, 15], 
         UndirectedEdge[68, 85], 62, 26, 
         UndirectedEdge[49, 52], 16, 
         UndirectedEdge[62, 63], 82, 
         UndirectedEdge[42, 44], 15, 27, 1, 
         UndirectedEdge[88, 91], 
         UndirectedEdge[57, 62], 
         UndirectedEdge[46, 53], 94, 
         UndirectedEdge[41, 50], 102, 83, 
         UndirectedEdge[40, 43], 
         UndirectedEdge[21, 23], 
         UndirectedEdge[62, 70], 103, 35, 
         UndirectedEdge[47, 51], 
         UndirectedEdge[33, 35], 52, 60, 49, 
         UndirectedEdge[13, 19], 104, 
         UndirectedEdge[69, 77], 61, 
         UndirectedEdge[35, 36], 
         UndirectedEdge[100, 101], 
         UndirectedEdge[70, 72], 9, 
         UndirectedEdge[38, 46], 3, 
         UndirectedEdge[99, 104], 
         UndirectedEdge[54, 69], 
         UndirectedEdge[98, 100], 11, 55, 20, 
         UndirectedEdge[72, 78], 79, 
         UndirectedEdge[91, 92], 
         UndirectedEdge[76, 77], 
         UndirectedEdge[24, 26], 72, 
         UndirectedEdge[92, 95], 75, 2, 
         UndirectedEdge[76, 97], 
         UndirectedEdge[50, 51]}, 
       GraphHighlightStyle -> {
        "Thick", UndirectedEdge[49, 52] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[2, 3] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 80 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[55, 57] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[40, 43] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[33, 35] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 92 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 81 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[91, 92] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 10 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 59 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 99 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[95, 96] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 73 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[36, 41] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[26, 33] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[27, 31] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 7 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[20, 32] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 49 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[14, 15] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[8, 9] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 95 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[78, 80] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 60 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[74, 79] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[83, 88] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 74 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[38, 46] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 89 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[3, 5] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[10, 12] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[17, 22] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 35 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[90, 93] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 6 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 31 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 77 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 13 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 51 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[28, 37] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[13, 19] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 65 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 46 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 76 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[41, 50] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 69 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 40 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[86, 87] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 57 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[9, 16] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 64 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 55 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 37 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[21, 23] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 19 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[35, 36] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[7, 8] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 32 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[71, 76] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[50, 51] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 15 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[25, 27] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[53, 65] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 71 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 1 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 41 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 66 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 70 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[92, 95] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[72, 78] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[39, 40] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[59, 60] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 63 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[96, 99] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[54, 69] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 53 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[84, 86] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 29 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 61 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[61, 79] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 38 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[79, 85] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[37, 45] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[42, 44] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[55, 64] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[23, 29] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 54 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 36 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 87 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[62, 70] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 21 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[50, 54] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 84 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[19, 25] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[30, 36] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 30 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[15, 19] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 85 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[102, 103] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 101 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[48, 49] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 45 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 90 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[11, 13] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[20, 21] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[67, 94] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[16, 17] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 3 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 11 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[99, 104] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 27 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 43 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[12, 21] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[103, 104] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[82, 84] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[22, 24] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 52 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 16 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[88, 91] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 100 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[6, 10] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 22 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 83 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[11, 12] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[81, 82] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 33 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[18, 24] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[100, 101] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[40, 49] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 42 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 18 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 34 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[46, 53] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 56 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[76, 77] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[57, 66] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 2 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 78 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 88 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 91 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 25 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[60, 63] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 14 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[6, 7] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 58 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[62, 63] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[73, 81] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[45, 59] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 8 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[98, 100] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[32, 39] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[2, 4] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[1, 4] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 102 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 93 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 82 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[52, 55] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 67 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[74, 91] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 94 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[56, 65] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 98 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[76, 97] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 9 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 17 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[67, 71] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 68 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[57, 62] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 103 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 79 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 104 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 12 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[5, 13] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 44 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[101, 102] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 20 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 62 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[68, 85] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 48 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[47, 51] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[78, 86] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 26 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[81, 90] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 23 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 75 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[58, 61] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 96 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[24, 26] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[70, 72] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 24 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[69, 77] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 4 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 28 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[43, 44] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 47 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 50 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[56, 75] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[75, 83] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[89, 90] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 72 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 97 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 39 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[33, 34] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 5 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[85, 89] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 86 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}}, EdgeWeight -> CompressedData["
1:eJwN0fk7FAgABmBHrsassZFtiw5RmcQmKjm+EhUpHUTGNePIEXPPYGLMYNxH
ytFBtKF1lZVUtLSh0j5soYiENuvYypHcbT+8f8G7nhpy3EdaSkqq5LuXEz6m
0abxmG/dTbvVzUa2sVWgKD4Sauwflh7fiUXmjjZblcIYnJMLahmyiIfG0Klv
/8qdB+dSUFTdwUvofJplLFFJQ7NMyrM+KRFmWbXkvUVhiNdc2yy1modznMW6
zb50FHYRXj1cSsfA8ot5ZhUh8LCbSk9SomCT7inZKmkh6k6lDGhMhKPDKKSp
sEKAKycjRigCFkip+8l/uzDQsXxhfs4mCKK0k0+c7cMgr62g2+8QigLSuh02
xHMwUnco6r4fiheEqt+IiwkwnBjP2WsvwL4eioZMtjcCV/nEOWm4o7/SVpNg
ysKZQcWno80SVLtOjWs7h0M99O4Ts2A6yj09zmtIxHDQu33Pyl4E+YA9f7IH
2JgbksulmMagvbW3ROVIJJQOWhwyd2eif6Go/J1uAGrV8hwNOphwcqVVsi2i
MNXS+TZdQYiOy2pxZF4yXlx1K8u8yYR4+FU1mSLEh7FZnZLdsfCKsWaFfxJg
l87Gl+POEph//G96RWs8PP22re7ti4bcjKpN2R0n5AW7OBt0pGPCLuBMzn46
ckkV1vOTCTAo5Qp6RyKxN7CQEbMiBcoXRKN66VEYLmNQsxmp6FOMWmMaIUQP
YfuYoygS9e3E4AD3CExVubVM1lARqzOroXdYiD92VlYbz/Lg/XJebno0Aq6d
g2aUaxLM9Rhb+NRmQOvyPRu2+wkEj67q4U/xEan43rzOT4jEMKu21w3xMKjM
sc6/KMaPzEVaFl2MH+7u2ZxnIsSDLNddXg488J4rL6v+/tGlfn5fxapEfEna
wp6Jp2EjdXilnzYfVgaSdfs4XJiw05VLrfmYK53XdC+koKtL1pn8IhgzB3hV
vg4MjPTOuO5qSYCwKtvloAkXCto3a6+/5cMzc6lxPTEKX5U19Rs3s3DA/s3I
5F8M9JUz5pZJxaImo/gseYs33in3GZPtvKFE2tz+uIINyy/6jwRMDs4NG2q1
zAaDLO1o6ehDg/iiy+6m11Twhg51bjNhoeTqfcPk1AAQ2b2W+ZqheDjd99nc
jwrtMXZ2gQIT3J99jtTKc3GMFKTntJWF2scL8X1HaIi5J6JlLPlAOk5+cMVi
ANzsRhM9jvoin8JTtPULQaTWDRybjEJL8sfU7AwOnC3GxSKlEATVb8jrVw+B
btKZAXZ4LEaGZ5+2t0lQY3sr26OcBVk/u8SvaixsfaV7gzzpC0rdHj1VIg2n
C4zeq/l54U2r2D9X9SzGavKojXIR4BaZZd7I9USCkcmj5w8D0bChhsQm+mOr
b7ROeQcD+73fXLk+TkP1SpPmJ3QubpjkVESqpuKXFtm1lWI2cqvbf9L7loYt
lh9KG5w8cDx3sEmclACZBfniHct8cTqz0WjAPgBlVz0/DDSzQXWiP6835CO/
XfEZdz4Iny09aGqEYPh/Ozjj/YCOpk9btQUHvNBUrJXaZMlCxa9pihXaaWB2
6+RdrveAzwRJcWQdE1rp11/RJgU49fJ5zeHe0zj5jG1Ot4iE2+HmwtJMOrbt
qHe/H5MC3u1D+aa/n8XarqvdXa+F0DkxFP1PEx+jR2VMLY75w6xAQCJtYmHT
gcTPDT2JsBQ+yikuiMVTff3SzOQLaCf6XrtHoGJpp/MDwrQEIVaM944uQlAI
2wOPmCTCU9SWk7iGAw9O7pf0W+FwzIpKbszkgz/9QCWLGQa9uJzcF2IOFuQ0
ajkWLPwP2e946w==
        "], VertexCoordinates -> CompressedData["
1:eJwt1Hs0lHkcBvDBiUlSolxDNZouSo2XMtheuVaThtZlOqTpJFJis5nGYa2N
XFIUJlG5xNayp0yyYlKDapMiayK51Ig0uUVZVLTPe87+MWfO5zzP9/v+zvvO
O8v2h3kGKtNotGB8qO+lRkM7NQx50gcnnpWy8whyiltolmbAk3LkDuWDVwhy
Y07WsCOctDZoNBWeqHqVMgPbz9djxMBuYXfSuvR50kVJ7RPz4LYG/RRbWDu6
9XPHZYK0CRiqmdLjSUdV0l89gEP7qlf3wyvZjeZbYQvLI5kTcOwO2uxq2NVH
McLG/OiN8mXfLxFk10eJQgRv+p1pPgYPB3YeOgwPmjix+uDcYwmCKrjgUHJW
C6wiyHcX4nwRa/RpJfDBeyfzNsOdXdlXymBFTsOmTlxPEVu1ZB1cq7TOvxYu
lipns+AIt5CCAzBxhd5rB+f5Xnsyif3mWtdnHWBnmVNeGhxFvMiYziXInrJQ
j4NwZl/YxgnY4ZONQIJ5ZrJZ/wjsppvrrY5c5Hj6yAD8aDqXm4Tc9KOUVgeL
bvcaquN804X2e91guUztvAC5csdgtwk8W916mYk8WKH+lxZcYFEzUwVbCZ+E
VOcQJGeXb68S9nf45xdJ4eHKyEIG5isSa7iUg58yPsxBfxHHqlwETxn5nk1H
f1eDjTgRTl1slX0K/YBKeurERYJMl9B3Z8Im2qufvYMj9TJm98JflaPkTbDn
gpb3mrDD/Nd11fB6A5ZxMfbXL2i6dhaWLfSassT+JAttZzd4ZFjU9ytMe7fn
Ng1++7XXMwte/mZhiSybII3mEi9lujxpnZXg832Ye+bC0UXY12bC1yuD1WIU
w83o849OHsuDO3uObtNEXlc0NRVPzTMThNTv9W665gI+LFNvnTRA36x5ImYz
XNxhp3kS5+X8fSlo6AJBWme+Oh2M3JEWPtQBJ29trI6FdV2iPCRwWGByEwv9
a4pylRuwjuIXdwfYcjbmUQJ8U+gVVIF+ibPTRiG8/aLsuSrc5dax/BBcnN+5
wwD9mt1Kk3awBnn1ZwX1e/YWvTeHtTVrHtYjZ/movlCFebqVLTTqfWo5Llej
9gti7/qgP5FntWVWRJDT203NIpE/1TAMnIHXD3j6ecHXvx0W9sPBJmunQtF3
UWPwX8NkT6n3SuQZ4cvS3sCzms5xP8AVtcnv2+GG732jPeiz6ew9L+Anifkh
n3Ge/dOfQmvhsHyp7TTyeXwrURkcsmF+vSueD1uc+K8YtuZdNS5EzlkyaXIR
Zqz5cZMu5u1d+ySZMNKWpbge/+0XQQysPdY2XYl5M3FRF2U2o4LLQr7KValw
Hyx9vsKFer7nmNvaObCLhm3qNrj9cBSXCzfrsG+KsV+37YzAFQ43s7VWho9H
sjZvh5WSJFGL4Z3rH0nZsLH89htn7LfTmfPFCXambgz2RfNZXhvg4t9SSgKo
831t0DOBPeJlw0PI8w0suhlwybj9Ci3kf3C7/b5lEWRcS/8zOfbn10cLlZAb
0RuUqfsbUTB+sx85nm7AecyPuXoEjcAP/9R1tEFumxIha4Hl+8L2u8H0UnF8
Rdb/7w/6lvUuvPuw+oHIJTVwIrEm/BYsDmNqyWGLL+r/pMIGyc/p1nD8x+C3
SfDO4PO+nvAt1+v6nnDzhopi6n143NT5ciiTIO82Okczcb/Lo/1PfYf9zvqY
RqG/l2PZPQgr4tJqU6jruTdLxDB/94DHGPqtQWp+PnBphqUO9f9zR9xx7kMG
QY6rqTRT59vyE23mHhxblKRRiryHZvm6FnaPMRUOwI+zJaJ0+JQ3M2gV+j0Z
iQl7YGPDuBP+sAcHE/Ct8cZ9qdT/aU6Tuyr8H1Ql8gA=
        "]}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$1", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$7", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$6", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$7", Automatic, Center], 
          DynamicLocation["VertexID$8", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$16", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$12", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$16", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$18", Automatic, Center], 
          DynamicLocation["VertexID$24", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$20", Automatic, Center], 
          DynamicLocation["VertexID$32", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$21", Automatic, Center], 
          DynamicLocation["VertexID$23", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$24", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$23", Automatic, Center], 
          DynamicLocation["VertexID$29", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$33", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$31", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$28", Automatic, Center], 
          DynamicLocation["VertexID$37", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$36", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$32", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$33", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$33", Automatic, Center], 
          DynamicLocation["VertexID$35", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$35", Automatic, Center], 
          DynamicLocation["VertexID$36", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$36", Automatic, Center], 
          DynamicLocation["VertexID$41", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$37", Automatic, Center], 
          DynamicLocation["VertexID$45", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$38", Automatic, Center], 
          DynamicLocation["VertexID$46", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$41", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$42", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$45", Automatic, Center], 
          DynamicLocation["VertexID$59", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$53", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$49", Automatic, Center], 
          DynamicLocation["VertexID$52", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$54", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$56", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$52", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$53", Automatic, Center], 
          DynamicLocation["VertexID$65", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$54", Automatic, Center], 
          DynamicLocation["VertexID$69", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$64", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$65", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$75", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$62", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$66", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$58", Automatic, Center], 
          DynamicLocation["VertexID$61", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$74", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$59", Automatic, Center], 
          DynamicLocation["VertexID$60", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$60", Automatic, Center], 
          DynamicLocation["VertexID$63", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$78", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$61", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$62", Automatic, Center], 
          DynamicLocation["VertexID$63", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$62", Automatic, Center], 
          DynamicLocation["VertexID$70", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$72", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$74", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$83", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$70", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$81", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$67", Automatic, Center], 
          DynamicLocation["VertexID$71", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$67", Automatic, Center], 
          DynamicLocation["VertexID$94", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$68", Automatic, Center], 
          DynamicLocation["VertexID$85", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$69", Automatic, Center], 
         DynamicLocation["VertexID$76", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$69", Automatic, Center], 
          DynamicLocation["VertexID$77", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$70", Automatic, Center], 
          DynamicLocation["VertexID$72", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$70", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$71", Automatic, Center], 
          DynamicLocation["VertexID$76", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$72", Automatic, Center], 
          DynamicLocation["VertexID$78", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$72", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$81", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$73", Automatic, Center], 
         DynamicLocation["VertexID$89", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$91", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$75", Automatic, Center], 
          DynamicLocation["VertexID$83", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$76", Automatic, Center], 
          DynamicLocation["VertexID$77", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$76", Automatic, Center], 
          DynamicLocation["VertexID$97", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$78", Automatic, Center], 
          DynamicLocation["VertexID$80", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$78", Automatic, Center], 
          DynamicLocation["VertexID$86", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$79", Automatic, Center], 
          DynamicLocation["VertexID$85", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$79", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$80", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$81", Automatic, Center], 
          DynamicLocation["VertexID$82", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$81", Automatic, Center], 
          DynamicLocation["VertexID$90", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$82", Automatic, Center], 
          DynamicLocation["VertexID$84", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$83", Automatic, Center], 
          DynamicLocation["VertexID$88", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$84", Automatic, Center], 
          DynamicLocation["VertexID$86", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$85", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$85", Automatic, Center], 
         DynamicLocation["VertexID$95", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$86", Automatic, Center], 
          DynamicLocation["VertexID$87", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$87", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$88", Automatic, Center], 
          DynamicLocation["VertexID$91", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$89", Automatic, Center], 
          DynamicLocation["VertexID$90", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$96", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$90", Automatic, Center], 
          DynamicLocation["VertexID$93", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$91", Automatic, Center], 
          DynamicLocation["VertexID$92", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$92", Automatic, Center], 
          DynamicLocation["VertexID$95", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$92", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$93", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$94", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$95", Automatic, Center], 
          DynamicLocation["VertexID$96", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$96", Automatic, Center], 
          DynamicLocation["VertexID$99", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$97", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$98", Automatic, Center], 
          DynamicLocation["VertexID$100", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$99", Automatic, Center], 
          DynamicLocation["VertexID$104", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$100", Automatic, Center], 
          DynamicLocation["VertexID$101", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$101", Automatic, Center], 
          DynamicLocation["VertexID$102", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$102", Automatic, Center], 
          DynamicLocation["VertexID$103", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$103", Automatic, Center], 
          DynamicLocation["VertexID$104", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.422626, 20.621989}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.414987, 20.620784}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410396, 20.619338}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.421725, 20.619097}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404087, 20.61737}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394259, 20.616607}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.390182, 20.616285}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388987, 20.614291}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389989, 20.613922}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394222, 20.613268}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399836, 20.613055}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397181, 20.612857}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401945, 20.612573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.413654, 20.611969}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409706, 20.61207}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38838, 20.610063}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386802, 20.610147}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.381142, 20.610304}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405749, 20.610382}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399391, 20.609277}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39687, 20.6092101}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386442, 20.609114}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391256, 20.609}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38332, 20.608429}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406867, 20.606573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382298, 20.605955}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408668, 20.605739}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.417602, 20.604407}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39074, 20.604502}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.377236, 20.604497}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406611, 20.603862}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399448, 20.603633}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.383215, 20.601403}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.384053, 20.601178}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.380798, 20.600813}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.37591, 20.600506}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.416328, 20.599833}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393797, 20.598759}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398492, 20.597667}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399739, 20.596995}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.372366, 20.596679}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407375, 20.596437}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403316, 20.596183}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407122, 20.595779}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.415007, 20.595231}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392271, 20.594612}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382999, 20.593451}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397006, 20.593169}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398421, 20.592702}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378101, 20.592499}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378986, 20.591885}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401697, 20.591693}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391041, 20.591448}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376605, 20.590774}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404874, 20.590529}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386949, 20.589948}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.4063, 20.589964}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39598, 20.589817}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.413461, 20.589804}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.41146, 20.589439}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397337, 20.589346}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40859, 20.589364}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410065, 20.589232}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404232, 20.589193}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389918, 20.588889}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405837, 20.588632}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.363755, 20.588653}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40052, 20.588274}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376417, 20.588207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40807, 20.587852}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$70"], 
         InsetBox[
          FormBox[
           StyleBox["70", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$70", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$70"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.370604, 20.587858}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$71"], 
         InsetBox[
          FormBox[
           StyleBox["71", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$71", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$71"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409321, 20.587412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$72"], 
         InsetBox[
          FormBox[
           StyleBox["72", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$72", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$72"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403276, 20.58711}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$73"], 
         InsetBox[
          FormBox[
           StyleBox["73", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$73", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$73"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395114, 20.587163}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$74"], 
         InsetBox[
          FormBox[
           StyleBox["74", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$74", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$74"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.385845, 20.587028}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$75"], 
         InsetBox[
          FormBox[
           StyleBox["75", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$75", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$75"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.375135, 20.58707}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$76"], 
         InsetBox[
          FormBox[
           StyleBox["76", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$76", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$76"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376311, 20.586834}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$77"], 
         InsetBox[
          FormBox[
           StyleBox["77", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$77", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$77"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410515, 20.58699}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$78"], 
         InsetBox[
          FormBox[
           StyleBox["78", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$78", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$78"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396554, 20.586675}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$79"], 
         InsetBox[
          FormBox[
           StyleBox["79", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$79", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$79"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.412089, 20.586412}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$80"], 
         InsetBox[
          FormBox[
           StyleBox["80", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$80", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$80"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405086, 20.5865}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$81"], 
         InsetBox[
          FormBox[
           StyleBox["81", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$81", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$81"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407296, 20.585882}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$82"], 
         InsetBox[
          FormBox[
           StyleBox["82", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$82", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$82"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388782, 20.58596}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$83"], 
         InsetBox[
          FormBox[
           StyleBox["83", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$83", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$83"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40857, 20.585536}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$84"], 
         InsetBox[
          FormBox[
           StyleBox["84", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$84", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$84"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399559, 20.585669}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$85"], 
         InsetBox[
          FormBox[
           StyleBox["85", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$85", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$85"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409745, 20.585233}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$86"], 
         InsetBox[
          FormBox[
           StyleBox["86", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$86", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$86"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410668, 20.584801}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$87"], 
         InsetBox[
          FormBox[
           StyleBox["87", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$87", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$87"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391535, 20.584951}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$88"], 
         InsetBox[
          FormBox[
           StyleBox["88", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$88", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$88"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402166, 20.584754}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$89"], 
         InsetBox[
          FormBox[
           StyleBox["89", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$89", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$89"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404362, 20.58418}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$90"], 
         InsetBox[
          FormBox[
           StyleBox["90", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$90", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$90"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393923, 20.58412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$91"], 
         InsetBox[
          FormBox[
           StyleBox["91", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$91", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$91"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395651, 20.583284}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$92"], 
         InsetBox[
          FormBox[
           StyleBox["92", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$92", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$92"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407144, 20.581739}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$93"], 
         InsetBox[
          FormBox[
           StyleBox["93", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$93", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$93"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.361785, 20.582024}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$94"], 
         InsetBox[
          FormBox[
           StyleBox["94", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$94", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$94"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398079, 20.581724}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$95"], 
         InsetBox[
          FormBox[
           StyleBox["95", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$95", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$95"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399094, 20.580837}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$96"], 
         InsetBox[
          FormBox[
           StyleBox["96", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$96", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$96"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.374104, 20.579458}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$97"], 
         InsetBox[
          FormBox[
           StyleBox["97", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$97", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$97"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382268, 20.577798}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$98"], 
         InsetBox[
          FormBox[
           StyleBox["98", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$98", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$98"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40221, 20.577133}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$99"], 
         InsetBox[
          FormBox[
           StyleBox["99", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$99", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$99"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38544, 20.577177}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$100"], 
         InsetBox[
          FormBox[
           StyleBox["100", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$100", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$100"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389128, 20.576425}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$101"], 
         InsetBox[
          FormBox[
           StyleBox["101", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$101", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$101"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393091, 20.5756}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$102"], 
         InsetBox[
          FormBox[
           StyleBox["102", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$102", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$102"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39633, 20.574997}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$103"], 
         InsetBox[
          FormBox[
           StyleBox["103", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$103", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$103"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399193, 20.574315}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$104"], 
         InsetBox[
          FormBox[
           StyleBox["104", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$104", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$104"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.34087136409994`}, {-107.53887136408935`, 93.71452489352248}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weights", "Subsection"],

Cell[BoxData["35068.30444362371`"], "Output"],

Cell[BoxData["18713.828161572943`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Undirected Quer\[EAcute]taro Urban Zone Dataset - GSH-T Solution Graph\
\>", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
       74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
       92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104}, {
      Null, CompressedData["
1:eJwVxdV2ggAAAFBF7O5ARQwsDEBRUQwsyhY7sfa2D9tPbnu45yLK91IBVCrV
z5//1YAaBDQAqNFq7KANDGt1WqtOrzPpjXqXQW8wGpxGk9FhspgtZqvZY/Fa
3Ta7DbK7HU6nzxVwRd1Bj9cT8/p9AV/EHwmEglAQDkVDCBSG4HAqgkaTcAzO
xdJIHEGRRLyUSCaIZDaVSxXQNFpMZ9LlTD6DZ4lsP4flsXwFK2C1QrNYLlZL
ZKlaxss0XsEZgiQokiLrlVplUKWrPWpIjWuNWrfeq4uNdoOlGVpotprzVqc1
ZQbMrN1pjzpcl+3yPaG3Ykfsoj/uXwb8QB5Kw8mIGy3HEjfl1vyC3wsz4ShO
xJWoSBPpPp1PNzN5dppvF8vFYble7VbP9Ubey9fNdnveHXev/WF/O5yPp9P1
9D4/Lvfr7fq5Pe6K8nx8PV+v9/vz+foFCE1JZg==
       "]}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {3, 
         UndirectedEdge[50, 54], 
         UndirectedEdge[17, 22], 
         UndirectedEdge[33, 35], 73, 91, 21, 35, 79, 
         UndirectedEdge[83, 88], 10, 
         UndirectedEdge[66, 81], 15, 
         UndirectedEdge[2, 3], 63, 
         UndirectedEdge[27, 31], 
         UndirectedEdge[81, 90], 66, 
         UndirectedEdge[89, 90], 49, 
         UndirectedEdge[40, 49], 85, 
         UndirectedEdge[49, 52], 86, 
         UndirectedEdge[26, 33], 
         UndirectedEdge[88, 91], 52, 62, 
         UndirectedEdge[48, 49], 17, 
         UndirectedEdge[10, 12], 2, 
         UndirectedEdge[72, 78], 
         UndirectedEdge[85, 89], 83, 
         UndirectedEdge[92, 95], 
         UndirectedEdge[3, 15], 90, 72, 
         UndirectedEdge[24, 26], 12, 
         UndirectedEdge[91, 92], 78, 
         UndirectedEdge[25, 27], 
         UndirectedEdge[70, 72], 57, 
         UndirectedEdge[74, 91], 51, 
         UndirectedEdge[33, 34], 
         UndirectedEdge[11, 13], 
         UndirectedEdge[52, 55], 40, 
         UndirectedEdge[73, 81], 70, 89, 
         UndirectedEdge[9, 16], 
         UndirectedEdge[40, 43], 33, 
         UndirectedEdge[42, 44], 55, 
         UndirectedEdge[22, 24], 56, 
         UndirectedEdge[51, 56], 13, 
         UndirectedEdge[60, 63], 
         UndirectedEdge[57, 66], 
         UndirectedEdge[9, 10], 
         UndirectedEdge[55, 57], 27, 22, 26, 
         UndirectedEdge[13, 19], 31, 
         UndirectedEdge[62, 70], 
         UndirectedEdge[56, 75], 48, 
         UndirectedEdge[16, 17], 
         UndirectedEdge[31, 42], 42, 
         UndirectedEdge[43, 44], 
         UndirectedEdge[54, 69], 
         UndirectedEdge[39, 40], 
         UndirectedEdge[74, 79], 69, 60, 
         UndirectedEdge[75, 83], 
         UndirectedEdge[15, 19], 44, 81, 11, 25, 
         UndirectedEdge[69, 76], 76, 
         UndirectedEdge[50, 51], 
         UndirectedEdge[62, 63], 
         UndirectedEdge[19, 25], 
         UndirectedEdge[79, 85], 
         UndirectedEdge[11, 12], 39, 34, 95, 75, 24, 50, 
         UndirectedEdge[78, 86], 9, 88, 
         UndirectedEdge[12, 21], 19, 74, 92, 16, 54, 43, 
         UndirectedEdge[57, 62]}, 
       GraphHighlightStyle -> {"Thick", 15 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[33, 34] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 39 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[11, 12] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[10, 12] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 66 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[78, 86] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 54 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 90 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 2 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[39, 40] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 72 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[60, 63] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 81 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 42 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 51 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 52 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 62 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 35 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 76 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 19 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 17 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 55 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[62, 63] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[81, 90] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 11 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[85, 89] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[3, 15] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[22, 24] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 56 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[89, 90] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 70 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 43 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 83 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 16 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 44 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[57, 66] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[24, 26] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[25, 27] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[27, 31] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[51, 56] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 91 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[50, 51] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 74 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[79, 85] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[72, 78] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[70, 72] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[26, 33] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 48 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 40 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[2, 3] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[75, 83] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[66, 81] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 26 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[42, 44] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 9 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[74, 79] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 31 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 33 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 75 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[88, 91] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 79 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 88 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[48, 49] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 57 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 95 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 73 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 63 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[9, 10] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[69, 76] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 50 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[56, 75] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[55, 57] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[83, 88] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 78 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 13 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 34 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[31, 42] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[92, 95] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[49, 52] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[13, 19] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 92 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[12, 21] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[9, 16] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 3 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 12 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[33, 35] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[17, 22] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 25 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[62, 70] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 86 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[54, 69] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[19, 25] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 89 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 24 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 21 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 69 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 10 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 49 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 22 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 60 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[40, 49] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[43, 44] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[15, 19] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[57, 62] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[50, 54] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[52, 55] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 27 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[11, 13] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 85 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[40, 43] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[73, 81] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         UndirectedEdge[74, 91] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[91, 92] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 
         UndirectedEdge[16, 17] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}}, EdgeWeight -> CompressedData["
1:eJwN0fk7FAgABmBHrsassZFtiw5RmcQmKjm+EhUpHUTGNePIEXPPYGLMYNxH
ytFBtKF1lZVUtLSh0j5soYiENuvYypHcbT+8f8G7nhpy3EdaSkqq5LuXEz6m
0abxmG/dTbvVzUa2sVWgKD4Sauwflh7fiUXmjjZblcIYnJMLahmyiIfG0Klv
/8qdB+dSUFTdwUvofJplLFFJQ7NMyrM+KRFmWbXkvUVhiNdc2yy1modznMW6
zb50FHYRXj1cSsfA8ot5ZhUh8LCbSk9SomCT7inZKmkh6k6lDGhMhKPDKKSp
sEKAKycjRigCFkip+8l/uzDQsXxhfs4mCKK0k0+c7cMgr62g2+8QigLSuh02
xHMwUnco6r4fiheEqt+IiwkwnBjP2WsvwL4eioZMtjcCV/nEOWm4o7/SVpNg
ysKZQcWno80SVLtOjWs7h0M99O4Ts2A6yj09zmtIxHDQu33Pyl4E+YA9f7IH
2JgbksulmMagvbW3ROVIJJQOWhwyd2eif6Go/J1uAGrV8hwNOphwcqVVsi2i
MNXS+TZdQYiOy2pxZF4yXlx1K8u8yYR4+FU1mSLEh7FZnZLdsfCKsWaFfxJg
l87Gl+POEph//G96RWs8PP22re7ti4bcjKpN2R0n5AW7OBt0pGPCLuBMzn46
ckkV1vOTCTAo5Qp6RyKxN7CQEbMiBcoXRKN66VEYLmNQsxmp6FOMWmMaIUQP
YfuYoygS9e3E4AD3CExVubVM1lARqzOroXdYiD92VlYbz/Lg/XJebno0Aq6d
g2aUaxLM9Rhb+NRmQOvyPRu2+wkEj67q4U/xEan43rzOT4jEMKu21w3xMKjM
sc6/KMaPzEVaFl2MH+7u2ZxnIsSDLNddXg488J4rL6v+/tGlfn5fxapEfEna
wp6Jp2EjdXilnzYfVgaSdfs4XJiw05VLrfmYK53XdC+koKtL1pn8IhgzB3hV
vg4MjPTOuO5qSYCwKtvloAkXCto3a6+/5cMzc6lxPTEKX5U19Rs3s3DA/s3I
5F8M9JUz5pZJxaImo/gseYs33in3GZPtvKFE2tz+uIINyy/6jwRMDs4NG2q1
zAaDLO1o6ehDg/iiy+6m11Twhg51bjNhoeTqfcPk1AAQ2b2W+ZqheDjd99nc
jwrtMXZ2gQIT3J99jtTKc3GMFKTntJWF2scL8X1HaIi5J6JlLPlAOk5+cMVi
ANzsRhM9jvoin8JTtPULQaTWDRybjEJL8sfU7AwOnC3GxSKlEATVb8jrVw+B
btKZAXZ4LEaGZ5+2t0lQY3sr26OcBVk/u8SvaixsfaV7gzzpC0rdHj1VIg2n
C4zeq/l54U2r2D9X9SzGavKojXIR4BaZZd7I9USCkcmj5w8D0bChhsQm+mOr
b7ROeQcD+73fXLk+TkP1SpPmJ3QubpjkVESqpuKXFtm1lWI2cqvbf9L7loYt
lh9KG5w8cDx3sEmclACZBfniHct8cTqz0WjAPgBlVz0/DDSzQXWiP6835CO/
XfEZdz4Iny09aGqEYPh/Ozjj/YCOpk9btQUHvNBUrJXaZMlCxa9pihXaaWB2
6+RdrveAzwRJcWQdE1rp11/RJgU49fJ5zeHe0zj5jG1Ot4iE2+HmwtJMOrbt
qHe/H5MC3u1D+aa/n8XarqvdXa+F0DkxFP1PEx+jR2VMLY75w6xAQCJtYmHT
gcTPDT2JsBQ+yikuiMVTff3SzOQLaCf6XrtHoGJpp/MDwrQEIVaM944uQlAI
2wOPmCTCU9SWk7iGAw9O7pf0W+FwzIpKbszkgz/9QCWLGQa9uJzcF2IOFuQ0
ajkWLPwP2e946w==
        "], VertexCoordinates -> CompressedData["
1:eJwt1Hs0lHkcBvDBiUlSolxDNZouSo2XMtheuVaThtZlOqTpJFJis5nGYa2N
XFIUJlG5xNayp0yyYlKDapMiayK51Ig0uUVZVLTPe87+MWfO5zzP9/v+zvvO
O8v2h3kGKtNotGB8qO+lRkM7NQx50gcnnpWy8whyiltolmbAk3LkDuWDVwhy
Y07WsCOctDZoNBWeqHqVMgPbz9djxMBuYXfSuvR50kVJ7RPz4LYG/RRbWDu6
9XPHZYK0CRiqmdLjSUdV0l89gEP7qlf3wyvZjeZbYQvLI5kTcOwO2uxq2NVH
McLG/OiN8mXfLxFk10eJQgRv+p1pPgYPB3YeOgwPmjix+uDcYwmCKrjgUHJW
C6wiyHcX4nwRa/RpJfDBeyfzNsOdXdlXymBFTsOmTlxPEVu1ZB1cq7TOvxYu
lipns+AIt5CCAzBxhd5rB+f5Xnsyif3mWtdnHWBnmVNeGhxFvMiYziXInrJQ
j4NwZl/YxgnY4ZONQIJ5ZrJZ/wjsppvrrY5c5Hj6yAD8aDqXm4Tc9KOUVgeL
bvcaquN804X2e91guUztvAC5csdgtwk8W916mYk8WKH+lxZcYFEzUwVbCZ+E
VOcQJGeXb68S9nf45xdJ4eHKyEIG5isSa7iUg58yPsxBfxHHqlwETxn5nk1H
f1eDjTgRTl1slX0K/YBKeurERYJMl9B3Z8Im2qufvYMj9TJm98JflaPkTbDn
gpb3mrDD/Nd11fB6A5ZxMfbXL2i6dhaWLfSassT+JAttZzd4ZFjU9ytMe7fn
Ng1++7XXMwte/mZhiSybII3mEi9lujxpnZXg832Ye+bC0UXY12bC1yuD1WIU
w83o849OHsuDO3uObtNEXlc0NRVPzTMThNTv9W665gI+LFNvnTRA36x5ImYz
XNxhp3kS5+X8fSlo6AJBWme+Oh2M3JEWPtQBJ29trI6FdV2iPCRwWGByEwv9
a4pylRuwjuIXdwfYcjbmUQJ8U+gVVIF+ibPTRiG8/aLsuSrc5dax/BBcnN+5
wwD9mt1Kk3awBnn1ZwX1e/YWvTeHtTVrHtYjZ/movlCFebqVLTTqfWo5Llej
9gti7/qgP5FntWVWRJDT203NIpE/1TAMnIHXD3j6ecHXvx0W9sPBJmunQtF3
UWPwX8NkT6n3SuQZ4cvS3sCzms5xP8AVtcnv2+GG732jPeiz6ew9L+Anifkh
n3Ge/dOfQmvhsHyp7TTyeXwrURkcsmF+vSueD1uc+K8YtuZdNS5EzlkyaXIR
Zqz5cZMu5u1d+ySZMNKWpbge/+0XQQysPdY2XYl5M3FRF2U2o4LLQr7KValw
Hyx9vsKFer7nmNvaObCLhm3qNrj9cBSXCzfrsG+KsV+37YzAFQ43s7VWho9H
sjZvh5WSJFGL4Z3rH0nZsLH89htn7LfTmfPFCXambgz2RfNZXhvg4t9SSgKo
831t0DOBPeJlw0PI8w0suhlwybj9Ci3kf3C7/b5lEWRcS/8zOfbn10cLlZAb
0RuUqfsbUTB+sx85nm7AecyPuXoEjcAP/9R1tEFumxIha4Hl+8L2u8H0UnF8
Rdb/7w/6lvUuvPuw+oHIJTVwIrEm/BYsDmNqyWGLL+r/pMIGyc/p1nD8x+C3
SfDO4PO+nvAt1+v6nnDzhopi6n143NT5ciiTIO82Okczcb/Lo/1PfYf9zvqY
RqG/l2PZPQgr4tJqU6jruTdLxDB/94DHGPqtQWp+PnBphqUO9f9zR9xx7kMG
QY6rqTRT59vyE23mHhxblKRRiryHZvm6FnaPMRUOwI+zJaJ0+JQ3M2gV+j0Z
iQl7YGPDuBP+sAcHE/Ct8cZ9qdT/aU6Tuyr8H1Ql8gA=
        "]}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$16", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$12", Automatic, Center], 
          DynamicLocation["VertexID$21", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$16", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$24", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$33", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$31", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$37", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$31", Automatic, Center], 
          DynamicLocation["VertexID$42", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$33", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$33", Automatic, Center], 
          DynamicLocation["VertexID$35", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$42", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$49", Automatic, Center], 
          DynamicLocation["VertexID$52", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$54", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$52", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$54", Automatic, Center], 
          DynamicLocation["VertexID$69", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$75", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$62", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$66", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$74", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$60", Automatic, Center], 
          DynamicLocation["VertexID$63", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$78", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$79", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$62", Automatic, Center], 
          DynamicLocation["VertexID$63", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$62", Automatic, Center], 
          DynamicLocation["VertexID$70", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$72", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$74", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$83", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$70", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$66", Automatic, Center], 
          DynamicLocation["VertexID$81", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$94", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$85", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$69", Automatic, Center], 
          DynamicLocation["VertexID$76", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$69", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$70", Automatic, Center], 
          DynamicLocation["VertexID$72", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$70", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$71", Automatic, Center], 
         DynamicLocation["VertexID$76", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$72", Automatic, Center], 
          DynamicLocation["VertexID$78", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$72", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$81", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$73", Automatic, Center], 
         DynamicLocation["VertexID$89", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$91", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$75", Automatic, Center], 
          DynamicLocation["VertexID$83", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$76", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$76", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$78", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$78", Automatic, Center], 
          DynamicLocation["VertexID$86", Automatic, Center]}], 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$79", Automatic, Center], 
          DynamicLocation["VertexID$85", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$79", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$80", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$81", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$81", Automatic, Center], 
          DynamicLocation["VertexID$90", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$82", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$83", Automatic, Center], 
          DynamicLocation["VertexID$88", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$84", Automatic, Center], 
         DynamicLocation["VertexID$86", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$85", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$85", Automatic, Center], 
         DynamicLocation["VertexID$95", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$86", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$87", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$88", Automatic, Center], 
          DynamicLocation["VertexID$91", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$89", Automatic, Center], 
          DynamicLocation["VertexID$90", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$96", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$90", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$91", Automatic, Center], 
          DynamicLocation["VertexID$92", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$92", Automatic, Center], 
          DynamicLocation["VertexID$95", Automatic, Center]}], 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$92", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$93", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$94", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$96", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$96", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$97", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$98", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$99", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$100", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$101", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$102", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$103", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.422626, 20.621989}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.414987, 20.620784}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410396, 20.619338}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.421725, 20.619097}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404087, 20.61737}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394259, 20.616607}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.390182, 20.616285}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.388987, 20.614291}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389989, 20.613922}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394222, 20.613268}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399836, 20.613055}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397181, 20.612857}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401945, 20.612573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413654, 20.611969}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409706, 20.61207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38838, 20.610063}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386802, 20.610147}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.381142, 20.610304}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405749, 20.610382}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399391, 20.609277}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39687, 20.6092101}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386442, 20.609114}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391256, 20.609}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38332, 20.608429}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406867, 20.606573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382298, 20.605955}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408668, 20.605739}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.417602, 20.604407}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39074, 20.604502}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.377236, 20.604497}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406611, 20.603862}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399448, 20.603633}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.383215, 20.601403}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.384053, 20.601178}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.380798, 20.600813}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.37591, 20.600506}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.416328, 20.599833}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393797, 20.598759}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398492, 20.597667}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399739, 20.596995}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.372366, 20.596679}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407375, 20.596437}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403316, 20.596183}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407122, 20.595779}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.415007, 20.595231}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392271, 20.594612}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382999, 20.593451}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397006, 20.593169}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398421, 20.592702}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378101, 20.592499}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378986, 20.591885}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401697, 20.591693}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391041, 20.591448}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376605, 20.590774}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404874, 20.590529}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386949, 20.589948}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.4063, 20.589964}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39598, 20.589817}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413461, 20.589804}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.41146, 20.589439}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.397337, 20.589346}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40859, 20.589364}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410065, 20.589232}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404232, 20.589193}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389918, 20.588889}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405837, 20.588632}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.363755, 20.588653}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40052, 20.588274}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376417, 20.588207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40807, 20.587852}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$70"], 
         InsetBox[
          FormBox[
           StyleBox["70", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$70", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$70"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.370604, 20.587858}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$71"], 
         InsetBox[
          FormBox[
           StyleBox["71", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$71", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$71"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409321, 20.587412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$72"], 
         InsetBox[
          FormBox[
           StyleBox["72", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$72", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$72"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403276, 20.58711}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$73"], 
         InsetBox[
          FormBox[
           StyleBox["73", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$73", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$73"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395114, 20.587163}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$74"], 
         InsetBox[
          FormBox[
           StyleBox["74", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$74", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$74"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.385845, 20.587028}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$75"], 
         InsetBox[
          FormBox[
           StyleBox["75", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$75", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$75"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.375135, 20.58707}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$76"], 
         InsetBox[
          FormBox[
           StyleBox["76", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$76", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$76"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.376311, 20.586834}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$77"], 
         InsetBox[
          FormBox[
           StyleBox["77", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$77", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$77"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410515, 20.58699}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$78"], 
         InsetBox[
          FormBox[
           StyleBox["78", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$78", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$78"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396554, 20.586675}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$79"], 
         InsetBox[
          FormBox[
           StyleBox["79", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$79", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$79"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.412089, 20.586412}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$80"], 
         InsetBox[
          FormBox[
           StyleBox["80", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$80", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$80"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405086, 20.5865}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$81"], 
         InsetBox[
          FormBox[
           StyleBox["81", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$81", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$81"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407296, 20.585882}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$82"], 
         InsetBox[
          FormBox[
           StyleBox["82", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$82", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$82"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388782, 20.58596}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$83"], 
         InsetBox[
          FormBox[
           StyleBox["83", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$83", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$83"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40857, 20.585536}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$84"], 
         InsetBox[
          FormBox[
           StyleBox["84", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$84", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$84"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399559, 20.585669}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$85"], 
         InsetBox[
          FormBox[
           StyleBox["85", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$85", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$85"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409745, 20.585233}, 0.00007049631196042688], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$86"], 
         InsetBox[
          FormBox[
           StyleBox["86", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$86", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$86"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410668, 20.584801}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$87"], 
         InsetBox[
          FormBox[
           StyleBox["87", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$87", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$87"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391535, 20.584951}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$88"], 
         InsetBox[
          FormBox[
           StyleBox["88", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$88", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$88"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402166, 20.584754}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$89"], 
         InsetBox[
          FormBox[
           StyleBox["89", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$89", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$89"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404362, 20.58418}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$90"], 
         InsetBox[
          FormBox[
           StyleBox["90", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$90", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$90"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393923, 20.58412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$91"], 
         InsetBox[
          FormBox[
           StyleBox["91", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$91", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$91"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395651, 20.583284}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$92"], 
         InsetBox[
          FormBox[
           StyleBox["92", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$92", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$92"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407144, 20.581739}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$93"], 
         InsetBox[
          FormBox[
           StyleBox["93", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$93", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$93"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.361785, 20.582024}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$94"], 
         InsetBox[
          FormBox[
           StyleBox["94", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$94", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$94"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398079, 20.581724}, 0.00007049631196042688], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$95"], 
         InsetBox[
          FormBox[
           StyleBox["95", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$95", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$95"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399094, 20.580837}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$96"], 
         InsetBox[
          FormBox[
           StyleBox["96", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$96", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$96"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.374104, 20.579458}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$97"], 
         InsetBox[
          FormBox[
           StyleBox["97", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$97", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$97"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382268, 20.577798}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$98"], 
         InsetBox[
          FormBox[
           StyleBox["98", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$98", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$98"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40221, 20.577133}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$99"], 
         InsetBox[
          FormBox[
           StyleBox["99", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$99", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$99"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38544, 20.577177}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$100"], 
         InsetBox[
          FormBox[
           StyleBox["100", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$100", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$100"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389128, 20.576425}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$101"], 
         InsetBox[
          FormBox[
           StyleBox["101", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$101", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$101"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393091, 20.5756}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$102"], 
         InsetBox[
          FormBox[
           StyleBox["102", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$102", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$102"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39633, 20.574997}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$103"], 
         InsetBox[
          FormBox[
           StyleBox["103", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$103", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$103"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399193, 20.574315}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$104"], 
         InsetBox[
          FormBox[
           StyleBox["104", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$104", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$104"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.34087136409994`}, {-107.53887136408935`, 93.71452489352248}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weights", "Subsection"],

Cell[BoxData["17342.110790214312`"], "Output"],

Cell[BoxData["15964.222989883416`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Undirected Quer\[EAcute]taro Urban Zone Dataset - GSH-P Solution Graph\
\>", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
       74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
       92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104}, {
      Null, CompressedData["
1:eJwVxdV2ggAAAFBF7O5ARQwsDEBRUQwsyhY7sfa2D9tPbnu45yLK91IBVCrV
z5//1YAaBDQAqNFq7KANDGt1WqtOrzPpjXqXQW8wGpxGk9FhspgtZqvZY/Fa
3Ta7DbK7HU6nzxVwRd1Bj9cT8/p9AV/EHwmEglAQDkVDCBSG4HAqgkaTcAzO
xdJIHEGRRLyUSCaIZDaVSxXQNFpMZ9LlTD6DZ4lsP4flsXwFK2C1QrNYLlZL
ZKlaxss0XsEZgiQokiLrlVplUKWrPWpIjWuNWrfeq4uNdoOlGVpotprzVqc1
ZQbMrN1pjzpcl+3yPaG3Ykfsoj/uXwb8QB5Kw8mIGy3HEjfl1vyC3wsz4ShO
xJWoSBPpPp1PNzN5dppvF8vFYble7VbP9Ubey9fNdnveHXev/WF/O5yPp9P1
9D4/Lvfr7fq5Pe6K8nx8PV+v9/vz+foFCE1JZg==
       "]}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[33, 34], 
         UndirectedEdge[50, 51], 86, 
         UndirectedEdge[82, 84], 83, 47, 13, 
         UndirectedEdge[34, 47], 11, 40, 
         UndirectedEdge[31, 32], 
         UndirectedEdge[43, 44], 92, 57, 
         UndirectedEdge[57, 66], 8, 61, 
         UndirectedEdge[5, 13], 
         UndirectedEdge[92, 95], 77, 34, 22, 39, 
         UndirectedEdge[17, 22], 14, 33, 
         UndirectedEdge[72, 78], 
         UndirectedEdge[50, 54], 43, 
         UndirectedEdge[68, 85], 26, 82, 
         UndirectedEdge[74, 79], 4, 
         UndirectedEdge[69, 77], 9, 
         UndirectedEdge[55, 57], 
         UndirectedEdge[84, 86], 
         UndirectedEdge[47, 51], 95, 85, 78, 74, 
         UndirectedEdge[70, 72], 
         UndirectedEdge[3, 5], 10, 
         UndirectedEdge[74, 91], 
         UndirectedEdge[4, 14], 
         UndirectedEdge[65, 83], 
         UndirectedEdge[85, 89], 
         UndirectedEdge[76, 77], 81, 51, 
         UndirectedEdge[39, 40], 
         UndirectedEdge[32, 39], 5, 91, 
         UndirectedEdge[22, 24], 55, 32, 50, 89, 58, 17, 79, 
         UndirectedEdge[58, 74], 
         UndirectedEdge[73, 89], 12, 24, 
         UndirectedEdge[2, 3], 
         UndirectedEdge[24, 26], 
         UndirectedEdge[27, 31], 27, 69, 
         UndirectedEdge[9, 10], 
         UndirectedEdge[8, 9], 
         UndirectedEdge[61, 68], 
         UndirectedEdge[8, 17], 
         UndirectedEdge[26, 33], 70, 
         UndirectedEdge[73, 81], 
         UndirectedEdge[65, 74], 
         UndirectedEdge[85, 95], 
         UndirectedEdge[11, 12], 84, 66, 44, 54, 76, 
         UndirectedEdge[91, 92], 72, 
         UndirectedEdge[10, 12], 3, 
         UndirectedEdge[54, 69], 
         UndirectedEdge[58, 61], 65, 
         UndirectedEdge[78, 86], 68, 73, 
         UndirectedEdge[81, 82], 
         UndirectedEdge[11, 13], 31, 
         UndirectedEdge[66, 70], 2, 
         UndirectedEdge[2, 4], 
         UndirectedEdge[40, 43], 
         UndirectedEdge[79, 85], 
         UndirectedEdge[44, 55], 
         UndirectedEdge[14, 27]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0fk7FAgABmBHrsassZFtiw5RmcQmKjm+EhUpHUTGNePIEXPPYGLMYNxH
ytFBtKF1lZVUtLSh0j5soYiENuvYypHcbT+8f8G7nhpy3EdaSkqq5LuXEz6m
0abxmG/dTbvVzUa2sVWgKD4Sauwflh7fiUXmjjZblcIYnJMLahmyiIfG0Klv
/8qdB+dSUFTdwUvofJplLFFJQ7NMyrM+KRFmWbXkvUVhiNdc2yy1modznMW6
zb50FHYRXj1cSsfA8ot5ZhUh8LCbSk9SomCT7inZKmkh6k6lDGhMhKPDKKSp
sEKAKycjRigCFkip+8l/uzDQsXxhfs4mCKK0k0+c7cMgr62g2+8QigLSuh02
xHMwUnco6r4fiheEqt+IiwkwnBjP2WsvwL4eioZMtjcCV/nEOWm4o7/SVpNg
ysKZQcWno80SVLtOjWs7h0M99O4Ts2A6yj09zmtIxHDQu33Pyl4E+YA9f7IH
2JgbksulmMagvbW3ROVIJJQOWhwyd2eif6Go/J1uAGrV8hwNOphwcqVVsi2i
MNXS+TZdQYiOy2pxZF4yXlx1K8u8yYR4+FU1mSLEh7FZnZLdsfCKsWaFfxJg
l87Gl+POEph//G96RWs8PP22re7ti4bcjKpN2R0n5AW7OBt0pGPCLuBMzn46
ckkV1vOTCTAo5Qp6RyKxN7CQEbMiBcoXRKN66VEYLmNQsxmp6FOMWmMaIUQP
YfuYoygS9e3E4AD3CExVubVM1lARqzOroXdYiD92VlYbz/Lg/XJebno0Aq6d
g2aUaxLM9Rhb+NRmQOvyPRu2+wkEj67q4U/xEan43rzOT4jEMKu21w3xMKjM
sc6/KMaPzEVaFl2MH+7u2ZxnIsSDLNddXg488J4rL6v+/tGlfn5fxapEfEna
wp6Jp2EjdXilnzYfVgaSdfs4XJiw05VLrfmYK53XdC+koKtL1pn8IhgzB3hV
vg4MjPTOuO5qSYCwKtvloAkXCto3a6+/5cMzc6lxPTEKX5U19Rs3s3DA/s3I
5F8M9JUz5pZJxaImo/gseYs33in3GZPtvKFE2tz+uIINyy/6jwRMDs4NG2q1
zAaDLO1o6ehDg/iiy+6m11Twhg51bjNhoeTqfcPk1AAQ2b2W+ZqheDjd99nc
jwrtMXZ2gQIT3J99jtTKc3GMFKTntJWF2scL8X1HaIi5J6JlLPlAOk5+cMVi
ANzsRhM9jvoin8JTtPULQaTWDRybjEJL8sfU7AwOnC3GxSKlEATVb8jrVw+B
btKZAXZ4LEaGZ5+2t0lQY3sr26OcBVk/u8SvaixsfaV7gzzpC0rdHj1VIg2n
C4zeq/l54U2r2D9X9SzGavKojXIR4BaZZd7I9USCkcmj5w8D0bChhsQm+mOr
b7ROeQcD+73fXLk+TkP1SpPmJ3QubpjkVESqpuKXFtm1lWI2cqvbf9L7loYt
lh9KG5w8cDx3sEmclACZBfniHct8cTqz0WjAPgBlVz0/DDSzQXWiP6835CO/
XfEZdz4Iny09aGqEYPh/Ozjj/YCOpk9btQUHvNBUrJXaZMlCxa9pihXaaWB2
6+RdrveAzwRJcWQdE1rp11/RJgU49fJ5zeHe0zj5jG1Ot4iE2+HmwtJMOrbt
qHe/H5MC3u1D+aa/n8XarqvdXa+F0DkxFP1PEx+jR2VMLY75w6xAQCJtYmHT
gcTPDT2JsBQ+yikuiMVTff3SzOQLaCf6XrtHoGJpp/MDwrQEIVaM944uQlAI
2wOPmCTCU9SWk7iGAw9O7pf0W+FwzIpKbszkgz/9QCWLGQa9uJzcF2IOFuQ0
ajkWLPwP2e946w==
        "], VertexCoordinates -> CompressedData["
1:eJwt1Hs0lHkcBvDBiUlSolxDNZouSo2XMtheuVaThtZlOqTpJFJis5nGYa2N
XFIUJlG5xNayp0yyYlKDapMiayK51Ig0uUVZVLTPe87+MWfO5zzP9/v+zvvO
O8v2h3kGKtNotGB8qO+lRkM7NQx50gcnnpWy8whyiltolmbAk3LkDuWDVwhy
Y07WsCOctDZoNBWeqHqVMgPbz9djxMBuYXfSuvR50kVJ7RPz4LYG/RRbWDu6
9XPHZYK0CRiqmdLjSUdV0l89gEP7qlf3wyvZjeZbYQvLI5kTcOwO2uxq2NVH
McLG/OiN8mXfLxFk10eJQgRv+p1pPgYPB3YeOgwPmjix+uDcYwmCKrjgUHJW
C6wiyHcX4nwRa/RpJfDBeyfzNsOdXdlXymBFTsOmTlxPEVu1ZB1cq7TOvxYu
lipns+AIt5CCAzBxhd5rB+f5Xnsyif3mWtdnHWBnmVNeGhxFvMiYziXInrJQ
j4NwZl/YxgnY4ZONQIJ5ZrJZ/wjsppvrrY5c5Hj6yAD8aDqXm4Tc9KOUVgeL
bvcaquN804X2e91guUztvAC5csdgtwk8W916mYk8WKH+lxZcYFEzUwVbCZ+E
VOcQJGeXb68S9nf45xdJ4eHKyEIG5isSa7iUg58yPsxBfxHHqlwETxn5nk1H
f1eDjTgRTl1slX0K/YBKeurERYJMl9B3Z8Im2qufvYMj9TJm98JflaPkTbDn
gpb3mrDD/Nd11fB6A5ZxMfbXL2i6dhaWLfSassT+JAttZzd4ZFjU9ytMe7fn
Ng1++7XXMwte/mZhiSybII3mEi9lujxpnZXg832Ye+bC0UXY12bC1yuD1WIU
w83o849OHsuDO3uObtNEXlc0NRVPzTMThNTv9W665gI+LFNvnTRA36x5ImYz
XNxhp3kS5+X8fSlo6AJBWme+Oh2M3JEWPtQBJ29trI6FdV2iPCRwWGByEwv9
a4pylRuwjuIXdwfYcjbmUQJ8U+gVVIF+ibPTRiG8/aLsuSrc5dax/BBcnN+5
wwD9mt1Kk3awBnn1ZwX1e/YWvTeHtTVrHtYjZ/movlCFebqVLTTqfWo5Llej
9gti7/qgP5FntWVWRJDT203NIpE/1TAMnIHXD3j6ecHXvx0W9sPBJmunQtF3
UWPwX8NkT6n3SuQZ4cvS3sCzms5xP8AVtcnv2+GG732jPeiz6ew9L+Anifkh
n3Ge/dOfQmvhsHyp7TTyeXwrURkcsmF+vSueD1uc+K8YtuZdNS5EzlkyaXIR
Zqz5cZMu5u1d+ySZMNKWpbge/+0XQQysPdY2XYl5M3FRF2U2o4LLQr7KValw
Hyx9vsKFer7nmNvaObCLhm3qNrj9cBSXCzfrsG+KsV+37YzAFQ43s7VWho9H
sjZvh5WSJFGL4Z3rH0nZsLH89htn7LfTmfPFCXambgz2RfNZXhvg4t9SSgKo
831t0DOBPeJlw0PI8w0suhlwybj9Ci3kf3C7/b5lEWRcS/8zOfbn10cLlZAb
0RuUqfsbUTB+sx85nm7AecyPuXoEjcAP/9R1tEFumxIha4Hl+8L2u8H0UnF8
Rdb/7w/6lvUuvPuw+oHIJTVwIrEm/BYsDmNqyWGLL+r/pMIGyc/p1nD8x+C3
SfDO4PO+nvAt1+v6nnDzhopi6n143NT5ciiTIO82Okczcb/Lo/1PfYf9zvqY
RqG/l2PZPQgr4tJqU6jruTdLxDB/94DHGPqtQWp+PnBphqUO9f9zR9xx7kMG
QY6rqTRT59vyE23mHhxblKRRiryHZvm6FnaPMRUOwI+zJaJ0+JQ3M2gV+j0Z
iQl7YGPDuBP+sAcHE/Ct8cZ9qdT/aU6Tuyr8H1Ql8gA=
        "]}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$14", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$24", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$33", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$31", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$37", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$31", Automatic, Center], 
          DynamicLocation["VertexID$32", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$32", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$33", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$34", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$54", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$56", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$54", Automatic, Center], 
          DynamicLocation["VertexID$69", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$75", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$66", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$58", Automatic, Center], 
          DynamicLocation["VertexID$61", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$58", Automatic, Center], 
          DynamicLocation["VertexID$74", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$78", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$61", Automatic, Center], 
          DynamicLocation["VertexID$68", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$79", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$70", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$72", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$74", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$83", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$66", Automatic, Center], 
          DynamicLocation["VertexID$70", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$81", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$94", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$68", Automatic, Center], 
          DynamicLocation["VertexID$85", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$69", Automatic, Center], 
         DynamicLocation["VertexID$76", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$69", Automatic, Center], 
          DynamicLocation["VertexID$77", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$70", Automatic, Center], 
          DynamicLocation["VertexID$72", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$70", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$71", Automatic, Center], 
         DynamicLocation["VertexID$76", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$72", Automatic, Center], 
          DynamicLocation["VertexID$78", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$72", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$81", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$91", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$83", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$76", Automatic, Center], 
          DynamicLocation["VertexID$77", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$76", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$78", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$78", Automatic, Center], 
          DynamicLocation["VertexID$86", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$79", Automatic, Center], 
          DynamicLocation["VertexID$85", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$79", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$80", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$81", Automatic, Center], 
          DynamicLocation["VertexID$82", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$81", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$82", Automatic, Center], 
          DynamicLocation["VertexID$84", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$88", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$84", Automatic, Center], 
          DynamicLocation["VertexID$86", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$85", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$85", Automatic, Center], 
          DynamicLocation["VertexID$95", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$86", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$87", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$91", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$96", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$90", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$91", Automatic, Center], 
          DynamicLocation["VertexID$92", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$92", Automatic, Center], 
          DynamicLocation["VertexID$95", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$92", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$93", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$94", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$96", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$96", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$97", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$98", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$99", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$100", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$101", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$102", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$103", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.422626, 20.621989}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.414987, 20.620784}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410396, 20.619338}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.421725, 20.619097}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404087, 20.61737}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394259, 20.616607}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.390182, 20.616285}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388987, 20.614291}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389989, 20.613922}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394222, 20.613268}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399836, 20.613055}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397181, 20.612857}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401945, 20.612573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.413654, 20.611969}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409706, 20.61207}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38838, 20.610063}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386802, 20.610147}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.381142, 20.610304}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.405749, 20.610382}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399391, 20.609277}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39687, 20.6092101}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386442, 20.609114}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391256, 20.609}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38332, 20.608429}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.406867, 20.606573}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382298, 20.605955}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408668, 20.605739}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.417602, 20.604407}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39074, 20.604502}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.377236, 20.604497}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406611, 20.603862}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399448, 20.603633}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.383215, 20.601403}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.384053, 20.601178}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.380798, 20.600813}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.37591, 20.600506}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.416328, 20.599833}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393797, 20.598759}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398492, 20.597667}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399739, 20.596995}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.372366, 20.596679}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407375, 20.596437}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403316, 20.596183}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407122, 20.595779}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.415007, 20.595231}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392271, 20.594612}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382999, 20.593451}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.397006, 20.593169}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398421, 20.592702}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378101, 20.592499}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378986, 20.591885}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.401697, 20.591693}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391041, 20.591448}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376605, 20.590774}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404874, 20.590529}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.386949, 20.589948}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.4063, 20.589964}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39598, 20.589817}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413461, 20.589804}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41146, 20.589439}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397337, 20.589346}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40859, 20.589364}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410065, 20.589232}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404232, 20.589193}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389918, 20.588889}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405837, 20.588632}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.363755, 20.588653}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40052, 20.588274}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376417, 20.588207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40807, 20.587852}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$70"], 
         InsetBox[
          FormBox[
           StyleBox["70", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$70", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$70"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.370604, 20.587858}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$71"], 
         InsetBox[
          FormBox[
           StyleBox["71", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$71", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$71"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409321, 20.587412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$72"], 
         InsetBox[
          FormBox[
           StyleBox["72", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$72", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$72"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403276, 20.58711}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$73"], 
         InsetBox[
          FormBox[
           StyleBox["73", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$73", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$73"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395114, 20.587163}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$74"], 
         InsetBox[
          FormBox[
           StyleBox["74", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$74", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$74"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.385845, 20.587028}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$75"], 
         InsetBox[
          FormBox[
           StyleBox["75", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$75", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$75"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.375135, 20.58707}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$76"], 
         InsetBox[
          FormBox[
           StyleBox["76", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$76", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$76"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376311, 20.586834}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$77"], 
         InsetBox[
          FormBox[
           StyleBox["77", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$77", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$77"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410515, 20.58699}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$78"], 
         InsetBox[
          FormBox[
           StyleBox["78", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$78", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$78"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396554, 20.586675}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$79"], 
         InsetBox[
          FormBox[
           StyleBox["79", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$79", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$79"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.412089, 20.586412}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$80"], 
         InsetBox[
          FormBox[
           StyleBox["80", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$80", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$80"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405086, 20.5865}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$81"], 
         InsetBox[
          FormBox[
           StyleBox["81", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$81", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$81"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407296, 20.585882}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$82"], 
         InsetBox[
          FormBox[
           StyleBox["82", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$82", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$82"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388782, 20.58596}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$83"], 
         InsetBox[
          FormBox[
           StyleBox["83", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$83", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$83"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40857, 20.585536}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$84"], 
         InsetBox[
          FormBox[
           StyleBox["84", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$84", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$84"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399559, 20.585669}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$85"], 
         InsetBox[
          FormBox[
           StyleBox["85", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$85", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$85"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409745, 20.585233}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$86"], 
         InsetBox[
          FormBox[
           StyleBox["86", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$86", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$86"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410668, 20.584801}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$87"], 
         InsetBox[
          FormBox[
           StyleBox["87", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$87", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$87"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391535, 20.584951}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$88"], 
         InsetBox[
          FormBox[
           StyleBox["88", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$88", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$88"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402166, 20.584754}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$89"], 
         InsetBox[
          FormBox[
           StyleBox["89", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$89", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$89"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404362, 20.58418}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$90"], 
         InsetBox[
          FormBox[
           StyleBox["90", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$90", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$90"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393923, 20.58412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$91"], 
         InsetBox[
          FormBox[
           StyleBox["91", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$91", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$91"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395651, 20.583284}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$92"], 
         InsetBox[
          FormBox[
           StyleBox["92", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$92", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$92"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407144, 20.581739}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$93"], 
         InsetBox[
          FormBox[
           StyleBox["93", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$93", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$93"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.361785, 20.582024}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$94"], 
         InsetBox[
          FormBox[
           StyleBox["94", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$94", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$94"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398079, 20.581724}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$95"], 
         InsetBox[
          FormBox[
           StyleBox["95", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$95", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$95"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399094, 20.580837}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$96"], 
         InsetBox[
          FormBox[
           StyleBox["96", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$96", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$96"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.374104, 20.579458}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$97"], 
         InsetBox[
          FormBox[
           StyleBox["97", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$97", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$97"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382268, 20.577798}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$98"], 
         InsetBox[
          FormBox[
           StyleBox["98", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$98", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$98"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40221, 20.577133}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$99"], 
         InsetBox[
          FormBox[
           StyleBox["99", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$99", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$99"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38544, 20.577177}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$100"], 
         InsetBox[
          FormBox[
           StyleBox["100", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$100", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$100"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389128, 20.576425}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$101"], 
         InsetBox[
          FormBox[
           StyleBox["101", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$101", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$101"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393091, 20.5756}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$102"], 
         InsetBox[
          FormBox[
           StyleBox["102", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$102", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$102"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39633, 20.574997}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$103"], 
         InsetBox[
          FormBox[
           StyleBox["103", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$103", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$103"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399193, 20.574315}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$104"], 
         InsetBox[
          FormBox[
           StyleBox["104", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$104", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$104"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{16.585786437626894`, 
       359.450642050582}, {-142.72304205056912`, 128.50312672705707`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15,
  ImageSize->{483.99999999999966`, Automatic}]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["19456.3809047215`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Undirected Quer\[EAcute]taro Urban Zone Dataset - GSH-C Solution Graph\
\>", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
       74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
       92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104}, {
      Null, CompressedData["
1:eJwVxdV2ggAAAFBF7O5ARQwsDEBRUQwsyhY7sfa2D9tPbnu45yLK91IBVCrV
z5//1YAaBDQAqNFq7KANDGt1WqtOrzPpjXqXQW8wGpxGk9FhspgtZqvZY/Fa
3Ta7DbK7HU6nzxVwRd1Bj9cT8/p9AV/EHwmEglAQDkVDCBSG4HAqgkaTcAzO
xdJIHEGRRLyUSCaIZDaVSxXQNFpMZ9LlTD6DZ4lsP4flsXwFK2C1QrNYLlZL
ZKlaxss0XsEZgiQokiLrlVplUKWrPWpIjWuNWrfeq4uNdoOlGVpotprzVqc1
ZQbMrN1pjzpcl+3yPaG3Ykfsoj/uXwb8QB5Kw8mIGy3HEjfl1vyC3wsz4ShO
xJWoSBPpPp1PNzN5dppvF8vFYble7VbP9Ubey9fNdnveHXev/WF/O5yPp9P1
9D4/Lvfr7fq5Pe6K8nx8PV+v9/vz+foFCE1JZg==
       "]}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[33, 34], 
         UndirectedEdge[51, 54], 
         UndirectedEdge[50, 51], 86, 
         UndirectedEdge[82, 84], 83, 47, 
         UndirectedEdge[71, 76], 13, 
         UndirectedEdge[34, 47], 11, 40, 
         UndirectedEdge[31, 32], 
         UndirectedEdge[43, 44], 92, 57, 
         UndirectedEdge[57, 66], 8, 61, 75, 
         UndirectedEdge[5, 13], 
         UndirectedEdge[92, 95], 77, 34, 22, 39, 
         UndirectedEdge[17, 22], 14, 33, 
         UndirectedEdge[72, 78], 
         UndirectedEdge[50, 54], 43, 
         UndirectedEdge[68, 85], 71, 26, 82, 
         UndirectedEdge[74, 79], 4, 
         UndirectedEdge[69, 77], 9, 
         UndirectedEdge[55, 57], 
         UndirectedEdge[84, 86], 
         UndirectedEdge[47, 51], 95, 85, 78, 74, 
         UndirectedEdge[70, 72], 
         UndirectedEdge[3, 5], 10, 
         UndirectedEdge[74, 91], 
         UndirectedEdge[4, 14], 
         UndirectedEdge[65, 83], 
         UndirectedEdge[85, 89], 
         UndirectedEdge[76, 77], 81, 51, 56, 
         UndirectedEdge[39, 40], 
         UndirectedEdge[32, 39], 5, 91, 
         UndirectedEdge[22, 24], 55, 32, 50, 89, 58, 17, 79, 
         UndirectedEdge[58, 74], 
         UndirectedEdge[73, 89], 12, 24, 
         UndirectedEdge[24, 26], 
         UndirectedEdge[2, 3], 
         UndirectedEdge[56, 75], 
         UndirectedEdge[27, 31], 
         UndirectedEdge[51, 56], 27, 69, 
         UndirectedEdge[9, 10], 
         UndirectedEdge[8, 9], 
         UndirectedEdge[61, 68], 
         UndirectedEdge[54, 71], 
         UndirectedEdge[8, 17], 
         UndirectedEdge[26, 33], 70, 
         UndirectedEdge[73, 81], 
         UndirectedEdge[65, 74], 
         UndirectedEdge[85, 95], 
         UndirectedEdge[11, 12], 84, 66, 44, 54, 
         UndirectedEdge[75, 83], 76, 
         UndirectedEdge[91, 92], 72, 
         UndirectedEdge[10, 12], 3, 
         UndirectedEdge[54, 69], 
         UndirectedEdge[58, 61], 65, 
         UndirectedEdge[78, 86], 68, 73, 
         UndirectedEdge[81, 82], 
         UndirectedEdge[11, 13], 31, 
         UndirectedEdge[66, 70], 2, 
         UndirectedEdge[2, 4], 
         UndirectedEdge[40, 43], 
         UndirectedEdge[79, 85], 
         UndirectedEdge[44, 55], 
         UndirectedEdge[14, 27]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0fk7FAgABmBHrsassZFtiw5RmcQmKjm+EhUpHUTGNePIEXPPYGLMYNxH
ytFBtKF1lZVUtLSh0j5soYiENuvYypHcbT+8f8G7nhpy3EdaSkqq5LuXEz6m
0abxmG/dTbvVzUa2sVWgKD4Sauwflh7fiUXmjjZblcIYnJMLahmyiIfG0Klv
/8qdB+dSUFTdwUvofJplLFFJQ7NMyrM+KRFmWbXkvUVhiNdc2yy1modznMW6
zb50FHYRXj1cSsfA8ot5ZhUh8LCbSk9SomCT7inZKmkh6k6lDGhMhKPDKKSp
sEKAKycjRigCFkip+8l/uzDQsXxhfs4mCKK0k0+c7cMgr62g2+8QigLSuh02
xHMwUnco6r4fiheEqt+IiwkwnBjP2WsvwL4eioZMtjcCV/nEOWm4o7/SVpNg
ysKZQcWno80SVLtOjWs7h0M99O4Ts2A6yj09zmtIxHDQu33Pyl4E+YA9f7IH
2JgbksulmMagvbW3ROVIJJQOWhwyd2eif6Go/J1uAGrV8hwNOphwcqVVsi2i
MNXS+TZdQYiOy2pxZF4yXlx1K8u8yYR4+FU1mSLEh7FZnZLdsfCKsWaFfxJg
l87Gl+POEph//G96RWs8PP22re7ti4bcjKpN2R0n5AW7OBt0pGPCLuBMzn46
ckkV1vOTCTAo5Qp6RyKxN7CQEbMiBcoXRKN66VEYLmNQsxmp6FOMWmMaIUQP
YfuYoygS9e3E4AD3CExVubVM1lARqzOroXdYiD92VlYbz/Lg/XJebno0Aq6d
g2aUaxLM9Rhb+NRmQOvyPRu2+wkEj67q4U/xEan43rzOT4jEMKu21w3xMKjM
sc6/KMaPzEVaFl2MH+7u2ZxnIsSDLNddXg488J4rL6v+/tGlfn5fxapEfEna
wp6Jp2EjdXilnzYfVgaSdfs4XJiw05VLrfmYK53XdC+koKtL1pn8IhgzB3hV
vg4MjPTOuO5qSYCwKtvloAkXCto3a6+/5cMzc6lxPTEKX5U19Rs3s3DA/s3I
5F8M9JUz5pZJxaImo/gseYs33in3GZPtvKFE2tz+uIINyy/6jwRMDs4NG2q1
zAaDLO1o6ehDg/iiy+6m11Twhg51bjNhoeTqfcPk1AAQ2b2W+ZqheDjd99nc
jwrtMXZ2gQIT3J99jtTKc3GMFKTntJWF2scL8X1HaIi5J6JlLPlAOk5+cMVi
ANzsRhM9jvoin8JTtPULQaTWDRybjEJL8sfU7AwOnC3GxSKlEATVb8jrVw+B
btKZAXZ4LEaGZ5+2t0lQY3sr26OcBVk/u8SvaixsfaV7gzzpC0rdHj1VIg2n
C4zeq/l54U2r2D9X9SzGavKojXIR4BaZZd7I9USCkcmj5w8D0bChhsQm+mOr
b7ROeQcD+73fXLk+TkP1SpPmJ3QubpjkVESqpuKXFtm1lWI2cqvbf9L7loYt
lh9KG5w8cDx3sEmclACZBfniHct8cTqz0WjAPgBlVz0/DDSzQXWiP6835CO/
XfEZdz4Iny09aGqEYPh/Ozjj/YCOpk9btQUHvNBUrJXaZMlCxa9pihXaaWB2
6+RdrveAzwRJcWQdE1rp11/RJgU49fJ5zeHe0zj5jG1Ot4iE2+HmwtJMOrbt
qHe/H5MC3u1D+aa/n8XarqvdXa+F0DkxFP1PEx+jR2VMLY75w6xAQCJtYmHT
gcTPDT2JsBQ+yikuiMVTff3SzOQLaCf6XrtHoGJpp/MDwrQEIVaM944uQlAI
2wOPmCTCU9SWk7iGAw9O7pf0W+FwzIpKbszkgz/9QCWLGQa9uJzcF2IOFuQ0
ajkWLPwP2e946w==
        "], VertexCoordinates -> CompressedData["
1:eJwt1Hs0lHkcBvDBiUlSolxDNZouSo2XMtheuVaThtZlOqTpJFJis5nGYa2N
XFIUJlG5xNayp0yyYlKDapMiayK51Ig0uUVZVLTPe87+MWfO5zzP9/v+zvvO
O8v2h3kGKtNotGB8qO+lRkM7NQx50gcnnpWy8whyiltolmbAk3LkDuWDVwhy
Y07WsCOctDZoNBWeqHqVMgPbz9djxMBuYXfSuvR50kVJ7RPz4LYG/RRbWDu6
9XPHZYK0CRiqmdLjSUdV0l89gEP7qlf3wyvZjeZbYQvLI5kTcOwO2uxq2NVH
McLG/OiN8mXfLxFk10eJQgRv+p1pPgYPB3YeOgwPmjix+uDcYwmCKrjgUHJW
C6wiyHcX4nwRa/RpJfDBeyfzNsOdXdlXymBFTsOmTlxPEVu1ZB1cq7TOvxYu
lipns+AIt5CCAzBxhd5rB+f5Xnsyif3mWtdnHWBnmVNeGhxFvMiYziXInrJQ
j4NwZl/YxgnY4ZONQIJ5ZrJZ/wjsppvrrY5c5Hj6yAD8aDqXm4Tc9KOUVgeL
bvcaquN804X2e91guUztvAC5csdgtwk8W916mYk8WKH+lxZcYFEzUwVbCZ+E
VOcQJGeXb68S9nf45xdJ4eHKyEIG5isSa7iUg58yPsxBfxHHqlwETxn5nk1H
f1eDjTgRTl1slX0K/YBKeurERYJMl9B3Z8Im2qufvYMj9TJm98JflaPkTbDn
gpb3mrDD/Nd11fB6A5ZxMfbXL2i6dhaWLfSassT+JAttZzd4ZFjU9ytMe7fn
Ng1++7XXMwte/mZhiSybII3mEi9lujxpnZXg832Ye+bC0UXY12bC1yuD1WIU
w83o849OHsuDO3uObtNEXlc0NRVPzTMThNTv9W665gI+LFNvnTRA36x5ImYz
XNxhp3kS5+X8fSlo6AJBWme+Oh2M3JEWPtQBJ29trI6FdV2iPCRwWGByEwv9
a4pylRuwjuIXdwfYcjbmUQJ8U+gVVIF+ibPTRiG8/aLsuSrc5dax/BBcnN+5
wwD9mt1Kk3awBnn1ZwX1e/YWvTeHtTVrHtYjZ/movlCFebqVLTTqfWo5Llej
9gti7/qgP5FntWVWRJDT203NIpE/1TAMnIHXD3j6ecHXvx0W9sPBJmunQtF3
UWPwX8NkT6n3SuQZ4cvS3sCzms5xP8AVtcnv2+GG732jPeiz6ew9L+Anifkh
n3Ge/dOfQmvhsHyp7TTyeXwrURkcsmF+vSueD1uc+K8YtuZdNS5EzlkyaXIR
Zqz5cZMu5u1d+ySZMNKWpbge/+0XQQysPdY2XYl5M3FRF2U2o4LLQr7KValw
Hyx9vsKFer7nmNvaObCLhm3qNrj9cBSXCzfrsG+KsV+37YzAFQ43s7VWho9H
sjZvh5WSJFGL4Z3rH0nZsLH89htn7LfTmfPFCXambgz2RfNZXhvg4t9SSgKo
831t0DOBPeJlw0PI8w0suhlwybj9Ci3kf3C7/b5lEWRcS/8zOfbn10cLlZAb
0RuUqfsbUTB+sx85nm7AecyPuXoEjcAP/9R1tEFumxIha4Hl+8L2u8H0UnF8
Rdb/7w/6lvUuvPuw+oHIJTVwIrEm/BYsDmNqyWGLL+r/pMIGyc/p1nD8x+C3
SfDO4PO+nvAt1+v6nnDzhopi6n143NT5ciiTIO82Okczcb/Lo/1PfYf9zvqY
RqG/l2PZPQgr4tJqU6jruTdLxDB/94DHGPqtQWp+PnBphqUO9f9zR9xx7kMG
QY6rqTRT59vyE23mHhxblKRRiryHZvm6FnaPMRUOwI+zJaJ0+JQ3M2gV+j0Z
iQl7YGPDuBP+sAcHE/Ct8cZ9qdT/aU6Tuyr8H1Ql8gA=
        "]}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$4", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$14", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$13", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$15", Automatic, Center], 
         DynamicLocation["VertexID$19", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$24", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$33", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$31", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$37", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$31", Automatic, Center], 
          DynamicLocation["VertexID$32", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$32", Automatic, Center], 
          DynamicLocation["VertexID$39", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$33", Automatic, Center], 
          DynamicLocation["VertexID$34", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$34", Automatic, Center], 
          DynamicLocation["VertexID$47", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$47", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$54", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$54", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$54", Automatic, Center], 
          DynamicLocation["VertexID$69", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$54", Automatic, Center], 
          DynamicLocation["VertexID$71", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$75", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$66", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$58", Automatic, Center], 
          DynamicLocation["VertexID$61", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$58", Automatic, Center], 
          DynamicLocation["VertexID$74", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$78", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$61", Automatic, Center], 
          DynamicLocation["VertexID$68", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$79", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$70", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$72", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$74", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$83", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$66", Automatic, Center], 
          DynamicLocation["VertexID$70", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$81", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$94", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$68", Automatic, Center], 
          DynamicLocation["VertexID$85", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$69", Automatic, Center], 
         DynamicLocation["VertexID$76", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$69", Automatic, Center], 
          DynamicLocation["VertexID$77", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$70", Automatic, Center], 
          DynamicLocation["VertexID$72", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$70", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$71", Automatic, Center], 
          DynamicLocation["VertexID$76", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$72", Automatic, Center], 
          DynamicLocation["VertexID$78", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$72", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$81", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$91", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$75", Automatic, Center], 
          DynamicLocation["VertexID$83", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$76", Automatic, Center], 
          DynamicLocation["VertexID$77", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$76", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$78", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$78", Automatic, Center], 
          DynamicLocation["VertexID$86", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$79", Automatic, Center], 
          DynamicLocation["VertexID$85", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$79", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$80", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$81", Automatic, Center], 
          DynamicLocation["VertexID$82", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$81", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$82", Automatic, Center], 
          DynamicLocation["VertexID$84", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$88", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$84", Automatic, Center], 
          DynamicLocation["VertexID$86", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$85", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$85", Automatic, Center], 
          DynamicLocation["VertexID$95", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$86", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$87", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$91", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$96", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$90", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$91", Automatic, Center], 
          DynamicLocation["VertexID$92", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$92", Automatic, Center], 
          DynamicLocation["VertexID$95", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$92", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$93", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$94", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$96", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$96", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$97", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$98", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$99", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$100", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$101", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$102", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$103", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.422626, 20.621989}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.414987, 20.620784}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410396, 20.619338}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.421725, 20.619097}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404087, 20.61737}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394259, 20.616607}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.390182, 20.616285}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388987, 20.614291}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389989, 20.613922}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394222, 20.613268}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399836, 20.613055}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397181, 20.612857}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401945, 20.612573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.413654, 20.611969}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409706, 20.61207}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38838, 20.610063}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386802, 20.610147}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.381142, 20.610304}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.405749, 20.610382}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399391, 20.609277}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39687, 20.6092101}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386442, 20.609114}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391256, 20.609}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38332, 20.608429}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.406867, 20.606573}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382298, 20.605955}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408668, 20.605739}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.417602, 20.604407}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39074, 20.604502}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.377236, 20.604497}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406611, 20.603862}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399448, 20.603633}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.383215, 20.601403}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.384053, 20.601178}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.380798, 20.600813}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.37591, 20.600506}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.416328, 20.599833}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393797, 20.598759}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398492, 20.597667}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399739, 20.596995}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.372366, 20.596679}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407375, 20.596437}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403316, 20.596183}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407122, 20.595779}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.415007, 20.595231}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392271, 20.594612}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382999, 20.593451}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.397006, 20.593169}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398421, 20.592702}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378101, 20.592499}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378986, 20.591885}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.401697, 20.591693}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391041, 20.591448}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376605, 20.590774}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404874, 20.590529}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386949, 20.589948}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.4063, 20.589964}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39598, 20.589817}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413461, 20.589804}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41146, 20.589439}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397337, 20.589346}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40859, 20.589364}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410065, 20.589232}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404232, 20.589193}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389918, 20.588889}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405837, 20.588632}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.363755, 20.588653}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40052, 20.588274}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376417, 20.588207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40807, 20.587852}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$70"], 
         InsetBox[
          FormBox[
           StyleBox["70", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$70", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$70"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.370604, 20.587858}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$71"], 
         InsetBox[
          FormBox[
           StyleBox["71", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$71", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$71"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409321, 20.587412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$72"], 
         InsetBox[
          FormBox[
           StyleBox["72", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$72", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$72"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403276, 20.58711}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$73"], 
         InsetBox[
          FormBox[
           StyleBox["73", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$73", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$73"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395114, 20.587163}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$74"], 
         InsetBox[
          FormBox[
           StyleBox["74", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$74", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$74"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.385845, 20.587028}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$75"], 
         InsetBox[
          FormBox[
           StyleBox["75", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$75", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$75"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.375135, 20.58707}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$76"], 
         InsetBox[
          FormBox[
           StyleBox["76", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$76", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$76"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376311, 20.586834}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$77"], 
         InsetBox[
          FormBox[
           StyleBox["77", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$77", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$77"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410515, 20.58699}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$78"], 
         InsetBox[
          FormBox[
           StyleBox["78", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$78", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$78"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396554, 20.586675}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$79"], 
         InsetBox[
          FormBox[
           StyleBox["79", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$79", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$79"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.412089, 20.586412}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$80"], 
         InsetBox[
          FormBox[
           StyleBox["80", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$80", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$80"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405086, 20.5865}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$81"], 
         InsetBox[
          FormBox[
           StyleBox["81", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$81", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$81"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407296, 20.585882}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$82"], 
         InsetBox[
          FormBox[
           StyleBox["82", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$82", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$82"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388782, 20.58596}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$83"], 
         InsetBox[
          FormBox[
           StyleBox["83", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$83", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$83"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40857, 20.585536}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$84"], 
         InsetBox[
          FormBox[
           StyleBox["84", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$84", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$84"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399559, 20.585669}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$85"], 
         InsetBox[
          FormBox[
           StyleBox["85", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$85", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$85"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409745, 20.585233}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$86"], 
         InsetBox[
          FormBox[
           StyleBox["86", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$86", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$86"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410668, 20.584801}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$87"], 
         InsetBox[
          FormBox[
           StyleBox["87", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$87", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$87"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391535, 20.584951}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$88"], 
         InsetBox[
          FormBox[
           StyleBox["88", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$88", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$88"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402166, 20.584754}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$89"], 
         InsetBox[
          FormBox[
           StyleBox["89", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$89", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$89"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404362, 20.58418}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$90"], 
         InsetBox[
          FormBox[
           StyleBox["90", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$90", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$90"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.393923, 20.58412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$91"], 
         InsetBox[
          FormBox[
           StyleBox["91", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$91", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$91"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395651, 20.583284}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$92"], 
         InsetBox[
          FormBox[
           StyleBox["92", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$92", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$92"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407144, 20.581739}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$93"], 
         InsetBox[
          FormBox[
           StyleBox["93", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$93", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$93"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.361785, 20.582024}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$94"], 
         InsetBox[
          FormBox[
           StyleBox["94", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$94", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$94"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398079, 20.581724}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$95"], 
         InsetBox[
          FormBox[
           StyleBox["95", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$95", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$95"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399094, 20.580837}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$96"], 
         InsetBox[
          FormBox[
           StyleBox["96", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$96", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$96"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.374104, 20.579458}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$97"], 
         InsetBox[
          FormBox[
           StyleBox["97", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$97", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$97"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382268, 20.577798}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$98"], 
         InsetBox[
          FormBox[
           StyleBox["98", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$98", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$98"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40221, 20.577133}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$99"], 
         InsetBox[
          FormBox[
           StyleBox["99", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$99", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$99"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38544, 20.577177}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$100"], 
         InsetBox[
          FormBox[
           StyleBox["100", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$100", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$100"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389128, 20.576425}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$101"], 
         InsetBox[
          FormBox[
           StyleBox["101", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$101", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$101"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393091, 20.5756}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$102"], 
         InsetBox[
          FormBox[
           StyleBox["102", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$102", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$102"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39633, 20.574997}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$103"], 
         InsetBox[
          FormBox[
           StyleBox["103", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$103", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$103"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399193, 20.574315}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$104"], 
         InsetBox[
          FormBox[
           StyleBox["104", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$104", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$104"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.34087136409994`}, {-107.53887136408935`, 93.71452489352248}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["22446.558064142104`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Undirected Quer\[EAcute]taro Urban Zone Dataset - GC-IE Solution Graph\
\>", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
       74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
       92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104}, {
      Null, CompressedData["
1:eJwVxdV2ggAAAFBF7O5ARQwsDEBRUQwsyhY7sfa2D9tPbnu45yLK91IBVCrV
z5//1YAaBDQAqNFq7KANDGt1WqtOrzPpjXqXQW8wGpxGk9FhspgtZqvZY/Fa
3Ta7DbK7HU6nzxVwRd1Bj9cT8/p9AV/EHwmEglAQDkVDCBSG4HAqgkaTcAzO
xdJIHEGRRLyUSCaIZDaVSxXQNFpMZ9LlTD6DZ4lsP4flsXwFK2C1QrNYLlZL
ZKlaxss0XsEZgiQokiLrlVplUKWrPWpIjWuNWrfeq4uNdoOlGVpotprzVqc1
ZQbMrN1pjzpcl+3yPaG3Ykfsoj/uXwb8QB5Kw8mIGy3HEjfl1vyC3wsz4ShO
xJWoSBPpPp1PNzN5dppvF8vFYble7VbP9Ubey9fNdnveHXev/WF/O5yPp9P1
9D4/Lvfr7fq5Pe6K8nx8PV+v9/vz+foFCE1JZg==
       "]}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[51, 54], 
         UndirectedEdge[49, 61], 46, 
         UndirectedEdge[50, 51], 83, 
         UndirectedEdge[56, 65], 30, 
         UndirectedEdge[89, 96], 
         UndirectedEdge[71, 76], 13, 11, 40, 
         UndirectedEdge[15, 19], 
         UndirectedEdge[13, 19], 
         UndirectedEdge[46, 48], 
         UndirectedEdge[43, 44], 
         UndirectedEdge[35, 50], 48, 62, 8, 61, 
         UndirectedEdge[40, 49], 22, 39, 
         UndirectedEdge[17, 22], 14, 
         UndirectedEdge[72, 78], 43, 
         UndirectedEdge[35, 36], 
         UndirectedEdge[68, 85], 71, 26, 
         UndirectedEdge[74, 79], 4, 9, 
         UndirectedEdge[26, 30], 95, 85, 78, 74, 
         UndirectedEdge[95, 96], 10, 
         UndirectedEdge[4, 14], 
         UndirectedEdge[65, 83], 51, 56, 
         UndirectedEdge[39, 40], 19, 15, 
         UndirectedEdge[62, 70], 
         UndirectedEdge[22, 24], 50, 89, 
         UndirectedEdge[39, 48], 17, 
         UndirectedEdge[64, 66], 79, 
         UndirectedEdge[73, 89], 12, 24, 
         UndirectedEdge[24, 26], 63, 36, 
         UndirectedEdge[61, 79], 
         UndirectedEdge[51, 56], 27, 
         UndirectedEdge[9, 10], 
         UndirectedEdge[8, 9], 
         UndirectedEdge[62, 63], 
         UndirectedEdge[61, 68], 
         UndirectedEdge[54, 71], 
         UndirectedEdge[8, 17], 
         UndirectedEdge[63, 72], 70, 
         UndirectedEdge[85, 95], 
         UndirectedEdge[65, 74], 96, 
         UndirectedEdge[11, 12], 84, 66, 44, 54, 76, 
         UndirectedEdge[14, 15], 72, 
         UndirectedEdge[10, 12], 88, 
         UndirectedEdge[72, 84], 35, 64, 65, 
         UndirectedEdge[30, 36], 68, 73, 49, 
         UndirectedEdge[11, 13], 
         UndirectedEdge[66, 70], 
         UndirectedEdge[64, 73], 
         UndirectedEdge[83, 88], 
         UndirectedEdge[40, 43], 
         UndirectedEdge[14, 27]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0fk7FAgABmBHrsassZFtiw5RmcQmKjm+EhUpHUTGNePIEXPPYGLMYNxH
ytFBtKF1lZVUtLSh0j5soYiENuvYypHcbT+8f8G7nhpy3EdaSkqq5LuXEz6m
0abxmG/dTbvVzUa2sVWgKD4Sauwflh7fiUXmjjZblcIYnJMLahmyiIfG0Klv
/8qdB+dSUFTdwUvofJplLFFJQ7NMyrM+KRFmWbXkvUVhiNdc2yy1modznMW6
zb50FHYRXj1cSsfA8ot5ZhUh8LCbSk9SomCT7inZKmkh6k6lDGhMhKPDKKSp
sEKAKycjRigCFkip+8l/uzDQsXxhfs4mCKK0k0+c7cMgr62g2+8QigLSuh02
xHMwUnco6r4fiheEqt+IiwkwnBjP2WsvwL4eioZMtjcCV/nEOWm4o7/SVpNg
ysKZQcWno80SVLtOjWs7h0M99O4Ts2A6yj09zmtIxHDQu33Pyl4E+YA9f7IH
2JgbksulmMagvbW3ROVIJJQOWhwyd2eif6Go/J1uAGrV8hwNOphwcqVVsi2i
MNXS+TZdQYiOy2pxZF4yXlx1K8u8yYR4+FU1mSLEh7FZnZLdsfCKsWaFfxJg
l87Gl+POEph//G96RWs8PP22re7ti4bcjKpN2R0n5AW7OBt0pGPCLuBMzn46
ckkV1vOTCTAo5Qp6RyKxN7CQEbMiBcoXRKN66VEYLmNQsxmp6FOMWmMaIUQP
YfuYoygS9e3E4AD3CExVubVM1lARqzOroXdYiD92VlYbz/Lg/XJebno0Aq6d
g2aUaxLM9Rhb+NRmQOvyPRu2+wkEj67q4U/xEan43rzOT4jEMKu21w3xMKjM
sc6/KMaPzEVaFl2MH+7u2ZxnIsSDLNddXg488J4rL6v+/tGlfn5fxapEfEna
wp6Jp2EjdXilnzYfVgaSdfs4XJiw05VLrfmYK53XdC+koKtL1pn8IhgzB3hV
vg4MjPTOuO5qSYCwKtvloAkXCto3a6+/5cMzc6lxPTEKX5U19Rs3s3DA/s3I
5F8M9JUz5pZJxaImo/gseYs33in3GZPtvKFE2tz+uIINyy/6jwRMDs4NG2q1
zAaDLO1o6ehDg/iiy+6m11Twhg51bjNhoeTqfcPk1AAQ2b2W+ZqheDjd99nc
jwrtMXZ2gQIT3J99jtTKc3GMFKTntJWF2scL8X1HaIi5J6JlLPlAOk5+cMVi
ANzsRhM9jvoin8JTtPULQaTWDRybjEJL8sfU7AwOnC3GxSKlEATVb8jrVw+B
btKZAXZ4LEaGZ5+2t0lQY3sr26OcBVk/u8SvaixsfaV7gzzpC0rdHj1VIg2n
C4zeq/l54U2r2D9X9SzGavKojXIR4BaZZd7I9USCkcmj5w8D0bChhsQm+mOr
b7ROeQcD+73fXLk+TkP1SpPmJ3QubpjkVESqpuKXFtm1lWI2cqvbf9L7loYt
lh9KG5w8cDx3sEmclACZBfniHct8cTqz0WjAPgBlVz0/DDSzQXWiP6835CO/
XfEZdz4Iny09aGqEYPh/Ozjj/YCOpk9btQUHvNBUrJXaZMlCxa9pihXaaWB2
6+RdrveAzwRJcWQdE1rp11/RJgU49fJ5zeHe0zj5jG1Ot4iE2+HmwtJMOrbt
qHe/H5MC3u1D+aa/n8XarqvdXa+F0DkxFP1PEx+jR2VMLY75w6xAQCJtYmHT
gcTPDT2JsBQ+yikuiMVTff3SzOQLaCf6XrtHoGJpp/MDwrQEIVaM944uQlAI
2wOPmCTCU9SWk7iGAw9O7pf0W+FwzIpKbszkgz/9QCWLGQa9uJzcF2IOFuQ0
ajkWLPwP2e946w==
        "], VertexCoordinates -> CompressedData["
1:eJwt1Hs0lHkcBvDBiUlSolxDNZouSo2XMtheuVaThtZlOqTpJFJis5nGYa2N
XFIUJlG5xNayp0yyYlKDapMiayK51Ig0uUVZVLTPe87+MWfO5zzP9/v+zvvO
O8v2h3kGKtNotGB8qO+lRkM7NQx50gcnnpWy8whyiltolmbAk3LkDuWDVwhy
Y07WsCOctDZoNBWeqHqVMgPbz9djxMBuYXfSuvR50kVJ7RPz4LYG/RRbWDu6
9XPHZYK0CRiqmdLjSUdV0l89gEP7qlf3wyvZjeZbYQvLI5kTcOwO2uxq2NVH
McLG/OiN8mXfLxFk10eJQgRv+p1pPgYPB3YeOgwPmjix+uDcYwmCKrjgUHJW
C6wiyHcX4nwRa/RpJfDBeyfzNsOdXdlXymBFTsOmTlxPEVu1ZB1cq7TOvxYu
lipns+AIt5CCAzBxhd5rB+f5Xnsyif3mWtdnHWBnmVNeGhxFvMiYziXInrJQ
j4NwZl/YxgnY4ZONQIJ5ZrJZ/wjsppvrrY5c5Hj6yAD8aDqXm4Tc9KOUVgeL
bvcaquN804X2e91guUztvAC5csdgtwk8W916mYk8WKH+lxZcYFEzUwVbCZ+E
VOcQJGeXb68S9nf45xdJ4eHKyEIG5isSa7iUg58yPsxBfxHHqlwETxn5nk1H
f1eDjTgRTl1slX0K/YBKeurERYJMl9B3Z8Im2qufvYMj9TJm98JflaPkTbDn
gpb3mrDD/Nd11fB6A5ZxMfbXL2i6dhaWLfSassT+JAttZzd4ZFjU9ytMe7fn
Ng1++7XXMwte/mZhiSybII3mEi9lujxpnZXg832Ye+bC0UXY12bC1yuD1WIU
w83o849OHsuDO3uObtNEXlc0NRVPzTMThNTv9W665gI+LFNvnTRA36x5ImYz
XNxhp3kS5+X8fSlo6AJBWme+Oh2M3JEWPtQBJ29trI6FdV2iPCRwWGByEwv9
a4pylRuwjuIXdwfYcjbmUQJ8U+gVVIF+ibPTRiG8/aLsuSrc5dax/BBcnN+5
wwD9mt1Kk3awBnn1ZwX1e/YWvTeHtTVrHtYjZ/movlCFebqVLTTqfWo5Llej
9gti7/qgP5FntWVWRJDT203NIpE/1TAMnIHXD3j6ecHXvx0W9sPBJmunQtF3
UWPwX8NkT6n3SuQZ4cvS3sCzms5xP8AVtcnv2+GG732jPeiz6ew9L+Anifkh
n3Ge/dOfQmvhsHyp7TTyeXwrURkcsmF+vSueD1uc+K8YtuZdNS5EzlkyaXIR
Zqz5cZMu5u1d+ySZMNKWpbge/+0XQQysPdY2XYl5M3FRF2U2o4LLQr7KValw
Hyx9vsKFer7nmNvaObCLhm3qNrj9cBSXCzfrsG+KsV+37YzAFQ43s7VWho9H
sjZvh5WSJFGL4Z3rH0nZsLH89htn7LfTmfPFCXambgz2RfNZXhvg4t9SSgKo
831t0DOBPeJlw0PI8w0suhlwybj9Ci3kf3C7/b5lEWRcS/8zOfbn10cLlZAb
0RuUqfsbUTB+sx85nm7AecyPuXoEjcAP/9R1tEFumxIha4Hl+8L2u8H0UnF8
Rdb/7w/6lvUuvPuw+oHIJTVwIrEm/BYsDmNqyWGLL+r/pMIGyc/p1nD8x+C3
SfDO4PO+nvAt1+v6nnDzhopi6n143NT5ciiTIO82Okczcb/Lo/1PfYf9zvqY
RqG/l2PZPQgr4tJqU6jruTdLxDB/94DHGPqtQWp+PnBphqUO9f9zR9xx7kMG
QY6rqTRT59vyE23mHhxblKRRiryHZvm6FnaPMRUOwI+zJaJ0+JQ3M2gV+j0Z
iQl7YGPDuBP+sAcHE/Ct8cZ9qdT/aU6Tuyr8H1Ql8gA=
        "]}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$14", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$20", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$19", Automatic, Center], 
         DynamicLocation["VertexID$25", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$24", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$30", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$33", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$37", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$30", Automatic, Center], 
          DynamicLocation["VertexID$36", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$35", Automatic, Center], 
          DynamicLocation["VertexID$36", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$35", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$40", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$39", Automatic, Center], 
          DynamicLocation["VertexID$48", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$43", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$40", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$42", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$48", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$49", Automatic, Center], 
          DynamicLocation["VertexID$61", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$50", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$54", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$65", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$69", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$54", Automatic, Center], 
          DynamicLocation["VertexID$71", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$65", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$75", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$74", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$78", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$61", Automatic, Center], 
          DynamicLocation["VertexID$68", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$61", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$62", Automatic, Center], 
          DynamicLocation["VertexID$63", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$62", Automatic, Center], 
          DynamicLocation["VertexID$70", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$63", Automatic, Center], 
          DynamicLocation["VertexID$72", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$64", Automatic, Center], 
          DynamicLocation["VertexID$66", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$64", Automatic, Center], 
          DynamicLocation["VertexID$73", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$74", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$83", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$66", Automatic, Center], 
          DynamicLocation["VertexID$70", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$81", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$94", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$68", Automatic, Center], 
          DynamicLocation["VertexID$85", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$69", Automatic, Center], 
         DynamicLocation["VertexID$76", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$69", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$70", Automatic, Center], 
         DynamicLocation["VertexID$72", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$70", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$71", Automatic, Center], 
          DynamicLocation["VertexID$76", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$72", Automatic, Center], 
          DynamicLocation["VertexID$78", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$72", Automatic, Center], 
          DynamicLocation["VertexID$84", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$73", Automatic, Center], 
         DynamicLocation["VertexID$81", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$74", Automatic, Center], 
         DynamicLocation["VertexID$91", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$83", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$76", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$76", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$78", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$78", Automatic, Center], 
         DynamicLocation["VertexID$86", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$79", Automatic, Center], 
         DynamicLocation["VertexID$85", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$79", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$80", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$81", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$81", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$82", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$83", Automatic, Center], 
          DynamicLocation["VertexID$88", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$84", Automatic, Center], 
         DynamicLocation["VertexID$86", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$85", Automatic, Center], 
         DynamicLocation["VertexID$89", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$85", Automatic, Center], 
          DynamicLocation["VertexID$95", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$86", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$87", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$91", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$89", Automatic, Center], 
          DynamicLocation["VertexID$96", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$90", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$91", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$92", Automatic, Center], 
         DynamicLocation["VertexID$95", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$92", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$93", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$94", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$95", Automatic, Center], 
          DynamicLocation["VertexID$96", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$96", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$97", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$98", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$99", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$100", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$101", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$102", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$103", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.422626, 20.621989}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.414987, 20.620784}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410396, 20.619338}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.421725, 20.619097}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404087, 20.61737}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394259, 20.616607}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.390182, 20.616285}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388987, 20.614291}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389989, 20.613922}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394222, 20.613268}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399836, 20.613055}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397181, 20.612857}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401945, 20.612573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.413654, 20.611969}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409706, 20.61207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38838, 20.610063}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386802, 20.610147}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.381142, 20.610304}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405749, 20.610382}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399391, 20.609277}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39687, 20.6092101}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386442, 20.609114}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391256, 20.609}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38332, 20.608429}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.406867, 20.606573}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382298, 20.605955}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408668, 20.605739}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.417602, 20.604407}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39074, 20.604502}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.377236, 20.604497}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.406611, 20.603862}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399448, 20.603633}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.383215, 20.601403}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.384053, 20.601178}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.380798, 20.600813}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.37591, 20.600506}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.416328, 20.599833}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393797, 20.598759}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398492, 20.597667}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399739, 20.596995}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.372366, 20.596679}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407375, 20.596437}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403316, 20.596183}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407122, 20.595779}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.415007, 20.595231}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392271, 20.594612}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382999, 20.593451}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397006, 20.593169}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398421, 20.592702}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378101, 20.592499}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378986, 20.591885}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.401697, 20.591693}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391041, 20.591448}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376605, 20.590774}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404874, 20.590529}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386949, 20.589948}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.4063, 20.589964}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39598, 20.589817}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413461, 20.589804}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41146, 20.589439}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397337, 20.589346}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40859, 20.589364}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410065, 20.589232}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404232, 20.589193}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389918, 20.588889}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405837, 20.588632}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.363755, 20.588653}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40052, 20.588274}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.376417, 20.588207}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40807, 20.587852}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$70"], 
         InsetBox[
          FormBox[
           StyleBox["70", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$70", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$70"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.370604, 20.587858}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$71"], 
         InsetBox[
          FormBox[
           StyleBox["71", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$71", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$71"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409321, 20.587412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$72"], 
         InsetBox[
          FormBox[
           StyleBox["72", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$72", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$72"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403276, 20.58711}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$73"], 
         InsetBox[
          FormBox[
           StyleBox["73", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$73", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$73"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395114, 20.587163}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$74"], 
         InsetBox[
          FormBox[
           StyleBox["74", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$74", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$74"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.385845, 20.587028}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$75"], 
         InsetBox[
          FormBox[
           StyleBox["75", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$75", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$75"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.375135, 20.58707}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$76"], 
         InsetBox[
          FormBox[
           StyleBox["76", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$76", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$76"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.376311, 20.586834}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$77"], 
         InsetBox[
          FormBox[
           StyleBox["77", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$77", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$77"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410515, 20.58699}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$78"], 
         InsetBox[
          FormBox[
           StyleBox["78", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$78", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$78"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396554, 20.586675}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$79"], 
         InsetBox[
          FormBox[
           StyleBox["79", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$79", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$79"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.412089, 20.586412}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$80"], 
         InsetBox[
          FormBox[
           StyleBox["80", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$80", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$80"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.405086, 20.5865}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$81"], 
         InsetBox[
          FormBox[
           StyleBox["81", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$81", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$81"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407296, 20.585882}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$82"], 
         InsetBox[
          FormBox[
           StyleBox["82", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$82", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$82"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388782, 20.58596}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$83"], 
         InsetBox[
          FormBox[
           StyleBox["83", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$83", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$83"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40857, 20.585536}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$84"], 
         InsetBox[
          FormBox[
           StyleBox["84", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$84", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$84"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399559, 20.585669}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$85"], 
         InsetBox[
          FormBox[
           StyleBox["85", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$85", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$85"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409745, 20.585233}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$86"], 
         InsetBox[
          FormBox[
           StyleBox["86", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$86", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$86"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410668, 20.584801}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$87"], 
         InsetBox[
          FormBox[
           StyleBox["87", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$87", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$87"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391535, 20.584951}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$88"], 
         InsetBox[
          FormBox[
           StyleBox["88", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$88", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$88"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402166, 20.584754}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$89"], 
         InsetBox[
          FormBox[
           StyleBox["89", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$89", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$89"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404362, 20.58418}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$90"], 
         InsetBox[
          FormBox[
           StyleBox["90", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$90", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$90"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393923, 20.58412}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$91"], 
         InsetBox[
          FormBox[
           StyleBox["91", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$91", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$91"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.395651, 20.583284}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$92"], 
         InsetBox[
          FormBox[
           StyleBox["92", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$92", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$92"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407144, 20.581739}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$93"], 
         InsetBox[
          FormBox[
           StyleBox["93", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$93", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$93"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.361785, 20.582024}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$94"], 
         InsetBox[
          FormBox[
           StyleBox["94", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$94", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$94"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398079, 20.581724}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$95"], 
         InsetBox[
          FormBox[
           StyleBox["95", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$95", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$95"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399094, 20.580837}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$96"], 
         InsetBox[
          FormBox[
           StyleBox["96", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$96", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$96"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.374104, 20.579458}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$97"], 
         InsetBox[
          FormBox[
           StyleBox["97", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$97", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$97"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382268, 20.577798}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$98"], 
         InsetBox[
          FormBox[
           StyleBox["98", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$98", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$98"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40221, 20.577133}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$99"], 
         InsetBox[
          FormBox[
           StyleBox["99", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$99", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$99"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38544, 20.577177}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$100"], 
         InsetBox[
          FormBox[
           StyleBox["100", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$100", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$100"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389128, 20.576425}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$101"], 
         InsetBox[
          FormBox[
           StyleBox["101", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$101", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$101"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393091, 20.5756}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$102"], 
         InsetBox[
          FormBox[
           StyleBox["102", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$102", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$102"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39633, 20.574997}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$103"], 
         InsetBox[
          FormBox[
           StyleBox["103", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$103", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$103"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399193, 20.574315}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$104"], 
         InsetBox[
          FormBox[
           StyleBox["104", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$104", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$104"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.34087136409994`}, {-107.5388713640748, 93.71452489350793}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15,
  ImageSize->{Automatic, 291.0697484415284}]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["19453.279313247695`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Undirected Quer\[EAcute]taro Urban Zone Dataset - GC-PO Solution Graph\
\>", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
       74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
       92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104}, {
      Null, CompressedData["
1:eJwVxdV2ggAAAFBF7O5ARQwsDEBRUQwsyhY7sfa2D9tPbnu45yLK91IBVCrV
z5//1YAaBDQAqNFq7KANDGt1WqtOrzPpjXqXQW8wGpxGk9FhspgtZqvZY/Fa
3Ta7DbK7HU6nzxVwRd1Bj9cT8/p9AV/EHwmEglAQDkVDCBSG4HAqgkaTcAzO
xdJIHEGRRLyUSCaIZDaVSxXQNFpMZ9LlTD6DZ4lsP4flsXwFK2C1QrNYLlZL
ZKlaxss0XsEZgiQokiLrlVplUKWrPWpIjWuNWrfeq4uNdoOlGVpotprzVqc1
ZQbMrN1pjzpcl+3yPaG3Ykfsoj/uXwb8QB5Kw8mIGy3HEjfl1vyC3wsz4ShO
xJWoSBPpPp1PNzN5dppvF8vFYble7VbP9Ubey9fNdnveHXev/WF/O5yPp9P1
9D4/Lvfr7fq5Pe6K8nx8PV+v9/vz+foFCE1JZg==
       "]}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[53, 65], 
         UndirectedEdge[50, 51], 83, 
         UndirectedEdge[56, 65], 
         UndirectedEdge[89, 96], 13, 11, 
         UndirectedEdge[15, 19], 
         UndirectedEdge[13, 19], 
         UndirectedEdge[43, 44], 92, 
         UndirectedEdge[35, 50], 
         UndirectedEdge[11, 20], 57, 48, 62, 8, 
         UndirectedEdge[57, 66], 61, 
         UndirectedEdge[5, 13], 
         UndirectedEdge[92, 95], 77, 22, 
         UndirectedEdge[66, 81], 
         UndirectedEdge[17, 22], 20, 33, 
         UndirectedEdge[72, 78], 
         UndirectedEdge[50, 54], 43, 26, 
         UndirectedEdge[74, 79], 
         UndirectedEdge[69, 77], 9, 
         UndirectedEdge[55, 57], 
         UndirectedEdge[25, 27], 95, 78, 74, 
         UndirectedEdge[70, 72], 25, 
         UndirectedEdge[3, 5], 
         UndirectedEdge[95, 96], 
         UndirectedEdge[65, 83], 
         UndirectedEdge[76, 77], 81, 
         UndirectedEdge[68, 73], 51, 56, 5, 19, 15, 
         UndirectedEdge[22, 24], 
         UndirectedEdge[62, 70], 55, 
         UndirectedEdge[31, 42], 50, 89, 
         UndirectedEdge[48, 58], 58, 42, 17, 79, 
         UndirectedEdge[33, 35], 
         UndirectedEdge[73, 89], 24, 
         UndirectedEdge[24, 26], 
         UndirectedEdge[2, 3], 
         UndirectedEdge[42, 44], 
         UndirectedEdge[27, 31], 
         UndirectedEdge[61, 79], 
         UndirectedEdge[51, 56], 27, 69, 
         UndirectedEdge[8, 9], 
         UndirectedEdge[61, 68], 
         UndirectedEdge[8, 17], 
         UndirectedEdge[26, 33], 
         UndirectedEdge[57, 62], 70, 
         UndirectedEdge[73, 81], 
         UndirectedEdge[65, 74], 96, 66, 44, 54, 76, 72, 3, 
         UndirectedEdge[54, 69], 35, 
         UndirectedEdge[58, 61], 65, 
         UndirectedEdge[19, 25], 68, 73, 53, 
         UndirectedEdge[11, 13], 31, 2, 
         UndirectedEdge[44, 55]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0fk7FAgABmBHrsassZFtiw5RmcQmKjm+EhUpHUTGNePIEXPPYGLMYNxH
ytFBtKF1lZVUtLSh0j5soYiENuvYypHcbT+8f8G7nhpy3EdaSkqq5LuXEz6m
0abxmG/dTbvVzUa2sVWgKD4Sauwflh7fiUXmjjZblcIYnJMLahmyiIfG0Klv
/8qdB+dSUFTdwUvofJplLFFJQ7NMyrM+KRFmWbXkvUVhiNdc2yy1modznMW6
zb50FHYRXj1cSsfA8ot5ZhUh8LCbSk9SomCT7inZKmkh6k6lDGhMhKPDKKSp
sEKAKycjRigCFkip+8l/uzDQsXxhfs4mCKK0k0+c7cMgr62g2+8QigLSuh02
xHMwUnco6r4fiheEqt+IiwkwnBjP2WsvwL4eioZMtjcCV/nEOWm4o7/SVpNg
ysKZQcWno80SVLtOjWs7h0M99O4Ts2A6yj09zmtIxHDQu33Pyl4E+YA9f7IH
2JgbksulmMagvbW3ROVIJJQOWhwyd2eif6Go/J1uAGrV8hwNOphwcqVVsi2i
MNXS+TZdQYiOy2pxZF4yXlx1K8u8yYR4+FU1mSLEh7FZnZLdsfCKsWaFfxJg
l87Gl+POEph//G96RWs8PP22re7ti4bcjKpN2R0n5AW7OBt0pGPCLuBMzn46
ckkV1vOTCTAo5Qp6RyKxN7CQEbMiBcoXRKN66VEYLmNQsxmp6FOMWmMaIUQP
YfuYoygS9e3E4AD3CExVubVM1lARqzOroXdYiD92VlYbz/Lg/XJebno0Aq6d
g2aUaxLM9Rhb+NRmQOvyPRu2+wkEj67q4U/xEan43rzOT4jEMKu21w3xMKjM
sc6/KMaPzEVaFl2MH+7u2ZxnIsSDLNddXg488J4rL6v+/tGlfn5fxapEfEna
wp6Jp2EjdXilnzYfVgaSdfs4XJiw05VLrfmYK53XdC+koKtL1pn8IhgzB3hV
vg4MjPTOuO5qSYCwKtvloAkXCto3a6+/5cMzc6lxPTEKX5U19Rs3s3DA/s3I
5F8M9JUz5pZJxaImo/gseYs33in3GZPtvKFE2tz+uIINyy/6jwRMDs4NG2q1
zAaDLO1o6ehDg/iiy+6m11Twhg51bjNhoeTqfcPk1AAQ2b2W+ZqheDjd99nc
jwrtMXZ2gQIT3J99jtTKc3GMFKTntJWF2scL8X1HaIi5J6JlLPlAOk5+cMVi
ANzsRhM9jvoin8JTtPULQaTWDRybjEJL8sfU7AwOnC3GxSKlEATVb8jrVw+B
btKZAXZ4LEaGZ5+2t0lQY3sr26OcBVk/u8SvaixsfaV7gzzpC0rdHj1VIg2n
C4zeq/l54U2r2D9X9SzGavKojXIR4BaZZd7I9USCkcmj5w8D0bChhsQm+mOr
b7ROeQcD+73fXLk+TkP1SpPmJ3QubpjkVESqpuKXFtm1lWI2cqvbf9L7loYt
lh9KG5w8cDx3sEmclACZBfniHct8cTqz0WjAPgBlVz0/DDSzQXWiP6835CO/
XfEZdz4Iny09aGqEYPh/Ozjj/YCOpk9btQUHvNBUrJXaZMlCxa9pihXaaWB2
6+RdrveAzwRJcWQdE1rp11/RJgU49fJ5zeHe0zj5jG1Ot4iE2+HmwtJMOrbt
qHe/H5MC3u1D+aa/n8XarqvdXa+F0DkxFP1PEx+jR2VMLY75w6xAQCJtYmHT
gcTPDT2JsBQ+yikuiMVTff3SzOQLaCf6XrtHoGJpp/MDwrQEIVaM944uQlAI
2wOPmCTCU9SWk7iGAw9O7pf0W+FwzIpKbszkgz/9QCWLGQa9uJzcF2IOFuQ0
ajkWLPwP2e946w==
        "], VertexCoordinates -> CompressedData["
1:eJwt1Hs0lHkcBvDBiUlSolxDNZouSo2XMtheuVaThtZlOqTpJFJis5nGYa2N
XFIUJlG5xNayp0yyYlKDapMiayK51Ig0uUVZVLTPe87+MWfO5zzP9/v+zvvO
O8v2h3kGKtNotGB8qO+lRkM7NQx50gcnnpWy8whyiltolmbAk3LkDuWDVwhy
Y07WsCOctDZoNBWeqHqVMgPbz9djxMBuYXfSuvR50kVJ7RPz4LYG/RRbWDu6
9XPHZYK0CRiqmdLjSUdV0l89gEP7qlf3wyvZjeZbYQvLI5kTcOwO2uxq2NVH
McLG/OiN8mXfLxFk10eJQgRv+p1pPgYPB3YeOgwPmjix+uDcYwmCKrjgUHJW
C6wiyHcX4nwRa/RpJfDBeyfzNsOdXdlXymBFTsOmTlxPEVu1ZB1cq7TOvxYu
lipns+AIt5CCAzBxhd5rB+f5Xnsyif3mWtdnHWBnmVNeGhxFvMiYziXInrJQ
j4NwZl/YxgnY4ZONQIJ5ZrJZ/wjsppvrrY5c5Hj6yAD8aDqXm4Tc9KOUVgeL
bvcaquN804X2e91guUztvAC5csdgtwk8W916mYk8WKH+lxZcYFEzUwVbCZ+E
VOcQJGeXb68S9nf45xdJ4eHKyEIG5isSa7iUg58yPsxBfxHHqlwETxn5nk1H
f1eDjTgRTl1slX0K/YBKeurERYJMl9B3Z8Im2qufvYMj9TJm98JflaPkTbDn
gpb3mrDD/Nd11fB6A5ZxMfbXL2i6dhaWLfSassT+JAttZzd4ZFjU9ytMe7fn
Ng1++7XXMwte/mZhiSybII3mEi9lujxpnZXg832Ye+bC0UXY12bC1yuD1WIU
w83o849OHsuDO3uObtNEXlc0NRVPzTMThNTv9W665gI+LFNvnTRA36x5ImYz
XNxhp3kS5+X8fSlo6AJBWme+Oh2M3JEWPtQBJ29trI6FdV2iPCRwWGByEwv9
a4pylRuwjuIXdwfYcjbmUQJ8U+gVVIF+ibPTRiG8/aLsuSrc5dax/BBcnN+5
wwD9mt1Kk3awBnn1ZwX1e/YWvTeHtTVrHtYjZ/movlCFebqVLTTqfWo5Llej
9gti7/qgP5FntWVWRJDT203NIpE/1TAMnIHXD3j6ecHXvx0W9sPBJmunQtF3
UWPwX8NkT6n3SuQZ4cvS3sCzms5xP8AVtcnv2+GG732jPeiz6ew9L+Anifkh
n3Ge/dOfQmvhsHyp7TTyeXwrURkcsmF+vSueD1uc+K8YtuZdNS5EzlkyaXIR
Zqz5cZMu5u1d+ySZMNKWpbge/+0XQQysPdY2XYl5M3FRF2U2o4LLQr7KValw
Hyx9vsKFer7nmNvaObCLhm3qNrj9cBSXCzfrsG+KsV+37YzAFQ43s7VWho9H
sjZvh5WSJFGL4Z3rH0nZsLH89htn7LfTmfPFCXambgz2RfNZXhvg4t9SSgKo
831t0DOBPeJlw0PI8w0suhlwybj9Ci3kf3C7/b5lEWRcS/8zOfbn10cLlZAb
0RuUqfsbUTB+sx85nm7AecyPuXoEjcAP/9R1tEFumxIha4Hl+8L2u8H0UnF8
Rdb/7w/6lvUuvPuw+oHIJTVwIrEm/BYsDmNqyWGLL+r/pMIGyc/p1nD8x+C3
SfDO4PO+nvAt1+v6nnDzhopi6n143NT5ciiTIO82Okczcb/Lo/1PfYf9zvqY
RqG/l2PZPQgr4tJqU6jruTdLxDB/94DHGPqtQWp+PnBphqUO9f9zR9xx7kMG
QY6rqTRT59vyE23mHhxblKRRiryHZvm6FnaPMRUOwI+zJaJ0+JQ3M2gV+j0Z
iQl7YGPDuBP+sAcHE/Ct8cZ9qdT/aU6Tuyr8H1Ql8gA=
        "]}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$2", Automatic, Center], 
          DynamicLocation["VertexID$3", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$3", Automatic, Center], 
          DynamicLocation["VertexID$5", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$14", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$5", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$9", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$8", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$9", Automatic, Center], 
         DynamicLocation["VertexID$16", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$10", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$11", Automatic, Center], 
         DynamicLocation["VertexID$12", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$16", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$24", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$27", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$33", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$27", Automatic, Center], 
          DynamicLocation["VertexID$31", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$37", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$31", Automatic, Center], 
          DynamicLocation["VertexID$42", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$33", Automatic, Center], 
          DynamicLocation["VertexID$35", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$35", Automatic, Center], 
          DynamicLocation["VertexID$50", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$43", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$42", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$43", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$46", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$58", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$54", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$53", Automatic, Center], 
          DynamicLocation["VertexID$65", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$54", Automatic, Center], 
          DynamicLocation["VertexID$69", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$57", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$64", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$65", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$75", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$62", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$57", Automatic, Center], 
          DynamicLocation["VertexID$66", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$58", Automatic, Center], 
          DynamicLocation["VertexID$61", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$74", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$78", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$61", Automatic, Center], 
          DynamicLocation["VertexID$68", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$61", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$62", Automatic, Center], 
          DynamicLocation["VertexID$70", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$72", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$74", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$83", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$66", Automatic, Center], 
         DynamicLocation["VertexID$70", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$66", Automatic, Center], 
          DynamicLocation["VertexID$81", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$94", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$68", Automatic, Center], 
          DynamicLocation["VertexID$73", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$85", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$69", Automatic, Center], 
         DynamicLocation["VertexID$76", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$69", Automatic, Center], 
          DynamicLocation["VertexID$77", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$70", Automatic, Center], 
          DynamicLocation["VertexID$72", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$70", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$71", Automatic, Center], 
         DynamicLocation["VertexID$76", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$72", Automatic, Center], 
          DynamicLocation["VertexID$78", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$72", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$81", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$74", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$74", Automatic, Center], 
         DynamicLocation["VertexID$91", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$83", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$76", Automatic, Center], 
          DynamicLocation["VertexID$77", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$76", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$78", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$78", Automatic, Center], 
         DynamicLocation["VertexID$86", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$79", Automatic, Center], 
         DynamicLocation["VertexID$85", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$79", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$80", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$81", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$81", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$82", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$88", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$84", Automatic, Center], 
         DynamicLocation["VertexID$86", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$85", Automatic, Center], 
         DynamicLocation["VertexID$89", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$85", Automatic, Center], 
         DynamicLocation["VertexID$95", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$86", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$87", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$91", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$89", Automatic, Center], 
          DynamicLocation["VertexID$96", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$90", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$91", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$92", Automatic, Center], 
          DynamicLocation["VertexID$95", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$92", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$93", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$94", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$95", Automatic, Center], 
          DynamicLocation["VertexID$96", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$96", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$97", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$98", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$99", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$100", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$101", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$102", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$103", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.422626, 20.621989}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.414987, 20.620784}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410396, 20.619338}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.421725, 20.619097}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404087, 20.61737}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394259, 20.616607}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.390182, 20.616285}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388987, 20.614291}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389989, 20.613922}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394222, 20.613268}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399836, 20.613055}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.397181, 20.612857}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401945, 20.612573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413654, 20.611969}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409706, 20.61207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38838, 20.610063}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386802, 20.610147}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.381142, 20.610304}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405749, 20.610382}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399391, 20.609277}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39687, 20.6092101}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386442, 20.609114}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391256, 20.609}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38332, 20.608429}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406867, 20.606573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382298, 20.605955}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.408668, 20.605739}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.417602, 20.604407}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39074, 20.604502}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.377236, 20.604497}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406611, 20.603862}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399448, 20.603633}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.383215, 20.601403}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.384053, 20.601178}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.380798, 20.600813}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.37591, 20.600506}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.416328, 20.599833}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393797, 20.598759}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398492, 20.597667}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399739, 20.596995}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.372366, 20.596679}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407375, 20.596437}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403316, 20.596183}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407122, 20.595779}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.415007, 20.595231}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.392271, 20.594612}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382999, 20.593451}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397006, 20.593169}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398421, 20.592702}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378101, 20.592499}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378986, 20.591885}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.401697, 20.591693}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391041, 20.591448}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376605, 20.590774}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404874, 20.590529}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386949, 20.589948}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.4063, 20.589964}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.39598, 20.589817}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413461, 20.589804}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41146, 20.589439}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397337, 20.589346}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40859, 20.589364}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410065, 20.589232}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404232, 20.589193}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389918, 20.588889}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405837, 20.588632}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.363755, 20.588653}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40052, 20.588274}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376417, 20.588207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40807, 20.587852}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$70"], 
         InsetBox[
          FormBox[
           StyleBox["70", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$70", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$70"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.370604, 20.587858}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$71"], 
         InsetBox[
          FormBox[
           StyleBox["71", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$71", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$71"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409321, 20.587412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$72"], 
         InsetBox[
          FormBox[
           StyleBox["72", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$72", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$72"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403276, 20.58711}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$73"], 
         InsetBox[
          FormBox[
           StyleBox["73", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$73", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$73"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395114, 20.587163}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$74"], 
         InsetBox[
          FormBox[
           StyleBox["74", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$74", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$74"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.385845, 20.587028}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$75"], 
         InsetBox[
          FormBox[
           StyleBox["75", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$75", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$75"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.375135, 20.58707}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$76"], 
         InsetBox[
          FormBox[
           StyleBox["76", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$76", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$76"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376311, 20.586834}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$77"], 
         InsetBox[
          FormBox[
           StyleBox["77", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$77", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$77"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410515, 20.58699}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$78"], 
         InsetBox[
          FormBox[
           StyleBox["78", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$78", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$78"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396554, 20.586675}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$79"], 
         InsetBox[
          FormBox[
           StyleBox["79", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$79", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$79"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.412089, 20.586412}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$80"], 
         InsetBox[
          FormBox[
           StyleBox["80", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$80", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$80"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405086, 20.5865}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$81"], 
         InsetBox[
          FormBox[
           StyleBox["81", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$81", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$81"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407296, 20.585882}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$82"], 
         InsetBox[
          FormBox[
           StyleBox["82", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$82", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$82"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388782, 20.58596}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$83"], 
         InsetBox[
          FormBox[
           StyleBox["83", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$83", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$83"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40857, 20.585536}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$84"], 
         InsetBox[
          FormBox[
           StyleBox["84", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$84", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$84"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399559, 20.585669}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$85"], 
         InsetBox[
          FormBox[
           StyleBox["85", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$85", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$85"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409745, 20.585233}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$86"], 
         InsetBox[
          FormBox[
           StyleBox["86", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$86", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$86"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410668, 20.584801}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$87"], 
         InsetBox[
          FormBox[
           StyleBox["87", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$87", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$87"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391535, 20.584951}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$88"], 
         InsetBox[
          FormBox[
           StyleBox["88", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$88", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$88"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402166, 20.584754}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$89"], 
         InsetBox[
          FormBox[
           StyleBox["89", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$89", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$89"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404362, 20.58418}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$90"], 
         InsetBox[
          FormBox[
           StyleBox["90", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$90", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$90"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393923, 20.58412}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$91"], 
         InsetBox[
          FormBox[
           StyleBox["91", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$91", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$91"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.395651, 20.583284}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$92"], 
         InsetBox[
          FormBox[
           StyleBox["92", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$92", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$92"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407144, 20.581739}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$93"], 
         InsetBox[
          FormBox[
           StyleBox["93", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$93", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$93"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.361785, 20.582024}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$94"], 
         InsetBox[
          FormBox[
           StyleBox["94", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$94", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$94"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398079, 20.581724}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$95"], 
         InsetBox[
          FormBox[
           StyleBox["95", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$95", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$95"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399094, 20.580837}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$96"], 
         InsetBox[
          FormBox[
           StyleBox["96", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$96", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$96"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.374104, 20.579458}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$97"], 
         InsetBox[
          FormBox[
           StyleBox["97", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$97", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$97"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382268, 20.577798}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$98"], 
         InsetBox[
          FormBox[
           StyleBox["98", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$98", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$98"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40221, 20.577133}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$99"], 
         InsetBox[
          FormBox[
           StyleBox["99", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$99", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$99"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38544, 20.577177}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$100"], 
         InsetBox[
          FormBox[
           StyleBox["100", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$100", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$100"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389128, 20.576425}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$101"], 
         InsetBox[
          FormBox[
           StyleBox["101", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$101", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$101"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393091, 20.5756}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$102"], 
         InsetBox[
          FormBox[
           StyleBox["102", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$102", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$102"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39633, 20.574997}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$103"], 
         InsetBox[
          FormBox[
           StyleBox["103", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$103", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$103"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399193, 20.574315}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$104"], 
         InsetBox[
          FormBox[
           StyleBox["104", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$104", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$104"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{14.585786437626894`, 
       268.34087136409994`}, {-107.53887136408935`, 93.71452489352248}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["17067.056960684626`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Undirected Quer\[EAcute]taro Urban Zone Dataset - Shared Vertex Solution Graph\
\>", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
       20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
       38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
       56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
       74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
       92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104}, {
      Null, CompressedData["
1:eJwVxdV2ggAAAFBF7O5ARQwsDEBRUQwsyhY7sfa2D9tPbnu45yLK91IBVCrV
z5//1YAaBDQAqNFq7KANDGt1WqtOrzPpjXqXQW8wGpxGk9FhspgtZqvZY/Fa
3Ta7DbK7HU6nzxVwRd1Bj9cT8/p9AV/EHwmEglAQDkVDCBSG4HAqgkaTcAzO
xdJIHEGRRLyUSCaIZDaVSxXQNFpMZ9LlTD6DZ4lsP4flsXwFK2C1QrNYLlZL
ZKlaxss0XsEZgiQokiLrlVplUKWrPWpIjWuNWrfeq4uNdoOlGVpotprzVqc1
ZQbMrN1pjzpcl+3yPaG3Ykfsoj/uXwb8QB5Kw8mIGy3HEjfl1vyC3wsz4ShO
xJWoSBPpPp1PNzN5dppvF8vFYble7VbP9Ubey9fNdnveHXev/WF/O5yPp9P1
9D4/Lvfr7fq5Pe6K8nx8PV+v9/vz+foFCE1JZg==
       "]}, {
      VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Medium}, GraphHighlight -> {
         UndirectedEdge[53, 65], 
         UndirectedEdge[49, 61], 46, 
         UndirectedEdge[50, 51], 83, 
         UndirectedEdge[56, 65], 
         UndirectedEdge[46, 53], 16, 13, 11, 
         UndirectedEdge[15, 19], 
         UndirectedEdge[13, 19], 
         UndirectedEdge[46, 48], 
         UndirectedEdge[11, 20], 48, 61, 22, 
         UndirectedEdge[66, 81], 20, 
         UndirectedEdge[17, 22], 14, 33, 
         UndirectedEdge[72, 78], 
         UndirectedEdge[50, 54], 26, 4, 9, 95, 
         UndirectedEdge[55, 64], 85, 78, 
         UndirectedEdge[70, 72], 25, 10, 
         UndirectedEdge[4, 14], 
         UndirectedEdge[85, 89], 
         UndirectedEdge[65, 83], 81, 51, 
         UndirectedEdge[9, 16], 56, 19, 15, 
         UndirectedEdge[22, 24], 55, 
         UndirectedEdge[31, 42], 50, 89, 42, 17, 
         UndirectedEdge[64, 66], 79, 
         UndirectedEdge[73, 89], 12, 
         UndirectedEdge[69, 76], 24, 
         UndirectedEdge[24, 26], 
         UndirectedEdge[42, 44], 
         UndirectedEdge[61, 79], 
         UndirectedEdge[51, 56], 69, 
         UndirectedEdge[9, 10], 
         UndirectedEdge[26, 33], 70, 
         UndirectedEdge[73, 81], 
         UndirectedEdge[85, 95], 
         UndirectedEdge[11, 12], 66, 44, 54, 76, 
         UndirectedEdge[14, 15], 72, 
         UndirectedEdge[10, 12], 88, 
         UndirectedEdge[54, 69], 64, 
         UndirectedEdge[48, 49], 65, 
         UndirectedEdge[19, 25], 73, 49, 53, 
         UndirectedEdge[11, 13], 31, 
         UndirectedEdge[66, 70], 
         UndirectedEdge[83, 88], 
         UndirectedEdge[25, 31], 
         UndirectedEdge[79, 85], 
         UndirectedEdge[44, 55], 
         UndirectedEdge[16, 17]}, GraphHighlightStyle -> {"Thick"}, 
       EdgeWeight -> CompressedData["
1:eJwN0fk7FAgABmBHrsassZFtiw5RmcQmKjm+EhUpHUTGNePIEXPPYGLMYNxH
ytFBtKF1lZVUtLSh0j5soYiENuvYypHcbT+8f8G7nhpy3EdaSkqq5LuXEz6m
0abxmG/dTbvVzUa2sVWgKD4Sauwflh7fiUXmjjZblcIYnJMLahmyiIfG0Klv
/8qdB+dSUFTdwUvofJplLFFJQ7NMyrM+KRFmWbXkvUVhiNdc2yy1modznMW6
zb50FHYRXj1cSsfA8ot5ZhUh8LCbSk9SomCT7inZKmkh6k6lDGhMhKPDKKSp
sEKAKycjRigCFkip+8l/uzDQsXxhfs4mCKK0k0+c7cMgr62g2+8QigLSuh02
xHMwUnco6r4fiheEqt+IiwkwnBjP2WsvwL4eioZMtjcCV/nEOWm4o7/SVpNg
ysKZQcWno80SVLtOjWs7h0M99O4Ts2A6yj09zmtIxHDQu33Pyl4E+YA9f7IH
2JgbksulmMagvbW3ROVIJJQOWhwyd2eif6Go/J1uAGrV8hwNOphwcqVVsi2i
MNXS+TZdQYiOy2pxZF4yXlx1K8u8yYR4+FU1mSLEh7FZnZLdsfCKsWaFfxJg
l87Gl+POEph//G96RWs8PP22re7ti4bcjKpN2R0n5AW7OBt0pGPCLuBMzn46
ckkV1vOTCTAo5Qp6RyKxN7CQEbMiBcoXRKN66VEYLmNQsxmp6FOMWmMaIUQP
YfuYoygS9e3E4AD3CExVubVM1lARqzOroXdYiD92VlYbz/Lg/XJebno0Aq6d
g2aUaxLM9Rhb+NRmQOvyPRu2+wkEj67q4U/xEan43rzOT4jEMKu21w3xMKjM
sc6/KMaPzEVaFl2MH+7u2ZxnIsSDLNddXg488J4rL6v+/tGlfn5fxapEfEna
wp6Jp2EjdXilnzYfVgaSdfs4XJiw05VLrfmYK53XdC+koKtL1pn8IhgzB3hV
vg4MjPTOuO5qSYCwKtvloAkXCto3a6+/5cMzc6lxPTEKX5U19Rs3s3DA/s3I
5F8M9JUz5pZJxaImo/gseYs33in3GZPtvKFE2tz+uIINyy/6jwRMDs4NG2q1
zAaDLO1o6ehDg/iiy+6m11Twhg51bjNhoeTqfcPk1AAQ2b2W+ZqheDjd99nc
jwrtMXZ2gQIT3J99jtTKc3GMFKTntJWF2scL8X1HaIi5J6JlLPlAOk5+cMVi
ANzsRhM9jvoin8JTtPULQaTWDRybjEJL8sfU7AwOnC3GxSKlEATVb8jrVw+B
btKZAXZ4LEaGZ5+2t0lQY3sr26OcBVk/u8SvaixsfaV7gzzpC0rdHj1VIg2n
C4zeq/l54U2r2D9X9SzGavKojXIR4BaZZd7I9USCkcmj5w8D0bChhsQm+mOr
b7ROeQcD+73fXLk+TkP1SpPmJ3QubpjkVESqpuKXFtm1lWI2cqvbf9L7loYt
lh9KG5w8cDx3sEmclACZBfniHct8cTqz0WjAPgBlVz0/DDSzQXWiP6835CO/
XfEZdz4Iny09aGqEYPh/Ozjj/YCOpk9btQUHvNBUrJXaZMlCxa9pihXaaWB2
6+RdrveAzwRJcWQdE1rp11/RJgU49fJ5zeHe0zj5jG1Ot4iE2+HmwtJMOrbt
qHe/H5MC3u1D+aa/n8XarqvdXa+F0DkxFP1PEx+jR2VMLY75w6xAQCJtYmHT
gcTPDT2JsBQ+yikuiMVTff3SzOQLaCf6XrtHoGJpp/MDwrQEIVaM944uQlAI
2wOPmCTCU9SWk7iGAw9O7pf0W+FwzIpKbszkgz/9QCWLGQa9uJzcF2IOFuQ0
ajkWLPwP2e946w==
        "], VertexCoordinates -> CompressedData["
1:eJwt1Hs0lHkcBvDBiUlSolxDNZouSo2XMtheuVaThtZlOqTpJFJis5nGYa2N
XFIUJlG5xNayp0yyYlKDapMiayK51Ig0uUVZVLTPe87+MWfO5zzP9/v+zvvO
O8v2h3kGKtNotGB8qO+lRkM7NQx50gcnnpWy8whyiltolmbAk3LkDuWDVwhy
Y07WsCOctDZoNBWeqHqVMgPbz9djxMBuYXfSuvR50kVJ7RPz4LYG/RRbWDu6
9XPHZYK0CRiqmdLjSUdV0l89gEP7qlf3wyvZjeZbYQvLI5kTcOwO2uxq2NVH
McLG/OiN8mXfLxFk10eJQgRv+p1pPgYPB3YeOgwPmjix+uDcYwmCKrjgUHJW
C6wiyHcX4nwRa/RpJfDBeyfzNsOdXdlXymBFTsOmTlxPEVu1ZB1cq7TOvxYu
lipns+AIt5CCAzBxhd5rB+f5Xnsyif3mWtdnHWBnmVNeGhxFvMiYziXInrJQ
j4NwZl/YxgnY4ZONQIJ5ZrJZ/wjsppvrrY5c5Hj6yAD8aDqXm4Tc9KOUVgeL
bvcaquN804X2e91guUztvAC5csdgtwk8W916mYk8WKH+lxZcYFEzUwVbCZ+E
VOcQJGeXb68S9nf45xdJ4eHKyEIG5isSa7iUg58yPsxBfxHHqlwETxn5nk1H
f1eDjTgRTl1slX0K/YBKeurERYJMl9B3Z8Im2qufvYMj9TJm98JflaPkTbDn
gpb3mrDD/Nd11fB6A5ZxMfbXL2i6dhaWLfSassT+JAttZzd4ZFjU9ytMe7fn
Ng1++7XXMwte/mZhiSybII3mEi9lujxpnZXg832Ye+bC0UXY12bC1yuD1WIU
w83o849OHsuDO3uObtNEXlc0NRVPzTMThNTv9W665gI+LFNvnTRA36x5ImYz
XNxhp3kS5+X8fSlo6AJBWme+Oh2M3JEWPtQBJ29trI6FdV2iPCRwWGByEwv9
a4pylRuwjuIXdwfYcjbmUQJ8U+gVVIF+ibPTRiG8/aLsuSrc5dax/BBcnN+5
wwD9mt1Kk3awBnn1ZwX1e/YWvTeHtTVrHtYjZ/movlCFebqVLTTqfWo5Llej
9gti7/qgP5FntWVWRJDT203NIpE/1TAMnIHXD3j6ecHXvx0W9sPBJmunQtF3
UWPwX8NkT6n3SuQZ4cvS3sCzms5xP8AVtcnv2+GG732jPeiz6ew9L+Anifkh
n3Ge/dOfQmvhsHyp7TTyeXwrURkcsmF+vSueD1uc+K8YtuZdNS5EzlkyaXIR
Zqz5cZMu5u1d+ySZMNKWpbge/+0XQQysPdY2XYl5M3FRF2U2o4LLQr7KValw
Hyx9vsKFer7nmNvaObCLhm3qNrj9cBSXCzfrsG+KsV+37YzAFQ43s7VWho9H
sjZvh5WSJFGL4Z3rH0nZsLH89htn7LfTmfPFCXambgz2RfNZXhvg4t9SSgKo
831t0DOBPeJlw0PI8w0suhlwybj9Ci3kf3C7/b5lEWRcS/8zOfbn10cLlZAb
0RuUqfsbUTB+sx85nm7AecyPuXoEjcAP/9R1tEFumxIha4Hl+8L2u8H0UnF8
Rdb/7w/6lvUuvPuw+oHIJTVwIrEm/BYsDmNqyWGLL+r/pMIGyc/p1nD8x+C3
SfDO4PO+nvAt1+v6nnDzhopi6n143NT5ciiTIO82Okczcb/Lo/1PfYf9zvqY
RqG/l2PZPQgr4tJqU6jruTdLxDB/94DHGPqtQWp+PnBphqUO9f9zR9xx7kMG
QY6rqTRT59vyE23mHhxblKRRiryHZvm6FnaPMRUOwI+zJaJ0+JQ3M2gV+j0Z
iQl7YGPDuBP+sAcHE/Ct8cZ9qdT/aU6Tuyr8H1Ql8gA=
        "]}]], Typeset`boxes, 
    Typeset`boxes$s2d = GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$2", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$1", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$3", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$2", Automatic, Center], 
         DynamicLocation["VertexID$4", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$5", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$3", Automatic, Center], 
         DynamicLocation["VertexID$15", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$4", Automatic, Center], 
          DynamicLocation["VertexID$14", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$4", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$6", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$5", Automatic, Center], 
         DynamicLocation["VertexID$13", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$7", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$6", Automatic, Center], 
         DynamicLocation["VertexID$10", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$8", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$7", Automatic, Center], 
         DynamicLocation["VertexID$18", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$9", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$8", Automatic, Center], 
         DynamicLocation["VertexID$17", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$10", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$9", Automatic, Center], 
          DynamicLocation["VertexID$16", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$10", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$12", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$13", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$11", Automatic, Center], 
          DynamicLocation["VertexID$20", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$12", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$13", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$14", Automatic, Center], 
          DynamicLocation["VertexID$15", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$14", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$15", Automatic, Center], 
          DynamicLocation["VertexID$19", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$16", Automatic, Center], 
          DynamicLocation["VertexID$17", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$17", Automatic, Center], 
          DynamicLocation["VertexID$22", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$24", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$18", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$19", Automatic, Center], 
          DynamicLocation["VertexID$25", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$21", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$20", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$21", Automatic, Center], 
         DynamicLocation["VertexID$23", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$22", Automatic, Center], 
          DynamicLocation["VertexID$24", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$22", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$23", Automatic, Center], 
         DynamicLocation["VertexID$29", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$24", Automatic, Center], 
          DynamicLocation["VertexID$26", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$25", Automatic, Center], 
         DynamicLocation["VertexID$27", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$25", Automatic, Center], 
          DynamicLocation["VertexID$31", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$26", Automatic, Center], 
         DynamicLocation["VertexID$30", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$26", Automatic, Center], 
          DynamicLocation["VertexID$33", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$28", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$27", Automatic, Center], 
         DynamicLocation["VertexID$31", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$28", Automatic, Center], 
         DynamicLocation["VertexID$37", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$29", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$30", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$31", Automatic, Center], 
         DynamicLocation["VertexID$32", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$31", Automatic, Center], 
          DynamicLocation["VertexID$42", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$32", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$34", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$35", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$33", Automatic, Center], 
         DynamicLocation["VertexID$38", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$34", Automatic, Center], 
         DynamicLocation["VertexID$47", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$36", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$35", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$36", Automatic, Center], 
         DynamicLocation["VertexID$41", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$42", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$37", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$39", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$38", Automatic, Center], 
         DynamicLocation["VertexID$46", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$40", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$39", Automatic, Center], 
         DynamicLocation["VertexID$48", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$43", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$40", Automatic, Center], 
         DynamicLocation["VertexID$49", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$50", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$41", Automatic, Center], 
         DynamicLocation["VertexID$67", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$42", Automatic, Center], 
          DynamicLocation["VertexID$44", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$44", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$43", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$44", Automatic, Center], 
         DynamicLocation["VertexID$45", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$44", Automatic, Center], 
          DynamicLocation["VertexID$55", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$45", Automatic, Center], 
         DynamicLocation["VertexID$59", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$48", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$46", Automatic, Center], 
          DynamicLocation["VertexID$53", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$51", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$47", Automatic, Center], 
         DynamicLocation["VertexID$53", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$48", Automatic, Center], 
          DynamicLocation["VertexID$49", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$48", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$49", Automatic, Center], 
         DynamicLocation["VertexID$52", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$49", Automatic, Center], 
          DynamicLocation["VertexID$61", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$51", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$50", Automatic, Center], 
          DynamicLocation["VertexID$54", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$51", Automatic, Center], 
         DynamicLocation["VertexID$54", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$51", Automatic, Center], 
          DynamicLocation["VertexID$56", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$55", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$52", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$53", Automatic, Center], 
         DynamicLocation["VertexID$58", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$53", Automatic, Center], 
          DynamicLocation["VertexID$65", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$54", Automatic, Center], 
          DynamicLocation["VertexID$69", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$54", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$55", Automatic, Center], 
         DynamicLocation["VertexID$57", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$55", Automatic, Center], 
          DynamicLocation["VertexID$64", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$56", Automatic, Center], 
          DynamicLocation["VertexID$65", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$56", Automatic, Center], 
         DynamicLocation["VertexID$75", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$62", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$57", Automatic, Center], 
         DynamicLocation["VertexID$66", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$61", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$58", Automatic, Center], 
         DynamicLocation["VertexID$74", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$60", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$59", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$60", Automatic, Center], 
         DynamicLocation["VertexID$78", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$61", Automatic, Center], 
         DynamicLocation["VertexID$68", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$61", Automatic, Center], 
          DynamicLocation["VertexID$79", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$63", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$62", Automatic, Center], 
         DynamicLocation["VertexID$70", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$63", Automatic, Center], 
         DynamicLocation["VertexID$72", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$64", Automatic, Center], 
          DynamicLocation["VertexID$66", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$64", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$65", Automatic, Center], 
         DynamicLocation["VertexID$74", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$65", Automatic, Center], 
          DynamicLocation["VertexID$83", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$66", Automatic, Center], 
          DynamicLocation["VertexID$70", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$66", Automatic, Center], 
          DynamicLocation["VertexID$81", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$71", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$67", Automatic, Center], 
         DynamicLocation["VertexID$94", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$73", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$68", Automatic, Center], 
         DynamicLocation["VertexID$85", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$69", Automatic, Center], 
          DynamicLocation["VertexID$76", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$69", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$70", Automatic, Center], 
          DynamicLocation["VertexID$72", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$70", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$71", Automatic, Center], 
         DynamicLocation["VertexID$76", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$72", Automatic, Center], 
          DynamicLocation["VertexID$78", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$72", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$81", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$73", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$74", Automatic, Center], 
         DynamicLocation["VertexID$79", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$74", Automatic, Center], 
         DynamicLocation["VertexID$91", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$83", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$75", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$76", Automatic, Center], 
         DynamicLocation["VertexID$77", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$76", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$78", Automatic, Center], 
         DynamicLocation["VertexID$80", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$78", Automatic, Center], 
         DynamicLocation["VertexID$86", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$79", Automatic, Center], 
          DynamicLocation["VertexID$85", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$79", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$80", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$81", Automatic, Center], 
         DynamicLocation["VertexID$82", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$81", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$82", Automatic, Center], 
         DynamicLocation["VertexID$84", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$83", Automatic, Center], 
          DynamicLocation["VertexID$88", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$83", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$84", Automatic, Center], 
         DynamicLocation["VertexID$86", Automatic, Center]}], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$85", Automatic, Center], 
          DynamicLocation["VertexID$89", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       StyleBox[
        LineBox[{
          DynamicLocation["VertexID$85", Automatic, Center], 
          DynamicLocation["VertexID$95", Automatic, Center]}], 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], 
       LineBox[{
         DynamicLocation["VertexID$86", Automatic, Center], 
         DynamicLocation["VertexID$87", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$87", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$91", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$88", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$90", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$89", Automatic, Center], 
         DynamicLocation["VertexID$96", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$90", Automatic, Center], 
         DynamicLocation["VertexID$93", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$91", Automatic, Center], 
         DynamicLocation["VertexID$92", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$92", Automatic, Center], 
         DynamicLocation["VertexID$95", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$92", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$93", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$94", Automatic, Center], 
         DynamicLocation["VertexID$97", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$96", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$95", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$96", Automatic, Center], 
         DynamicLocation["VertexID$99", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$97", Automatic, Center], 
         DynamicLocation["VertexID$98", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$98", Automatic, Center], 
         DynamicLocation["VertexID$100", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$99", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$100", Automatic, Center], 
         DynamicLocation["VertexID$101", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$101", Automatic, Center], 
         DynamicLocation["VertexID$102", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$102", Automatic, Center], 
         DynamicLocation["VertexID$103", Automatic, Center]}], 
       LineBox[{
         DynamicLocation["VertexID$103", Automatic, Center], 
         DynamicLocation["VertexID$104", Automatic, Center]}]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{-100.422626, 20.621989}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.414987, 20.620784}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410396, 20.619338}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.421725, 20.619097}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404087, 20.61737}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.394259, 20.616607}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.390182, 20.616285}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.388987, 20.614291}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389989, 20.613922}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.394222, 20.613268}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399836, 20.613055}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397181, 20.612857}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$12"], 
         InsetBox[
          FormBox[
           StyleBox["12", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$12", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$12"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.401945, 20.612573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$13"], 
         InsetBox[
          FormBox[
           StyleBox["13", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$13", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$13"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.413654, 20.611969}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$14"], 
         InsetBox[
          FormBox[
           StyleBox["14", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$14", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$14"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409706, 20.61207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$15"], 
         InsetBox[
          FormBox[
           StyleBox["15", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$15", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$15"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38838, 20.610063}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$16"], 
         InsetBox[
          FormBox[
           StyleBox["16", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$16", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$16"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386802, 20.610147}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$17"], 
         InsetBox[
          FormBox[
           StyleBox["17", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$17", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$17"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.381142, 20.610304}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$18"], 
         InsetBox[
          FormBox[
           StyleBox["18", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$18", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$18"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405749, 20.610382}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$19"], 
         InsetBox[
          FormBox[
           StyleBox["19", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$19", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$19"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399391, 20.609277}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$20"], 
         InsetBox[
          FormBox[
           StyleBox["20", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$20", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$20"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39687, 20.6092101}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$21"], 
         InsetBox[
          FormBox[
           StyleBox["21", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$21", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$21"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386442, 20.609114}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$22"], 
         InsetBox[
          FormBox[
           StyleBox["22", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$22", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$22"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.391256, 20.609}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$23"], 
         InsetBox[
          FormBox[
           StyleBox["23", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$23", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$23"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.38332, 20.608429}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$24"], 
         InsetBox[
          FormBox[
           StyleBox["24", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$24", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$24"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406867, 20.606573}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$25"], 
         InsetBox[
          FormBox[
           StyleBox["25", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$25", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$25"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.382298, 20.605955}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$26"], 
         InsetBox[
          FormBox[
           StyleBox["26", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$26", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$26"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.408668, 20.605739}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$27"], 
         InsetBox[
          FormBox[
           StyleBox["27", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$27", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$27"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.417602, 20.604407}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$28"], 
         InsetBox[
          FormBox[
           StyleBox["28", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$28", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$28"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39074, 20.604502}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$29"], 
         InsetBox[
          FormBox[
           StyleBox["29", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$29", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$29"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.377236, 20.604497}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$30"], 
         InsetBox[
          FormBox[
           StyleBox["30", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$30", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$30"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.406611, 20.603862}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$31"], 
         InsetBox[
          FormBox[
           StyleBox["31", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$31", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$31"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399448, 20.603633}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$32"], 
         InsetBox[
          FormBox[
           StyleBox["32", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$32", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$32"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.383215, 20.601403}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$33"], 
         InsetBox[
          FormBox[
           StyleBox["33", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$33", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$33"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.384053, 20.601178}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$34"], 
         InsetBox[
          FormBox[
           StyleBox["34", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$34", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$34"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.380798, 20.600813}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$35"], 
         InsetBox[
          FormBox[
           StyleBox["35", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$35", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$35"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.37591, 20.600506}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$36"], 
         InsetBox[
          FormBox[
           StyleBox["36", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$36", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$36"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.416328, 20.599833}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$37"], 
         InsetBox[
          FormBox[
           StyleBox["37", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$37", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$37"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393797, 20.598759}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$38"], 
         InsetBox[
          FormBox[
           StyleBox["38", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$38", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$38"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.398492, 20.597667}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$39"], 
         InsetBox[
          FormBox[
           StyleBox["39", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$39", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$39"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399739, 20.596995}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$40"], 
         InsetBox[
          FormBox[
           StyleBox["40", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$40", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$40"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.372366, 20.596679}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$41"], 
         InsetBox[
          FormBox[
           StyleBox["41", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$41", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$41"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407375, 20.596437}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$42"], 
         InsetBox[
          FormBox[
           StyleBox["42", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$42", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$42"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.403316, 20.596183}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$43"], 
         InsetBox[
          FormBox[
           StyleBox["43", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$43", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$43"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.407122, 20.595779}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$44"], 
         InsetBox[
          FormBox[
           StyleBox["44", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$44", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$44"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.415007, 20.595231}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$45"], 
         InsetBox[
          FormBox[
           StyleBox["45", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$45", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$45"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.392271, 20.594612}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$46"], 
         InsetBox[
          FormBox[
           StyleBox["46", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$46", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$46"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382999, 20.593451}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$47"], 
         InsetBox[
          FormBox[
           StyleBox["47", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$47", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$47"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397006, 20.593169}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$48"], 
         InsetBox[
          FormBox[
           StyleBox["48", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$48", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$48"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398421, 20.592702}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$49"], 
         InsetBox[
          FormBox[
           StyleBox["49", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$49", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$49"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378101, 20.592499}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$50"], 
         InsetBox[
          FormBox[
           StyleBox["50", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$50", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$50"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.378986, 20.591885}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$51"], 
         InsetBox[
          FormBox[
           StyleBox["51", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$51", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$51"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.401697, 20.591693}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$52"], 
         InsetBox[
          FormBox[
           StyleBox["52", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$52", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$52"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391041, 20.591448}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$53"], 
         InsetBox[
          FormBox[
           StyleBox["53", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$53", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$53"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376605, 20.590774}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$54"], 
         InsetBox[
          FormBox[
           StyleBox["54", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$54", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$54"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404874, 20.590529}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$55"], 
         InsetBox[
          FormBox[
           StyleBox["55", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$55", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$55"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.386949, 20.589948}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$56"], 
         InsetBox[
          FormBox[
           StyleBox["56", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$56", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$56"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.4063, 20.589964}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$57"], 
         InsetBox[
          FormBox[
           StyleBox["57", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$57", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$57"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39598, 20.589817}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$58"], 
         InsetBox[
          FormBox[
           StyleBox["58", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$58", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$58"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.413461, 20.589804}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$59"], 
         InsetBox[
          FormBox[
           StyleBox["59", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$59", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$59"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.41146, 20.589439}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$60"], 
         InsetBox[
          FormBox[
           StyleBox["60", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$60", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$60"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.397337, 20.589346}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$61"], 
         InsetBox[
          FormBox[
           StyleBox["61", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$61", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$61"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40859, 20.589364}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$62"], 
         InsetBox[
          FormBox[
           StyleBox["62", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$62", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$62"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410065, 20.589232}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$63"], 
         InsetBox[
          FormBox[
           StyleBox["63", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$63", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$63"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.404232, 20.589193}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$64"], 
         InsetBox[
          FormBox[
           StyleBox["64", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$64", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$64"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.389918, 20.588889}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$65"], 
         InsetBox[
          FormBox[
           StyleBox["65", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$65", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$65"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405837, 20.588632}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$66"], 
         InsetBox[
          FormBox[
           StyleBox["66", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$66", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$66"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.363755, 20.588653}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$67"], 
         InsetBox[
          FormBox[
           StyleBox["67", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$67", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$67"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40052, 20.588274}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$68"], 
         InsetBox[
          FormBox[
           StyleBox["68", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$68", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$68"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.376417, 20.588207}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$69"], 
         InsetBox[
          FormBox[
           StyleBox["69", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$69", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$69"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.40807, 20.587852}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$70"], 
         InsetBox[
          FormBox[
           StyleBox["70", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$70", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$70"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.370604, 20.587858}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$71"], 
         InsetBox[
          FormBox[
           StyleBox["71", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$71", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$71"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.409321, 20.587412}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$72"], 
         InsetBox[
          FormBox[
           StyleBox["72", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$72", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$72"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.403276, 20.58711}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$73"], 
         InsetBox[
          FormBox[
           StyleBox["73", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$73", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$73"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.395114, 20.587163}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$74"], 
         InsetBox[
          FormBox[
           StyleBox["74", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$74", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$74"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.385845, 20.587028}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$75"], 
         InsetBox[
          FormBox[
           StyleBox["75", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$75", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$75"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.375135, 20.58707}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$76"], 
         InsetBox[
          FormBox[
           StyleBox["76", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$76", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$76"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.376311, 20.586834}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$77"], 
         InsetBox[
          FormBox[
           StyleBox["77", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$77", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$77"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.410515, 20.58699}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$78"], 
         InsetBox[
          FormBox[
           StyleBox["78", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$78", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$78"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.396554, 20.586675}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$79"], 
         InsetBox[
          FormBox[
           StyleBox["79", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$79", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$79"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.412089, 20.586412}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$80"], 
         InsetBox[
          FormBox[
           StyleBox["80", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$80", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$80"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.405086, 20.5865}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$81"], 
         InsetBox[
          FormBox[
           StyleBox["81", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$81", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$81"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407296, 20.585882}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$82"], 
         InsetBox[
          FormBox[
           StyleBox["82", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$82", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$82"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.388782, 20.58596}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$83"], 
         InsetBox[
          FormBox[
           StyleBox["83", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$83", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$83"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40857, 20.585536}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$84"], 
         InsetBox[
          FormBox[
           StyleBox["84", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$84", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$84"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.399559, 20.585669}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$85"], 
         InsetBox[
          FormBox[
           StyleBox["85", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$85", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$85"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.409745, 20.585233}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$86"], 
         InsetBox[
          FormBox[
           StyleBox["86", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$86", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$86"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.410668, 20.584801}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$87"], 
         InsetBox[
          FormBox[
           StyleBox["87", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$87", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$87"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.391535, 20.584951}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$88"], 
         InsetBox[
          FormBox[
           StyleBox["88", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$88", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$88"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.402166, 20.584754}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$89"], 
         InsetBox[
          FormBox[
           StyleBox["89", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$89", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$89"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.404362, 20.58418}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$90"], 
         InsetBox[
          FormBox[
           StyleBox["90", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$90", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$90"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393923, 20.58412}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$91"], 
         InsetBox[
          FormBox[
           StyleBox["91", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$91", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$91"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.395651, 20.583284}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$92"], 
         InsetBox[
          FormBox[
           StyleBox["92", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$92", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$92"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.407144, 20.581739}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$93"], 
         InsetBox[
          FormBox[
           StyleBox["93", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$93", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$93"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.361785, 20.582024}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$94"], 
         InsetBox[
          FormBox[
           StyleBox["94", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$94", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$94"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{-100.398079, 20.581724}, 0.00007049631196042688], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$95"], 
         InsetBox[
          FormBox[
           StyleBox["95", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$95", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$95"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399094, 20.580837}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$96"], 
         InsetBox[
          FormBox[
           StyleBox["96", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$96", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$96"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.374104, 20.579458}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$97"], 
         InsetBox[
          FormBox[
           StyleBox["97", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$97", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$97"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.382268, 20.577798}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$98"], 
         InsetBox[
          FormBox[
           StyleBox["98", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$98", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$98"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.40221, 20.577133}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$99"], 
         InsetBox[
          FormBox[
           StyleBox["99", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$99", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$99"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.38544, 20.577177}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$100"], 
         InsetBox[
          FormBox[
           StyleBox["100", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$100", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$100"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.389128, 20.576425}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$101"], 
         InsetBox[
          FormBox[
           StyleBox["101", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$101", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$101"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.393091, 20.5756}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$102"], 
         InsetBox[
          FormBox[
           StyleBox["102", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$102", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$102"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.39633, 20.574997}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$103"], 
         InsetBox[
          FormBox[
           StyleBox["103", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$103", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$103"], 
       TagBox[{
         TagBox[
          DiskBox[{-100.399193, 20.574315}, 0.00007049631196042688], 
          "DynamicName", BoxID -> "VertexID$104"], 
         InsetBox[
          FormBox[
           StyleBox["104", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$104", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$104"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{19.1199999999569, 
       459.94294871087186`}, {-181.29774871078553`, 166.59671949645622`}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15,
  ImageSize->{621.333333333333, Automatic}]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["15722.96415023009`"], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1319, 744},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
CellContext->Notebook,
TrackCellChangeTimes->False,
Magnification:>0.75 Inherited,
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 106, 3, 72, "Title"],
Cell[1595, 40, 312, 5, 56, "Subsection"],
Cell[CellGroupData[{
Cell[1932, 49, 79, 0, 41, "Section"],
Cell[2014, 51, 59564, 1339, 247, "Output"],
Cell[CellGroupData[{
Cell[61603, 1394, 34, 0, 35, "Subsection"],
Cell[61640, 1396, 44, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[61733, 1402, 96, 2, 51, "Section"],
Cell[61832, 1406, 132675, 3217, 229, "Output"],
Cell[CellGroupData[{
Cell[194532, 4627, 35, 0, 35, "Subsection"],
Cell[194570, 4629, 45, 0, 24, "Output"],
Cell[194618, 4631, 46, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[194713, 4637, 97, 2, 51, "Section"],
Cell[194813, 4641, 108946, 2573, 229, "Output"],
Cell[CellGroupData[{
Cell[303784, 7218, 35, 0, 35, "Subsection"],
Cell[303822, 7220, 46, 0, 24, "Output"],
Cell[303871, 7222, 46, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[303966, 7228, 97, 2, 51, "Section"],
Cell[304066, 7232, 101172, 2478, 303, "Output"],
Cell[CellGroupData[{
Cell[405263, 9714, 34, 0, 35, "Subsection"],
Cell[405300, 9716, 44, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[405393, 9722, 97, 2, 51, "Section"],
Cell[405493, 9726, 102851, 2534, 229, "Output"],
Cell[CellGroupData[{
Cell[508369, 12264, 34, 0, 35, "Subsection"],
Cell[508406, 12266, 46, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[508501, 12272, 97, 2, 51, "Section"],
Cell[508601, 12276, 99628, 2427, 229, "Output"],
Cell[CellGroupData[{
Cell[608254, 14707, 34, 0, 35, "Subsection"],
Cell[608291, 14709, 46, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[608386, 14715, 97, 2, 51, "Section"],
Cell[608486, 14719, 99580, 2426, 229, "Output"],
Cell[CellGroupData[{
Cell[708091, 17149, 34, 0, 35, "Subsection"],
Cell[708128, 17151, 46, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[708223, 17157, 105, 2, 51, "Section"],
Cell[708331, 17161, 97626, 2362, 384, "Output"],
Cell[CellGroupData[{
Cell[805982, 19527, 34, 0, 35, "Subsection"],
Cell[806019, 19529, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature mw0W5Tts1ERb8AgRKn7yYMw5 *)
