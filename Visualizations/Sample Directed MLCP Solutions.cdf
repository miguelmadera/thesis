(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[    129338,       3018]
NotebookOptionsPosition[    128555,       2968]
NotebookOutlinePosition[    129013,       2988]
CellTagsIndexPosition[    128970,       2985]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["MLCP Solution Visualizations: Directed Sample Solutions", "Title"],

Cell["\<\
Every unreduced and reduced solution for the directed sample graph is \
displayed here. For algorithms that produce multiple solutions, only the best \
solution is shown here. Unreduced solutions are displayed in red, reduced \
solutions (if they exist) are displayed in blue. Total graph weight is \
compared to Undirected solution because directed solution includes 2-way \
edges.\
\>", "Subsection"],

Cell[CellGroupData[{

Cell["Directed Sample Dataset - Base Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
      11}, {{{1, 2}, {2, 1}, {8, 1}, {9, 1}, {2, 3}, {2, 5}, {5, 2}, {3, 4}, {
       4, 3}, {3, 5}, {5, 3}, {3, 6}, {6, 3}, {4, 7}, {4, 10}, {10, 4}, {5, 
       6}, {6, 5}, {9, 5}, {6, 7}, {7, 6}, {7, 9}, {9, 7}, {11, 8}, {9, 10}, {
       11, 9}, {10, 11}}, Null}, {EdgeShapeFunction -> {
         GraphElementData["ShortFilledArrow", "ArrowSize" -> 0.01]}, 
       EdgeWeight -> {1., 1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 3.605551275463989, 2.8284271247461903`, 
        2.8284271247461903`, 4.242640687119286, 4.242640687119286, 
        4.47213595499958, 4.47213595499958, 2.23606797749979, 4., 4., 
        1.4142135623730951`, 1.4142135623730951`, 2., 3., 3., 
        4.123105625617661, 4.123105625617661, 5., 5.0990195135927845`, 
        3.1622776601683795`, 6.324555320336759}, 
       GraphLayout -> {
        "EdgeLayout" -> {
          "DividedEdgeBundling", "CoulombConstant" -> 0, "VelocityDamping" -> 
           0.2, "SmoothEdge" -> False, "NewForce" -> False, "Connectivity" -> 
           True, "Compatibility" -> True, "Threshold" -> 0.1, "CoulombDecay" -> 
           1, "LaneWidth" -> 1}}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}, 
       VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Small}}]]}, 
    TagBox[GraphicsGroupBox[{
       {Hue[0.6, 0.7, 0.5], Opacity[0.7], 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdy8sJgEAQBNHGk39NYY3EGAxB8GwKHVqHprLgoQaG5h2qnPdxNZLK+9/W
a/e6W90dNhxYHXrYcGD16GHDgTWghw0H1ogeNhxYE3rYcGDN6GHDgbWghw0H
1vr7AbiWJDE=
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdy7sJgFAQRNHByL+2oJVMDZYgGNuCpU1pKi8Q78IynOAu+7kdlaTl+XfL
zS67ls309wUbFpwRPWxYcAb0sGHB6dHDhgWnQw8bFpwWPWxYcBr0sGHBqdHD
hvX5BpWlJDE=
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdzcsNgzAQRVGLCvgHCBA7lUwNVGAhsU5KcCmUQAkuhVIg8iZzRxo9ndV1
62fZMmOMu/+36XJJ+067w1Jon3AotW2lHWFfa5sGfVha9OHwQL9DH/Y9+gP6
sDzRh8OI/oQ+7Gf0X+jD1mp/4QjnDj34gP/2ApeEJ4Y=
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxFy+mJwzAUhVGRCrLvi5xKXgkhFZiB+T1TgkpxCSlBJaQEl5IEHxOB+Djw
bvPzd/+dpJSa9/90eNMYeh0a/GyGFu648j+nmR0HT8c999meH5zm7jj4xoXz
uOf+Yr9wx8GZC9ezcstpqRzcnew5uHJ/tFu54+DMhetBueW0Vg7u9vYcXLnf
2W3ccXDmwnWr3HL69gVA+jLr
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxVzdupQyEURVFJHXl48qrDGm4FEsh3WtilWIqlWEoSMvbHFWQxwInb4/X3
3JVSts/97u+c27+tdm2/bTy58+Dg4MGdJzde+Q+Xi+XKq+p4cufBwcGDO09u
vLhyuVquPE86Du7cOLLnkf3RZs8rey43y5XnwTsO7tw4sueR/d5mzyt7Lne7
b290zzXn
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdxdENgjAURuEbJvBRFOGWSTqCcYKGxGcZoaN0BEboCI7QUdDwQP5zkpMv
LJ/XuzOz8Pvv0RzFb1ALXGGEF9hc3WCGT+iwTWqGDuuoJmiwPNQI26Bm6LDe
1QQNlpsaYevVDB3Wq5qgne767jZ3
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdj9kNwzAMQ4nOkPQ+7E6iGTpBEKDfXUGjZBSPklHaIuQHbcAgnmlJVJ0/
r/cOQP3dv27nGaZr3bR1mtSFGt07qJPqSvePXFRfvA7klZoP7wPyIv/ufUGe
5N+6OeQi/+pzQW6ckxfPAXLIP3sukJtynTwnyCH/6LlBbsyZB98D5JC/971A
btwjx25Pcsgf+C5/iC9mWy/b
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJw9zdkJAzEMRVGROrLIk6UO1ZAKzEC+04JKUSkqxaUkQVcZGB7H+OKxv5+v
g4iM7//b+h5We6xd91rDiR0HTuxYTrWze2zdY+0eS/dn3r/R4cSOAyd2LBfe
7x5b91i7x9K98v6Vezix4cATO3Y8cWDDiRUvLFgG5xv3cGLDgSd27HjiwIYT
K15Ytv9+AP3ANec=
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJw1y00Ng1AQReFJFbCg/11AUrpGwkggVUCadF0kPAlIQAISngQkIAEJbTPn
TjK5+Ranfn2e752Z1b//b9zDY2+xWxObr7ErTnjBjjM2PKu/xE7q8ageJ/V4
UH+O7dXjTj129bhVf4qt1ONCPTb1eLvTH2NXnPCCHWdseFZ/iJ3U41E9Turx
oH4f26vHnXrs6nGrvoyt1ONCPTb1pX8BEsE1EQ==
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRFGLiPu+IXFCHa6BEpCIaWFKB+TIf6XV6Onb6znvyBhjv//X
3+H87i6wttAG1ooOa0GHNaPDmtBhjeiwBnRYPTqsDh1Wiw6rQYdVo9MVXMIF
nMMZnMIJHLsXPogfsQ==
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxVzc1NQ0EMhVGLChIg/EPmpRKXgKgAIbGGElzKK4ESXAIlpBRAc7JgpNHV
WXzy8vbx8n4WEcvv/9v5bvLf9vXcVy5uTh63Oi7uq7lHLs47yysHx71+5z4X
NyePBx0X96X7XJyPllcOjif9hftc3Jw89jou7nP3uXgMPT9zcHFv535x8fHU
82bRc3Jv5n5y8crJ36ee45A/6wsy6w==
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdxdEJwjAURuFLJ/DRam1vOklGECcIBZ/tCBklI3SEjOAIGaVKofCfA4cv
LJ/XuzOz8Pvv0TWKCVbovZphg/GmFmh3NcEKfVAzbDA+1AJtVBOs0Cc1wwbd
1SfMcIMNXoIa4QoL/EKbT3eVEDZ3
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxVzcsJQjEQhtFgDde3i9xOUoMVBMG1LUwpU0pKSSkq97gwMHycxU/Wx+v+
3JVS1s99u71T+2vVedzaeHDn5ODg5M6DG8/fP1zOypXnwY4Hd04ODk7uPLjx
5Mrlolx57u14cOfk4ODkzoMbT65crsqV52LHgzsnBwcndx7ceHLlctOlvQGo
2TRj
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdj9sJw0AMBEVqyPt5ciVXQyowgXynBZXiUq4UlxKHmzUoBrOMx3vS+evz
fG/MzJf3l/251p7bnhUOOOR3PZs8bDdyTx8OOOQP9OVhu5NH+nDAIX+iLw/b
gzzThwMO+Qt9edgKyb0KrHtXefYe5XUPefaa5OEmz9xZXnt53qN4nls9zxk9
nxvqk5PnXvP83/z33YY1v7gQL9s=
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJw9ytsJQjEQBNDFGnzrR24nW4MVBMFvW0gpKSWlpBSVHFxYhjPM9nw/XruI
2L7/y3X3XLlfOW8rCw9O7ly5cePKnZMHF54cHAf91Y4HJ3eu3Lhx5c7JgwtP
Do6j/mLHg5M7V27cuHLn5MGFJwfHSX+248HJnSs3bly5c/LgwpPj/M8PqNk0
Yw==
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdzU0JgEAQQOHBBB48+rOrBjDCRhATiOBZIxjBCBvBCBvBCEYwgsqCMG9g
eHyXGTstw5yIiH33a5zcxWaxIxxgU2hv8AW7UtvDUuE/HODU4D68wh4+YbHa
HTzCOxzgGza1dg9v8AFfcNpoO3iFPXzC0v5+AMHNJ1o=
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdzk0NgDAMhuEGBRw48rMBApAwCQQFhIQzSEACEiYBCZOABCQgAcgOpG+T
5stz+Vo7LcOciIh998s4rYuZxTwbbQ+vsINT+Kq1D3iDe9jAt9UO8A6PcAcL
fBptD6+wg1M4VPgHFtiX6IevQnuDDRxy3Ifl9wM/iyda
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJw1zdkNwlAMRFGLCsLyQVjfIxTiGqgAIfFNCy4lpbgUSoHIdyJF1slkNP31
ebxXZtb/73LruXvdXd2c6n63dQMndjxjw4HzVvepPnb1cVMfm/qd/Q19nNjx
jA0Hzsa++tjVx019bPr/yv6a7zhxXNhXjkP5mX3l2JWf2FeOTfmR/YHvOHEc
2FeOQ/nIvnLsyvfsK8c2+A8B0jCH
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[{{1., 4.}, {3., 9.}}]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxVkMkNwjAQAFe8uAmEEG4cKtkaqMBC4g0lbCkugRJcSkoBtMPDlqLRWLLH
m+7+vD0GItJ9vx99bdVZOVMLccV16ew3EDfcVs6Ah9qZG4hHPK7pNiUT+wlX
XP/dunyH4daW3cBcmXdlPOJxRxeXPV3mSvtybj3Q5b/0uOF2pIuHE/sL+nie
0z/TxeVCZ0Yftyn3Bc5PnC9c8Izr2Fl1nB/Rw/PQ+cYFl6t+AM3eMvE=
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdy7sNgEAQA1GLCvhDep1cDZSAREwLLs2lcYiIWWllvWDKeR9XJ6m0f/e7
vX7b158rbDiwBvSw4cAa0cOGA2tCDxsOrBk9bDiwFvSw4cBa0cOGA2tDDxsO
3PYBItIj7w==
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJw1zdsJQjEQhOHFCrw9eHejFrI1WEEQfLaFlJJStpSUopJ/DhyGL5Mh5fV5
vhdmVn7/P+e3i5nLmQ07tv3MUI+r+gPn6nFXf5yZ6vFQfyJX9NixnXlfPa7q
L5yrx139lffV46HeyTXvYMcNB05csRXuaY+79ji1x0P7G7lhjx03HDhxxXbn
nva4a49Tezy0f5Db+AJKhzCH
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJwtzt1NAkEUhuGJV6CCeymKuuvfNSWcEowVEBKusYRTAiVsCZQwJVACJVAC
mHk22bx5NvNlZ9jsfrc3pZTh+v63PY/R2rWOXB9aY9GafOLgfLLn/tl+rpy8
XtpzeVEeuc6ce7XnEwfnmz13vf29/3HygYPLoPzD9c59OPnMwfFuz3uut+7L
yasPO04ufOQ6be0/neM/Dq5cuPvyfdK65uQDB5dvncQFnIkyRg==
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRNEREfdt7sQJdbgGSkAipgVKB+TIf6XV6Onb8z6uSJL9/l9/
u/Mbu9AJnMIZnMMFXMIVXId+YDXosFp0WB06rB4dlkGHNaDDGtFhTeiwZnRY
CzqsFR3W5l5njB+x
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[BezierCurveBox[CompressedData["
1:eJw1zdcNg0AQhOE1jgU4R86pDa4GV4CQ/OwWKIVSKIUSnHPW/ot0Gn03jM4l
m9U6EBH3O//Ub+k1G5rZQjOta3osuJjT10gsOLS+qpnP6HGM0wr31uNsSl/m
fSy4cPQBiQWH1pd4P6THMU6Fe+uxn+BPpP+N8Vudj/BLLean2g/xg/0A39n3
8Y29+cq+hy/su/jMvoNP7M1H9m18YN/Ce/ZNvGNv3kZf/sQ21w==
          "]]]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[{{5., 1.}, {1., 4.}}]}, 
        {Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], ArrowBox[{{5., 1.}, {6., 4.}}]}}, 
       {Hue[0.6, 0.2, 0.8], EdgeForm[{GrayLevel[0], Opacity[
        0.7]}], {DiskBox[{3., 9.}, 0.], InsetBox[
          StyleBox["1",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {3., 9.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{4., 9.}, 0.], InsetBox[
          StyleBox["2",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {4., 9.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{9., 9.}, 0.], InsetBox[
          StyleBox["3",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {9., 9.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{11., 7.}, 0.], InsetBox[
          StyleBox["4",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {11., 7.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 6.}, 0.], InsetBox[
          StyleBox["5",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6., 6.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{7., 5.}, 0.], InsetBox[
          StyleBox["6",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {7., 5.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{10., 5.}, 0.], InsetBox[
          StyleBox["7",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {10., 5.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{1., 4.}, 0.], InsetBox[
          StyleBox["8",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {1., 4.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{6., 4.}, 0.], InsetBox[
          StyleBox["9",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {6., 4.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{11., 3.}, 0.], InsetBox[
          StyleBox["10",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {11., 3.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}, {DiskBox[{5., 1.}, 0.], InsetBox[
          StyleBox["11",
           StripOnInput->False,
           FontSize->9], Offset[{2, 2}, {5., 1.}], ImageScaled[{0, 0}],
          BaseStyle->"Graphics"]}}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["68.72411140389733`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Directed Sample Dataset - MCST Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
      11}, {{{1, 2}, {2, 1}, {8, 1}, {9, 1}, {2, 3}, {2, 5}, {5, 2}, {3, 4}, {
       4, 3}, {3, 5}, {5, 3}, {3, 6}, {6, 3}, {4, 7}, {4, 10}, {10, 4}, {5, 
       6}, {6, 5}, {9, 5}, {6, 7}, {7, 6}, {7, 9}, {9, 7}, {11, 8}, {9, 10}, {
       11, 9}, {10, 11}}, Null}, {EdgeShapeFunction -> {
         GraphElementData["ShortFilledArrow", "ArrowSize" -> 0.01]}, 
       EdgeWeight -> {1., 1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 3.605551275463989, 2.8284271247461903`, 
        2.8284271247461903`, 4.242640687119286, 4.242640687119286, 
        4.47213595499958, 4.47213595499958, 2.23606797749979, 4., 4., 
        1.4142135623730951`, 1.4142135623730951`, 2., 3., 3., 
        4.123105625617661, 4.123105625617661, 5., 5.0990195135927845`, 
        3.1622776601683795`, 6.324555320336759}, 
       GraphHighlight -> {1, 3, 4, 11, 5, 
         DirectedEdge[11, 8], 10, 
         DirectedEdge[5, 2], 6, 2, 
         DirectedEdge[5, 3], 
         DirectedEdge[6, 7], 
         DirectedEdge[3, 4], 9, 
         DirectedEdge[4, 10], 7, 8, 
         DirectedEdge[5, 6], 
         DirectedEdge[9, 5], 
         DirectedEdge[11, 9], 
         DirectedEdge[2, 1]}, 
       GraphHighlightStyle -> {"Thick", 1 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, DirectedEdge[3, 4] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 3 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 5 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 6 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, DirectedEdge[5, 2] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, DirectedEdge[9, 5] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, DirectedEdge[5, 6] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 2 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 11 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         DirectedEdge[11, 9] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 7 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 9 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, DirectedEdge[5, 3] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, DirectedEdge[6, 7] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         DirectedEdge[11, 8] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 
         DirectedEdge[4, 10] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 8 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 4 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, 10 -> {VertexStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}, DirectedEdge[2, 1] -> {EdgeStyle -> Directive[
             RGBColor[1, 0, 0], 
             Thickness[Large]]}}, 
       GraphLayout -> {
        "EdgeLayout" -> {
          "DividedEdgeBundling", "CoulombConstant" -> 0, "VelocityDamping" -> 
           0.2, "SmoothEdge" -> False, "NewForce" -> False, "Connectivity" -> 
           True, "Compatibility" -> True, "Threshold" -> 0.1, "CoulombDecay" -> 
           1, "LaneWidth" -> 1}}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}, 
       VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Small}}]], Typeset`boxes, Typeset`boxes$s2d = 
    GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdy8sJgEAQBNHGk39NYY3EGAxB8GwKHVqHprLgoQaG5h2qnPdxNZLK+9/W
a/e6W90dNhxYHXrYcGD16GHDgTWghw0H1ogeNhxYE3rYcGDN6GHDgbWghw0H
1vr7AbiWJDE=
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxdy7sJgFAQRNHByL+2oJVMDZYgGNuCpU1pKi8Q78IynOAu+7kdlaTl+XfL
zS67ls309wUbFpwRPWxYcAb0sGHB6dHDhgWnQw8bFpwWPWxYcBr0sGHBqdHD
hvX5BpWlJDE=
           "]]]}, 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzcsNgzAQRVGLCvgHCBA7lUwNVGAhsU5KcCmUQAkuhVIg8iZzRxo9ndV1
62fZMmOMu/+36XJJ+067w1Jon3AotW2lHWFfa5sGfVha9OHwQL9DH/Y9+gP6
sDzRh8OI/oQ+7Gf0X+jD1mp/4QjnDj34gP/2ApeEJ4Y=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxFy+mJwzAUhVGRCrLvi5xKXgkhFZiB+T1TgkpxCSlBJaQEl5IEHxOB+Djw
bvPzd/+dpJSa9/90eNMYeh0a/GyGFu648j+nmR0HT8c999meH5zm7jj4xoXz
uOf+Yr9wx8GZC9ezcstpqRzcnew5uHJ/tFu54+DMhetBueW0Vg7u9vYcXLnf
2W3ccXDmwnWr3HL69gVA+jLr
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxVzdupQyEURVFJHXl48qrDGm4FEsh3WtilWIqlWEoSMvbHFWQxwInb4/X3
3JVSts/97u+c27+tdm2/bTy58+Dg4MGdJzde+Q+Xi+XKq+p4cufBwcGDO09u
vLhyuVquPE86Du7cOLLnkf3RZs8rey43y5XnwTsO7tw4sueR/d5mzyt7Lne7
b290zzXn
           "]]]}, 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdxdENgjAURuEbJvBRFOGWSTqCcYKGxGcZoaN0BEboCI7QUdDwQP5zkpMv
LJ/XuzOz8Pvv0RzFb1ALXGGEF9hc3WCGT+iwTWqGDuuoJmiwPNQI26Bm6LDe
1QQNlpsaYevVDB3Wq5qgne767jZ3
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdj9kNwzAMQ4nOkPQ+7E6iGTpBEKDfXUGjZBSPklHaIuQHbcAgnmlJVJ0/
r/cOQP3dv27nGaZr3bR1mtSFGt07qJPqSvePXFRfvA7klZoP7wPyIv/ufUGe
5N+6OeQi/+pzQW6ckxfPAXLIP3sukJtynTwnyCH/6LlBbsyZB98D5JC/971A
btwjx25Pcsgf+C5/iC9mWy/b
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw9zdkJAzEMRVGROrLIk6UO1ZAKzEC+04JKUSkqxaUkQVcZGB7H+OKxv5+v
g4iM7//b+h5We6xd91rDiR0HTuxYTrWze2zdY+0eS/dn3r/R4cSOAyd2LBfe
7x5b91i7x9K98v6Vezix4cATO3Y8cWDDiRUvLFgG5xv3cGLDgSd27HjiwIYT
K15Ytv9+AP3ANec=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1y00Ng1AQReFJFbCg/11AUrpGwkggVUCadF0kPAlIQAISngQkIAEJbTPn
TjK5+Ranfn2e752Z1b//b9zDY2+xWxObr7ErTnjBjjM2PKu/xE7q8ageJ/V4
UH+O7dXjTj129bhVf4qt1ONCPTb1eLvTH2NXnPCCHWdseFZ/iJ3U41E9Turx
oH4f26vHnXrs6nGrvoyt1ONCPTb1pX8BEsE1EQ==
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRFGLiPu+IXFCHa6BEpCIaWFKB+TIf6XV6Onb6znvyBhjv//X
3+H87i6wttAG1ooOa0GHNaPDmtBhjeiwBnRYPTqsDh1Wiw6rQYdVo9MVXMIF
nMMZnMIJHLsXPogfsQ==
           "]]]}, 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxVzc1NQ0EMhVGLChIg/EPmpRKXgKgAIbGGElzKK4ESXAIlpBRAc7JgpNHV
WXzy8vbx8n4WEcvv/9v5bvLf9vXcVy5uTh63Oi7uq7lHLs47yysHx71+5z4X
NyePBx0X96X7XJyPllcOjif9hftc3Jw89jou7nP3uXgMPT9zcHFv535x8fHU
82bRc3Jv5n5y8crJ36ee45A/6wsy6w==
           "]]]}, 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxdxdEJwjAURuFLJ/DRam1vOklGECcIBZ/tCBklI3SEjOAIGaVKofCfA4cv
LJ/XuzOz8Pvv0TWKCVbovZphg/GmFmh3NcEKfVAzbDA+1AJtVBOs0Cc1wwbd
1SfMcIMNXoIa4QoL/EKbT3eVEDZ3
           "]]]}, 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxVzcsJQjEQhtFgDde3i9xOUoMVBMG1LUwpU0pKSSkq97gwMHycxU/Wx+v+
3JVS1s99u71T+2vVedzaeHDn5ODg5M6DG8/fP1zOypXnwY4Hd04ODk7uPLjx
5Mrlolx57u14cOfk4ODkzoMbT65crsqV52LHgzsnBwcndx7ceHLlctOlvQGo
2TRj
           "]]]}, 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdj9sJw0AMBEVqyPt5ciVXQyowgXynBZXiUq4UlxKHmzUoBrOMx3vS+evz
fG/MzJf3l/251p7bnhUOOOR3PZs8bDdyTx8OOOQP9OVhu5NH+nDAIX+iLw/b
gzzThwMO+Qt9edgKyb0KrHtXefYe5XUPefaa5OEmz9xZXnt53qN4nls9zxk9
nxvqk5PnXvP83/z33YY1v7gQL9s=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw9ytsJQjEQBNDFGnzrR24nW4MVBMFvW0gpKSWlpBSVHFxYhjPM9nw/XruI
2L7/y3X3XLlfOW8rCw9O7ly5cePKnZMHF54cHAf91Y4HJ3eu3Lhx5c7JgwtP
Do6j/mLHg5M7V27cuHLn5MGFJwfHSX+248HJnSs3bly5c/LgwpPj/M8PqNk0
Yw==
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxdzU0JgEAQQOHBBB48+rOrBjDCRhATiOBZIxjBCBvBCBvBCEYwgsqCMG9g
eHyXGTstw5yIiH33a5zcxWaxIxxgU2hv8AW7UtvDUuE/HODU4D68wh4+YbHa
HTzCOxzgGza1dg9v8AFfcNpoO3iFPXzC0v5+AMHNJ1o=
           "]]]}, 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzk0NgDAMhuEGBRw48rMBApAwCQQFhIQzSEACEiYBCZOABCQgAcgOpG+T
5stz+Vo7LcOciIh998s4rYuZxTwbbQ+vsINT+Kq1D3iDe9jAt9UO8A6PcAcL
fBptD6+wg1M4VPgHFtiX6IevQnuDDRxy3Ifl9wM/iyda
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdkNwlAMRFGLCsLyQVjfIxTiGqgAIfFNCy4lpbgUSoHIdyJF1slkNP31
ebxXZtb/73LruXvdXd2c6n63dQMndjxjw4HzVvepPnb1cVMfm/qd/Q19nNjx
jA0Hzsa++tjVx019bPr/yv6a7zhxXNhXjkP5mX3l2JWf2FeOTfmR/YHvOHEc
2FeOQ/nIvnLsyvfsK8c2+A8B0jCH
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{1., 4.}, {3., 9.}}]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVkMkNwjAQAFe8uAmEEG4cKtkaqMBC4g0lbCkugRJcSkoBtMPDlqLRWLLH
m+7+vD0GItJ9vx99bdVZOVMLccV16ew3EDfcVs6Ah9qZG4hHPK7pNiUT+wlX
XP/dunyH4daW3cBcmXdlPOJxRxeXPV3mSvtybj3Q5b/0uOF2pIuHE/sL+nie
0z/TxeVCZ0Yftyn3Bc5PnC9c8Izr2Fl1nB/Rw/PQ+cYFl6t+AM3eMvE=
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxdy7sNgEAQA1GLCvhDep1cDZSAREwLLs2lcYiIWWllvWDKeR9XJ6m0f/e7
vX7b158rbDiwBvSw4cAa0cOGA2tCDxsOrBk9bDiwFvSw4cBa0cOGA2tDDxsO
3PYBItIj7w==
           "]]]}, 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdsJQjEQhOHFCrw9eHejFrI1WEEQfLaFlJJStpSUopJ/DhyGL5Mh5fV5
vhdmVn7/P+e3i5nLmQ07tv3MUI+r+gPn6nFXf5yZ6vFQfyJX9NixnXlfPa7q
L5yrx139lffV46HeyTXvYMcNB05csRXuaY+79ji1x0P7G7lhjx03HDhxxXbn
nva4a49Tezy0f5Db+AJKhzCH
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJwtzt1NAkEUhuGJV6CCeymKuuvfNSWcEowVEBKusYRTAiVsCZQwJVACJVAC
mHk22bx5NvNlZ9jsfrc3pZTh+v63PY/R2rWOXB9aY9GafOLgfLLn/tl+rpy8
XtpzeVEeuc6ce7XnEwfnmz13vf29/3HygYPLoPzD9c59OPnMwfFuz3uut+7L
yasPO04ufOQ6be0/neM/Dq5cuPvyfdK65uQDB5dvncQFnIkyRg==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRNEREfdt7sQJdbgGSkAipgVKB+TIf6XV6Onb8z6uSJL9/l9/
u/Mbu9AJnMIZnMMFXMIVXId+YDXosFp0WB06rB4dlkGHNaDDGtFhTeiwZnRY
CzqsFR3W5l5njB+x
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdcNg0AQhOE1jgU4R86pDa4GV4CQ/OwWKIVSKIUSnHPW/ot0Gn03jM4l
m9U6EBH3O//Ub+k1G5rZQjOta3osuJjT10gsOLS+qpnP6HGM0wr31uNsSl/m
fSy4cPQBiQWH1pd4P6THMU6Fe+uxn+BPpP+N8Vudj/BLLean2g/xg/0A39n3
8Y29+cq+hy/su/jMvoNP7M1H9m18YN/Ce/ZNvGNv3kZf/sQ21w==
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[{{5., 1.}, {1., 4.}}]}, 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False], 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[{{5., 1.}, {6., 4.}}]}, 
        Directive[
         RGBColor[1, 0, 0], 
         Thickness[Large]], StripOnInput -> False]}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{3., 9.}, 0.], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{4., 9.}, 0.], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{9., 9.}, 0.], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{11., 7.}, 0.], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{10., 5.}, 0.], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{1., 4.}, 0.], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{11., 3.}, 0.], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{5., 1.}, 0.], 
           Directive[
            RGBColor[1, 0, 0], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{12.760693309010094`, 
       298.6664134721733}, {-122.88761347217661`, 108.2393066909899}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15,
  ImageSize->{398.6666666666622, Automatic}]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weights", "Subsection"],

Cell[BoxData["30.25311030987094`"], "Output"],

Cell[BoxData["3.414213562373095`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Directed Sample Dataset - GSH-T Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
      11}, {{{1, 2}, {2, 1}, {8, 1}, {9, 1}, {2, 3}, {2, 5}, {5, 2}, {3, 4}, {
       4, 3}, {3, 5}, {5, 3}, {3, 6}, {6, 3}, {4, 7}, {4, 10}, {10, 4}, {5, 
       6}, {6, 5}, {9, 5}, {6, 7}, {7, 6}, {7, 9}, {9, 7}, {11, 8}, {9, 10}, {
       11, 9}, {10, 11}}, Null}, {EdgeShapeFunction -> {
         GraphElementData["ShortFilledArrow", "ArrowSize" -> 0.01]}, 
       EdgeWeight -> {1., 1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 3.605551275463989, 2.8284271247461903`, 
        2.8284271247461903`, 4.242640687119286, 4.242640687119286, 
        4.47213595499958, 4.47213595499958, 2.23606797749979, 4., 4., 
        1.4142135623730951`, 1.4142135623730951`, 2., 3., 3., 
        4.123105625617661, 4.123105625617661, 5., 5.0990195135927845`, 
        3.1622776601683795`, 6.324555320336759}, GraphHighlight -> {
         DirectedEdge[9, 5], 6, 5, 9, 
         DirectedEdge[5, 6]}, 
       GraphHighlightStyle -> {
        "Thick", DirectedEdge[5, 6] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, DirectedEdge[9, 5] -> {EdgeStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 5 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 9 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}, 6 -> {VertexStyle -> Directive[
             RGBColor[0, 0, 1], 
             Thickness[Large]]}}, 
       GraphLayout -> {
        "EdgeLayout" -> {
          "DividedEdgeBundling", "CoulombConstant" -> 0, "VelocityDamping" -> 
           0.2, "SmoothEdge" -> False, "NewForce" -> False, "Connectivity" -> 
           True, "Compatibility" -> True, "Threshold" -> 0.1, "CoulombDecay" -> 
           1, "LaneWidth" -> 1}}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}, 
       VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Small}}]], Typeset`boxes, Typeset`boxes$s2d = 
    GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdy8sJgEAQBNHGk39NYY3EGAxB8GwKHVqHprLgoQaG5h2qnPdxNZLK+9/W
a/e6W90dNhxYHXrYcGD16GHDgTWghw0H1ogeNhxYE3rYcGDN6GHDgbWghw0H
1vr7AbiWJDE=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdy7sJgFAQRNHByL+2oJVMDZYgGNuCpU1pKi8Q78IynOAu+7kdlaTl+XfL
zS67ls309wUbFpwRPWxYcAb0sGHB6dHDhgWnQw8bFpwWPWxYcBr0sGHBqdHD
hvX5BpWlJDE=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzcsNgzAQRVGLCvgHCBA7lUwNVGAhsU5KcCmUQAkuhVIg8iZzRxo9ndV1
62fZMmOMu/+36XJJ+067w1Jon3AotW2lHWFfa5sGfVha9OHwQL9DH/Y9+gP6
sDzRh8OI/oQ+7Gf0X+jD1mp/4QjnDj34gP/2ApeEJ4Y=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxFy+mJwzAUhVGRCrLvi5xKXgkhFZiB+T1TgkpxCSlBJaQEl5IEHxOB+Djw
bvPzd/+dpJSa9/90eNMYeh0a/GyGFu648j+nmR0HT8c999meH5zm7jj4xoXz
uOf+Yr9wx8GZC9ezcstpqRzcnew5uHJ/tFu54+DMhetBueW0Vg7u9vYcXLnf
2W3ccXDmwnWr3HL69gVA+jLr
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVzdupQyEURVFJHXl48qrDGm4FEsh3WtilWIqlWEoSMvbHFWQxwInb4/X3
3JVSts/97u+c27+tdm2/bTy58+Dg4MGdJzde+Q+Xi+XKq+p4cufBwcGDO09u
vLhyuVquPE86Du7cOLLnkf3RZs8rey43y5XnwTsO7tw4sueR/d5mzyt7Lne7
b290zzXn
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdxdENgjAURuEbJvBRFOGWSTqCcYKGxGcZoaN0BEboCI7QUdDwQP5zkpMv
LJ/XuzOz8Pvv0RzFb1ALXGGEF9hc3WCGT+iwTWqGDuuoJmiwPNQI26Bm6LDe
1QQNlpsaYevVDB3Wq5qgne767jZ3
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdj9kNwzAMQ4nOkPQ+7E6iGTpBEKDfXUGjZBSPklHaIuQHbcAgnmlJVJ0/
r/cOQP3dv27nGaZr3bR1mtSFGt07qJPqSvePXFRfvA7klZoP7wPyIv/ufUGe
5N+6OeQi/+pzQW6ckxfPAXLIP3sukJtynTwnyCH/6LlBbsyZB98D5JC/971A
btwjx25Pcsgf+C5/iC9mWy/b
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw9zdkJAzEMRVGROrLIk6UO1ZAKzEC+04JKUSkqxaUkQVcZGB7H+OKxv5+v
g4iM7//b+h5We6xd91rDiR0HTuxYTrWze2zdY+0eS/dn3r/R4cSOAyd2LBfe
7x5b91i7x9K98v6Vezix4cATO3Y8cWDDiRUvLFgG5xv3cGLDgSd27HjiwIYT
K15Ytv9+AP3ANec=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1y00Ng1AQReFJFbCg/11AUrpGwkggVUCadF0kPAlIQAISngQkIAEJbTPn
TjK5+Ranfn2e752Z1b//b9zDY2+xWxObr7ErTnjBjjM2PKu/xE7q8ageJ/V4
UH+O7dXjTj129bhVf4qt1ONCPTb1eLvTH2NXnPCCHWdseFZ/iJ3U41E9Turx
oH4f26vHnXrs6nGrvoyt1ONCPTb1pX8BEsE1EQ==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRFGLiPu+IXFCHa6BEpCIaWFKB+TIf6XV6Onb6znvyBhjv//X
3+H87i6wttAG1ooOa0GHNaPDmtBhjeiwBnRYPTqsDh1Wiw6rQYdVo9MVXMIF
nMMZnMIJHLsXPogfsQ==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVzc1NQ0EMhVGLChIg/EPmpRKXgKgAIbGGElzKK4ESXAIlpBRAc7JgpNHV
WXzy8vbx8n4WEcvv/9v5bvLf9vXcVy5uTh63Oi7uq7lHLs47yysHx71+5z4X
NyePBx0X96X7XJyPllcOjif9hftc3Jw89jou7nP3uXgMPT9zcHFv535x8fHU
82bRc3Jv5n5y8crJ36ee45A/6wsy6w==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdxdEJwjAURuFLJ/DRam1vOklGECcIBZ/tCBklI3SEjOAIGaVKofCfA4cv
LJ/XuzOz8Pvv0TWKCVbovZphg/GmFmh3NcEKfVAzbDA+1AJtVBOs0Cc1wwbd
1SfMcIMNXoIa4QoL/EKbT3eVEDZ3
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxVzcsJQjEQhtFgDde3i9xOUoMVBMG1LUwpU0pKSSkq97gwMHycxU/Wx+v+
3JVS1s99u71T+2vVedzaeHDn5ODg5M6DG8/fP1zOypXnwY4Hd04ODk7uPLjx
5Mrlolx57u14cOfk4ODkzoMbT65crsqV52LHgzsnBwcndx7ceHLlctOlvQGo
2TRj
           "]]]}, 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdj9sJw0AMBEVqyPt5ciVXQyowgXynBZXiUq4UlxKHmzUoBrOMx3vS+evz
fG/MzJf3l/251p7bnhUOOOR3PZs8bDdyTx8OOOQP9OVhu5NH+nDAIX+iLw/b
gzzThwMO+Qt9edgKyb0KrHtXefYe5XUPefaa5OEmz9xZXnt53qN4nls9zxk9
nxvqk5PnXvP83/z33YY1v7gQL9s=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw9ytsJQjEQBNDFGnzrR24nW4MVBMFvW0gpKSWlpBSVHFxYhjPM9nw/XruI
2L7/y3X3XLlfOW8rCw9O7ly5cePKnZMHF54cHAf91Y4HJ3eu3Lhx5c7JgwtP
Do6j/mLHg5M7V27cuHLn5MGFJwfHSX+248HJnSs3bly5c/LgwpPj/M8PqNk0
Yw==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzU0JgEAQQOHBBB48+rOrBjDCRhATiOBZIxjBCBvBCBvBCEYwgsqCMG9g
eHyXGTstw5yIiH33a5zcxWaxIxxgU2hv8AW7UtvDUuE/HODU4D68wh4+YbHa
HTzCOxzgGza1dg9v8AFfcNpoO3iFPXzC0v5+AMHNJ1o=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzk0NgDAMhuEGBRw48rMBApAwCQQFhIQzSEACEiYBCZOABCQgAcgOpG+T
5stz+Vo7LcOciIh998s4rYuZxTwbbQ+vsINT+Kq1D3iDe9jAt9UO8A6PcAcL
fBptD6+wg1M4VPgHFtiX6IevQnuDDRxy3Ifl9wM/iyda
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdkNwlAMRFGLCsLyQVjfIxTiGqgAIfFNCy4lpbgUSoHIdyJF1slkNP31
ebxXZtb/73LruXvdXd2c6n63dQMndjxjw4HzVvepPnb1cVMfm/qd/Q19nNjx
jA0Hzsa++tjVx019bPr/yv6a7zhxXNhXjkP5mX3l2JWf2FeOTfmR/YHvOHEc
2FeOQ/nIvnLsyvfsK8c2+A8B0jCH
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{1., 4.}, {3., 9.}}]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVkMkNwjAQAFe8uAmEEG4cKtkaqMBC4g0lbCkugRJcSkoBtMPDlqLRWLLH
m+7+vD0GItJ9vx99bdVZOVMLccV16ew3EDfcVs6Ah9qZG4hHPK7pNiUT+wlX
XP/dunyH4daW3cBcmXdlPOJxRxeXPV3mSvtybj3Q5b/0uOF2pIuHE/sL+nie
0z/TxeVCZ0Yftyn3Bc5PnC9c8Izr2Fl1nB/Rw/PQ+cYFl6t+AM3eMvE=
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxdy7sNgEAQA1GLCvhDep1cDZSAREwLLs2lcYiIWWllvWDKeR9XJ6m0f/e7
vX7b158rbDiwBvSw4cAa0cOGA2tCDxsOrBk9bDiwFvSw4cBa0cOGA2tDDxsO
3PYBItIj7w==
           "]]]}, 
        Directive[
         RGBColor[0, 0, 1], 
         Thickness[Large]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdsJQjEQhOHFCrw9eHejFrI1WEEQfLaFlJJStpSUopJ/DhyGL5Mh5fV5
vhdmVn7/P+e3i5nLmQ07tv3MUI+r+gPn6nFXf5yZ6vFQfyJX9NixnXlfPa7q
L5yrx139lffV46HeyTXvYMcNB05csRXuaY+79ji1x0P7G7lhjx03HDhxxXbn
nva4a49Tezy0f5Db+AJKhzCH
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJwtzt1NAkEUhuGJV6CCeymKuuvfNSWcEowVEBKusYRTAiVsCZQwJVACJVAC
mHk22bx5NvNlZ9jsfrc3pZTh+v63PY/R2rWOXB9aY9GafOLgfLLn/tl+rpy8
XtpzeVEeuc6ce7XnEwfnmz13vf29/3HygYPLoPzD9c59OPnMwfFuz3uut+7L
yasPO04ufOQ6be0/neM/Dq5cuPvyfdK65uQDB5dvncQFnIkyRg==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRNEREfdt7sQJdbgGSkAipgVKB+TIf6XV6Onb8z6uSJL9/l9/
u/Mbu9AJnMIZnMMFXMIVXId+YDXosFp0WB06rB4dlkGHNaDDGtFhTeiwZnRY
CzqsFR3W5l5njB+x
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdcNg0AQhOE1jgU4R86pDa4GV4CQ/OwWKIVSKIUSnHPW/ot0Gn03jM4l
m9U6EBH3O//Ub+k1G5rZQjOta3osuJjT10gsOLS+qpnP6HGM0wr31uNsSl/m
fSy4cPQBiQWH1pd4P6THMU6Fe+uxn+BPpP+N8Vudj/BLLean2g/xg/0A39n3
8Y29+cq+hy/su/jMvoNP7M1H9m18YN/Ce/ZNvGNv3kZf/sQ21w==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{5., 1.}, {1., 4.}}]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{5., 1.}, {6., 4.}}]}}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{3., 9.}, 0.], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 9.}, 0.], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{9., 9.}, 0.], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 7.}, 0.], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{10., 5.}, 0.], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.], 
           Directive[
            RGBColor[0, 0, 1], 
            Thickness[Large]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 3.}, 0.], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{12.760693309010094`, 
       270.24641347217647`}, {-111.4444134721763, 98.23930669098989}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weights", "Subsection"],

Cell[BoxData["3.414213562373095`"], "Output"],

Cell[BoxData["3.414213562373095`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Directed Sample Dataset - GSH-P Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
      11}, {{{1, 2}, {2, 1}, {8, 1}, {9, 1}, {2, 3}, {2, 5}, {5, 2}, {3, 4}, {
       4, 3}, {3, 5}, {5, 3}, {3, 6}, {6, 3}, {4, 7}, {4, 10}, {10, 4}, {5, 
       6}, {6, 5}, {9, 5}, {6, 7}, {7, 6}, {7, 9}, {9, 7}, {11, 8}, {9, 10}, {
       11, 9}, {10, 11}}, Null}, {EdgeShapeFunction -> {
         GraphElementData["ShortFilledArrow", "ArrowSize" -> 0.01]}, 
       EdgeWeight -> {1., 1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 3.605551275463989, 2.8284271247461903`, 
        2.8284271247461903`, 4.242640687119286, 4.242640687119286, 
        4.47213595499958, 4.47213595499958, 2.23606797749979, 4., 4., 
        1.4142135623730951`, 1.4142135623730951`, 2., 3., 3., 
        4.123105625617661, 4.123105625617661, 5., 5.0990195135927845`, 
        3.1622776601683795`, 6.324555320336759}, GraphHighlight -> {
         DirectedEdge[9, 5], 6, 5, 9, 
         DirectedEdge[5, 6]}, GraphHighlightStyle -> {"Thick"}, 
       GraphLayout -> {
        "EdgeLayout" -> {
          "DividedEdgeBundling", "CoulombConstant" -> 0, "VelocityDamping" -> 
           0.2, "SmoothEdge" -> False, "NewForce" -> False, "Connectivity" -> 
           True, "Compatibility" -> True, "Threshold" -> 0.1, "CoulombDecay" -> 
           1, "LaneWidth" -> 1}}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}, 
       VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Small}}]], Typeset`boxes, Typeset`boxes$s2d = 
    GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdy8sJgEAQBNHGk39NYY3EGAxB8GwKHVqHprLgoQaG5h2qnPdxNZLK+9/W
a/e6W90dNhxYHXrYcGD16GHDgTWghw0H1ogeNhxYE3rYcGDN6GHDgbWghw0H
1vr7AbiWJDE=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdy7sJgFAQRNHByL+2oJVMDZYgGNuCpU1pKi8Q78IynOAu+7kdlaTl+XfL
zS67ls309wUbFpwRPWxYcAb0sGHB6dHDhgWnQw8bFpwWPWxYcBr0sGHBqdHD
hvX5BpWlJDE=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzcsNgzAQRVGLCvgHCBA7lUwNVGAhsU5KcCmUQAkuhVIg8iZzRxo9ndV1
62fZMmOMu/+36XJJ+067w1Jon3AotW2lHWFfa5sGfVha9OHwQL9DH/Y9+gP6
sDzRh8OI/oQ+7Gf0X+jD1mp/4QjnDj34gP/2ApeEJ4Y=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxFy+mJwzAUhVGRCrLvi5xKXgkhFZiB+T1TgkpxCSlBJaQEl5IEHxOB+Djw
bvPzd/+dpJSa9/90eNMYeh0a/GyGFu648j+nmR0HT8c999meH5zm7jj4xoXz
uOf+Yr9wx8GZC9ezcstpqRzcnew5uHJ/tFu54+DMhetBueW0Vg7u9vYcXLnf
2W3ccXDmwnWr3HL69gVA+jLr
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVzdupQyEURVFJHXl48qrDGm4FEsh3WtilWIqlWEoSMvbHFWQxwInb4/X3
3JVSts/97u+c27+tdm2/bTy58+Dg4MGdJzde+Q+Xi+XKq+p4cufBwcGDO09u
vLhyuVquPE86Du7cOLLnkf3RZs8rey43y5XnwTsO7tw4sueR/d5mzyt7Lne7
b290zzXn
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdxdENgjAURuEbJvBRFOGWSTqCcYKGxGcZoaN0BEboCI7QUdDwQP5zkpMv
LJ/XuzOz8Pvv0RzFb1ALXGGEF9hc3WCGT+iwTWqGDuuoJmiwPNQI26Bm6LDe
1QQNlpsaYevVDB3Wq5qgne767jZ3
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdj9kNwzAMQ4nOkPQ+7E6iGTpBEKDfXUGjZBSPklHaIuQHbcAgnmlJVJ0/
r/cOQP3dv27nGaZr3bR1mtSFGt07qJPqSvePXFRfvA7klZoP7wPyIv/ufUGe
5N+6OeQi/+pzQW6ckxfPAXLIP3sukJtynTwnyCH/6LlBbsyZB98D5JC/971A
btwjx25Pcsgf+C5/iC9mWy/b
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw9zdkJAzEMRVGROrLIk6UO1ZAKzEC+04JKUSkqxaUkQVcZGB7H+OKxv5+v
g4iM7//b+h5We6xd91rDiR0HTuxYTrWze2zdY+0eS/dn3r/R4cSOAyd2LBfe
7x5b91i7x9K98v6Vezix4cATO3Y8cWDDiRUvLFgG5xv3cGLDgSd27HjiwIYT
K15Ytv9+AP3ANec=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1y00Ng1AQReFJFbCg/11AUrpGwkggVUCadF0kPAlIQAISngQkIAEJbTPn
TjK5+Ranfn2e752Z1b//b9zDY2+xWxObr7ErTnjBjjM2PKu/xE7q8ageJ/V4
UH+O7dXjTj129bhVf4qt1ONCPTb1eLvTH2NXnPCCHWdseFZ/iJ3U41E9Turx
oH4f26vHnXrs6nGrvoyt1ONCPTb1pX8BEsE1EQ==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRFGLiPu+IXFCHa6BEpCIaWFKB+TIf6XV6Onb6znvyBhjv//X
3+H87i6wttAG1ooOa0GHNaPDmtBhjeiwBnRYPTqsDh1Wiw6rQYdVo9MVXMIF
nMMZnMIJHLsXPogfsQ==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVzc1NQ0EMhVGLChIg/EPmpRKXgKgAIbGGElzKK4ESXAIlpBRAc7JgpNHV
WXzy8vbx8n4WEcvv/9v5bvLf9vXcVy5uTh63Oi7uq7lHLs47yysHx71+5z4X
NyePBx0X96X7XJyPllcOjif9hftc3Jw89jou7nP3uXgMPT9zcHFv535x8fHU
82bRc3Jv5n5y8crJ36ee45A/6wsy6w==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdxdEJwjAURuFLJ/DRam1vOklGECcIBZ/tCBklI3SEjOAIGaVKofCfA4cv
LJ/XuzOz8Pvv0TWKCVbovZphg/GmFmh3NcEKfVAzbDA+1AJtVBOs0Cc1wwbd
1SfMcIMNXoIa4QoL/EKbT3eVEDZ3
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxVzcsJQjEQhtFgDde3i9xOUoMVBMG1LUwpU0pKSSkq97gwMHycxU/Wx+v+
3JVS1s99u71T+2vVedzaeHDn5ODg5M6DG8/fP1zOypXnwY4Hd04ODk7uPLjx
5Mrlolx57u14cOfk4ODkzoMbT65crsqV52LHgzsnBwcndx7ceHLlctOlvQGo
2TRj
           "]]]}, 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdj9sJw0AMBEVqyPt5ciVXQyowgXynBZXiUq4UlxKHmzUoBrOMx3vS+evz
fG/MzJf3l/251p7bnhUOOOR3PZs8bDdyTx8OOOQP9OVhu5NH+nDAIX+iLw/b
gzzThwMO+Qt9edgKyb0KrHtXefYe5XUPefaa5OEmz9xZXnt53qN4nls9zxk9
nxvqk5PnXvP83/z33YY1v7gQL9s=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw9ytsJQjEQBNDFGnzrR24nW4MVBMFvW0gpKSWlpBSVHFxYhjPM9nw/XruI
2L7/y3X3XLlfOW8rCw9O7ly5cePKnZMHF54cHAf91Y4HJ3eu3Lhx5c7JgwtP
Do6j/mLHg5M7V27cuHLn5MGFJwfHSX+248HJnSs3bly5c/LgwpPj/M8PqNk0
Yw==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzU0JgEAQQOHBBB48+rOrBjDCRhATiOBZIxjBCBvBCBvBCEYwgsqCMG9g
eHyXGTstw5yIiH33a5zcxWaxIxxgU2hv8AW7UtvDUuE/HODU4D68wh4+YbHa
HTzCOxzgGza1dg9v8AFfcNpoO3iFPXzC0v5+AMHNJ1o=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzk0NgDAMhuEGBRw48rMBApAwCQQFhIQzSEACEiYBCZOABCQgAcgOpG+T
5stz+Vo7LcOciIh998s4rYuZxTwbbQ+vsINT+Kq1D3iDe9jAt9UO8A6PcAcL
fBptD6+wg1M4VPgHFtiX6IevQnuDDRxy3Ifl9wM/iyda
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdkNwlAMRFGLCsLyQVjfIxTiGqgAIfFNCy4lpbgUSoHIdyJF1slkNP31
ebxXZtb/73LruXvdXd2c6n63dQMndjxjw4HzVvepPnb1cVMfm/qd/Q19nNjx
jA0Hzsa++tjVx019bPr/yv6a7zhxXNhXjkP5mX3l2JWf2FeOTfmR/YHvOHEc
2FeOQ/nIvnLsyvfsK8c2+A8B0jCH
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{1., 4.}, {3., 9.}}]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVkMkNwjAQAFe8uAmEEG4cKtkaqMBC4g0lbCkugRJcSkoBtMPDlqLRWLLH
m+7+vD0GItJ9vx99bdVZOVMLccV16ew3EDfcVs6Ah9qZG4hHPK7pNiUT+wlX
XP/dunyH4daW3cBcmXdlPOJxRxeXPV3mSvtybj3Q5b/0uOF2pIuHE/sL+nie
0z/TxeVCZ0Yftyn3Bc5PnC9c8Izr2Fl1nB/Rw/PQ+cYFl6t+AM3eMvE=
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxdy7sNgEAQA1GLCvhDep1cDZSAREwLLs2lcYiIWWllvWDKeR9XJ6m0f/e7
vX7b158rbDiwBvSw4cAa0cOGA2tCDxsOrBk9bDiwFvSw4cBa0cOGA2tDDxsO
3PYBItIj7w==
           "]]]}, 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdsJQjEQhOHFCrw9eHejFrI1WEEQfLaFlJJStpSUopJ/DhyGL5Mh5fV5
vhdmVn7/P+e3i5nLmQ07tv3MUI+r+gPn6nFXf5yZ6vFQfyJX9NixnXlfPa7q
L5yrx139lffV46HeyTXvYMcNB05csRXuaY+79ji1x0P7G7lhjx03HDhxxXbn
nva4a49Tezy0f5Db+AJKhzCH
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJwtzt1NAkEUhuGJV6CCeymKuuvfNSWcEowVEBKusYRTAiVsCZQwJVACJVAC
mHk22bx5NvNlZ9jsfrc3pZTh+v63PY/R2rWOXB9aY9GafOLgfLLn/tl+rpy8
XtpzeVEeuc6ce7XnEwfnmz13vf29/3HygYPLoPzD9c59OPnMwfFuz3uut+7L
yasPO04ufOQ6be0/neM/Dq5cuPvyfdK65uQDB5dvncQFnIkyRg==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRNEREfdt7sQJdbgGSkAipgVKB+TIf6XV6Onb8z6uSJL9/l9/
u/Mbu9AJnMIZnMMFXMIVXId+YDXosFp0WB06rB4dlkGHNaDDGtFhTeiwZnRY
CzqsFR3W5l5njB+x
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdcNg0AQhOE1jgU4R86pDa4GV4CQ/OwWKIVSKIUSnHPW/ot0Gn03jM4l
m9U6EBH3O//Ub+k1G5rZQjOta3osuJjT10gsOLS+qpnP6HGM0wr31uNsSl/m
fSy4cPQBiQWH1pd4P6THMU6Fe+uxn+BPpP+N8Vudj/BLLean2g/xg/0A39n3
8Y29+cq+hy/su/jMvoNP7M1H9m18YN/Ce/ZNvGNv3kZf/sQ21w==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{5., 1.}, {1., 4.}}]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{5., 1.}, {6., 4.}}]}}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{3., 9.}, 0.], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 9.}, 0.], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{9., 9.}, 0.], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 7.}, 0.], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          DiskBox[{10., 5.}, 0.], "DynamicName", BoxID -> "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 3.}, 0.], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{12.760693309010094`, 
       270.24641347217647`}, {-111.4444134721763, 98.23930669098989}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["3.414213562373095`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Directed Sample Dataset - GSH-C Solution Graph", "Section"],

Cell[BoxData[
 GraphicsBox[
  NamespaceBox["NetworkGraphics",
   DynamicModuleBox[{Typeset`graph = HoldComplete[
     Graph[{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
      11}, {{{1, 2}, {2, 1}, {8, 1}, {9, 1}, {2, 3}, {2, 5}, {5, 2}, {3, 4}, {
       4, 3}, {3, 5}, {5, 3}, {3, 6}, {6, 3}, {4, 7}, {4, 10}, {10, 4}, {5, 
       6}, {6, 5}, {9, 5}, {6, 7}, {7, 6}, {7, 9}, {9, 7}, {11, 8}, {9, 10}, {
       11, 9}, {10, 11}}, Null}, {EdgeShapeFunction -> {
         GraphElementData["ShortFilledArrow", "ArrowSize" -> 0.01]}, 
       EdgeWeight -> {1., 1., 5.385164807134504, 5.830951894845301, 5., 
        3.605551275463989, 3.605551275463989, 2.8284271247461903`, 
        2.8284271247461903`, 4.242640687119286, 4.242640687119286, 
        4.47213595499958, 4.47213595499958, 2.23606797749979, 4., 4., 
        1.4142135623730951`, 1.4142135623730951`, 2., 3., 3., 
        4.123105625617661, 4.123105625617661, 5., 5.0990195135927845`, 
        3.1622776601683795`, 6.324555320336759}, GraphHighlight -> {
         DirectedEdge[9, 5], 
         DirectedEdge[6, 7], 6, 5, 9, 
         DirectedEdge[5, 6], 
         DirectedEdge[7, 9], 7}, GraphHighlightStyle -> {"Thick"}, 
       GraphLayout -> {
        "EdgeLayout" -> {
          "DividedEdgeBundling", "CoulombConstant" -> 0, "VelocityDamping" -> 
           0.2, "SmoothEdge" -> False, "NewForce" -> False, "Connectivity" -> 
           True, "Compatibility" -> True, "Threshold" -> 0.1, "CoulombDecay" -> 
           1, "LaneWidth" -> 1}}, 
       VertexCoordinates -> {{3, 9}, {4, 9}, {9, 9}, {11, 7}, {6, 6}, {7, 
        5}, {10, 5}, {1, 4}, {6, 4}, {11, 3}, {5, 1}}, 
       VertexLabels -> {"Name"}, VertexLabelStyle -> {9}, 
       VertexSize -> {Small}}]], Typeset`boxes, Typeset`boxes$s2d = 
    GraphicsGroupBox[{{
       Directive[
        Opacity[0.7], 
        Hue[0.6, 0.7, 0.5]], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdy8sJgEAQBNHGk39NYY3EGAxB8GwKHVqHprLgoQaG5h2qnPdxNZLK+9/W
a/e6W90dNhxYHXrYcGD16GHDgTWghw0H1ogeNhxYE3rYcGDN6GHDgbWghw0H
1vr7AbiWJDE=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdy7sJgFAQRNHByL+2oJVMDZYgGNuCpU1pKi8Q78IynOAu+7kdlaTl+XfL
zS67ls309wUbFpwRPWxYcAb0sGHB6dHDhgWnQw8bFpwWPWxYcBr0sGHBqdHD
hvX5BpWlJDE=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzcsNgzAQRVGLCvgHCBA7lUwNVGAhsU5KcCmUQAkuhVIg8iZzRxo9ndV1
62fZMmOMu/+36XJJ+067w1Jon3AotW2lHWFfa5sGfVha9OHwQL9DH/Y9+gP6
sDzRh8OI/oQ+7Gf0X+jD1mp/4QjnDj34gP/2ApeEJ4Y=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxFy+mJwzAUhVGRCrLvi5xKXgkhFZiB+T1TgkpxCSlBJaQEl5IEHxOB+Djw
bvPzd/+dpJSa9/90eNMYeh0a/GyGFu648j+nmR0HT8c999meH5zm7jj4xoXz
uOf+Yr9wx8GZC9ezcstpqRzcnew5uHJ/tFu54+DMhetBueW0Vg7u9vYcXLnf
2W3ccXDmwnWr3HL69gVA+jLr
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVzdupQyEURVFJHXl48qrDGm4FEsh3WtilWIqlWEoSMvbHFWQxwInb4/X3
3JVSts/97u+c27+tdm2/bTy58+Dg4MGdJzde+Q+Xi+XKq+p4cufBwcGDO09u
vLhyuVquPE86Du7cOLLnkf3RZs8rey43y5XnwTsO7tw4sueR/d5mzyt7Lne7
b290zzXn
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdxdENgjAURuEbJvBRFOGWSTqCcYKGxGcZoaN0BEboCI7QUdDwQP5zkpMv
LJ/XuzOz8Pvv0RzFb1ALXGGEF9hc3WCGT+iwTWqGDuuoJmiwPNQI26Bm6LDe
1QQNlpsaYevVDB3Wq5qgne767jZ3
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdj9kNwzAMQ4nOkPQ+7E6iGTpBEKDfXUGjZBSPklHaIuQHbcAgnmlJVJ0/
r/cOQP3dv27nGaZr3bR1mtSFGt07qJPqSvePXFRfvA7klZoP7wPyIv/ufUGe
5N+6OeQi/+pzQW6ckxfPAXLIP3sukJtynTwnyCH/6LlBbsyZB98D5JC/971A
btwjx25Pcsgf+C5/iC9mWy/b
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw9zdkJAzEMRVGROrLIk6UO1ZAKzEC+04JKUSkqxaUkQVcZGB7H+OKxv5+v
g4iM7//b+h5We6xd91rDiR0HTuxYTrWze2zdY+0eS/dn3r/R4cSOAyd2LBfe
7x5b91i7x9K98v6Vezix4cATO3Y8cWDDiRUvLFgG5xv3cGLDgSd27HjiwIYT
K15Ytv9+AP3ANec=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1y00Ng1AQReFJFbCg/11AUrpGwkggVUCadF0kPAlIQAISngQkIAEJbTPn
TjK5+Ranfn2e752Z1b//b9zDY2+xWxObr7ErTnjBjjM2PKu/xE7q8ageJ/V4
UH+O7dXjTj129bhVf4qt1ONCPTb1eLvTH2NXnPCCHWdseFZ/iJ3U41E9Turx
oH4f26vHnXrs6nGrvoyt1ONCPTb1pX8BEsE1EQ==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRFGLiPu+IXFCHa6BEpCIaWFKB+TIf6XV6Onb6znvyBhjv//X
3+H87i6wttAG1ooOa0GHNaPDmtBhjeiwBnRYPTqsDh1Wiw6rQYdVo9MVXMIF
nMMZnMIJHLsXPogfsQ==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVzc1NQ0EMhVGLChIg/EPmpRKXgKgAIbGGElzKK4ESXAIlpBRAc7JgpNHV
WXzy8vbx8n4WEcvv/9v5bvLf9vXcVy5uTh63Oi7uq7lHLs47yysHx71+5z4X
NyePBx0X96X7XJyPllcOjif9hftc3Jw89jou7nP3uXgMPT9zcHFv535x8fHU
82bRc3Jv5n5y8crJ36ee45A/6wsy6w==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdxdEJwjAURuFLJ/DRam1vOklGECcIBZ/tCBklI3SEjOAIGaVKofCfA4cv
LJ/XuzOz8Pvv0TWKCVbovZphg/GmFmh3NcEKfVAzbDA+1AJtVBOs0Cc1wwbd
1SfMcIMNXoIa4QoL/EKbT3eVEDZ3
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxVzcsJQjEQhtFgDde3i9xOUoMVBMG1LUwpU0pKSSkq97gwMHycxU/Wx+v+
3JVS1s99u71T+2vVedzaeHDn5ODg5M6DG8/fP1zOypXnwY4Hd04ODk7uPLjx
5Mrlolx57u14cOfk4ODkzoMbT65crsqV52LHgzsnBwcndx7ceHLlctOlvQGo
2TRj
           "]]]}, 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdj9sJw0AMBEVqyPt5ciVXQyowgXynBZXiUq4UlxKHmzUoBrOMx3vS+evz
fG/MzJf3l/251p7bnhUOOOR3PZs8bDdyTx8OOOQP9OVhu5NH+nDAIX+iLw/b
gzzThwMO+Qt9edgKyb0KrHtXefYe5XUPefaa5OEmz9xZXnt53qN4nls9zxk9
nxvqk5PnXvP83/z33YY1v7gQL9s=
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw9ytsJQjEQBNDFGnzrR24nW4MVBMFvW0gpKSWlpBSVHFxYhjPM9nw/XruI
2L7/y3X3XLlfOW8rCw9O7ly5cePKnZMHF54cHAf91Y4HJ3eu3Lhx5c7JgwtP
Do6j/mLHg5M7V27cuHLn5MGFJwfHSX+248HJnSs3bly5c/LgwpPj/M8PqNk0
Yw==
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxdzU0JgEAQQOHBBB48+rOrBjDCRhATiOBZIxjBCBvBCBvBCEYwgsqCMG9g
eHyXGTstw5yIiH33a5zcxWaxIxxgU2hv8AW7UtvDUuE/HODU4D68wh4+YbHa
HTzCOxzgGza1dg9v8AFfcNpoO3iFPXzC0v5+AMHNJ1o=
           "]]]}, 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdzk0NgDAMhuEGBRw48rMBApAwCQQFhIQzSEACEiYBCZOABCQgAcgOpG+T
5stz+Vo7LcOciIh998s4rYuZxTwbbQ+vsINT+Kq1D3iDe9jAt9UO8A6PcAcL
fBptD6+wg1M4VPgHFtiX6IevQnuDDRxy3Ifl9wM/iyda
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJw1zdkNwlAMRFGLCsLyQVjfIxTiGqgAIfFNCy4lpbgUSoHIdyJF1slkNP31
ebxXZtb/73LruXvdXd2c6n63dQMndjxjw4HzVvepPnb1cVMfm/qd/Q19nNjx
jA0Hzsa++tjVx019bPr/yv6a7zhxXNhXjkP5mX3l2JWf2FeOTfmR/YHvOHEc
2FeOQ/nIvnLsyvfsK8c2+A8B0jCH
           "]]]}, 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{1., 4.}, {3., 9.}}]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxVkMkNwjAQAFe8uAmEEG4cKtkaqMBC4g0lbCkugRJcSkoBtMPDlqLRWLLH
m+7+vD0GItJ9vx99bdVZOVMLccV16ew3EDfcVs6Ah9qZG4hHPK7pNiUT+wlX
XP/dunyH4daW3cBcmXdlPOJxRxeXPV3mSvtybj3Q5b/0uOF2pIuHE/sL+nie
0z/TxeVCZ0Yftyn3Bc5PnC9c8Izr2Fl1nB/Rw/PQ+cYFl6t+AM3eMvE=
          "]]]}, 
       StyleBox[{
         Arrowheads[{{0.01, 0.8, {
             GraphicsBox[
              
              FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
               0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
               0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
             0.533327810340424}}}], 
         ArrowBox[
          BezierCurveBox[CompressedData["
1:eJxdy7sNgEAQA1GLCvhDep1cDZSAREwLLs2lcYiIWWllvWDKeR9XJ6m0f/e7
vX7b158rbDiwBvSw4cAa0cOGA2tCDxsOrBk9bDiwFvSw4cBa0cOGA2tDDxsO
3PYBItIj7w==
           "]]]}, 
        Directive[
         Thickness[Large], 
         Hue[1, 1, 0.7], 
         Opacity[1]], StripOnInput -> False], {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdsJQjEQhOHFCrw9eHejFrI1WEEQfLaFlJJStpSUopJ/DhyGL5Mh5fV5
vhdmVn7/P+e3i5nLmQ07tv3MUI+r+gPn6nFXf5yZ6vFQfyJX9NixnXlfPa7q
L5yrx139lffV46HeyTXvYMcNB05csRXuaY+79ji1x0P7G7lhjx03HDhxxXbn
nva4a49Tezy0f5Db+AJKhzCH
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJwtzt1NAkEUhuGJV6CCeymKuuvfNSWcEowVEBKusYRTAiVsCZQwJVACJVAC
mHk22bx5NvNlZ9jsfrc3pZTh+v63PY/R2rWOXB9aY9GafOLgfLLn/tl+rpy8
XtpzeVEeuc6ce7XnEwfnmz13vf29/3HygYPLoPzD9c59OPnMwfFuz3uut+7L
yasPO04ufOQ6be0/neM/Dq5cuPvyfdK65uQDB5dvncQFnIkyRg==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJxdybkNgDAQRNEREfdt7sQJdbgGSkAipgVKB+TIf6XV6Onb8z6uSJL9/l9/
u/Mbu9AJnMIZnMMFXMIVXId+YDXosFp0WB06rB4dlkGHNaDDGtFhTeiwZnRY
CzqsFR3W5l5njB+x
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[
         BezierCurveBox[CompressedData["
1:eJw1zdcNg0AQhOE1jgU4R86pDa4GV4CQ/OwWKIVSKIUSnHPW/ot0Gn03jM4l
m9U6EBH3O//Ub+k1G5rZQjOta3osuJjT10gsOLS+qpnP6HGM0wr31uNsSl/m
fSy4cPQBiQWH1pd4P6THMU6Fe+uxn+BPpP+N8Vudj/BLLean2g/xg/0A39n3
8Y29+cq+hy/su/jMvoNP7M1H9m18YN/Ce/ZNvGNv3kZf/sQ21w==
          "]]]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{5., 1.}, {1., 4.}}]}, {
        Arrowheads[{{0.01, 0.8, {
            GraphicsBox[
             
             FilledCurveBox[{{{0, 2, 0}, {0, 1, 0}, {0, 1, 
              0}}}, {{{-0.6666528591843921, -0.3333333333333333}, \
{-0.533327810340424, 6.903741136987662*^-6}, {-0.6666528591843921, 
              0.3333333333333333}, {0., 6.903741136987662*^-6}}}]], 
            0.533327810340424}}}], 
        ArrowBox[{{5., 1.}, {6., 4.}}]}}, {
       Directive[
        Hue[0.6, 0.2, 0.8], 
        EdgeForm[
         Directive[
          GrayLevel[0], 
          Opacity[0.7]]]], 
       TagBox[{
         TagBox[
          DiskBox[{3., 9.}, 0.], "DynamicName", BoxID -> "VertexID$1"], 
         InsetBox[
          FormBox[
           StyleBox["1", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$1", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$1"], 
       TagBox[{
         TagBox[
          DiskBox[{4., 9.}, 0.], "DynamicName", BoxID -> "VertexID$2"], 
         InsetBox[
          FormBox[
           StyleBox["2", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$2", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$2"], 
       TagBox[{
         TagBox[
          DiskBox[{9., 9.}, 0.], "DynamicName", BoxID -> "VertexID$3"], 
         InsetBox[
          FormBox[
           StyleBox["3", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$3", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$3"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 7.}, 0.], "DynamicName", BoxID -> "VertexID$4"], 
         InsetBox[
          FormBox[
           StyleBox["4", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$4", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$4"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 6.}, 0.], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$5"], 
         InsetBox[
          FormBox[
           StyleBox["5", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$5", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$5"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{7., 5.}, 0.], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$6"], 
         InsetBox[
          FormBox[
           StyleBox["6", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$6", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$6"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{10., 5.}, 0.], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$7"], 
         InsetBox[
          FormBox[
           StyleBox["7", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$7", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$7"], 
       TagBox[{
         TagBox[
          DiskBox[{1., 4.}, 0.], "DynamicName", BoxID -> "VertexID$8"], 
         InsetBox[
          FormBox[
           StyleBox["8", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$8", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$8"], 
       TagBox[{
         TagBox[
          StyleBox[
           DiskBox[{6., 4.}, 0.], 
           Directive[
            RGBColor[0.85, 0.5, 0.5], 
            EdgeForm[{
              Thickness[Large], 
              Hue[1, 1, 0.7], 
              Opacity[1]}]], StripOnInput -> False], "DynamicName", BoxID -> 
          "VertexID$9"], 
         InsetBox[
          FormBox[
           StyleBox["9", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$9", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$9"], 
       TagBox[{
         TagBox[
          DiskBox[{11., 3.}, 0.], "DynamicName", BoxID -> "VertexID$10"], 
         InsetBox[
          FormBox[
           StyleBox["10", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$10", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$10"], 
       TagBox[{
         TagBox[
          DiskBox[{5., 1.}, 0.], "DynamicName", BoxID -> "VertexID$11"], 
         InsetBox[
          FormBox[
           StyleBox["11", 9, StripOnInput -> False], TraditionalForm], 
          Offset[{2, 2}, 
           DynamicLocation["VertexID$11", Automatic, {Right, Top}]], 
          ImageScaled[{0, 0}], BaseStyle -> "Graphics"]}, "DynamicName", 
        BoxID -> "VertexLabelID$11"]}}], $CellContext`flag}, 
    TagBox[
     DynamicBox[GraphComputation`NetworkGraphicsBox[
      3, Typeset`graph, Typeset`boxes, $CellContext`flag], {
      CachedValue :> Typeset`boxes, SingleEvaluation -> True, 
       SynchronousUpdating -> False, TrackedSymbols :> {$CellContext`flag}},
      ImageSizeCache->{{12.760693309010094`, 
       270.24641347217647`}, {-111.44441347217632`, 98.23930669098992}}],
     MouseAppearanceTag["NetworkGraphics"]],
    AllowKernelInitialization->False,
    UnsavedVariables:>{$CellContext`flag}]],
  DefaultBaseStyle->{
   "NetworkGraphics", FrontEnd`GraphicsHighlightColor -> Hue[0.8, 1., 0.6]},
  FrameTicks->None,
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->15]], "Output"],

Cell[CellGroupData[{

Cell["Graph Weight", "Subsection"],

Cell[BoxData["10.537319187990756`"], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1319, 744},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
CellContext->Notebook,
TrackCellChangeTimes->False,
Magnification:>0.75 Inherited,
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 72, 0, 72, "Title"],
Cell[1561, 37, 412, 7, 77, "Subsection"],
Cell[CellGroupData[{
Cell[1998, 48, 55, 0, 41, "Section"],
Cell[2056, 50, 20436, 408, 233, "Output"],
Cell[CellGroupData[{
Cell[22517, 462, 34, 0, 35, "Subsection"],
Cell[22554, 464, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[22648, 470, 64, 0, 51, "Section"],
Cell[22715, 472, 29334, 692, 256, "Output"],
Cell[CellGroupData[{
Cell[52074, 1168, 35, 0, 35, "Subsection"],
Cell[52112, 1170, 45, 0, 24, "Output"],
Cell[52160, 1172, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[52254, 1178, 65, 0, 51, "Section"],
Cell[52322, 1180, 25142, 576, 233, "Output"],
Cell[CellGroupData[{
Cell[77489, 1760, 35, 0, 35, "Subsection"],
Cell[77527, 1762, 45, 0, 24, "Output"],
Cell[77575, 1764, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[77669, 1770, 65, 0, 51, "Section"],
Cell[77737, 1772, 24934, 576, 233, "Output"],
Cell[CellGroupData[{
Cell[102696, 2352, 34, 0, 35, "Subsection"],
Cell[102733, 2354, 45, 0, 24, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[102827, 2360, 65, 0, 51, "Section"],
Cell[102895, 2362, 25512, 595, 233, "Output"],
Cell[CellGroupData[{
Cell[128432, 2961, 34, 0, 35, "Subsection"],
Cell[128469, 2963, 46, 0, 24, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature IxT8OQ4L1hCpNC10CNQV2bXZ *)
