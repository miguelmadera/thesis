(* ::Package:: *)

BeginPackage["ThesisMLCPOperations`"]

(*USAGE STATEMENTS*)
removeRedundanciesInMLCPSolution::usage = "removeRedundanciesInMLCPSolution[base_graph_vertex_list, base_graph_edge_list, base_graph_edgeweight_list, base_graph_polygons, mlcp_solution_vertex_list, mlcp_solution_edge_list] locates redundant vertices and edges in an MLCP solution.
Returns a list of {{new_mlcp_vertex_list}, {new_mlcp_edge_list}}."

greedySearchTreeSolution::usage = "greedySearchTreeSolution[starting_vertex, base_graph_vertex_list, base_graph_edge_list, base_graph_polygon_list, base_graph] obtains a solution to MLCP using a greedy search heuristic.
This strategy outputs a tree structure."

greedyUndirectedSearchPathSolution::usage = "greedyUndirectedSearchPathSolution[starting_vertex, base_graph_vertex_list, base_graph_edge_list,  base_graph_edgeweight_list,base_graph_polygon_list, base_graph] obtains a solution to MLCP using a greedy search heuristic for an undirected graph.
This strategy outputs a path structure."

greedyDirectedSearchPathSolution::usage = "greedyDirectedSearchPathSolution[starting_vertex, base_graph_vertex_list, base_graph_polygon_list, base_graph]obtains a solution to MLCP using a greedy search heuristic for a directed graph.
This strategy outputs a path structure."

visualizeMLCPSolutions::usage = "visualizeMLCPSolutions[vertex_list, solution_list] creates a dynamic visualization of MLCP solutions starting at particular vertices."

isMLCPInstance::usage = "isMLCPInstance[base_graph_vertex_list, base_graph_polygon_list, candidate_vertex_list] Returns True if candidate_vertex_list contains all of the necessary vertices to make up an MLCP solution."

sharedVertexSolution::usage = "sharedVertexSolution[base_graph_vertex_list, base_graph_polygon_list, base_graph] returns the vertices and edges of the mlcp solution obtained by implementing the shared vertex strategy."

minimumCostArborescence::usage = "minimumCostArborescence[starting_vertex, vertex_list, directed_edge_list, edgeweight_list, base_graph] returns the edges that make up the minimum cost arborescence."

closeUndirectedCycleSolution::usage = "closeUndirectedCycleSolution[base_graph_vertex_list, base_graph_edge_list, base_graph_edgeweight_list, base_graph, path_edge_list] transforms an undirected path solution into a cycle, simply closing the 2 endpoints of the path.
Returns a closed loop of edges if a cycle is possible. If not, returns Null."

closeDirectedCycleSolution::usage = "closeDirectedCycleSolution[base_graph_vertex_list, base_graph_edge_list, base_graph_edgeweight_list, base_graph, path_edge_list] transforms a directed path solution into a cycle, simply closing the 2 endpoints of the path.
Returns a closed loop of edges if a cycle is possible. If not, returns Null."

gravityCenterPolygonOrdering::usage = "gravityCenterPolygonOrdering[starting_vertex, base_graph_vertex_list, base_graph_polygon_list, base_graph, mst_graph_vertex_list, mst_graph] returns an MLCP solution for the given graph using the graph's polygon centers."

Begin["`Private`"]
SetDirectory @ NotebookDirectory[];
Needs["ThesisAuxiliaryFunctions`", "auxiliary_functions.m"];
Needs["ThesisGraphOperations`", "graph_operations.m"];


removeRedundanciesInMLCPSolution[baseGraphVertexList_, baseGraphEdgeList_, baseGraphEdgeweightList_, baseGraphPolygonList_, mlcpSolutionVertexList_, mlcpSolutionEdgeList_]:=
Module[
{
	(* Undirected edges require ordering. *)
	mlcpVertices = Flatten[Position[baseGraphVertexList, #]& /@ mlcpSolutionVertexList],
	mlcpEdges = If[isDirected @ mlcpSolutionEdgeList, mlcpSolutionEdgeList, setUndirectedEdgeOrdering @ mlcpSolutionEdgeList],
	baseEdges = If[isDirected @ baseGraphEdgeList, baseGraphEdgeList, setUndirectedEdgeOrdering @ baseGraphEdgeList],
	verticesRemoved = True,
	leafVerticesLength = 0,
	i = 0,
	trimmedVertices = {},
	leafVertices,
	mlcpVerticesRemaining,
	mlcpEdgesRemaining,
	mlcpEdgeWeightsRemaining
},
	(* Loop Control, exits once there are no more leaf vertices that can be removed. *)
	While[True,
		If[verticesRemoved == False, Break[], ##&[]];
		verticesRemoved = False;

		(* All leaf vertices from MLCP are sorted by length (weight) of adjoining edge, largest to smallest. *)
		leafVertices = getOrderedSubgraphLeafVertices[baseEdges, baseGraphEdgeweightList, mlcpVertices, mlcpEdges];
	
		(* For each of the leaf, vertices, loop until one is found. *)
		leafVerticesLength = Length @ leafVertices;
		For[i = 1, i <= leafVerticesLength, i++,
			{mlcpVerticesRemaining, mlcpEdgesRemaining} = removeVertex[leafVertices[[i]], mlcpVertices, mlcpEdges];

			(* Confirming that remaining graph is still an MLCP Solution. *)
			If[isMLCPInstance[baseGraphVertexList, baseGraphPolygonList, mlcpVerticesRemaining],
				verticesRemoved = True;
				trimmedVertices = Append[trimmedVertices, leafVertices[[i]]];
				mlcpVertices = mlcpVerticesRemaining;
				mlcpEdges = mlcpEdgesRemaining;
				
				(* Break out of for loop *)
				Break[];,
				
				(*If node is not redundant, do nothing*)
				##&[]
			]
		]
	];
	
	(* Vertices are returned as coordinates. *)
	If[ isDirected @ mlcpSolutionEdgeList,
		mlcpVertices = baseGraphVertexList[[#]]& /@ Union[mlcpEdges /. Rule -> Sequence];,
		mlcpVertices = baseGraphVertexList[[#]]& /@ Union[mlcpEdges /. UndirectedEdge -> Sequence];
	];
	{mlcpVertices, mlcpEdges}
];


greedySearchTreeSolution[startingVertex_, baseGraphVertexList_, baseGraphEdgeList_, baseGraphPolygonList_, baseGraph_]:=
Module[
{
	(*Treat edges as a list of lists as well*)
	availableEdges = {#}& /@ baseGraphEdgeList,
	availableVertices = baseGraphVertexList,
	availablePolygons = baseGraphPolygonList,

	(*Initial node is in numeric form, not in coordinate form*)
	currentVertices = {Part[baseGraphVertexList, startingVertex]},
	adjacentPolygons = {},
	currentFrontiers = {},
	subgraphVertices = {},
	subgraphEdges = {},
	subgraphPolygons = {},
	frontierVertices = {},
	pathEdges
},

	(*Continue until all polygons have been considered*)
	While[True,

		(*Clear variables*) 
		adjacentPolygons = {};
		currentFrontiers = {};

		(*Modify current and available vertices*)
		{availableVertices, subgraphVertices} = moveElementSet[availableVertices, subgraphVertices, currentVertices];
		
		(*Gets Adjacent Polygons*)
		adjacentPolygons = getAdjacentPolygonsToVertices[currentVertices, availablePolygons];

		(*Modify current and available Polygons*)
		{availablePolygons, subgraphPolygons} = moveElementSet[availablePolygons, subgraphPolygons, adjacentPolygons];

		(*Exits loop when all polygons have been accounted for, does nothing otherwise.*)
		If[Length[availablePolygons] == 0, Break[], ##&[]];

		(*Get all available vertices the available and checked polygons.*)
		frontierVertices = getFrontier[subgraphVertices, subgraphPolygons, availablePolygons];

		(*Function returns a list in the form of {edges, vertices}.
		These vertices should be removed from availableVertices to checkedVertices. 
		Should also be removed from frontierVertices and set to currentVertices*)
		{pathEdges, currentVertices} = findShortestPathToVertexSet[subgraphVertices, frontierVertices, baseGraphVertexList, baseGraph];

		(*Edges and Vertices should be moved from available lists to subgraph lists.*)
		{availableEdges, subgraphEdges} = moveElementSet[availableEdges, subgraphEdges, pathEdges];
	];

	(*Data is output so it can be used directly in Graph creation*)
	subgraphVertices = Flatten[Position[baseGraphVertexList, #]& /@ subgraphVertices];
	subgraphEdges = Flatten @ subgraphEdges;
	{subgraphVertices, subgraphEdges}
];


visualizeMLCPSolutions[vertexList_, graphList_] := 
Manipulate[
	graphList[[startingVertex]],
	{startingVertex, 1, Length @ vertexList, 1}
];


isMLCPInstance[baseGraphVertexList_, baseGraphPolygonList_, candidateVertexList_]:=
Module[
	{checkNodes},
	checkNodes = baseGraphVertexList[[#]]& /@ candidateVertexList;
	If[Length @ getAdjacentPolygonsToVertices[checkNodes, baseGraphPolygonList] == Length @ baseGraphPolygonList, True, False]
];


sharedVertexSolution[baseGraphVertices_, baseGraphPolygons_, baseGraph_]:=
Module[
{
	uncheckedPolygons = baseGraphPolygons,
	checkedPolygons = {},
	sharedVertexList = {},
	treeEdges = {},
	treeVertices = {},
	pathEdges = {},
	pathVertices = {},
	currentPolygon,
	currentVertex,
	vertexFrequencyList
},

	While[Length @ uncheckedPolygons > 0,
		currentPolygon = uncheckedPolygons[[1]];

		(* Vertex Frequency is obtained and sorted. *)
		vertexFrequencyList = {Length[Position[Flatten[uncheckedPolygons, 1], #]], #}& /@ currentPolygon;
		vertexFrequencyList = Sort[vertexFrequencyList, #1[[1]] > #2[[1]]&];

		(* If it intersects no uncheckedpolygons, it should be checked against EVERY polygon. *)
		If[vertexFrequencyList[[1]][[1]] == 1,
			vertexFrequencyList = {Length[Position[Flatten[baseGraphPolygons, 1], #]], #}& /@ currentPolygon;
			vertexFrequencyList = Sort[vertexFrequencyList, #1[[1]] > #2[[1]]&];
		];

		(* Most frequent vertex is added to list of shared vertices. *)
		currentVertex = vertexFrequencyList[[1]][[2]];
		sharedVertexList = Append[sharedVertexList, currentVertex];

		(* Adjacent polygons are removed from unchecked polygon list. *)
		uncheckedPolygons = Complement[uncheckedPolygons, getAdjacentPolygonsToVertex[currentVertex, uncheckedPolygons]];
	];

	(* First Element to start tree formation. *)
	{sharedVertexList, treeVertices} = moveElementSet[sharedVertexList, treeVertices, {sharedVertexList[[1]]}];

	While[Length @ sharedVertexList > 0,

		(* Find shortest path that extends the tree. *)
		{pathEdges, pathVertices} = findShortestPathToVertexSet[treeVertices, sharedVertexList, baseGraphVertices, baseGraph];

		(* Add the path to the tree. *)
		{sharedVertexList, treeVertices} = moveElementSet[sharedVertexList, treeVertices, pathVertices];
		treeEdges = treeEdges \[Union] pathEdges;
	];
	{treeVertices, treeEdges}
]



minimumCostArborescence[startingVertex_, vertexList_, directedEdgeList_, edgeweightList_, baseGraph_]:= 
Module[
{
	selectedVertex = Flatten[Position[vertexList, startingVertex]][[1]],
	uncheckedVertices = vertexList,
	uncheckedEdges = directedEdgeList,
	treeEdges = {},
	treeVertices = {},		
	selectedEdge,
	incomingEdges,
	outgoingEdges,
	currentEdgeWeights
},

	While[True,

		(* Add vertex to tree and check first exit condition. *)
		{uncheckedVertices, treeVertices} = moveElementSet[uncheckedVertices, treeVertices, {vertexList[[selectedVertex]]}];
		If[Length @ uncheckedVertices == 0, Break[]; ];

		(* Remove all incoming edges from tree. *)
		incomingEdges = Flatten @ Cases[{#}& /@ uncheckedEdges, {_->selectedVertex}];
		uncheckedEdges = Complement[uncheckedEdges, incomingEdges];

		(* Get outgoing edges from any vertex in the tree and check second exit condition. *)
		outgoingEdges = Flatten[Cases[{#}& /@ uncheckedEdges, {Position[vertexList, #][[1]][[1]]->_}]& /@ treeVertices];
		If[Length @ outgoingEdges == 0, Break[]; ];

		(* Select the edge with the lowest weight to add to tree. *)
		currentEdgeWeights = Flatten[edgeweightList[[#]]& /@ Flatten[Position[directedEdgeList, #]& /@ outgoingEdges]];
		selectedEdge = outgoingEdges[[Position[currentEdgeWeights, Min @ currentEdgeWeights][[1]]]][[1]];
		{uncheckedEdges, treeEdges} = moveElementSet[uncheckedEdges, treeEdges, {selectedEdge}];

		(* Set the next selected vertex. *)
		selectedVertex = selectedEdge[[2]];
	];
	treeEdges
];


greedyUndirectedSearchPathSolution[startingVertex_, baseGraphVertexList_, baseGraphEdgeList_, baseGraphEdgeweightList_, baseGraphPolygonList_, baseGraph_]:=
Module[
	{
		uncheckedEdges = setUndirectedEdgeOrdering @ baseGraphEdgeList,
		mutableEdges = setUndirectedEdgeOrdering @ baseGraphEdgeList,
		checkedPolygons = {},		
		uncheckedPolygons = baseGraphPolygonList,
		mutableEdgeWeights = baseGraphEdgeweightList,
		mutableGraph = baseGraph,
		checkedEdges,
		vertexPosition,
		incidentEdges,
		incidentWeights,
		startingEdge,
		endpoint1,
		endpoint2,	
		frontier,
		intersection,
		dropPosition,
		path
	},
	
	(* Set shortest incident edge as position of first  endpoints. Could be inefficient. *)
	vertexPosition = Flatten[Position[baseGraphVertexList, startingVertex]][[1]];
	incidentEdges = Cases[baseGraphEdgeList, vertexPosition<->_|_<->vertexPosition];
	incidentWeights = Flatten[baseGraphEdgeweightList[[Flatten[Position[baseGraphEdgeList, #]& /@ incidentEdges]]]];
	startingEdge = incidentEdges[[Position[incidentWeights, Min @ incidentWeights][[1]][[1]]]];
	checkedEdges = setUndirectedEdgeOrdering @ {startingEdge};
	endpoint1 = baseGraphVertexList[[startingEdge[[1]]]];
	endpoint2 = baseGraphVertexList[[startingEdge[[2]]]];

	(* Adjacent polygons are checked. *)
	While[Length @ uncheckedPolygons > 0,		
		{uncheckedPolygons, checkedPolygons} = moveElementSet[uncheckedPolygons, checkedPolygons, getAdjacentPolygonsToVertices[{endpoint1, endpoint2}, uncheckedPolygons]];

		(* Exit condition. *)
		If[Length @ uncheckedPolygons == 0, Break[]; ];

		(* Get frontier and shortest path to it. *)
		frontier = getFrontier[{endpoint1, endpoint2}, checkedPolygons, uncheckedPolygons];
		path = findShortestPathToVertexSet[{endpoint1, endpoint2}, frontier, baseGraphVertexList, mutableGraph];
		intersection = setUndirectedEdgeOrdering[Flatten @ path[[1]]] \[Intersection] setUndirectedEdgeOrdering[checkedEdges];

		(* Crosses existing path, new path must be obtained by repeatedly deleting edges from base graph. *)
		While[Length @ intersection != 0,
	
			(* Modify graph by removing intersected edges. *)
			dropPosition = Position[mutableEdges, intersection[[1]]][[1]][[1]];
			mutableEdges = Drop[mutableEdges, {dropPosition}];
			mutableEdgeWeights = Drop[mutableEdgeWeights, {dropPosition}];
			mutableGraph = createUndirectedGraph[baseGraphVertexList, mutableEdges, mutableEdgeWeights];
			
			(* Re-obtain frontier and path to it. *)
			frontier = getFrontier[{endpoint1, endpoint2}, checkedPolygons, uncheckedPolygons];
			path = findShortestPathToVertexSet[{endpoint1, endpoint2}, frontier, baseGraphVertexList, mutableGraph];
			intersection = setUndirectedEdgeOrdering[Flatten @ path[[1]]] \[Intersection] setUndirectedEdgeOrdering[checkedEdges];
		];

		(* Add new edges to graph. *)
		{uncheckedEdges, checkedEdges} = moveElementSet[uncheckedEdges, checkedEdges, path[[1]][[1]]];

		(* Update the endpoints. *)
		If[endpoint1 == baseGraphVertexList[[Flatten[path][[1]][[1]]]],
			endpoint1 = baseGraphVertexList[[Flatten[path, 2][[1]][[-1]]]];,
			endpoint2 = baseGraphVertexList[[Flatten[path, 2][[1]][[-1]]]];
		];
	];
	checkedEdges
];


greedyDirectedSearchPathSolution[startingVertex_, baseGraphVertexList_, baseGraphPolygonList_, baseGraph_]:=
Module[
{
	endpoint = startingVertex,
    uncheckedPolygons = Complement[baseGraphPolygonList, getAdjacentPolygonsToVertex[startingVertex, baseGraphPolygonList]],
    checkedPolygons = getAdjacentPolygonsToVertex[startingVertex, baseGraphPolygonList],
    
    possibleVertexList = baseGraphVertexList,
    visitedVertexList = {startingVertex},
    visitedEdgeList = {},
    
    mutableGraph = baseGraph,
    mutableVertexList,
    
    frontier,
    path,
    pathVertexList,
    pathEdgeList
},
	
	While[Length@uncheckedPolygons > 0,
    
		(* Get frontier and shortest path to it. *)
		frontier = getFrontier[{endpoint}, checkedPolygons, uncheckedPolygons];
        path = Quiet @ findShortestPathToVertexSet[{endpoint}, frontier, possibleVertexList, mutableGraph];
    
        (* Check if valid paths were found. *)
        If[Length @ path > 0,
		    {pathEdgeList, pathVertexList} = path;
		    visitedVertexList = Join[visitedVertexList, pathVertexList];
		    visitedEdgeList = Join[visitedEdgeList, pathEdgeList];,
     
		    (* Valid paths were not found, delete dead-end vertex and backtrack. *)
		    possibleVertexList = Complement[possibleVertexList, endpoint];
		    mutableGraph = VertexDelete[mutableGraph, Position[baseGraphVertexList, endpoint][[1]][[1]]];
         ];
    
        (* Reset endpoint and re-check polygons. *)
        endpoint = baseGraphVertexList[[Last @ Last @ Flatten @ pathEdgeList]];
        checkedPolygons = getAdjacentPolygonsToVertices[visitedVertexList, baseGraphPolygonList];
        uncheckedPolygons = Complement[baseGraphPolygonList, checkedPolygons];
	];
	Flatten@visitedEdgeList
];



closeUndirectedCycleSolution[baseGraphVertexList_, baseGraphEdgeList_, baseGraphEdgeWeightList_, baseGraph_, pathEdgeList_]:=
Module[
	{
		mutableEdges = setUndirectedEdgeOrdering @ baseGraphEdgeList,
		pathEdges = setUndirectedEdgeOrdering @ pathEdgeList,
		mutableGraph = baseGraph,
		mutableEdgeWeights = baseGraphEdgeWeightList,
		mutableVertices,
		endpoint1,
		endpoint2,
		path
	},

	(* Get the endpoint coordinates. *)
	{endpoint1, endpoint2} = Cases[Tally[pathEdgeList /. UndirectedEdge -> Sequence], {x_, 1} :> x];
	endpoint1 = baseGraphVertexList[[endpoint1]];
	endpoint2 = baseGraphVertexList[[endpoint2]];

	(* Create the modified graph (to make sure cycle does not repeat edges). *)
	mutableEdges = Complement[mutableEdges, pathEdges];
	mutableEdgeWeights = baseGraphEdgeWeightList[[Flatten[Position[setUndirectedEdgeOrdering[baseGraphEdgeList], #]& /@ mutableEdges]]];
	mutableGraph = Graph[Range @ Length @ baseGraphVertexList, mutableEdges, EdgeWeight -> mutableEdgeWeights, VertexCoordinates -> baseGraphVertexList];

	(* Path is generated and returned (if it exists) along with the path edges. *)
	path = Quiet @ findShortestPathToVertexSet[{endpoint1}, {endpoint2}, baseGraphVertexList, mutableGraph];
	If[ \[Not]ListQ[path] || Length @ path[[1]] == 0, Return[]; ];
	Join[pathEdgeList, Flatten @ path[[1]]]
];


closeDirectedCycleSolution[baseGraphVertexList_, baseGraphEdgeList_, baseGraphEdgeWeightList_, baseGraph_, pathEdgeList_]:=
Module[
	{
		mutableEdges = baseGraphEdgeList,
		pathEdges = pathEdgeList,
		mutableGraph = baseGraph,
		mutableEdgeWeights = baseGraphEdgeWeightList,
		mutableVertices,
		endpoint1,
		endpoint2,
		path
	},

	(* Get the endpoint coordinates. *)
	endpoint1 = baseGraphVertexList[[pathEdgeList[[-1]][[2]]]];
	endpoint2 = baseGraphVertexList[[pathEdgeList[[1]][[1]]]];

	(* Create the modified graph (to make sure cycle does not repeat edges). *)
	mutableEdges = Complement[mutableEdges, pathEdges];
	mutableEdgeWeights = baseGraphEdgeWeightList[[Flatten[Position[baseGraphEdgeList, #]& /@ mutableEdges]]];
	mutableGraph = Graph[Range @ Length @ baseGraphVertexList, mutableEdges, EdgeWeight -> mutableEdgeWeights, VertexCoordinates -> baseGraphVertexList];

	path = Quiet @ findShortestPathToVertexSet[{endpoint1}, {endpoint2}, baseGraphVertexList, mutableGraph];
	If[ \[Not]ListQ[path] || Length @ path[[1]] == 0, Return[];];
	Join[pathEdgeList, Flatten @ path[[1]]]
];


gravityCenterPolygonOrdering[startingVertex_, baseGraphVertexList_, baseGraphPolygonList_, baseGraph_, mstGraphVertexList_, mstGraph_]:=
Module[

	(* CenterStruct allows centers and polygons to be paired: {CENTER,POLYGON} *)
	{
		polygonCenterStruct = Partition[Riffle[mstGraphVertexList, baseGraphPolygonList], 2],
		uncheckedPolygons = Complement[baseGraphPolygonList, getAdjacentPolygonsToVertex[startingVertex, baseGraphPolygonList]],
		uncheckedVertices = Complement[baseGraphVertexList, {startingVertex}],
		checkedPolygons = getAdjacentPolygonsToVertex[startingVertex, baseGraphPolygonList],
		checkedVertices = {startingVertex},
		checkedEdges = {},
		selVertices = {},
		selEdges = {},
		selPolygons,
		selPosition,
		targetPolygon,
		checkedCenters,
		uncheckedCenters
	},

	(* Structure is partitioned into 2 structures to ensure all polygons are considered. *)
	checkedCenters = polygonCenterStruct[[#]]& /@ Flatten[Position[baseGraphPolygonList, #]& /@ checkedPolygons];
	uncheckedCenters = polygonCenterStruct[[#]]& /@ Flatten[Position[baseGraphPolygonList, #]& /@ uncheckedPolygons];

	While[Length @ uncheckedCenters != 0,

		(* Get closest polygon using polygon centers. *)
		{selEdges, selVertices} = findShortestPathToVertexSet[Flatten[Take[#, 1]& /@ checkedCenters, 1], Flatten[Take[#, 1]& /@ uncheckedCenters, 1], mstGraphVertexList, mstGraph];

		(* Get the position of the new closest polygon by center. *)
		selPosition = Position[Flatten[Take[#, 1]& /@ uncheckedCenters, 1], selVertices[[1]]][[1]][[1]];

		(* Use the selected position to select the target polygon and find a path to it. *)
		targetPolygon = uncheckedCenters[[selPosition]][[2]];
		{selEdges, selVertices} = findShortestPathToVertexSet[checkedVertices, targetPolygon, baseGraphVertexList, baseGraph];

		(* Add edges and vertices to final structures. *)
		checkedEdges = Append[checkedEdges, selEdges];
		{uncheckedVertices, checkedVertices} = moveElementSet[uncheckedVertices, checkedVertices, selVertices];

		(* Refresh Loop condition by rechecking which polygons are already checked.*)
		selPolygons = Select[uncheckedCenters, Length[checkedVertices \[Intersection] #[[2]]] > 0 &];
		{uncheckedCenters, checkedCenters} = moveElementSet[uncheckedCenters, checkedCenters, selPolygons];
	];

	(* Return edges and vertices. *)
	{Flatten @ checkedEdges, checkedVertices}
];


End[]
EndPackage[]
